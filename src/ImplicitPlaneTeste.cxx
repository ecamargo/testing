/*
 * ImplicitPlaneTeste.cxx
 *
 *  Created on: 05/08/2010
 *      Author: igor
 */

#include "vtkActor.h"
#include "vtkAppendPolyData.h"
#include "vtkClipPolyData.h"
#include "vtkCommand.h"
#include "vtkConeSource.h"
#include "vtkGlyph3D.h"
#include "vtkImplicitPlaneWidget.h"
#include "vtkInteractorEventRecorder.h"
#include "vtkLODActor.h"
#include "vtkPlane.h"
#include "vtkPolyData.h"
#include "vtkPolyDataMapper.h"
#include "vtkProperty.h"
#include "vtkRenderWindow.h"
#include "vtkRenderWindowInteractor.h"
#include "vtkRenderer.h"
#include "vtkSphereSource.h"

#include "vtkDebugLeaks.h"
#include "vtkDICOMImageReader.h"
#include "vtkOutlineFilter.h"
#include "vtkImageData.h"
#include "vtkCutter.h"


// This does the actual work: updates the vtkPlane implicit function.
// This in turn causes the pipeline to update and clip the object.
// Callback for the interaction
class vtkTIPWCallback : public vtkCommand
{
public:
  static vtkTIPWCallback *New()
    { return new vtkTIPWCallback; }
  virtual void Execute(vtkObject *caller, unsigned long, void*)
    {
      vtkImplicitPlaneWidget *planeWidget =
        reinterpret_cast<vtkImplicitPlaneWidget*>(caller);
      planeWidget->UpdatePlacement();
      planeWidget->GetPlane(this->Plane);
//      this->Actor->VisibilityOn();
      this->Cutter->Update();
//      this->Widget->UpdatePlacement();
    }
  vtkTIPWCallback():Plane(0),Cutter(0), Widget(0) {}
  vtkPlane *Plane;
  vtkCutter *Cutter;
  vtkImplicitPlaneWidget *Widget;
};

int main( int argc, char *argv[] )
{
  char fname[] = {"/home/workspace/DICOM_TEST/FUNCAOVE_7/"};

  vtkDICOMImageReader* v16 =  vtkDICOMImageReader::New();

  v16->SetDirectoryName(fname);

  v16->Update();

  vtkOutlineFilter* outline = vtkOutlineFilter::New();
  outline->SetInput(v16->GetOutput());

  vtkPolyDataMapper* outlineMapper = vtkPolyDataMapper::New();
  outlineMapper->SetInput(outline->GetOutput());

  vtkActor* outlineActor =  vtkActor::New();
  outlineActor->SetMapper( outlineMapper);




  // Create a mace out of filters.
  //
//  vtkSphereSource *sphere = vtkSphereSource::New();
//  vtkConeSource *cone = vtkConeSource::New();
//  vtkGlyph3D *glyph = vtkGlyph3D::New();
//  glyph->SetInputConnection(sphere->GetOutputPort());
//  glyph->SetSource(cone->GetOutput());
//  glyph->SetVectorModeToUseNormal();
//  glyph->SetScaleModeToScaleByVector();
//  glyph->SetScaleFactor(0.25);

  // The sphere and spikes are appended into a single polydata.
  // This just makes things simpler to manage.
//  vtkAppendPolyData *apd = vtkAppendPolyData::New();
//  apd->AddInput(glyph->GetOutput());
//  apd->AddInput(sphere->GetOutput());
//
//  vtkPolyDataMapper *maceMapper = vtkPolyDataMapper::New();
//  maceMapper->SetInputConnection(apd->GetOutputPort());
//
//  vtkLODActor *maceActor = vtkLODActor::New();
//  maceActor->SetMapper(maceMapper);
//  maceActor->VisibilityOn();

  // This portion of the code clips the mace with the vtkPlanes
  // implicit function. The clipped region is colored green.
  vtkPlane *plane = vtkPlane::New();
  plane->SetOrigin(v16->GetOutput()->GetCenter());
  plane->SetNormal(0,0,1);

  vtkCutter *clipper = vtkCutter::New();
  clipper->SetInput(v16->GetOutput());
  clipper->SetCutFunction(plane);
//  clipper->InsideOutOn();


  vtkPolyDataMapper *selectMapper = vtkPolyDataMapper::New();
  selectMapper->SetInputConnection(clipper->GetOutputPort());

  vtkLODActor *selectActor = vtkLODActor::New();
  selectActor->SetMapper(selectMapper);
  selectActor->GetProperty()->SetColor(0,1,0);
  selectActor->VisibilityOn();
//  selectActor->SetScale(1.01, 1.01, 1.01);

  // Create the RenderWindow, Renderer and both Actors
  //
  vtkRenderer *ren1 = vtkRenderer::New();
  vtkRenderer* ren2 = vtkRenderer::New();
  vtkRenderWindow *renWin = vtkRenderWindow::New();
  renWin->AddRenderer(ren1);
  renWin->AddRenderer(ren2);

  vtkRenderWindowInteractor *iren = vtkRenderWindowInteractor::New();
  iren->SetRenderWindow(renWin);

  // The SetInteractor method is how 3D widgets are associated with the render
  // window interactor. Internally, SetInteractor sets up a bunch of callbacks
  // using the Command/Observer mechanism (AddObserver()).
  vtkTIPWCallback *myCallback = vtkTIPWCallback::New();
  myCallback->Plane = plane;
  myCallback->Cutter = clipper;

  vtkImplicitPlaneWidget *planeWidget = vtkImplicitPlaneWidget::New();
  planeWidget->SetInteractor(iren);
//  planeWidget->SetPlaceFactor(1.25);
  planeWidget->SetInput(v16->GetOutput());
  planeWidget->PlaceWidget();
  planeWidget->SetOrigin(v16->GetOutput()->GetCenter());
  planeWidget->AddObserver(vtkCommand::InteractionEvent,myCallback);
  planeWidget->DrawPlaneOn();
  planeWidget->On();
  planeWidget->SetNormal(0,0,1);
  myCallback->Widget = planeWidget;

//  ren1->AddActor(maceActor);
  ren1->AddActor(selectActor);

  // Add the actors to the renderer, set the background and size
  //
  ren1->SetBackground(0.1, 0.2, 0.4);
  renWin->SetSize(800, 600);

  ren1->SetViewport(0,0,0.58333,1);
  ren2->SetViewport(0.58333,0,1,1);
  ren1->ResetCamera();
  ren2->ResetCamera();

  // render the image
  //
  iren->Initialize();
  renWin->Render();

  iren->Start();

  myCallback->Delete();
//  sphere->Delete();
//  cone->Delete();
//  glyph->Delete();
//  apd->Delete();
//  maceMapper->Delete();
//  maceActor->Delete();
//  plane->Delete();
//  clipper->Delete();
//  selectMapper->Delete();
//  selectActor->Delete();
  planeWidget->Delete();
  iren->Delete();
  renWin->Delete();
  ren1->Delete();
  ren2->Delete();

  return 0;
}
