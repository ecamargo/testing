/*
 * Delaunay2D.cxx
 *
 *  Created on: 24/10/2013
 *      Author: eduardo
 */




#include <vtkVersion.h>
#include <vtkCellArray.h>
#include <vtkPoints.h>
#include <vtkTriangle.h>
#include <vtkPolyData.h>
#include <vtkPolyDataReader.h>
#include <vtkPolyDataWriter.h>
#include <vtkSurfaceReconstructionFilter.h>
#include <vtkImageDataGeometryFilter.h>
#include <vtkContourFilter.h>
#include <vtkPointData.h>
#include <vtkLine.h>
#include <vtkCellLocator.h>
#include <vtkSmartPointer.h>
#include <vtkDelaunay2D.h>
#include <vtkPolyDataMapper.h>
#include <vtkActor.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkProperty.h>
#include <vtkVertexGlyphFilter.h>


vtkSmartPointer<vtkPolyData> ReadAndProcess(char *fileName)
{
  vtkSmartPointer<vtkPolyDataReader> reader =
    vtkSmartPointer<vtkPolyDataReader>::New();
  reader->SetFileName( fileName );
  reader->Update();


  vtkSmartPointer<vtkPolyData> polydata =
    vtkSmartPointer<vtkPolyData>::New();
  polydata->SetPoints( reader->GetOutput()->GetPoints() );
  polydata->Update();



  vtkSmartPointer<vtkSurfaceReconstructionFilter> sf =
  vtkSmartPointer<vtkSurfaceReconstructionFilter>::New();
  sf->SetInput( polydata );
//  sf->SetNeighborhoodSize(10);
//  sf->SetSampleSpacing(0.2);
  sf->Update();


  vtkSmartPointer<vtkContourFilter> cf =
  vtkSmartPointer<vtkContourFilter>::New();
  cf->SetValue(0,0.0);
  cf->SetInputConnection( sf->GetOutputPort() );
  cf->Update();


  vtkSmartPointer<vtkPolyDataWriter> writer =
  vtkSmartPointer<vtkPolyDataWriter>::New();
  writer->SetFileName("/home/eduardo/workspace/data_HeMoLab/Aneurisk/c0036/parentVessel_WARP_C0036_mesh.vtk");
  writer->SetInput(cf->GetOutput());
  writer->Write();


//  vtkSmartPointer<vtkImageDataGeometryFilter> imgToPoly =
//  vtkSmartPointer<vtkImageDataGeometryFilter>::New();
//  imgToPoly->SetInputConnection( cf->GetOutputPort() );
//  imgToPoly->Update();


//  // Triangulate the grid points
//  vtkSmartPointer<vtkDelaunay2D> delaunay =
//  vtkSmartPointer<vtkDelaunay2D>::New();
//#if VTK_MAJOR_VERSION <= 5
//  delaunay->SetInput(polydata);
//#else
//  delaunay->SetInputData(polydata);
//#endif
//  delaunay->Update();


  return cf->GetOutput();
}





int main(int, char *[])
{
  // Create a set of heighs on a grid.
  // This is often called a "terrain map".
  vtkSmartPointer<vtkPoints> points =
    vtkSmartPointer<vtkPoints>::New();

  double GridSize = 10.0;
  double increment = 1;
  for(double x = 0.0; x < GridSize; x+=increment)
    {
    for(double y = 0.0; y < GridSize; y+=increment)
      {
        points->InsertNextPoint(x, y, (x+y)/(y+1.0));
      }
    }


  char fileName[] = "/home/eduardo/workspace/data_HeMoLab/Aneurisk/c0036/parentVessel_WARP_C0036.vtk";

  // Add the grid points to a polydata object
  vtkSmartPointer<vtkPolyData> polydata;
//  polydata = vtkSmartPointer<vtkPolyData>::New();
//  polydata->SetPoints(points);
  polydata = ReadAndProcess( fileName );
  polydata->Update();





  // Visualize
  vtkSmartPointer<vtkPolyDataMapper> meshMapper =
    vtkSmartPointer<vtkPolyDataMapper>::New();
  meshMapper->SetInputConnection(polydata->GetProducerPort());

  vtkSmartPointer<vtkActor> meshActor =
    vtkSmartPointer<vtkActor>::New();
  meshActor->SetMapper(meshMapper);

  vtkSmartPointer<vtkVertexGlyphFilter> glyphFilter =
    vtkSmartPointer<vtkVertexGlyphFilter>::New();
#if VTK_MAJOR_VERSION <= 5
  glyphFilter->SetInputConnection(polydata->GetProducerPort());
#else
  glyphFilter->SetInputData(polydata);
#endif
  glyphFilter->Update();

  vtkSmartPointer<vtkPolyDataMapper> pointMapper =
    vtkSmartPointer<vtkPolyDataMapper>::New();
  pointMapper->SetInputConnection(glyphFilter->GetOutputPort());

  vtkSmartPointer<vtkActor> pointActor =
    vtkSmartPointer<vtkActor>::New();
  pointActor->GetProperty()->SetColor(1,0,0);
  pointActor->GetProperty()->SetPointSize(3);
  pointActor->SetMapper(pointMapper);

  vtkSmartPointer<vtkRenderer> renderer =
    vtkSmartPointer<vtkRenderer>::New();
  vtkSmartPointer<vtkRenderWindow> renderWindow =
    vtkSmartPointer<vtkRenderWindow>::New();
  renderWindow->AddRenderer(renderer);
  renderWindow->SetSize(1024, 800);
  vtkSmartPointer<vtkRenderWindowInteractor> renderWindowInteractor =
    vtkSmartPointer<vtkRenderWindowInteractor>::New();
  renderWindowInteractor->SetRenderWindow(renderWindow);

  renderer->AddActor(meshActor);
  renderer->AddActor(pointActor);
  renderer->SetBackground(.3, .6, .3); // Background color green

  renderWindow->Render();
  renderWindowInteractor->Start();

  return EXIT_SUCCESS;
}
