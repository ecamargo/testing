
#include <vtkSmartPointer.h>
#include <vtkLine.h>
#include <vtkCellArray.h>
#include <vtkTubeFilter.h>
#include <vtkLineSource.h>
#include <vtkPolyData.h>
#include <vtkPolyDataReader.h>
#include <vtkPolyDataMapper.h>
#include <vtkActor.h>
#include <vtkRenderWindow.h>
#include <vtkRenderer.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkProperty.h>
#include <vtkPointData.h>
//#include "stdio.h"

#include <iostream>
using namespace std;


//-----------------------------------------------------------------------------
int main( int argc, char **argv )
{
  vtkSmartPointer<vtkPolyDataReader> reader = vtkSmartPointer<vtkPolyDataReader>::New();
  reader->SetFileName("");
  reader->Update();

  
  // Create a tube (cylinder) around the line
  vtkSmartPointer<vtkTubeFilter> tubeFilter = vtkSmartPointer<vtkTubeFilter>::New();
  tubeFilter->SetInputConnection( reader->GetOutputPort() );
  tubeFilter->SetRadius( 0.02 );
  tubeFilter->SetNumberOfSides(20);
  tubeFilter->Update();


  vtkSmartPointer<vtkPolyData> tube = vtkSmartPointer<vtkPolyData>::New();
  tube->DeepCopy( tubeFilter->GetOutput() );

  int nV = tube->GetNumberOfPoints();

  // RBG array (could add A channel too I guess...)
  // Varying from blue to red
  vtkSmartPointer<vtkUnsignedCharArray> colors = vtkSmartPointer<vtkUnsignedCharArray>::New();
  colors->SetName("Colors");
  colors->SetNumberOfComponents(3);
  colors->SetNumberOfTuples(nV);
  for(int i=0; i < nV ; i++)
    {
    colors->InsertTuple3(i, int(255*i/(nV-1)) , 0 , int(255*(nV-1-i)/(nV-1)) );
    }
  tube->GetPointData()->SetScalars( colors );
  tube->Update();


  vtkPolyDataMapper *mapperCenterLine = vtkPolyDataMapper::New();
  mapperCenterLine->SetInput( tube );
  mapperCenterLine->ScalarVisibilityOn();
  mapperCenterLine->SetScalarModeToUsePointData();
  mapperCenterLine->SelectColorArray("Colors");


  vtkSmartPointer<vtkActor> tubeActor = vtkSmartPointer<vtkActor>::New();
  tubeActor->GetProperty()->SetOpacity(0.5); //Make the tube have some transparency.
  tubeActor->SetMapper( mapperCenterLine );


  // Create a renderer, render window, and interactor
  vtkSmartPointer<vtkRenderer> renderer = vtkSmartPointer<vtkRenderer>::New();
  vtkSmartPointer<vtkRenderWindow> renderWindow = vtkSmartPointer<vtkRenderWindow>::New();
  renderWindow->AddRenderer(renderer);
  vtkSmartPointer<vtkRenderWindowInteractor> renderWindowInteractor = vtkSmartPointer<vtkRenderWindowInteractor>::New();
  renderWindowInteractor->SetRenderWindow(renderWindow);
 
  // Add the actor to the scene
  renderer->AddActor(tubeActor);
  renderer->SetBackground(0,1,0);

  // Render and interact
  renderWindow->Render();
  renderWindowInteractor->Start();
    
  return 0;
}


