#!/bin/bash

input=/home/eduardo/workspace/data_HeMoLab/modelos/3D/testeBranch/carotida_furada.vtk
centerline=/home/eduardo/workspace/data_HeMoLab/modelos/3D/testeBranch/centerline.vtk
centerline_branch=/home/eduardo/workspace/data_HeMoLab/modelos/3D/testeBranch/centerline_branch.vtk
input_branch=/home/eduardo/workspace/data_HeMoLab/modelos/3D/testeBranch/carotida_furada_branch.vtk
arrayName=MaximumInscribedSphereRadius
GroupIdsArrayName=GroupIds
blankingarrayName=Blanking


#vmtksurfacereader -ifile $fileName --pipe vmtkcenterlines --pipe vmtkbranchextractor --pipe vmtkbranchclipper --pipe vmtksurfaceviewer -array GroupIds


vmtkcenterlines -ifile $input -ofile $centerline -endpoints 1

vmtkbranchextractor -ifile $centerline -ofile $centerline_branch -radiusarray $arrayName

vmtkbranchclipper -ifile $input -centerlinesfile $centerline_branch -groupidsarray $GroupIdsArrayName -radiusarray $arrayName -blankingarray $blankingarrayName -ofile $input_branch


