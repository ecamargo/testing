/*=========================================================================
 Module    : Aneurysm Applications
 File      :
 Copyright : (C)opyright 2013++
 See COPYRIGHT statement in top level directory.
 Authors   : D. Millan
 Modified  :
 Purpose   : This program computes tree bool operations of two PolyDatas (Intsersection, Union and Diference)
 Date      :
 Version   :
 Changes   :
 This software is distributed WITHOUT ANY WARRANTY; without even vtkVoxelContoursToSurfaceFilter
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the above copyright notices for more information.
 =========================================================================*/
// C standard lib
#include<math.h>
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<time.h>
#include<sys/timeb.h>
//C++ STL
#include<iostream>
#include<iterator>
//VTK lib
#include<vtkstd/exception>
#include<vtkMath.h>
#include<vtkCellArray.h>
#include<vtkPolyData.h>
#include<vtkPolyDataReader.h>
#include<vtkPointData.h>
#include<vtkPolyDataWriter.h>
#include<vtkDoubleArray.h>
#include<vtkFloatArray.h>
#include<vtkPolyDataNormals.h>
#include<vtkContourFilter.h>
#include<vtkVoxelContoursToSurfaceFilter.h>
#include<vtkSmartPointer.h>
#include<vtkTriangleFilter.h>
#include<vtkThreshold.h>
#include<vtkDataSetSurfaceFilter.h>
#include<vtkReverseSense.h>
#include<vtkAppendPolyData.h>
#include<vtkGeometryFilter.h>
#include<vtkStructuredGridOutlineFilter.h>
#include<vtkSphereSource.h>
#include<vtkCylinderSource.h>
#include<vtkActor.h>
#include<vtkPolyDataMapper.h>
#include<vtkRenderWindow.h>
#include<vtkRenderWindowInteractor.h>

#if VTK_MINOR_VERSION <= 9
//  #include<vtkHMImplicitPolyDataDistance.h>
  #include<vtkHMIntersectionPolyDataFilter.h>
  #include<vtkHMBooleanOperationPolyDataFilter.h>
  #include<vtkHMDistancePolyDataFilter.h>
#else
//  #include<vtkImplicitPolyDataDistance.h>
  #include<vtkIntersectionPolyDataFilter.h>
  #include<vtkBooleanOperationPolyDataFilter.h>
  #include<vtkDistancePolyDataFilter.h>
#endif


#include<vtkActor.h>
#include<vtkAppendPolyData.h>
#include<vtkDataSetSurfaceFilter.h>
#include<vtkPolyDataMapper.h>
#include<vtkRenderer.h>
#include<vtkRenderWindow.h>
#include<vtkRenderWindowInteractor.h>
#include<vtkReverseSense.h>
#include<vtkSmartPointer.h>
#include<vtkSphereSource.h>
#include<vtkCylinderSource.h>
#include<vtkTriangleFilter.h>
#include<vtkThreshold.h>
#include<vtkCleanPolyData.h>

// ABS: computes the absolute value of a numbervtkGeometryFilter
#define ABS(x) ( ((x) < 0) ? -(x) : (x) )vtkPolyDataMapper

//Functions in cpp files
int Usage ( );
int Usage ( char *str );
int InputReader ( int argc , char *argv[] , bool *help , bool *verbose );
vtkPolyData *getSurfaceByIsoCurve ( vtkPolyData *meshToEvaluate , double contour );
vtkPolyData *getIntersectionOfTwoMesh ( vtkPolyData *firstMesh , vtkPolyData *secondMesh );
void writePolyData ( vtkPolyData *meshToWrite , const char *nameFile );
static vtkActor* GetBooleanOperationActor ( double x , int operation );
static vtkActor* GetBooleanOperationActor ( vtkPolyDataReader *readA , vtkPolyDataReader *readB , int operation );

// main
int main ( int argc , char *argv[] )
    {
        if ( argc < 3 )
            return Usage( );
        const char *input_nameA = NULL;         // Name of the firts PolyData
        const char *input_nameB = NULL;         // Second PolyData
        const char *output_directory = NULL;    // Directory where the tree operations are saved
        bool help = false;
        bool verbose = false;

        // Fill in TIMEBUF with information about the current time.
        double time;
        struct timeb time0 , time1;

        // * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
        //NOTE [0] Parameters setting
        //
        input_nameA = argv[1];
        argc--;
        argv++;

        input_nameB = argv[1];
        argc--;
        argv++;

        output_directory = argv[1];
        argc--;
        argv++;
        if ( strcmp( "-h", output_directory ) == 0 || strcmp( "--help", output_directory ) == 0 )
            return Usage( );
        // Input parameters are readed
        if ( InputReader( argc, argv, &help, &verbose ) )
            return 1;
        if ( help )
            //return Usage( );
            // * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
            //NOTE [1] Initial geometries are loaded
            //
            ftime( &time0 );

        //reference surface A
        vtkPolyDataReader *readerA = vtkPolyDataReader::New( );
        readerA->SetFileName( input_nameA );
        try
            {    //vtkContourFilter
                readerA->Update( );
            }
        catch ( vtkstd::exception& e )
            {
                cout << "ExceptionObject caught !" << endl;
                cout << e.what( ) << endl;
                return -1;
            }
        vtkPolyDataReader *readerB = vtkPolyDataReader::New( );
        readerB->SetFileName( input_nameB );
        try
            {
                readerB->Update( );
            }
        catch ( vtkstd::exception& e )
            {
                cout << "ExceptionObject caught !" << endl;
                cout << e.what( ) << endl;
                return -1;
            }
        //vtkPolyData *surfB = readerB->GetOutput( );
        ftime( &time1 );
        time = difftime( time1.time, time0.time ) + ( time1.millitm - time0.millitm ) * 0.001;
        if ( verbose )
            {
                printf( "Input data surfaces readed in ............................. %6.3f\n", time );
                //printf( "\tInput surface A have: nPts=%4d  nElem=%4d\n", (int) surfA->GetNumberOfPoints( ), (int) surfA->GetNumberOfPolys( ) );
                //printf( "\tInput surface B have: nPts=%4d  nElem=%4d\n", (int) surfB->GetNumberOfPoints( ), (int) surfB->GetNumberOfPolys( ) );
            }

        // * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
        //NOTE [2] The distance between each point of the surface A and the closest point on
        // the surface B is computed here
        //vtkSmartPointer
        ftime( &time0 );

        ftime( &time1 );
        time = difftime( time1.time, time0.time ) + ( time1.millitm - time0.millitm ) * 0.001;
        if ( verbose )
            {
                printf( "Distances between the points of surfA and the surfB in .... %6.3f\n", time );
            }

        char output_nameU[120];
        strcpy( output_nameU, output_directory );
        strcat( output_nameU, "U_PolyData.vtk" );

        char output_nameI[120];
        strcpy( output_nameI, output_directory );
        strcat( output_nameI, "I_PolyData.vtk" );

        char output_nameD[120];
        strcpy( output_nameD, output_directory );
        strcat( output_nameD, "D_PolyData.vtk" );



        #if VTK_MINOR_VERSION <= 9
          vtkActor *unionActor = GetBooleanOperationActor( readerA, readerB, vtkHMBooleanOperationPolyDataFilter::VTK_UNION );
        #else
          vtkActor *unionActor = GetBooleanOperationActor( readerA, readerB, vtkBooleanOperationPolyDataFilter::VTK_UNION );
        #endif
        vtkPolyData *unionPolyData = ( vtkPolyData* ) unionActor->GetMapper( )->GetInput( );
        //vtkCleanPolyData *clean = vtkCleanPolyData::New( );
        //clean->SetInput( unionPolyData );
        //clean->SetTolerance( 0.1 );
        //clean->PointMergingOn( );
        //clean->Update( );
        //clean->GetOutput( )->BuildLinks( ); //in order to can sort
        //clean->GetOutput( )->BuildCells( );
        //clean->GetOutput( )->Update( );
        //writePolyData( clean->GetOutput( ), output_nameU );
        writePolyData( unionPolyData, output_nameU );
        unionActor->Delete( );



        #if VTK_MINOR_VERSION <= 9
          vtkActor *intersectionActor = GetBooleanOperationActor( readerA, readerB, vtkHMBooleanOperationPolyDataFilter::VTK_INTERSECTION );
        #else
          vtkActor *intersectionActor = GetBooleanOperationActor( readerA, readerB, vtkBooleanOperationPolyDataFilter::VTK_INTERSECTION );
        #endif
        //vtkActor *intersectionActor = GetBooleanOperationActor( 0.0, vtkBooleanOperationPolyDataFilter::VTK_INTERSECTION );
        vtkPolyData *intersectionPolyData = ( vtkPolyData* ) intersectionActor->GetMapper( )->GetInput( );
        writePolyData( intersectionPolyData, output_nameI );
        intersectionActor->Delete( );




        #if VTK_MINOR_VERSION <= 9
          vtkActor *differenceActor = GetBooleanOperationActor( readerA, readerB, vtkHMBooleanOperationPolyDataFilter::VTK_DIFFERENCE );
        #else
          vtkActor *differenceActor = GetBooleanOperationActor( readerA, readerB, vtkBooleanOperationPolyDataFilter::VTK_DIFFERENCE );
        #endif
        //vtkActor *differenceActor = GetBooleanOperationActor( 0, vtkBooleanOperationPolyDataFilter::VTK_DIFFERENCE );
        vtkPolyData *differencePolyData = ( vtkPolyData* ) differenceActor->GetMapper( )->GetInput( );
        //vtkCleanPolyData *clean = vtkCleanPolyData::New( );
        //clean->SetInput( differencePolyData );
        //clean->SetTolerance( 0.1 );
        //clean->PointMergingOn( );
        //clean->ConvertLinesToPointsOn();
        //clean->ConvertPolysToLinesOn();
        //clean->ConvertStripsToPolysOn();
        //clean->
        //clean->Update( );
        //clean->GetOutput( )->BuildLinks( ); //in order to can sort
        //clean->GetOutput( )->BuildCells( );
        //clean->GetOutput( )->Update( );

        //writePolyData( clean->GetOutput(), output_nameD );
        writePolyData( differencePolyData, output_nameD );
        differenceActor->Delete( );
        //intersectionPolyData->Delete();
        //unionPolyData->Delete();
        //intersectionActor->Delete();
        //unionActor->Delete();



        readerA->Delete( );
        readerB->Delete( );
        return 0;
    }

static vtkActor* GetBooleanOperationActor ( vtkPolyDataReader *readA , vtkPolyDataReader *readB , int operation )
    {

        vtkSmartPointer<vtkTriangleFilter> triangleFilterA = vtkSmartPointer<vtkTriangleFilter>::New( );
        triangleFilterA->SetInputConnection( readA->GetOutputPort( ) );
        triangleFilterA->Update( );

        vtkSmartPointer<vtkTriangleFilter> triangleFilterB = vtkSmartPointer<vtkTriangleFilter>::New( );
        triangleFilterB->SetInputConnection( readB->GetOutputPort( ) );
        triangleFilterB->Update( );

        #if VTK_MINOR_VERSION <= 9
          vtkSmartPointer<vtkHMIntersectionPolyDataFilter> intersection = vtkSmartPointer<vtkHMIntersectionPolyDataFilter>::New( );
        #else
          vtkSmartPointer<vtkIntersectionPolyDataFilter> intersection = vtkSmartPointer<vtkIntersectionPolyDataFilter>::New( );
        #endif
        intersection->SetInputConnection( 0, triangleFilterA->GetOutputPort( ) );

        //intersection->SetInputConnection(1, cyli->GetOutputPort());
        intersection->SetInputConnection( 1, triangleFilterB->GetOutputPort( ) );

        #if VTK_MINOR_VERSION <= 9
          vtkSmartPointer<vtkHMDistancePolyDataFilter> distance = vtkSmartPointer<vtkHMDistancePolyDataFilter>::New( );
        #else
          vtkSmartPointer<vtkDistancePolyDataFilter> distance = vtkSmartPointer<vtkDistancePolyDataFilter>::New( );
        #endif
        distance->SetInputConnection( 0, intersection->GetOutputPort( 1 ) );
        distance->SetInputConnection( 1, intersection->GetOutputPort( 2 ) );

        vtkSmartPointer<vtkThreshold> thresh1 = vtkSmartPointer<vtkThreshold>::New( );
        thresh1->AllScalarsOn( );
        thresh1->SetInputArrayToProcess( 0, 0, 0, vtkDataObject::FIELD_ASSOCIATION_CELLS, "Distance" );
        //thresh1->SetInputArrayToProcess( 0, 0, 0, vtkDataObject::FIELD_ASSOCIATION_POINTS_THEN_CELLS, "Distance" );
        //thresh1->SetInputArrayToProcess( 0, 0, 0, vtkDataObject::FIELD_ASSOCIATION_POINTS, "Distance" );
        thresh1->SetInputConnection( distance->GetOutputPort( 0 ) );

        vtkSmartPointer<vtkThreshold> thresh2 = vtkSmartPointer<vtkThreshold>::New( );
        thresh2->AllScalarsOn( );
        thresh2->SetInputArrayToProcess( 0, 0, 0, vtkDataObject::FIELD_ASSOCIATION_CELLS, "Distance" );
        //thresh2->SetInputArrayToProcess( 0, 0, 0, vtkDataObject::FIELD_ASSOCIATION_POINTS_THEN_CELLS, "Distance" );
        //thresh2->SetInputArrayToProcess( 0, 0, 0, vtkDataObject::FIELD_ASSOCIATION_POINTS, "Distance" );
        thresh2->SetInputConnection( distance->GetOutputPort( 1 ) );


        #if VTK_MINOR_VERSION <= 9
          if ( operation == vtkHMBooleanOperationPolyDataFilter::VTK_UNION )
        #else
          if ( operation == vtkBooleanOperationPolyDataFilter::VTK_UNION )
        #endif
            {
                thresh1->ThresholdByUpper( 0.0 );
                thresh2->ThresholdByUpper( 0.0 );

                //Aneurism Sac Phantom
                //thresh1->ThresholdByUpper( 0.0 );
                //thresh2->ThresholdByUpper( 2.5 );

                //Aneurism Sac Original
                //thresh1->ThresholdByUpper( 0.0 );
                //thresh2->ThresholdByUpper( 2.5 );
            }
        #if VTK_MINOR_VERSION <= 9
          else if ( operation == vtkHMBooleanOperationPolyDataFilter::VTK_INTERSECTION )
        #else
          else if ( operation == vtkBooleanOperationPolyDataFilter::VTK_INTERSECTION )
        #endif
              {
              thresh1->ThresholdByLower( 0.0 );
              thresh2->ThresholdByLower( 0.0 );

              //Capa Aneurisma Phantom
              //thresh1->ThresholdByLower( -1 );
              //thresh2->ThresholdByLower( 0.0 );
              //Parent Vessel Original
              //thresh1->ThresholdByLower( 0.25 );
              ////thresh2->ThresholdByLower( 0.0 );

              //Capa Aneurisma Original
              //thresh1->ThresholdByLower( -0.18 );
              //thresh2->ThresholdByLower( 0.0 );

              //Capa Aneurisma Original
              //thresh1->ThresholdByLower( -0.0 );
              //thresh2->ThresholdByLower( 0.0 );
              }
        else // Difference
            {
                thresh1->ThresholdByUpper( 0 ); //>0
                thresh2->ThresholdByLower( 0 ); //<0
            }

        vtkSmartPointer<vtkDataSetSurfaceFilter> surface1 = vtkSmartPointer<vtkDataSetSurfaceFilter>::New( );
        surface1->SetInputConnection( thresh1->GetOutputPort( ) );
        vtkCleanPolyData *clean = vtkCleanPolyData::New( );
        clean->SetInput( surface1->GetOutput( ) );
        //clean->SetTolerance( 0.1 );
        clean->PointMergingOn( );
        clean->ConvertLinesToPointsOn( );
        clean->ConvertPolysToLinesOn( );
        clean->ConvertStripsToPolysOn( );
        //clean->
        clean->Update( );
        clean->GetOutput( )->BuildLinks( ); //in order to can sort
        clean->GetOutput( )->BuildCells( );
        clean->GetOutput( )->Update( );
        surface1->GetOutput( )->DeepCopy( clean->GetOutput( ) );

        //writePolyData( surface1->GetOutput(), "surfaceD1.vtk" );
        //getchar();

        vtkSmartPointer<vtkDataSetSurfaceFilter> surface2 = vtkSmartPointer<vtkDataSetSurfaceFilter>::New( );
        surface2->SetInputConnection( thresh2->GetOutputPort( ) );
        vtkCleanPolyData *clean2 = vtkCleanPolyData::New( );
        clean2->SetInput( surface2->GetOutput( ) );
        //clean->SetTolerance( 0.1 );
        clean2->PointMergingOn( );
        clean2->ConvertLinesToPointsOn( );
        clean2->ConvertPolysToLinesOn( );
        clean2->ConvertStripsToPolysOn( );
        //clean->
        //printf("OK");
        clean2->Update( );
        clean2->GetOutput( )->BuildLinks( ); //in order to can sort
        clean2->GetOutput( )->BuildCells( );
        clean2->GetOutput( )->Update( );
        surface2->GetOutput( )->DeepCopy( clean2->GetOutput( ) );

        //writePolyData( surface2->GetOutput(), "surfaceD2.vtk" );

        vtkSmartPointer<vtkReverseSense> reverseSense = vtkSmartPointer<vtkReverseSense>::New( );
        reverseSense->SetInputConnection( surface2->GetOutputPort( ) );

        if ( operation == 2 ) // difference printf("OK");
            {
                reverseSense->ReverseCellsOn( );
                reverseSense->ReverseNormalsOn( );
            }

        vtkSmartPointer<vtkAppendPolyData> appender = vtkSmartPointer<vtkAppendPolyData>::New( );
        appender->SetInputConnection( surface1->GetOutputPort( ) );

        if ( operation == 2 )
            {
                appender->AddInputConnection( reverseSense->GetOutputPort( ) );
            }
        else
            {
                appender->AddInputConnection( surface2->GetOutputPort( ) );
            }

        vtkSmartPointer<vtkPolyDataMapper> mapper = vtkSmartPointer<vtkPolyDataMapper>::New( );
        mapper->SetInputConnection( appender->GetOutputPort( ) );
        mapper->ScalarVisibilityOff( );

        vtkActor *actor = vtkActor::New( );
        actor->SetMapper( mapper );
        clean2->Delete();
        clean->Delete();
        return actor;
    }

static vtkActor* GetBooleanOperationActor ( double x , int operation )
    {
        double centerSeparation = 1.0;
        vtkSmartPointer<vtkSphereSource> sphere1 = vtkSmartPointer<vtkSphereSource>::New( );
        sphere1->SetCenter( -centerSeparation + x, 0.0, 0.0 );
        sphere1->SetThetaResolution( 90 );
        sphere1->SetPhiResolution( 90 );
        sphere1->SetRadius( 2.0 );

        vtkSmartPointer<vtkCylinderSource> cyli = vtkSmartPointer<vtkCylinderSource>::New( );
        cyli->SetHeight( 6.0 );
        cyli->SetRadius( 1.0 );
        cyli->SetResolution( 90 );
        cyli->SetCenter( centerSeparation + x, 0.0, 0.0 );

        vtkSmartPointer<vtkTriangleFilter> triangleFilterA = vtkSmartPointer<vtkTriangleFilter>::New( );
        triangleFilterA->SetInputConnection( sphere1->GetOutputPort( ) );
        triangleFilterA->Update( );

        writePolyData( triangleFilterA->GetOutput( ), "esphere.vtk" );

        vtkSmartPointer<vtkTriangleFilter> triangleFilterB = vtkSmartPointer<vtkTriangleFilter>::New( );
        triangleFilterB->SetInputConnection( cyli->GetOutputPort( ) );
        triangleFilterB->Update( );

        writePolyData( triangleFilterB->GetOutput( ), "cilinder.vtk" );

        #if VTK_MINOR_VERSION <= 9
          vtkSmartPointer<vtkHMIntersectionPolyDataFilter> intersection = vtkSmartPointer<vtkHMIntersectionPolyDataFilter>::New( );
        #else
          vtkSmartPointer<vtkIntersectionPolyDataFilter> intersection = vtkSmartPointer<vtkIntersectionPolyDataFilter>::New( );
        #endif
        intersection->SetInputConnection( 0, triangleFilterA->GetOutputPort( ) );
        //intersection->SetInputConnection(1, cyli->GetOutputPort());
        intersection->SetInputConnection( 1, triangleFilterB->GetOutputPort( ) );

        #if VTK_MINOR_VERSION <= 9
          vtkSmartPointer<vtkHMDistancePolyDataFilter> distance = vtkSmartPointer<vtkHMDistancePolyDataFilter>::New( );
        #else
          vtkSmartPointer<vtkDistancePolyDataFilter> distance = vtkSmartPointer<vtkDistancePolyDataFilter>::New( );
        #endif
        distance->SetInputConnection( 0, intersection->GetOutputPort( 1 ) );
        distance->SetInputConnection( 1, intersection->GetOutputPort( 2 ) );

        vtkSmartPointer<vtkThreshold> thresh1 = vtkSmartPointer<vtkThreshold>::New( );
        thresh1->AllScalarsOn( );
        thresh1->SetInputArrayToProcess( 0, 0, 0, vtkDataObject::FIELD_ASSOCIATION_CELLS, "Distance" );
        thresh1->SetInputConnection( distance->GetOutputPort( 0 ) );

        vtkSmartPointer<vtkThreshold> thresh2 = vtkSmartPointer<vtkThreshold>::New( );
        thresh2->AllScalarsOn( );
        thresh2->SetInputArrayToProcess( 0, 0, 0, vtkDataObject::FIELD_ASSOCIATION_CELLS, "Distance" );
        thresh2->SetInputConnection( distance->GetOutputPort( 1 ) );


        #if VTK_MINOR_VERSION <= 9
          if ( operation == vtkHMBooleanOperationPolyDataFilter::VTK_UNION )
        #else
          if ( operation == vtkBooleanOperationPolyDataFilter::VTK_UNION )
        #endif
            {
                thresh1->ThresholdByUpper( 0.0 );
                thresh2->ThresholdByUpper( 0.0 );
            }
        #if VTK_MINOR_VERSION <= 9
          else if ( operation == vtkHMBooleanOperationPolyDataFilter::VTK_INTERSECTION )
        #else
            else if ( operation == vtkBooleanOperationPolyDataFilter::VTK_INTERSECTION )
        #endif
            {
                thresh1->ThresholdByLower( 0.0 );
                thresh2->ThresholdByLower( 0.0 );
            }
        else // Difference
            {
                thresh1->ThresholdByUpper( 0.0 );
                thresh2->ThresholdByLower( 0.0 );
            }

        vtkSmartPointer<vtkDataSetSurfaceFilter> surface1 = vtkSmartPointer<vtkDataSetSurfaceFilter>::New( );
        surface1->SetInputConnection( thresh1->GetOutputPort( ) );

        //surface1->GetOutput();

        vtkSmartPointer<vtkDataSetSurfaceFilter> surface2 = vtkSmartPointer<vtkDataSetSurfaceFilter>::New( );
        surface2->SetInputConnection( thresh2->GetOutputPort( ) );

        vtkSmartPointer<vtkReverseSense> reverseSense = vtkSmartPointer<vtkReverseSense>::New( );
        reverseSense->SetInputConnection( surface2->GetOutputPort( ) );
        if ( operation == 2 ) // difference
            {
                reverseSense->ReverseCellsOn( );
                reverseSense->ReverseNormalsOn( );
            }

        vtkSmartPointer<vtkAppendPolyData> appender = vtkSmartPointer<vtkAppendPolyData>::New( );
        appender->SetInputConnection( surface1->GetOutputPort( ) );
        if ( operation == 2 )
            {
                appender->AddInputConnection( reverseSense->GetOutputPort( ) );
            }
        else
            {
                appender->AddInputConnection( surface2->GetOutputPort( ) );
            }

        vtkSmartPointer<vtkPolyDataMapper> mapper = vtkSmartPointer<vtkPolyDataMapper>::New( );
        mapper->SetInputConnection( appender->GetOutputPort( ) );
        mapper->ScalarVisibilityOff( );

        vtkActor *actor = vtkActor::New( );
        actor->SetMapper( mapper );

        return actor;
    }

void writePolyData ( vtkPolyData *meshToWrite , const char *nameFile )
    {
        vtkPolyDataWriter *writer = vtkPolyDataWriter::New( );
        writer->SetInput( meshToWrite );
        writer->SetFileName( nameFile );
        writer->Write( );
//        writer->SetFileTypeToBinary( );
        writer->Update( );
        writer->Delete( );
    }
vtkPolyData *getIntersectionOfTwoMesh ( vtkPolyData *firstMesh , vtkPolyData *secondMesh )
    {
        vtkPolyData *returnMesh = vtkPolyData::New( );
        //vtkIntersectionPolyDataFilter *intesectionOfPolyData = vtkIntersectionPolyDataFilter::New();
        #if VTK_MINOR_VERSION <= 9
          vtkSmartPointer<vtkHMIntersectionPolyDataFilter> intesectionOfPolyData = vtkSmartPointer<vtkHMIntersectionPolyDataFilter>::New( );
        #else
          vtkSmartPointer<vtkIntersectionPolyDataFilter> intesectionOfPolyData = vtkSmartPointer<vtkIntersectionPolyDataFilter>::New( );
        #endif
        //intesectionOfPolyData->SetInputConnection( 0, firstMesh  );
        //intesectionOfPolyData->SetInputConnection( 1, secondMesh );
        intesectionOfPolyData->SetInput( 0, firstMesh );
        intesectionOfPolyData->SetInput( 1, secondMesh );
        intesectionOfPolyData->Update( );
        return returnMesh;

    }
vtkPolyData *getSurfaceByIsoCurve ( vtkPolyData *meshToEvaluate , double contour )
    {
        vtkPolyData *meshReturn = vtkPolyData::New( );
        vtkContourFilter *marching = vtkContourFilter::New( );
        marching->SetInput( meshToEvaluate );
//marching->SetValue( 0, 0.5 );
        marching->SetValue( 0, 0.1 );
        marching->Update( );
//marching->SetValue(0, -3);
//marching->SetValue(1,2.1);
//marching->SetValue(0, -3);
//vtkSmartPointer<vtkVoxelContoursToSurfaceFilter> contoursToSurface = vtkSmartPointer<vtkVoxelContoursToSurfaceFilter>::New();
//contoursToSurface->SetInput( meshToEvaluate );
//contoursToSurface->SetSpacing();
//contoursToSurface->Update();
//contoursToSurface->SetInput()
//meshReturn->DeepCopy( marching->GetOutput( ) );
//marching->GetOutput()->DeepCopy(meshReturn);
//meshReturn->DeepCopy( contoursToSurface->GetOutput( ) );

        meshReturn->Update( );
        return marching->GetOutput( );
    }

// Usage example
int Usage ( )
    {
        printf( "Usage: ImplicitSurfaceDistance [infileA] [infileB] [outfile]  <parameters> \n" );
        printf( "This program computes the distance between each point of the surface A and the \n" );
        printf( "closest point on the surface B (not necessary a vertex)\n" );
        printf( "The input surfaces are in vtkPolyData structure (triangles)                \n" );
        printf( "Parameters:         	                                         			\n" );
        printf( "\t-h ; --help      Display this\n" );
        printf( "\t-v ; --verbose   Display information explaining what is being done		\n" );
        return 1;
    }
int Usage ( char *str )
    {
        printf( "\nERROR:: '%s' input parameter is NULL\n", str );
        printf( "A correct expression must be like: -%s val\n\n", str );
        return Usage( );
    }
// this functions performs the command line argument reading
int InputReader ( int argc , char *argv[] , bool *help , bool *verbose )
    {
        bool ok;
// The command line arguments are readed
        while ( argc > 1 )
            {
                ok = false;
                // Options:
                if ( ( ok == false ) && ( ( strcmp( argv[1], "-h" ) == 0 ) || ( strcmp( argv[1], "--help" ) == 0 ) ) )
                    {
                        argc--;
                        argv++;
                        ( *help ) = true;
                        ok = true;
                    }
                if ( ( ok == false ) && ( ( strcmp( argv[1], "-v" ) == 0 ) || ( strcmp( argv[1], "--verbose" ) == 0 ) ) )
                    {
                        argc--;
                        argv++;
                        ( *verbose ) = true;
                        ok = true;
                    }
                if ( ok == false )
                    {
                        printf( "\nERROR:: Can not parse argument %s\n\n", argv[1] );
                        Usage( );
                    }
            }

        return 0;
    }
