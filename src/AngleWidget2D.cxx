/*=========================================================================

  Program:   Visualization Toolkit
  Module:    TestAngleWidget2D.cxx

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
//
// This example tests the vtkAngleWidget.

#include "vtkSmartPointer.h"
#include "vtkAngleWidget.h"
#include "vtkAngleRepresentation2D.h"
#include "vtkSphereSource.h"
#include "vtkPolyDataMapper.h"
#include "vtkActor.h"
#include "vtkRenderer.h"
#include "vtkRenderWindow.h"
#include "vtkRenderWindowInteractor.h"
#include "vtkCommand.h"
#include "vtkCoordinate.h"
#include "vtkMath.h"
#include "vtkHandleWidget.h"
#include "vtkPointHandleRepresentation2D.h"
#include "vtkAxisActor2D.h"
#include "vtkLeaderActor2D.h"
#include "vtkProperty2D.h"
#include "vtkTesting.h"
#include "vtkTextProperty.h"


// This callback is responsible for setting the angle label.
class vtkAngleCallback : public vtkCommand
{
public:
  static vtkAngleCallback *New() 
  { return new vtkAngleCallback; }
  virtual void Execute(vtkObject*, unsigned long eid, void*)
  {
    if ( eid == vtkCommand::PlacePointEvent )
      {
      std::cout << "point placed\n";
      }
    else if ( eid == vtkCommand::InteractionEvent )
      {
      double point1[3], center[3], point2[3];
      this->Rep->GetPoint1WorldPosition(point1);
      this->Rep->GetCenterWorldPosition(center);
      this->Rep->GetPoint2WorldPosition(point2);
      std::cout << "Angle between " << "("
           << point1[0] << ","
           << point1[1] << ","
           << point1[2] << "), ("
           << center[0] << ","
           << center[1] << ","
           << center[2] << ") and ("
           << point2[0] << ","
           << point2[1] << ","
           << point2[2] << ") is "
           << this->Rep->GetAngle() << std::endl;
      }
  }
  vtkAngleRepresentation2D *Rep;
  vtkAngleCallback():Rep(0) {}
};


// The actual test function
int main( int argc, char *argv[] )
{
  // Create the RenderWindow, Renderer and both Actors
  //
  vtkSmartPointer< vtkRenderer > ren1 = vtkSmartPointer< vtkRenderer >::New();
  vtkSmartPointer< vtkRenderWindow > renWin = vtkSmartPointer< vtkRenderWindow >::New();
  renWin->AddRenderer(ren1);

  vtkSmartPointer< vtkRenderWindowInteractor > iren = vtkSmartPointer< vtkRenderWindowInteractor >::New();
  iren->SetRenderWindow(renWin);

  // Create a test pipeline
  //
  vtkSmartPointer< vtkSphereSource > ss = vtkSmartPointer< vtkSphereSource >::New();
  ss->SetRadius(10);
  vtkSmartPointer< vtkPolyDataMapper > mapper = vtkSmartPointer< vtkPolyDataMapper >::New();
  mapper->SetInput(ss->GetOutput());
  vtkSmartPointer< vtkActor > actor = vtkSmartPointer< vtkActor >::New();
  actor->SetMapper(mapper);



  vtkSmartPointer< vtkAngleRepresentation2D > rep = vtkSmartPointer< vtkAngleRepresentation2D >::New();
  rep->GetArc()->GetProperty()->SetColor(1,0,0);
  rep->GetArc()->GetProperty()->SetLineWidth(5);
  rep->GetRay1()->GetProperty()->SetColor(0,1,0);
  rep->GetRay2()->GetProperty()->SetColor(0,0,1);
  rep->GetRay1()->GetProperty()->SetLineWidth(5);
  rep->GetRay2()->GetProperty()->SetLineWidth(5);
  rep->GetArc()->GetLabelTextProperty()->SetColor(1,1,0);

  vtkSmartPointer< vtkAngleWidget > widget = vtkSmartPointer< vtkAngleWidget >::New();
  widget->SetInteractor( iren );
  widget->SetRepresentation( rep );
  widget->CreateDefaultRepresentation();


//  vtkSmartPointer< vtkAngleCallback > mcbk = vtkSmartPointer< vtkAngleCallback >::New();
//  mcbk->Rep = rep;
//  widget->AddObserver(vtkCommand::PlacePointEvent,mcbk);
//  widget->AddObserver(vtkCommand::InteractionEvent,mcbk);


  // Add the actors to the renderer, set the background and size
  ren1->AddActor(actor);
  ren1->SetBackground(0.1, 0.2, 0.4);
  renWin->SetSize(800, 600);

  // render the image
  iren->Initialize();
  renWin->Render();
  widget->On();
  widget->ProcessEventsOff();



  double p1[3], p2[3], c[3];

//  p1[0] = -4.68816;   p1[1] = 6.87057;    p1[2] = 20.5581;
//  c[0] = -4.12234;    c[1] = -6.54725;    c[2] = 20.5581;
//  p2[0] = 9.13382;    p2[1] =  1.37411;   p2[2] =  20.5581;


  c[0] = 38.1973;
  c[1] = 111.402;
  c[2] = 62.3598;

  p1[0] = 37.4224;
  p1[2] = 112.382;
  p1[1] = 62.2928;

  p2[0] = 36.7793;
  p2[1] = 207.951;
  p2[2] = 93.15;


  rep->GetPoint1Representation()->SetWorldPosition(p1);
  rep->GetRay1()->GetPosition2Coordinate()->SetValue(p1);
  rep->Ray1VisibilityOn();

  rep->GetCenterRepresentation()->SetWorldPosition(c);
  rep->GetRay1()->GetPositionCoordinate()->SetValue(c);
  rep->GetRay2()->GetPositionCoordinate()->SetValue(c);

  rep->GetPoint2Representation()->SetWorldPosition(p2);
  rep->GetRay2()->GetPosition2Coordinate()->SetValue(p2);
  rep->Ray2VisibilityOn();


  renWin->Render();  
  iren->Start();
}

