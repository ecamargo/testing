/*
 * CutMaterialTeste.cxx
 *
 *  Created on: 05/08/2010
 *      Author: igor
 */

#include "vtkDataSetReader.h"
#include "vtkDICOMImageReader.h"

#include <string.h>

#include "vtkRenderer.h"
#include "vtkRenderWindow.h"
#include "vtkRenderWindowInteractor.h"
#include "vtkPolyDataMapper.h"
#include "vtkActor.h"
#include "vtkCamera.h"
#include "vtkLookupTable.h"
#include "vtkProperty.h"
#include "vtkImageData.h"
#include "vtkImageMapToColors.h"
#include "vtkImageGaussianSource.h"
#include "vtkDoubleArray.h"
#include "vtkCutMaterial.h"
#include "vtkImageEllipsoidSource.h"
#include "vtkImageGaussianSource.h"
#include "vtkPointData.h"
#include "vtkCellData.h"

//-----------------------------------------------------------------------------
int main( int argc, char **argv )
{
  char fname[] = {"/home/workspace/DICOM_TEST/FUNCAOVE_7/"};

  vtkDICOMImageReader* v16 =  vtkDICOMImageReader::New();

  v16->SetDirectoryName(fname);
  cout << "Leitura iniciada" << endl;
  v16->Update();

  cout << "Leitura realizada" << endl;
//  # Lets create a data set.
//  vtkImageData *data = vtkImageData::New();
//    data->SetExtent(0, 31, 0, 31, 0, 31);
//    data->SetScalarTypeToFloat();

  int ext[6];

  v16->GetOutput()->GetExtent(ext);

//  # First the data array:
  vtkImageGaussianSource *gauss = vtkImageGaussianSource::New();
    gauss->SetWholeExtent(ext[0], ext[1], ext[2], ext[3], ext[4], ext[5]);
    gauss->SetCenter(v16->GetOutput()->GetCenter());
    gauss->SetMaximum(1.0);
    gauss->SetStandardDeviation(10.0);
    gauss->Update();

  vtkDoubleArray *a =  vtkDoubleArray::New();
  a->DeepCopy(gauss->GetOutput()->GetPointData()->GetScalars());
  a->SetName("Gauss");
//  data->GetCellData()->SetScalars(a);
  v16->GetOutput()->GetCellData()->SetScalars(a);
  gauss->Delete();


//  # Now the material array:
  vtkImageEllipsoidSource *ellipse = vtkImageEllipsoidSource::New();
//    ellipse->SetWholeExtent(0, 30, 0, 30, 0, 30);
    ellipse->SetWholeExtent(ext[0], ext[1], ext[2], ext[3], ext[4], ext[5]);
//    ellipse->SetCenter(11, 12, 13);
    ellipse->SetCenter(v16->GetOutput()->GetCenter());
    ellipse->SetRadius(150, 150, 1);
    ellipse->SetInValue(1);
    ellipse->SetOutValue(0);
    ellipse->SetOutputScalarTypeToInt();
    ellipse->Update();

  vtkDoubleArray *m = vtkDoubleArray::New();
  m->DeepCopy(ellipse->GetOutput()->GetPointData()->GetScalars());
  m->SetName("Material");
//  data->GetCellData()->AddArray(m);
  v16->GetOutput()->GetCellData()->AddArray(m);

  ellipse->Delete();

  vtkCutMaterial *cut = vtkCutMaterial::New();
    cut->SetInput(v16->GetOutput());
    cut->SetMaterialArrayName("Material");
    cut->SetMaterial(1);
    cut->SetArrayName("Gauss");
    cut->SetUpVector(1, 0, 0);
    cout << "Cut iniciando" << endl;
    cut->Update();
    cout << "Cut realizada" << endl;

  vtkPolyDataMapper *mapper2 = vtkPolyDataMapper::New();
  mapper2->SetInputConnection(cut->GetOutputPort());
  mapper2->SetScalarRange(0, 1);
//  mapper2 SetScalarModeToUseCellFieldData
//  mapper2 SetColorModeToMapScalars
//  mapper2 ColorByArrayComponent "vtkGhostLevels" 0

  vtkActor *actor2 = vtkActor::New();
  actor2->SetMapper(mapper2);
  actor2->SetPosition(1.5, 0, 0);

  vtkRenderer *ren = vtkRenderer::New();
  ren->AddActor(actor2);

  vtkRenderWindow *renWin = vtkRenderWindow::New();
  renWin->AddRenderer(ren);

//  vtkPoints *p = vtkPoints::New();
//  p->InsertNextPoint(cut->GetCenterPoint());
  double *p = cut->GetCenterPoint();
//  vtkDoubleArray *n = vtkDoubleArray::New();
  double *n = cut->GetNormal();
//  n->InsertNextTuplecut GetNormal]
  vtkCamera *cam = ren->GetActiveCamera();
  cam->SetFocalPoint(p);
  cam->SetViewUp(cut->GetUpVector());
  cam->SetPosition(n[0] + p[0], n[1] + p[1], n[2] + p[2]);
  ren->ResetCamera();




  vtkRenderWindowInteractor *iren = vtkRenderWindowInteractor::New();
  iren->SetRenderWindow(renWin);
  iren->Initialize();
//  iren->AddObserver UserEvent {wm deiconify .vtkInteract}
  iren->Start();

  v16->Delete();

  return 0;
}
