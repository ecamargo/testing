/*
 * OrthoPlanesTeste.cxx
 *
 *  Created on: 05/08/2010
 *      Author: igor
 */

#include "vtkDataSetReader.h"
#include "vtkDICOMImageReader.h"

#include <string.h>

#include "vtkActor.h"
#include "vtkCamera.h"
#include "vtkCellPicker.h"
#include "vtkCommand.h"
#include "vtkImageActor.h"
#include "vtkImageMapToColors.h"
#include "vtkImageOrthoPlanes.h"
#include "vtkImagePlaneWidget.h"
#include "vtkImageReader.h"
#include "vtkInteractorEventRecorder.h"
#include "vtkLookupTable.h"
#include "vtkOutlineFilter.h"
#include "vtkPolyDataMapper.h"
#include "vtkProperty.h"
#include "vtkRenderWindow.h"
#include "vtkRenderWindowInteractor.h"
#include "vtkRenderer.h"
#include "vtkVolume16Reader.h"
#include "vtkImageData.h"
#include "vtkMatrix4x4.h"
#include "vtkImageReslice.h"
#include "vtkMath.h"

//----------------------------------------------------------------------------
class vtkOrthoPlanesCallback : public vtkCommand
{
public:
  static vtkOrthoPlanesCallback *New()
  { return new vtkOrthoPlanesCallback; }

  void Execute( vtkObject *caller, unsigned long vtkNotUsed( event ),
                void *callData )
  {
    vtkImagePlaneWidget* self =
      reinterpret_cast< vtkImagePlaneWidget* >( caller );
    if(!self) return;

    double* wl = static_cast<double*>( callData );

    if ( self == this->WidgetX )
      {
      this->WidgetY->SetWindowLevel(wl[0],wl[1],1);
      this->WidgetZ->SetWindowLevel(wl[0],wl[1],1);
      }
    else if( self == this->WidgetY )
      {
      this->WidgetX->SetWindowLevel(wl[0],wl[1],1);
      this->WidgetZ->SetWindowLevel(wl[0],wl[1],1);
      }
    else if (self == this->WidgetZ)
      {
      this->WidgetX->SetWindowLevel(wl[0],wl[1],1);
      this->WidgetY->SetWindowLevel(wl[0],wl[1],1);
      }
  }

  vtkOrthoPlanesCallback():WidgetX( 0 ), WidgetY( 0 ), WidgetZ ( 0 ) {}

  vtkImagePlaneWidget* WidgetX;
  vtkImagePlaneWidget* WidgetY;
  vtkImagePlaneWidget* WidgetZ;
};

//-----------------------------------------------------------------------------
int main( int argc, char **argv )
{
  char fname[] = {"/home/eduardo/workspace/data_ImageLab/DICOM/FUNCAOVE_7"};

  vtkDICOMImageReader* v16 =  vtkDICOMImageReader::New();

  v16->SetDirectoryName(fname);

  v16->Update();

  vtkOutlineFilter* outline = vtkOutlineFilter::New();
    outline->SetInput(v16->GetOutput());

  vtkPolyDataMapper* outlineMapper = vtkPolyDataMapper::New();
    outlineMapper->SetInput(outline->GetOutput());

  vtkActor* outlineActor =  vtkActor::New();
    outlineActor->SetMapper( outlineMapper);

  vtkRenderer* ren1 = vtkRenderer::New();
  vtkRenderer* ren2 = vtkRenderer::New();
  vtkRenderer* ren3 = vtkRenderer::New();
  vtkRenderer* ren4 = vtkRenderer::New();


  vtkRenderWindow* renWin = vtkRenderWindow::New();
    renWin->AddRenderer(ren2);
    renWin->AddRenderer(ren3);
    renWin->AddRenderer(ren4);
    renWin->AddRenderer(ren1);


  vtkRenderWindowInteractor* iren = vtkRenderWindowInteractor::New();
    iren->SetRenderWindow(renWin);

  vtkCellPicker* picker = vtkCellPicker::New();
    picker->SetTolerance(0.005);

  vtkProperty* ipwProp = vtkProperty::New();
   //assign default props to the ipw's texture plane actor

  vtkImagePlaneWidget* planeWidgetX = vtkImagePlaneWidget::New();
    planeWidgetX->SetInteractor( iren);
    planeWidgetX->SetKeyPressActivationValue('x');
    planeWidgetX->SetPicker(picker);
    planeWidgetX->RestrictPlaneToVolumeOn();
    planeWidgetX->GetPlaneProperty()->SetColor(1,0,0);
    planeWidgetX->SetTexturePlaneProperty(ipwProp);
    planeWidgetX->TextureInterpolateOff();
    planeWidgetX->SetResliceInterpolateToNearestNeighbour();
    planeWidgetX->SetInput(v16->GetOutput());
    planeWidgetX->SetPlaneOrientationToXAxes();
    planeWidgetX->SetSliceIndex(32);
    planeWidgetX->DisplayTextOn();
    planeWidgetX->On();
    planeWidgetX->InteractionOff();
    planeWidgetX->InteractionOn();

  vtkImagePlaneWidget* planeWidgetY = vtkImagePlaneWidget::New();
    planeWidgetY->SetInteractor( iren);
    planeWidgetY->SetKeyPressActivationValue('y');
    planeWidgetY->SetPicker(picker);
    planeWidgetY->GetPlaneProperty()->SetColor(1,1,0);
    planeWidgetY->SetTexturePlaneProperty(ipwProp);
    planeWidgetY->TextureInterpolateOn();
    planeWidgetY->SetResliceInterpolateToLinear();
    planeWidgetY->SetInput(v16->GetOutput());
    planeWidgetY->SetPlaneOrientationToYAxes();
    planeWidgetY->SetSlicePosition(102.4);
    planeWidgetY->SetLookupTable( planeWidgetX->GetLookupTable());
    planeWidgetY->DisplayTextOff();
    planeWidgetY->UpdatePlacement();
    planeWidgetY->On();

  vtkImagePlaneWidget* planeWidgetZ = vtkImagePlaneWidget::New();
    planeWidgetZ->SetInteractor( iren);
    planeWidgetZ->SetKeyPressActivationValue('z');
    planeWidgetZ->SetPicker(picker);
    planeWidgetZ->GetPlaneProperty()->SetColor(0,0,1);
    planeWidgetZ->SetTexturePlaneProperty(ipwProp);
    planeWidgetZ->TextureInterpolateOn();
    planeWidgetZ->SetResliceInterpolateToCubic();
    planeWidgetZ->SetInput(v16->GetOutput());
    planeWidgetZ->SetPlaneOrientationToZAxes();
    planeWidgetZ->SetSliceIndex(25);
    planeWidgetZ->SetLookupTable( planeWidgetX->GetLookupTable());
    planeWidgetZ->DisplayTextOn();
    planeWidgetZ->On();

  vtkImageOrthoPlanes *orthoPlanes = vtkImageOrthoPlanes::New();
    orthoPlanes->SetPlane(0, planeWidgetX);
    orthoPlanes->SetPlane(1, planeWidgetY);
    orthoPlanes->SetPlane(2, planeWidgetZ);
    orthoPlanes->ResetPlanes();

   vtkOrthoPlanesCallback* cbk = vtkOrthoPlanesCallback::New();
   cbk->WidgetX = planeWidgetX;
   cbk->WidgetY = planeWidgetY;
   cbk->WidgetZ = planeWidgetZ;
   planeWidgetX->AddObserver( vtkCommand::EndWindowLevelEvent, cbk );
   planeWidgetY->AddObserver( vtkCommand::EndWindowLevelEvent, cbk );
   planeWidgetZ->AddObserver( vtkCommand::EndWindowLevelEvent, cbk );
   cbk->Delete();

  double wl[2];
  planeWidgetZ->GetWindowLevel(wl);

  // Add a 2D image to test the GetReslice method
  //
//  vtkImageMapToColors* colorMap = vtkImageMapToColors::New();
//    colorMap->PassAlphaToOutputOff();
//    colorMap->SetActiveComponent(0);
//    colorMap->SetOutputFormatToLuminance();
//    colorMap->SetInput(planeWidgetZ->GetResliceOutput());
//    colorMap->SetLookupTable(planeWidgetX->GetLookupTable());
//
//  vtkImageActor* imageActor = vtkImageActor::New();
//    imageActor->PickableOff();
//    imageActor->SetInput(colorMap->GetOutput());

  vtkImageMapToColors* colorMapX = vtkImageMapToColors::New();
    colorMapX->PassAlphaToOutputOff();
    colorMapX->SetActiveComponent(0);
    colorMapX->SetOutputFormatToLuminance();
    colorMapX->SetInput(planeWidgetX->GetResliceOutput());
    colorMapX->SetLookupTable(planeWidgetX->GetLookupTable());

  vtkImageActor* imageActorX = vtkImageActor::New();
    imageActorX->PickableOff();
    imageActorX->SetInput(colorMapX->GetOutput());

  vtkImageMapToColors* colorMapY = vtkImageMapToColors::New();
    colorMapY->PassAlphaToOutputOff();
    colorMapY->SetActiveComponent(0);
    colorMapY->SetOutputFormatToLuminance();
    colorMapY->SetInput(planeWidgetY->GetResliceOutput());
    colorMapY->SetLookupTable(planeWidgetX->GetLookupTable());

  vtkImageActor* imageActorY = vtkImageActor::New();
    imageActorY->PickableOff();
    imageActorY->SetInput(colorMapY->GetOutput());


  vtkMatrix4x4 *resliceAxes = vtkMatrix4x4::New();
//  resliceAxes->DeepCopy(axialElements);
//  resliceAxes->DeepCopy(coronalElements);
//  resliceAxes->DeepCopy(sagittalElements);

  double planeAxis1[3];
  double planeAxis2[3];
  planeWidgetZ->GetVector1(planeAxis1);
  planeWidgetZ->GetVector2(planeAxis2);

  // The x,y dimensions of the plane
  //
  vtkMath::Normalize(planeAxis1);
  vtkMath::Normalize(planeAxis2);

//  resliceAxes->DeepCopy(planeWidgetZ->GetReslice()->GetResliceAxes());
  // Set the point through which to slice
//  resliceAxes->SetElement(0, 3, center[0]);
//  resliceAxes->SetElement(1, 3, center[1]);
//  resliceAxes->SetElement(2, 3, center[2]);

  double normal[3];
  planeWidgetZ->GetNormal(normal);

  resliceAxes->Identity();
  for ( int i = 0; i < 3; i++ )
    {
    resliceAxes->SetElement(0,i,planeAxis1[i]);
    resliceAxes->SetElement(1,i,planeAxis2[i]);
    resliceAxes->SetElement(2,i,normal[i]);
    }

  double planeOrigin[4];
  planeWidgetZ->GetOrigin(planeOrigin);

  planeOrigin[3] = 1.0;
  double originXYZW[4];
  resliceAxes->MultiplyPoint(planeOrigin, originXYZW);

  resliceAxes->Transpose();
  double neworiginXYZW[4];
  resliceAxes->MultiplyPoint(originXYZW, neworiginXYZW);

  resliceAxes->SetElement(0,3,neworiginXYZW[0]);
  resliceAxes->SetElement(1,3,neworiginXYZW[1]);
  resliceAxes->SetElement(2,3,neworiginXYZW[2]);

  static double obliqueElements[16] = {
      1,  0,  0,  -162.498,
      0,  0.848925, -0.528513,  -378.959,
      0,  0.528513, 0.848925, -587.198,
      0,  0,  0,  1,  };

  resliceAxes->DeepCopy(obliqueElements);
  resliceAxes->SetElement(0,3,neworiginXYZW[0]);
  resliceAxes->SetElement(1,3,neworiginXYZW[1]);
  resliceAxes->SetElement(2,3,neworiginXYZW[2]);

  // Extract a slice in the desired orientation
  vtkImageReslice *reslice = vtkImageReslice::New();
  reslice->SetInputConnection(v16->GetOutputPort());
//  reslice->SetOutputDimensionality(2);
  reslice->SetResliceAxes(resliceAxes);
  reslice->SetInterpolationModeToLinear();

  // Create a greyscale lookup table
  vtkLookupTable *table = vtkLookupTable::New();
  table->SetRange(-300, 2700); // image intensity range
  table->SetValueRange(0.0, 1.0); // from black to white
  table->SetSaturationRange(0.0, 0.0); // no color saturation
  table->SetNumberOfColors( 256);
  table->SetHueRange( 0, 0);
  table->SetSaturationRange( 0, 0);
//  table->SetValueRange( 0 ,1);
  table->SetAlphaRange( 1, 1);
  table->SetRampToLinear();
  table->Build();

  // Map the image through the lookup table
  vtkImageMapToColors *colorMap = vtkImageMapToColors::New();
  colorMap->SetLookupTable(table);
  colorMap->SetInputConnection(reslice->GetOutputPort());

  // Display the image
  vtkImageActor *imageActor = vtkImageActor::New();
  imageActor->SetInput(colorMap->GetOutput());

  // Add the actors
  //
  ren1->AddActor( outlineActor);
  ren2->AddActor( imageActor);
  ren3->AddActor( imageActorX);
  ren4->AddActor( imageActorY);

  ren1->SetBackground( 0.1, 0.1, 0.2);
  ren2->SetBackground( 0.2, 0.1, 0.2);

  renWin->SetSize(800, 600);

//  ren1->SetViewport(0,0,0.58333,1);
//  ren2->SetViewport(0.58333,0,1,1);

  ren1->SetViewport(0,0,0.5,0.5);
  ren2->SetViewport(0.5,0,1,0.5);
  ren3->SetViewport(0,0.5,0.5,1);
  ren4->SetViewport(0.5,0.5,1,1);

  // Set the actors' postions
  //
  renWin->Render();
  iren->SetEventPosition( 175,175);
  iren->SetKeyCode('r');
  iren->InvokeEvent(vtkCommand::CharEvent,NULL);
  iren->SetEventPosition( 475,175);
  iren->SetKeyCode('r');
  iren->InvokeEvent(vtkCommand::CharEvent,NULL);
  renWin->Render();

//  ren1->GetActiveCamera()->Elevation(110);
//  ren1->GetActiveCamera()->SetViewUp(0, 0, -1);
//  ren1->GetActiveCamera()->Azimuth(45);
//  ren1->GetActiveCamera()->Dolly(1.15);
//  ren1->ResetCameraClippingRange();

  double bds[6];

  v16->GetOutput()->GetBounds(bds);

  cout << "Bounds: " << bds[0] << " " << bds[1] << " " << bds[2]
       << " " << bds[3] << " " << bds[4] << " " << bds[5] << endl;

  ren1->GetActiveCamera()->SetViewUp (0, -1, 0);
  ren1->GetActiveCamera()->SetPosition (0, 0, -1);
  ren1->GetActiveCamera()->SetFocalPoint (0, 0, 0);
  ren1->GetActiveCamera()->ComputeViewPlaneNormal();
  ren1->GetActiveCamera()->Dolly(1.5);
//  ren1->GetActiveCamera()->SetClippingRange(0, 20000);
  ren1->ResetCameraClippingRange();
  ren1->ResetCamera(bds);

  ren1->Render();
//  this->Render->GetActiveCamera()->SetViewUp (0, -1, 0);
//  this->Render->GetActiveCamera()->SetPosition (0, 0, -1);
//  this->Render->GetActiveCamera()->SetFocalPoint (0, 0, 0);
//  this->Render->GetActiveCamera()->ComputeViewPlaneNormal();
//  this->Render->GetActiveCamera()->Dolly(1.5);
//  this->Render->ResetCameraClippingRange();
//
//  this->Render->ResetCamera(bds);
//  this->Render->Render();

  // Interact with data
  // Render the image
  //
  iren->Initialize();
  renWin->Render();

  // Test SetKeyPressActivationValue for one of the widgets
  //
  iren->SetKeyCode('z');
  iren->InvokeEvent(vtkCommand::CharEvent,NULL);
  iren->SetKeyCode('z');
  iren->InvokeEvent(vtkCommand::CharEvent,NULL);

  iren->Start();

  resliceAxes->Delete();
  ipwProp->Delete();
  orthoPlanes->Delete();
  planeWidgetX->Delete();
  planeWidgetY->Delete();
  planeWidgetZ->Delete();
  colorMap->Delete();
  imageActor->Delete();
  colorMapX->Delete();
  imageActorX->Delete();
  colorMapY->Delete();
  imageActorY->Delete();
  picker->Delete();
  outlineActor->Delete();
  outlineMapper->Delete();
  outline->Delete();
  v16->Delete();
  iren->Delete();
  renWin->Delete();
  ren1->Delete();
  ren2->Delete();
  ren3->Delete();
  ren4->Delete();


  return 0;
}
