/*
 * AngleWidgetTeste.cxx
 *
 *  Created on: 19/11/2010
 *      Author: eduardo
 */

#include "vtkSmartPointer.h"
//#include "vtkAngleWidget.h"
//#include "vtkAngleRepresentation3D.h"
#include "vtkPolyDataMapper.h"
#include "vtkActor.h"
#include "vtkRenderer.h"
#include "vtkProperty.h"
#include "vtkRenderWindow.h"
#include "vtkRenderWindowInteractor.h"
#include "vtkCommand.h"
//#include "vtkXMLPolyDataReader.h"
//#include "vtkTestUtilities.h"
//#include "vtkRegressionTestImage.h"
#include "vtkDebugLeaks.h"
#include "vtkCoordinate.h"
#include "vtkMath.h"
#include "vtkHandleWidget.h"
#include "vtkSphereSource.h"
#include "vtkPointHandleRepresentation3D.h"
#include "vtkAxisActor2D.h"
#include "vtkProperty2D.h"

#include "imVolumeWidget.h"
#include "imVolumeRepresentation.h"
#include "vtkDICOMImageReader.h"
#include "vtkImagePlaneWidget.h"
#include "vtkCellPicker.h"
#include "vtkImageData.h"
#include "vtkCamera.h"

//#include "vtkSmartPointer.h"
//#include "vtkBiDimensionalWidget.h"
//#include "vtkBiDimensionalRepresentation2D.h"
//#include "vtkImageActor.h"
//#include "vtkImageData.h"
//#include "vtkRenderer.h"
//#include "vtkRenderWindow.h"
//#include "vtkRenderWindowInteractor.h"
//#include "vtkInteractorStyleImage.h"
//#include "vtkVolume16Reader.h"
//#include "vtkImageShiftScale.h"
//#include "vtkImageActor.h"
//#include "vtkCommand.h"
//#include "vtkInteractorEventRecorder.h"
//#include "vtkRegressionTestImage.h"
//#include "vtkDebugLeaks.h"
//#include "vtkTestUtilities.h"
//#include "vtkTransform.h"
//#include "vtkDICOMImageReader.h"


int main( int argc, char **argv )
{
  char fname[] = {"/home/workspace/DICOM_TEST/FUNCAOVE_7/"};
  if ( argc > 1 )
    {
    cout << "Mudando o diretorio para" << argv[1] << endl;
    strcpy(fname, argv[1]);
    }

  cout << "Diretorio :" << fname << endl;

  vtkDICOMImageReader* reader =  vtkDICOMImageReader::New();

  reader->SetDirectoryName(fname);

  reader->Update();

//  vtkSmartPointer<vtkSphereSource> ss
//    = vtkSmartPointer<vtkSphereSource>::New();

  // Create the RenderWindow, Renderer and both Actors
  //
  vtkSmartPointer<vtkRenderer> ren1 = vtkSmartPointer< vtkRenderer >::New();
  vtkSmartPointer< vtkRenderWindow > renWin = vtkSmartPointer< vtkRenderWindow >::New();



  renWin->AddRenderer(ren1);

  vtkSmartPointer< vtkRenderWindowInteractor > iren
    = vtkSmartPointer< vtkRenderWindowInteractor >::New();
  iren->SetRenderWindow(renWin);

  // Create a test pipeline
  //
//  vtkSmartPointer< vtkPolyDataMapper > mapper
//    = vtkSmartPointer< vtkPolyDataMapper >::New();
//  mapper->SetInput(ss->GetOutput());
//  vtkSmartPointer< vtkActor > actor = vtkSmartPointer< vtkActor >::New();
//  actor->SetMapper(mapper);

  // Create the widget and its representation
  vtkSmartPointer< vtkPointHandleRepresentation3D > handle
    = vtkSmartPointer< vtkPointHandleRepresentation3D >::New();
  handle->GetProperty()->SetColor(1,0,0);
//  vtkSmartPointer< vtkAngleRepresentation3D > rep
//    = vtkSmartPointer< vtkAngleRepresentation3D >::New();
//  rep->SetHandleRepresentation(handle);

  vtkSmartPointer< imVolumeRepresentation > rep
    = vtkSmartPointer< imVolumeRepresentation >::New();
  rep->SetHandleRepresentation(handle);

  vtkSmartPointer< imVolumeWidget > widget
    = vtkSmartPointer< imVolumeWidget >::New();
//  vtkSmartPointer< vtkAngleWidget > widget
//    = vtkSmartPointer< vtkAngleWidget >::New();

  widget->SetInteractor(iren);
  widget->CreateDefaultRepresentation();
  widget->SetRepresentation(rep);

//  vtkSmartPointer< vtkAngleCallback > mcbk
//    = vtkSmartPointer< vtkAngleCallback >::New();
//  mcbk->Rep = rep;
//  widget->AddObserver(vtkCommand::PlacePointEvent,mcbk);
//  widget->AddObserver(vtkCommand::InteractionEvent,mcbk);

  // Add the actors to the renderer, set the background and size
  //
//  ren1->AddActor(actor);
  ren1->SetBackground(0.1, 0.2, 0.4);
  renWin->SetSize(800, 600);


  vtkCellPicker* picker = vtkCellPicker::New();
    picker->SetTolerance(0.005);

  vtkProperty* ipwProp = vtkProperty::New();

  vtkImagePlaneWidget* planeWidgetX = vtkImagePlaneWidget::New();
    planeWidgetX->SetInteractor( iren);
    planeWidgetX->SetKeyPressActivationValue('x');
    planeWidgetX->SetPicker(picker);
    planeWidgetX->RestrictPlaneToVolumeOn();
    planeWidgetX->GetPlaneProperty()->SetColor(1,0,0);
    planeWidgetX->SetTexturePlaneProperty(ipwProp);
    planeWidgetX->TextureInterpolateOff();
    planeWidgetX->SetResliceInterpolateToNearestNeighbour();
    planeWidgetX->SetInput(reader->GetOutput());
    planeWidgetX->SetPlaneOrientationToXAxes();
    planeWidgetX->SetSliceIndex(56);
    planeWidgetX->DisplayTextOn();
    planeWidgetX->On();
    planeWidgetX->InteractionOff();
//    planeWidgetX->InteractionOn();

  vtkImagePlaneWidget* planeWidgetY = vtkImagePlaneWidget::New();
    planeWidgetY->SetInteractor( iren);
    planeWidgetY->SetKeyPressActivationValue('y');
    planeWidgetY->SetPicker(picker);
    planeWidgetY->GetPlaneProperty()->SetColor(1,1,0);
    planeWidgetY->SetTexturePlaneProperty(ipwProp);
    planeWidgetY->TextureInterpolateOn();
    planeWidgetY->SetResliceInterpolateToLinear();
    planeWidgetY->SetInput(reader->GetOutput());
    planeWidgetY->SetPlaneOrientationToYAxes();
    planeWidgetY->SetSlicePosition(56);
    planeWidgetY->SetLookupTable( planeWidgetX->GetLookupTable());
    planeWidgetY->DisplayTextOff();
    planeWidgetY->UpdatePlacement();
    planeWidgetY->On();
    planeWidgetY->InteractionOff();

  vtkImagePlaneWidget* planeWidgetZ = vtkImagePlaneWidget::New();
    planeWidgetZ->SetInteractor( iren);
    planeWidgetZ->SetKeyPressActivationValue('z');
    planeWidgetZ->SetPicker(picker);
    planeWidgetZ->GetPlaneProperty()->SetColor(0,0,1);
    planeWidgetZ->SetTexturePlaneProperty(ipwProp);
    planeWidgetZ->TextureInterpolateOn();
    planeWidgetZ->SetResliceInterpolateToCubic();
    planeWidgetZ->SetInput(reader->GetOutput());
    planeWidgetZ->SetPlaneOrientationToZAxes();
    planeWidgetZ->SetSliceIndex(53);
    planeWidgetZ->SetLookupTable( planeWidgetX->GetLookupTable());
    planeWidgetZ->DisplayTextOn();
    planeWidgetZ->On();
    planeWidgetZ->InteractionOff();

  ren1->GetActiveCamera()->SetViewUp (0, -1, 0);
  ren1->GetActiveCamera()->SetPosition(1, 0, 0);
  ren1->GetActiveCamera()->SetFocalPoint (0, 0, 0);
  ren1->GetActiveCamera()->ComputeViewPlaneNormal();
  ren1->GetActiveCamera()->Dolly(1.5);
//  ren1->GetActiveCamera()->SetClippingRange(0, 20000);
  ren1->ResetCameraClippingRange();
  ren1->ResetCamera();

  // render the image
  //
  iren->Initialize();
  renWin->Render();
  widget->On();
  renWin->Render();

  iren->Start();

//  // Create the pipeline
//  char fname[] = {"/home/workspace/DICOM_TEST/FUNCAOVE_7/"};
//  if ( argc > 1 )
//    {
//    cout << "Mudando o diretorio para" << argv[1] << endl;
//    strcpy(fname, argv[1]);
//    }
//
//  vtkSmartPointer<vtkDICOMImageReader> reader =  vtkSmartPointer<vtkDICOMImageReader>::New();
//
//  reader->SetDirectoryName(fname);
//
//  reader->Update();

//  char fname[] = {"/home/VTKData/Data/headsq/quarter"};
//
//    vtkSmartPointer<vtkVolume16Reader> v16 =
//      vtkSmartPointer<vtkVolume16Reader>::New();
//    v16->SetDataDimensions(64, 64);
//    v16->SetDataByteOrderToLittleEndian();
//    v16->SetImageRange(1, 93);
//    v16->SetDataSpacing(3.2, 3.2, 1.5);
//    v16->SetFilePrefix(fname);
//    v16->ReleaseDataFlagOn();
//    v16->SetDataMask(0x7fff);
//    v16->Update();
//
//    double range[2];
//    v16->GetOutput()->GetScalarRange(range);
//
//    vtkSmartPointer<vtkImageShiftScale> shifter =
//      vtkSmartPointer<vtkImageShiftScale>::New();
//    shifter->SetShift(-1.0*range[0]);
//    shifter->SetScale(255.0/(range[1]-range[0]));
//    shifter->SetOutputScalarTypeToUnsignedChar();
//    shifter->SetInputConnection(v16->GetOutputPort());
//    shifter->ReleaseDataFlagOff();
//    shifter->Update();
//
//    vtkSmartPointer<vtkImageActor> imageActor =
//      vtkSmartPointer<vtkImageActor>::New();
//    imageActor->SetInput(shifter->GetOutput());
//    imageActor->VisibilityOn();
//    imageActor->SetDisplayExtent(0, 63, 0, 63, 46, 46);
//    imageActor->InterpolateOn();
//
//    double bounds[6];
//    imageActor->GetBounds(bounds);
//
//    // Create the RenderWindow, Renderer and both Actors
//    //
//    vtkSmartPointer<vtkRenderer> ren1 =
//      vtkSmartPointer<vtkRenderer>::New();
//    vtkSmartPointer<vtkRenderWindow> renWin =
//      vtkSmartPointer<vtkRenderWindow>::New();
//    renWin->AddRenderer(ren1);
//
//    vtkSmartPointer<vtkRenderWindowInteractor> iren =
//      vtkSmartPointer<vtkRenderWindowInteractor>::New();
//    iren->SetRenderWindow(renWin);
//
//    vtkSmartPointer<vtkInteractorStyleImage> style =
//      vtkSmartPointer<vtkInteractorStyleImage>::New();
//    iren->SetInteractorStyle(style);
//
//    // VTK widgets consist of two parts: the widget part that handles event processing;
//    // and the widget representation that defines how the widget appears in the scene
//    // (i.e., matters pertaining to geometry).
//    vtkSmartPointer<vtkBiDimensionalRepresentation2D> rep =
//      vtkSmartPointer<vtkBiDimensionalRepresentation2D>::New();
//
//    vtkSmartPointer<vtkBiDimensionalWidget> widget =
//      vtkSmartPointer<vtkBiDimensionalWidget>::New();
//    widget->SetInteractor(iren);
//    widget->SetRepresentation(rep);
//
////    vtkSmartPointer<vtkBiDimensionalCallback> callback = vtkSmartPointer<vtkBiDimensionalCallback>::New();
////    widget->AddObserver(vtkCommand::EndInteractionEvent,callback);
//
//    // Add the actors to the renderer, set the background and size
//    ren1->AddActor(imageActor);
//    ren1->SetBackground(0.1, 0.2, 0.4);
//    renWin->SetSize(800, 600);
//
//
//    // render the image
//    //
//    iren->Initialize();
//    renWin->Render();
//    widget->On();
//
//
//    iren->Start();

  return 0;
}
