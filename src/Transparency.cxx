/*
 * Transparency.cxx
 *
 *  Created on: 01/04/2015
 *      Author: eduardo
 */




#include <vtkVersion.h>
#include <vtkSmartPointer.h>
#include <vtkLookupTable.h>
#include <vtkImageMapper3D.h>
#include <vtkImageData.h>
#include <vtkImageMapToColors.h>
#include <vtkJPEGReader.h>
#include <vtkPNGReader.h>
#include <vtkPointData.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkInteractorStyleImage.h>
#include <vtkRenderer.h>
#include <vtkImageActor.h>

int main(int argc, char* argv[])
{
  // Verify input arguments
  if ( argc != 3 )
    {
    std::cout << "Usage: " << argv[0]
              << " Filename(.jpg)" << " Filename(.png)" << std::endl;
    return EXIT_FAILURE;
    }

  vtkSmartPointer<vtkJPEGReader> reader = vtkSmartPointer<vtkJPEGReader>::New();
  reader->SetFileName(argv[1]);
  reader->Update();

  vtkImageData* image = reader->GetOutput();



  vtkSmartPointer<vtkPNGReader> readerMask = vtkSmartPointer<vtkPNGReader>::New();
  readerMask->SetFileName(argv[2]);
  readerMask->Update();

  vtkSmartPointer<vtkImageData> maskImage = readerMask->GetOutput();

  int extent[6];
  maskImage->GetExtent(extent);

//  pixel == 255 é branco
//  pixel == 0 é preto
// a imagem segmentada é binária onde pixel == 0 representa a região segmentada


//  for (int y = extent[2]; y < extent[3]; y++)
//    {
//    for (int x = extent[0]; x < extent[1]; x++)
//      {
//      unsigned char* pixel = static_cast<unsigned char*>(maskImage->GetScalarPointer(x,y,0));
//      if( pixel[0] <= 0.0 ) //pixel é preto
//        {
//        pixel[0] = 0.0;
//        }
//      else
//        {
//        pixel[0] = 1.0;
//        }
//      }
//    }

  vtkSmartPointer<vtkLookupTable> lookupTable = vtkSmartPointer<vtkLookupTable>::New();
  lookupTable->SetNumberOfTableValues(2);
  lookupTable->SetRange(0.0,1.0);
  lookupTable->SetTableValue( 0, 1.0, 0.0, 0.0, 0.3 ); //label 0 is opaque and red
  lookupTable->SetTableValue( 1, 0.0, 0.0, 0.0, 0.0 ); //label 1 is transparent
  lookupTable->Build();

  vtkSmartPointer<vtkImageMapToColors> mapTransparency = vtkSmartPointer<vtkImageMapToColors>::New();
  mapTransparency->SetLookupTable(lookupTable);
  mapTransparency->PassAlphaToOutputOn();
#if VTK_MAJOR_VERSION <= 5
  mapTransparency->SetInput(maskImage);
#else
  mapTransparency->SetInputData(maskImage);
#endif

  // Create actors
  vtkSmartPointer<vtkImageActor> imageActor = vtkSmartPointer<vtkImageActor>::New();
#if VTK_MAJOR_VERSION <= 5
  imageActor->SetInput(image);
#else
  imageActor->GetMapper()->SetInputData(image);
#endif

  vtkSmartPointer<vtkImageActor> maskActor = vtkSmartPointer<vtkImageActor>::New();
  maskActor->GetMapper()->SetInputConnection(mapTransparency->GetOutputPort());

  // Visualize
  vtkSmartPointer<vtkRenderer> renderer = vtkSmartPointer<vtkRenderer>::New();
  renderer->AddActor(imageActor);
  renderer->AddActor(maskActor);
  renderer->ResetCamera();

  vtkSmartPointer<vtkRenderWindow> renderWindow = vtkSmartPointer<vtkRenderWindow>::New();
  renderWindow->AddRenderer(renderer);
  renderWindow->SetSize(800, 600);

  vtkSmartPointer<vtkRenderWindowInteractor> renderWindowInteractor = vtkSmartPointer<vtkRenderWindowInteractor>::New();
  vtkSmartPointer<vtkInteractorStyleImage> style = vtkSmartPointer<vtkInteractorStyleImage>::New();

  renderWindowInteractor->SetInteractorStyle(style);

  renderWindowInteractor->SetRenderWindow(renderWindow);
  renderWindowInteractor->Initialize();
  renderWindowInteractor->Start();

  return EXIT_SUCCESS;
}
