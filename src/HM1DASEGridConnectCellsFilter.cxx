#include "vtkPolyDataReader.h"
#include "vtkDataSetMapper.h"


#include "vtkDoubleArray.h"
#include "vtkPoints.h"
#include "vtkIdList.h"
#include "vtkPolyData.h"
#include "vtkPointData.h"
#include "vtkCellType.h"
#include "vtkCell.h"





#include "vtkRenderWindowInteractor.h"
#include "vtkRenderWindow.h"
#include "vtkRenderer.h"

#include <sstream>
#include <iostream>
#include <list>

using namespace std;



typedef std::list<vtkIdType> ListOfInt;

ListOfInt VisitedList;
ListOfInt::iterator VisitListIt;


vtkPolyData *InputGrid, *OutputGrid;

vtkIdList *PointsOldIds;

// Compute Euclidean Distance
double computeDistance(double *p1, double *p2)
{
  return sqrt(pow(p1[0]-p2[0],2) + pow(p1[1]-p2[1],2) + pow(p1[2]-p2[2],2));
}

//-----------------------------------------------------------------------------------
bool PointIsBordersCell(int ptId, vtkIdList *cellIds)
{
  vtkIdList *ptList = vtkIdList::New();

  if( cellIds->GetNumberOfIds() > 1 )
    cout << "Houston, we have a problem!!! Um ponto esta em mais de uma célula." <<endl;


  InputGrid->GetCellPoints( cellIds->GetId(0), ptList);

  int lastId = ptList->GetNumberOfIds()-1;


//  cout << "\n\nbool PointIsBordersCell(int ptId, vtkIdList *cellIds, vtkPolyData *inputGrid)" <<endl;
//  cout << "ptId: " << ptId <<endl;
//  cout << "ptList->GetId( 0 ): " << ptList->GetId( 0 ) <<endl;
//  cout << "ptList->GetId( lastId ): " << ptList->GetId( lastId ) <<endl;


  if( (ptList->GetId( 0 ) == ptId) || (ptList->GetId( lastId ) == ptId) )
    {
    ptList->Delete();
    return true;
    }
  else
    {
    ptList->Delete();
    return false;
    }
}

//-----------------------------------------------------------------------------------
void MergeCellsUsingPoints( int ptId1, int ptId2 )
{
//  cout << "\t\t Ligando pontos "<< ptId1 << " e " << ptId2 << endl;

  double point[3], value;


  vtkIdList *cellIds1 = vtkIdList::New();
  vtkIdList *cellIds2 = vtkIdList::New();
  vtkIdList *ptList1 = vtkIdList::New();
  vtkIdList *ptList2 = vtkIdList::New();
  vtkIdList *insertedPointsList = vtkIdList::New();



  // células que usam o ponto ptId1, em princípio, só devem existir uma célula.
  InputGrid->GetPointCells( ptId1, cellIds1 );

  // células que usam o ponto ptId2, em princípio, só devem existir uma célula.
  InputGrid->GetPointCells( ptId2, cellIds2 );


  // lista de pontos da célula
  InputGrid->GetCellPoints( cellIds1->GetId(0), ptList1);

  // lista de pontos da célula
  InputGrid->GetCellPoints( cellIds2->GetId(0), ptList2);



  //  cout <<  ptList1->GetId( ptList1->GetNumberOfIds()-1 ) << " >> " << ptList2->GetId( ptList2->GetNumberOfIds()-1 ) <<endl;
  // identifica a ordem na qual as linhas devem ser unidas
  // pois a ordem que os pontos são inseridos na lista de pontos do novo grid informa a conectividade entre os mesmos.
  if( ptList1->GetId( ptList1->GetNumberOfIds()-1 ) < ptList2->GetId( ptList2->GetNumberOfIds()-1 ) )
    {
    for(int i=0; i < ptList2->GetNumberOfIds(); ++i)
      {
      // recupero o ponto do grid de entrada
      InputGrid->GetPoints()->GetPoint( ptList2->GetId(i), point );

      // insere o ponto no grid de saída e armazena a id retornada
      int pid = OutputGrid->GetPoints()->InsertNextPoint( point );
      insertedPointsList->InsertNextId( pid );

      PointsOldIds->SetId( ptList2->GetId(i), pid); // o ponto mudou para a posição pid

      value = InputGrid->GetPointData()->GetArray(0)->GetTuple1( i );
      OutputGrid->GetPointData()->GetArray(0)->InsertNextTuple1( value );
      }

    OutputGrid->InsertNextCell( VTK_LINE, insertedPointsList );

    insertedPointsList->Reset();


    for(int i=0; i < ptList1->GetNumberOfIds(); ++i)
      {
      int id = i;

      // se for o mesmo id apenas liga com a célula anterior pois o ponto já foi inserido no laço anterior
      if( ptList1->GetId(i) == ptId1 )
        {
//        cout << ptList1->GetId(i) << " == " << ptId1 <<endl;
        vtkIdList *tmpts = vtkIdList::New();

        OutputGrid->GetCellPoints( OutputGrid->GetNumberOfCells()-1, tmpts );

        id = tmpts->GetId( tmpts->GetNumberOfIds()-1 );

        tmpts->Delete();

        insertedPointsList->InsertNextId( id );

        continue;
        }

      // recupero o ponto do grid de entrada
      InputGrid->GetPoints()->GetPoint( ptList1->GetId(id), point );

      // insere o ponto no grid de saída e armazena a id retornada
      int pid = OutputGrid->GetPoints()->InsertNextPoint( point );
      insertedPointsList->InsertNextId( pid );

      PointsOldIds->SetId( ptList1->GetId(i), pid); // o ponto mudou para a posição pid

      value = InputGrid->GetPointData()->GetArray(0)->GetTuple1( id );
      OutputGrid->GetPointData()->GetArray(0)->InsertNextTuple1( value );
      }

    OutputGrid->InsertNextCell( VTK_LINE, insertedPointsList );
    }
  else
    {
    for(int i=0; i < ptList1->GetNumberOfIds(); ++i)
      {
      // recupero o ponto do grid de entrada
      InputGrid->GetPoints()->GetPoint( ptList1->GetId(i), point );

      // insere o ponto no grid de saída e armazena a id retornada
      int pid = OutputGrid->GetPoints()->InsertNextPoint( point );
      insertedPointsList->InsertNextId( pid );

      PointsOldIds->SetId( ptList1->GetId(i), pid); // o ponto mudou para a posição pid

      value = InputGrid->GetPointData()->GetArray(0)->GetTuple1( i );
      OutputGrid->GetPointData()->GetArray(0)->InsertNextTuple1( value );
      }

    OutputGrid->InsertNextCell( VTK_LINE, insertedPointsList );

    insertedPointsList->Reset();


    for(int i=0; i < ptList2->GetNumberOfIds(); ++i)
      {
      int id = i;

      // se for o mesmo id apenas liga com a célula anterior pois o ponto já foi inserido no laço anterior
      if( ptList2->GetId(i) == ptId2 )
        {
//        cout << ptList2->GetId(i) << " == " << ptId2 <<endl;
        vtkIdList *tmpts = vtkIdList::New();

        OutputGrid->GetCellPoints( OutputGrid->GetNumberOfCells()-1, tmpts );

        id = tmpts->GetId( 0 );

        tmpts->Delete();

        insertedPointsList->InsertNextId( id );

        continue;
        }

      // recupero o ponto do grid de entrada
      InputGrid->GetPoints()->GetPoint( ptList2->GetId(id), point );

      // insere o ponto no grid de saída e armazena a id retornada
      int pid = OutputGrid->GetPoints()->InsertNextPoint( point );
      insertedPointsList->InsertNextId( pid );

      PointsOldIds->SetId( ptList2->GetId(i), pid); // o ponto mudou para a posição pid

      value = InputGrid->GetPointData()->GetArray(0)->GetTuple1( id );
      OutputGrid->GetPointData()->GetArray(0)->InsertNextTuple1( value );
      }

    OutputGrid->InsertNextCell( VTK_LINE, insertedPointsList );
    }



  ptList1->Delete();
  ptList2->Delete();
  insertedPointsList->Delete();
  cellIds1->Delete();
  cellIds2->Delete();
}

//-----------------------------------------------------------------------------------
void DivideCellUsindPointId(int ptId)
{
//  cout << " \t DivideCellUsindPointId >> ptId: " << ptId <<endl;

  double point[3], value;

  vtkIdList *cellIds = vtkIdList::New();
  vtkIdList *ptList = vtkIdList::New();
  vtkIdList *insertedPointsList = vtkIdList::New();

  // células que usam o ponto ptId1, em princípio, só devem existir uma célula.
  InputGrid->GetPointCells( ptId, cellIds );

  // lista de pontos da célula
  InputGrid->GetCellPoints( cellIds->GetId(0), ptList );


  int i = 0;
  while( ptList->GetId(i) != ptId )
    {
//    cout << "while( ptList->GetId(i) < ptId )" << endl;

    // recupero o ponto do grid de entrada
    InputGrid->GetPoints()->GetPoint( ptList->GetId(i), point );

    // insere o ponto no grid de saída e armazena a id retornada
    int pid = OutputGrid->GetPoints()->InsertNextPoint( point );
    insertedPointsList->InsertNextId( pid  );

    PointsOldIds->SetId( ptList->GetId(i), pid); // o ponto mudou para a posição pid

    value = InputGrid->GetPointData()->GetArray(0)->GetTuple1( i );
    OutputGrid->GetPointData()->GetArray(0)->InsertNextTuple1( value );

    ++i;
    }

  OutputGrid->InsertNextCell( VTK_LINE, insertedPointsList );

  insertedPointsList->Reset();

//  cout << "ptId:" << ptId << endl;
//  cout << "i:" << i << endl;
//  cout << "ptList->GetId(i):" << ptList->GetId(i) << endl;
//  cout << "ptList->GetId( ptList->GetNumberOfIds()-1):" << ptList->GetId( ptList->GetNumberOfIds()-1) << endl;


  for( ;i < ptList->GetNumberOfIds(); ++i )
    {
//    cout << "for( ;i < ptList->GetNumberOfIds(); ++i )" << endl;
    if( ptList->GetId(i) == ptId )
      {
      vtkIdList *tmpts = vtkIdList::New();

      OutputGrid->GetCellPoints( OutputGrid->GetNumberOfCells()-1, tmpts );

      int id = tmpts->GetId( tmpts->GetNumberOfIds()-1 );

      tmpts->Delete();

      insertedPointsList->InsertNextId( id );

      continue;
      }


    // recupero o ponto do grid de entrada
    InputGrid->GetPoints()->GetPoint( ptList->GetId(i), point );

    // insere o ponto no grid de saída e armazena a id retornada
    int pid = OutputGrid->GetPoints()->InsertNextPoint( point );
    insertedPointsList->InsertNextId( pid );

    PointsOldIds->SetId( ptList->GetId(i), pid); // o ponto mudou para a posição pid

    value = InputGrid->GetPointData()->GetArray(0)->GetTuple1( i );
    OutputGrid->GetPointData()->GetArray(0)->InsertNextTuple1( value );
    }


  OutputGrid->InsertNextCell( VTK_LINE, insertedPointsList );



  ptList->Delete();
  cellIds->Delete();
  insertedPointsList->Delete();
}

//-----------------------------------------------------------------------------------
void MergeCellsBasedInPoints(int ptId1, int ptId2)
{
//  cout << "ptId1: " << ptId1 << endl;
//  cout << "ptId2: " << ptId2 << endl;

  bool P1_isBorder = false;
  bool P2_isBorder = false;

  vtkIdList *cellIds = vtkIdList::New();

  InputGrid->GetPointCells( ptId1, cellIds );


  cout << "  Verificando ponto " << ptId1 << "... ";
  if( PointIsBordersCell( ptId1, cellIds ) )
    {
    P1_isBorder = true;
    cout << "está na borda." << endl;
    }
  else
    {
    cout << "bifurcação." << endl;
    }


  cellIds->Reset();
  InputGrid->GetPointCells( ptId2, cellIds );


  cout << "  Verificando ponto " << ptId2 << "... ";
  if( PointIsBordersCell( ptId2, cellIds ) )
    {
    P2_isBorder = true;
    cout << "está na borda." << endl;
    }
  else
    {
    cout << "bifurcação." << endl;
    }



  if(P1_isBorder && P2_isBorder) // se os dois pontos estão na borda de células distintas
    {
    MergeCellsUsingPoints( ptId1, ptId2 );
    }
  else if( !P1_isBorder ) // se o ponto P1 esta dentro de uma célula
    {
    DivideCellUsindPointId( ptId1 );
    }
  else if( !P2_isBorder ) // se o ponto P2 esta dentro de uma célula
    {
    DivideCellUsindPointId( ptId2 );
    }

  cellIds->Delete();
}

//-----------------------------------------------------------------------------------
bool SearchTuple( int i, int j, vtkIntArray *linkedPoints )
{
  double *tuple;

  for(int k=0; k < linkedPoints->GetNumberOfTuples(); ++k)
    {
    tuple = linkedPoints->GetTuple2( k );

//    cout << "i: " << i << "j: " << j <<endl;
//    cout << "tuple[0]: " << tuple[0] << "tuple[1]: " << tuple[1] <<endl;


    if( ( tuple[0] == i && tuple[1] == j ) || ( tuple[0] == j && tuple[1] == i ) )
      return true;
    }

  return false;
}

//-----------------------------------------------------------------------------------
void ConnectGridCells(  )
{
  double dif_factor = 0.0001;

  int nPoints = InputGrid->GetNumberOfPoints();
//  cout << "InputGrid->GetNumberOfPoints(): " << nPoints <<endl;

  double currentPoint[3], point[3];
  double dist = 1000;
  int links = 0;

  vtkIntArray *linkedPoints = vtkIntArray::New();
  linkedPoints->SetNumberOfComponents(2);


  for(int i=0; i < nPoints; ++i)
    {
    // recupera a coordenada do ponto que será comparado com todos dos outros
    InputGrid->GetPoint( i, currentPoint );

    for(int j=0; j < nPoints; ++j)
      {
      // Se for o mesmo ponto ( ids iguais ) ou se já foram ligados não devem ser comparados.
      if( j == i || SearchTuple( i, j, linkedPoints ))
        {
        continue;
        }

      // recupera a coordenada do outro ponto para comparação
      InputGrid->GetPoint( j, point );


      // cálculo da distância Euclidiana
      dist = computeDistance( currentPoint, point );


      if( dist <= dif_factor) // está perto o suficiente para ser considerado o mesmo ponto?
        {
        cout << "\ni: " << i << "\tj: " << j << endl;

        // liga as células de pontos i e j
        MergeCellsBasedInPoints( i, j );

        // insere na lista os pontos linkados
        linkedPoints->InsertNextTuple2(i, j);

        ++links;
        }
      }
    }

  linkedPoints->Delete();

  cout << "\nlinks: " << links << endl;
}

//-----------------------------------------------------------------------------
int main( int argc, char **argv )
{
//  string fileName = "/home/eduardo/workspace/data_HeMoLab/modelos/Sansuke_1D_3dmax/exportaodo3dsmax/teste/Left_arm_Teste.vtk";
  string fileName = "/home/eduardo/workspace/data_HeMoLab/modelos/Sansuke_1D_3dmax/exportaodo3dsmax/teste/teste.vtk";


  vtkPolyDataReader *reader = vtkPolyDataReader::New();
  reader->SetFileName( fileName.c_str() );
  reader->Update();


  InputGrid = reader->GetOutput();
  OutputGrid = vtkPolyData::New();


  vtkDoubleArray *radius = vtkDoubleArray::New();
  radius->SetName("Radius");
  radius->SetNumberOfComponents(1);
  OutputGrid->GetPointData()->AddArray( radius );
  radius->Delete();

  vtkPoints *points = vtkPoints::New();
  OutputGrid->SetPoints( points );
  points->Delete();

  OutputGrid->Allocate();

  PointsOldIds = vtkIdList::New();
  PointsOldIds->SetNumberOfIds( InputGrid->GetNumberOfPoints() );

  for(int i=0; i < PointsOldIds->GetNumberOfIds(); ++i)
    PointsOldIds->SetId(i , -1);


  ConnectGridCells();


  vtkDataSetMapper *mapper = vtkDataSetMapper::New();
//  mapper->SetInput( InputGrid );
  mapper->SetInput( OutputGrid );

//  cout << *InputGrid->GetPointData() <<endl;
//  cout << "\n" <<endl;
//  cout << *OutputGrid <<endl;

  vtkActor *actor = vtkActor::New();
  actor->SetMapper( mapper );


  vtkRenderer *renderer = vtkRenderer::New();
  renderer->AddActor( actor );
  renderer->SetBackground(0.3,0.3,0.5);

  vtkRenderWindow *renWin = vtkRenderWindow::New();
  renWin->AddRenderer(renderer);

  vtkRenderWindowInteractor *iren = vtkRenderWindowInteractor::New();
  iren->SetRenderWindow(renWin);
  renWin->SetSize(1024, 768);

  renderer->Render();

  iren->Initialize();
  iren->Start();


  mapper->Delete();
  actor->Delete();
  renderer->Delete();
  renWin->Delete();
  iren->Delete();



  reader->Delete();

  OutputGrid->Delete();
  PointsOldIds->Delete();

  return 0;
}











// retorna true para cellid visitado
bool CellVisited(int cellid)
{
for (VisitListIt=VisitedList.begin(); VisitListIt!=VisitedList.end(); ++VisitListIt)
  {
  if (*(VisitListIt) == cellid)
   return true;
  }
  return false;
}


// retorna lista com ids dos cells filhos de uma determinada celula
vtkIdList * GetChildList( vtkPolyData *InputGrid, int cellId )
{
//   cout << "Obter elementos filhos de "<< cellId << endl;
  vtkIdList *childs = vtkIdList::New();

  vtkIdList *ptids = vtkIdList::New();
  ptids->SetNumberOfIds(1);

  vtkIdList *cellids = vtkIdList::New();

  vtkCell* cell = InputGrid->GetCell(cellId);

  int pt1 = cell->GetPointIds()->GetId(0);
  int pt2 = cell->GetPointIds()->GetId(1);

  ptids->SetId(0, pt1);


  // passa o id da celula que se quer descobrir os vizinhos - cellId
  // ponto que faz parte da celula atual e tambem parte dos vizinhos
  // os ids dos vizinhos vem em  vtkIdList *cellids
  InputGrid->GetCellNeighbors(cellId, ptids, cellids);


  if (cellids->GetNumberOfIds()) // se celula tem vizinhos
    {
//    cout << "retornando id filho " << cellids->GetId(0) << endl;

    bool Visited = false;

    for (int i = 0; i < cellids->GetNumberOfIds() ; ++i)
      {
      if (CellVisited(cellids->GetId(i)))
        Visited = true; // se vizinho ja foi visitado, entao esse vizinho não é filho da celula dada por CellId
      }

    if (!Visited) // se vizinho nao foi visitado, entao é filho
      {
      for (int i = 0; i < cellids->GetNumberOfIds() ; ++i)
        {
        childs->InsertNextId(cellids->GetId(i)); // insere na lista de filhos
//        cout << "Filhos obtido:  "<< cellids->GetId(i) << endl;
        }
      }
    }

  ptids->SetId(0, pt2); // repete o mesmo processo para o outro ponto da celula

  InputGrid->GetCellNeighbors(cellId, ptids, cellids);
  if (cellids->GetNumberOfIds() )
    {
//    cout << "retornando id filho " << cellids->GetId(0) << endl;

    bool Visited = false;

    for (int i = 0; i < cellids->GetNumberOfIds() ; ++i)
      {
      if (CellVisited(cellids->GetId(i)))
        Visited = true;
      }

    if (!Visited)
      {
      for (int i = 0; i < cellids->GetNumberOfIds() ; ++i)
        {
        childs->InsertNextId(cellids->GetId(i));
//        cout << "Filhos obtido:  "<< cellids->GetId(i) << endl;
        }
      }
    }

  // deleta vtkidlists auxiliares
  ptids->Delete();
  cellids->Delete();

  return childs;
}



//vtkPolyData * ConnectGridCell( vtkPolyData *inputGrid )
//{
//  double dif_factor = 0.001;
//
//
//  vtkPolyData *grid = vtkPolyData::New();
//
//  int nCells = inputGrid->GetNumberOfCells();
////  cout << "inputGrid->GetNumberOfCells(): " << nCells <<endl;
//
//
//  vtkCell *cell0, *cell1;
//  double cell0_point0[3], cell0_point1[3], cell1_point0[3], cell1_point1[3];
//  int cell0_id0, cell0_idn, cell1_id0, cell1_idn;
//
//  int links = 0;
//
//  for(int i=0; i < nCells; ++i)
//    {
////    vtkIdList *childs = GetChildList( inputGrid, i );
////    cout << "Filhos da cell: "<< i << " -- " << childs->GetNumberOfIds() <<endl;
////    childs->Delete();
//
////    cout << "i: " << i <<endl;
//
//    cell0 = inputGrid->GetCell( i );
//
//
//    cell0_id0 = cell0->GetPointIds()->GetId( 0 );
//    cell0_idn = cell0->GetPointIds()->GetId( cell0->GetNumberOfPoints()-1 );
//
//    inputGrid->GetPoint( cell0_id0, cell0_point0 );
//    inputGrid->GetPoint( cell0_idn, cell0_point1 );
//
//
//    for(int j=0; j < nCells; ++j)
//      {
//      if(j == i) continue;
//
//
////      cout << "\t j: " << j <<endl;
//
//      cell1 = inputGrid->GetCell( j );
//
//
//      cell1_id0 = cell1->GetPointIds()->GetId( 0 );
//      cell1_idn = cell1->GetPointIds()->GetId( cell1->GetNumberOfPoints()-1 );
//
//      inputGrid->GetPoint( cell1_id0, cell1_point0 );
//      inputGrid->GetPoint( cell1_idn, cell1_point1 );
//
//
//
//
//      // cálculo da distância Euclidiana entre 2 pontos
//      double dist;
//
//      dist = computeDistance(cell0_point1, cell1_point0);
////      cout << "cell0_point1[0]: " << cell0_point1[0] << "  cell0_point1[1]: " << cell0_point1[1] << "  cell0_point1[2]: " << cell0_point1[2] <<endl;
////      cout << "cell1_point0[0]: " << cell1_point0[0] << "  cell1_point0[1]: " << cell1_point0[1] << "  cell1_point0[2]: " << cell1_point0[2] <<endl;
//      cout << "dist: " << dist << endl;
//      cout << "dif_factor: " << dif_factor << endl;
//      cout << "\n\n" <<endl;
//
//      if( dist <= dif_factor )
//        {
////        cout << "Cellid: " << i << "  Point Id: " << cell0_idn << " == CellId: " << j << "  Point Id: " << cell1_id0 <<endl;
//        links++;
//        continue;
//        }
//
//
//      dist = computeDistance(cell0_point1, cell1_point1);
////      cout << "cell0_point1[0]: " << cell0_point1[0] << "  cell0_point1[1]: " << cell0_point1[1] << "  cell0_point1[2]: " << cell0_point1[2] <<endl;
////      cout << "cell1_point1[0]: " << cell1_point1[0] << "  cell1_point1[1]: " << cell1_point1[1] << "  cell1_point1[2]: " << cell1_point1[2] <<endl;
//      cout << "dist: " << dist << endl;
//      cout << "dif_factor: " << dif_factor << endl;
//      cout << "\n\n" <<endl;
//
//      if( dist <= dif_factor )
//        {
////        cout << "Cellid: "<< i << "  Point Id: " << cell0_idn << " == CellId: " << j << "  Point Id: " << cell1_idn <<endl;
//        links++;
//        continue;
//        }
//
//
//      dist = computeDistance(cell0_point0, cell1_point1);
////      cout << "cell0_point0[0]: " << cell0_point0[0] << "  cell0_point0[1]: " << cell0_point0[1] << "  cell0_point0[2]: " << cell0_point0[2] <<endl;
////      cout << "cell1_point1[0]: " << cell1_point1[0] << "  cell1_point1[1]: " << cell1_point1[1] << "  cell1_point1[2]: " << cell1_point1[2] <<endl;
//      cout << "dist: " << dist << endl;
//      cout << "dif_factor: " << dif_factor << endl;
//      cout << "\n\n" <<endl;
//
//      if( dist <= dif_factor )
//        {
////        cout << "Cellid: " << i << "  Point Id: " << cell0_id0 << " == CellId: " << j << "  Point Id: " << cell1_idn <<endl;
//        links++;
//        continue;
//        }
//
//
//      dist = computeDistance(cell0_point0, cell1_point0);
////      cout << "cell0_point0[0]: " << cell0_point0[0] << "  cell0_point0[1]: " << cell0_point0[1] << "  cell0_point0[2]: " << cell0_point0[2] <<endl;
////      cout << "cell1_point0[0]: " << cell1_point0[0] << "  cell1_point0[1]: " << cell1_point0[1] << "  cell1_point0[2]: " << cell1_point0[2] <<endl;
//      cout << "dist: " << dist << endl;
//      cout << "dif_factor: " << dif_factor << endl;
//      cout << "\n\n" <<endl;
//
//      if( dist <= dif_factor )
//        {
////        cout << "Cellid: " << i << "  Point Id: " << cell0_id0 << " == CellId: " << j << "  Point Id: " << cell1_id0 <<endl;
//        links++;
//        continue;
//        }
//      }
//    }
//
//
//  cout << "links: " << links << endl;
//
//  return grid;
//}
