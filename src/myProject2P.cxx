#include "vtkRenderWindowInteractor.h"
#include "vtkRenderWindow.h"
#include "vtkRenderer.h"

#include "vtkPolyDataReader.h"
#include "vtkPolyData.h"
#include "vtkPolyDataMapper.h"
#include "vtkActor.h"
#include "vtkProperty.h"
#include "vtkPointData.h"
#include "vtkMath.h"
#include "vtkPlaneSource.h"
#include "vtkTransform.h"
#include "vtkTransformFilter.h"
#include "vtkSphereSource.h"
#include "vtkGlyph3D.h"
#include "vtkArrowSource.h"
#include "vtkIdList.h"
#include "vtkCell.h"

#include <vtkAlgorithmOutput.h>

#include <string.h>


void ComputeMidPoint(int pointId1, int pointId2, vtkPolyData *poly, double midPoint[3])
{
  double p[3], p1[3];
  poly->GetPoint(pointId1, p);
  poly->GetPoint(pointId2, p1);

  midPoint[0] = (p[0] + p1[0])/2;
  midPoint[1] = (p[1] + p1[1])/2;
  midPoint[2] = (p[2] + p1[2])/2;
}

void ComputeNormal(int pointId, int cellId, vtkPolyData *centerline, double *normal)
{
  int ptId_0, ptId_1;
  double p0[3], p1[3];

  vtkCell *cell = centerline->GetCell( cellId );

  ptId_0 = cell->GetPointId(0);
  ptId_1 = cell->GetPointId(1);

  if( ptId_0  == pointId )
    {
    centerline->GetPoint( ptId_0, p0 );
    centerline->GetPoint( ptId_1, p1 );
    }
  else if( ptId_1 == pointId )
    {
    centerline->GetPoint( ptId_1, p0 );
    centerline->GetPoint( ptId_0, p1 );
    }

  normal[0] = p1[0] - p0[0];
  normal[1] = p1[1] - p0[1];
  normal[2] = p1[2] - p0[2];
}

void CreateNewPointsUsingPlane(vtkPolyData *centerline, vtkRenderer *renderer)
{
  int pointId1,pointId2;

  double p0[3], p1[3], normal[3], radius;

  for(pointId1=0; pointId1 < centerline->GetNumberOfPoints(); ++pointId1)
    {
    vtkIdList *cells = vtkIdList::New();
    centerline->GetPointCells(pointId1, cells);

    cout << "Point: " << pointId1 << " in Cells: "  << cells->GetNumberOfIds() <<endl;

    if(cells->GetNumberOfIds() > 2)
      {
      double n[3];
      ComputeNormal(pointId1, cells->GetId( 0 ), centerline, n);
      normal[0] += n[0];
      normal[1] += n[1];
      normal[2] += n[2];


      for(int k=1; k < cells->GetNumberOfIds(); ++k)
        {
        ComputeNormal(pointId1, cells->GetId(k), centerline, n);

//        cout << n[0] << endl;
//        cout << n[1] << endl;
//        cout << n[2] << endl;
        normal[0] += n[0];
        normal[1] += n[1];
        normal[2] += n[2];
        }
      }
    else
      {
      if( (pointId1 != 0) && (cells->GetNumberOfIds() == 1) ) //se for último ponto de um segmento arterial, inverte a nornal
//      if(pointId1 == centerline->GetNumberOfPoints()-1)
        pointId2 = pointId1-1;
      else
        pointId2 = pointId1+1;

        centerline->GetPoint( pointId1, p0 );
        centerline->GetPoint( pointId2, p1 );

        normal[0] = p1[0] - p0[0];
        normal[1] = p1[1] - p0[1];
        normal[2] = p1[2] - p0[2];
      }

      radius = centerline->GetPointData()->GetArray(0)->GetTuple1( pointId1 );
      centerline->GetPoint( pointId1, p0 ); //re-posicionando centro

      vtkPlaneSource *plane = vtkPlaneSource::New();
      plane->SetNormal( normal );
      plane->SetCenter( p0 );
      plane->SetResolution( 1, 1 );


      vtkTransform *transform = vtkTransform::New();
      transform->PostMultiply();
      transform->Translate( -p0[0], -p0[1], -p0[2] );
      transform->Scale( 2*radius, 2*radius, 2*radius );
      transform->Translate( p0[0], p0[1], p0[2] );

      vtkTransformFilter *transformFilter = vtkTransformFilter::New();
      transformFilter->SetInput(plane->GetOutput());
      transformFilter->SetTransform(transform);
      transformFilter->Update();

      vtkArrowSource *cone = vtkArrowSource::New();

      vtkGlyph3D *glyph = vtkGlyph3D::New();
      glyph->SetInputConnection(transformFilter->GetOutputPort());
      glyph->SetSource(cone->GetOutput());
      glyph->SetVectorModeToUseNormal();
      glyph->SetScaleModeToScaleByVector();
      glyph->SetScaleFactor(0.1);

      vtkPolyDataMapper *mapperNormals = vtkPolyDataMapper::New();
      mapperNormals->SetInput( glyph->GetOutput() );

      vtkActor *actorNormals = vtkActor::New();
      actorNormals->SetMapper( mapperNormals );
      actorNormals->GetProperty()->SetColor(1,0,0);




      double midPoint[3], colorSpheres[3];;
      double radiusSpheres = radius/10;
      colorSpheres[0] = 0.0;
      colorSpheres[1] = 0.0;
      colorSpheres[2] = 1;


      ComputeMidPoint(0, 1, transformFilter->GetPolyDataOutput(), midPoint);
      vtkSphereSource *sphere0 = vtkSphereSource::New();
      sphere0->SetCenter(midPoint);
      sphere0->SetRadius(radiusSpheres);

      vtkPolyDataMapper *mapperSphere0 = vtkPolyDataMapper::New();
      mapperSphere0->SetInput( sphere0->GetOutput() );

      vtkActor *actorSphere0 = vtkActor::New();
      actorSphere0->SetMapper( mapperSphere0 );
      actorSphere0->GetProperty()->SetColor(colorSpheres);


      ComputeMidPoint(0, 2, transformFilter->GetPolyDataOutput(), midPoint);
      vtkSphereSource *sphere1 = vtkSphereSource::New();
      sphere1->SetCenter(midPoint);
      sphere1->SetRadius(radiusSpheres);

      vtkPolyDataMapper *mapperSphere1 = vtkPolyDataMapper::New();
      mapperSphere1->SetInput( sphere1->GetOutput() );

      vtkActor *actorSphere1 = vtkActor::New();
      actorSphere1->SetMapper( mapperSphere1 );
      actorSphere1->GetProperty()->SetColor(colorSpheres);



      ComputeMidPoint(1, 3, transformFilter->GetPolyDataOutput(), midPoint);
      vtkSphereSource *sphere2 = vtkSphereSource::New();
      sphere2->SetCenter(midPoint);
      sphere2->SetRadius(radiusSpheres);

      vtkPolyDataMapper *mapperSphere2 = vtkPolyDataMapper::New();
      mapperSphere2->SetInput( sphere2->GetOutput() );

      vtkActor *actorSphere2 = vtkActor::New();
      actorSphere2->SetMapper( mapperSphere2 );
      actorSphere2->GetProperty()->SetColor(colorSpheres);



      ComputeMidPoint(2, 3, transformFilter->GetPolyDataOutput(), midPoint);
      vtkSphereSource *sphere3 = vtkSphereSource::New();
      sphere3->SetCenter(midPoint);
      sphere3->SetRadius(radiusSpheres);

      vtkPolyDataMapper *mapperSphere3 = vtkPolyDataMapper::New();
      mapperSphere3->SetInput( sphere3->GetOutput() );

      vtkActor *actorSphere3 = vtkActor::New();
      actorSphere3->SetMapper( mapperSphere3 );
      actorSphere3->GetProperty()->SetColor(colorSpheres);


      vtkPolyDataMapper *mapperPlane = vtkPolyDataMapper::New();
      mapperPlane->SetInputConnection( transformFilter->GetOutputPort() );

      vtkActor *actorPlane = vtkActor::New();
      actorPlane->SetMapper( mapperPlane );
      actorPlane->GetProperty()->SetColor(0.5,0.5,0);


      renderer->AddActor( actorPlane );
      renderer->AddActor( actorSphere0 );
      renderer->AddActor( actorSphere1 );
      renderer->AddActor( actorSphere2 );
      renderer->AddActor( actorSphere3 );
      renderer->AddActor( actorNormals );

      mapperPlane->Delete();
      actorPlane->Delete();
      plane->Delete();
      transform->Delete();
      transformFilter->Delete();

      sphere0->Delete();
      mapperSphere0->Delete();
      actorSphere0->Delete();

      sphere1->Delete();
      mapperSphere1->Delete();
      actorSphere1->Delete();

      sphere2->Delete();
      mapperSphere2->Delete();
      actorSphere2->Delete();

      sphere3->Delete();
      mapperSphere3->Delete();
      actorSphere3->Delete();
    }
}

//--------------------------------------------------------------------------------------------------
int main( int argc, char **argv )
{
  std::string fileNameCenterline = "/home/eduardo/workspace/data_HeMoLab/carotida_furada_cl.vtk";
  std::string fileNameMesh = "/home/eduardo/workspace/data_HeMoLab/modelos/modelo_3d/carotida_furada.vtk";

//  std::string fileNameCenterline = "/home/eduardo/workspace/data_HeMoLab/aorta_sem_subclavias_clip/aorta_sem_subclavias_cl.vtk";
//  std::string fileNameMesh = "/home/eduardo/workspace/data_HeMoLab/aorta_sem_subclavias_clip/aorta_sem_subclavias_polydata.vtk";

  double p0[3], p1[3], normal[3], radius;

  vtkPolyDataReader *mesh = vtkPolyDataReader::New();
  mesh->SetFileName(fileNameMesh.c_str());
  mesh->Update();

  vtkPolyDataMapper *mappermesh = vtkPolyDataMapper::New();
  mappermesh->SetInput( mesh->GetOutput() );

  vtkActor *actormesh = vtkActor::New();
  actormesh->SetMapper( mappermesh );
  actormesh->GetProperty()->SetOpacity(0.2);


  vtkPolyDataReader *centerline = vtkPolyDataReader::New();
  centerline->SetFileName(fileNameCenterline.c_str());
  centerline->Update();

  vtkPolyDataMapper *mappercenterline = vtkPolyDataMapper::New();
  mappercenterline->SetInput( centerline->GetOutput() );

  vtkActor *actorcenterline = vtkActor::New();
  actorcenterline->SetMapper( mappercenterline );
  actorcenterline->GetProperty()->SetColor(0,0.5,0);

  vtkRenderer *renderer = vtkRenderer::New();

  CreateNewPointsUsingPlane(centerline->GetOutput(), renderer);


  renderer->AddActor( actorcenterline );
  renderer->AddActor( actormesh );

  renderer->SetBackground(0.5,0.5,0.5);

  vtkRenderWindow *renWin = vtkRenderWindow::New();
  renWin->AddRenderer(renderer);

  vtkRenderWindowInteractor *iren = vtkRenderWindowInteractor::New();
  iren->SetRenderWindow(renWin);
  renWin->SetSize(1024, 768);

  renderer->Render();

  iren->Initialize();
  iren->Start();




  centerline->Delete();
  mappercenterline->Delete();
  actorcenterline->Delete();
  mesh->Delete();
  mappermesh->Delete();
  actormesh->Delete();
  renderer->Delete();
  renWin->Delete();
  iren->Delete();

  return 0;
}
