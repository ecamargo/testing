
#include <vtkRenderWindow.h>
#include <vtkRenderer.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkPolyDataMapper.h>
#include "vtkPolyDataReader.h"
#include "vtkPolyData.h"
#include "vtkMassProperties.h"
#include "vtkSmartPointer.h"
#include "vtkSphereSource.h"

#include <string.h>

using namespace std;




int main( int argc, char **argv )
{
  string path = "/home/nfs/aneurysm_project/morphIndexes/1287259/26896/ANGIOGRAFIA_5/volumetricImage_1/segmentationID_1/meshID_1/morphIdexes/massTests/";


/*
 * esfera de raio 09
 * phi e theta 50
 * volume 3.04247 (integrate variables) / 3.053628059 = [4/3*pi*r^3]
 * area 10.1602 (integrate variables) / 10.1787602 = [4*pi*r^2]
 */
  vtkSmartPointer<vtkSphereSource> sphere = vtkSmartPointer<vtkSphereSource>::New();
  sphere->SetCenter( 35.3, 112, 59.4 );
  sphere->SetRadius(0.9);
  sphere->SetPhiResolution(50);
  sphere->SetThetaResolution(50);


  vtkSmartPointer<vtkMassProperties> massProperties = vtkSmartPointer<vtkMassProperties>::New();
  massProperties->SetInput( sphere->GetOutput() );
  double a = massProperties->GetSurfaceArea();
  double v = massProperties->GetVolume();

  cout << "===== vtkSphereSource =====" << endl;
  cout << "Raio: " << sphere->GetRadius() <<endl;
  cout << "Volume [4/3*pi*r^3] = 3.053628059 / Integrate variables = 3.04247 / Volume Obtido (vtkMassProperties): " << v <<endl;
  cout << "Area [4*pi*r^2] = 10.1787602 / Integrate variables = 10.1602 / Area obtida (vtkMassProperties): " << a << endl <<endl;






  string file = "connectivity.vtk";
  string fileToRead = path + file;
  vtkSmartPointer<vtkPolyDataReader> reader = vtkSmartPointer<vtkPolyDataReader>::New();
  reader->SetFileName( fileToRead.c_str() );
  reader->Update();

  massProperties->SetInput( reader->GetOutput() );
  a = massProperties->GetSurfaceArea();
  v = massProperties->GetVolume();

  cout << "===== " << file << " =====" <<endl;
  cout << "Volume Obtido: " << v <<endl;
  cout << "Area obtida: " << a << endl <<endl;








  fileToRead = "/home/eduardo/workspace/data_HeMoLab/modelos/3D/cilinder.vtk";
  reader->SetFileName( fileToRead.c_str() );
  reader->Update();

  massProperties->SetInput( reader->GetOutput() );
  a = massProperties->GetSurfaceArea();
  v = massProperties->GetVolume();

  cout << "===== " << fileToRead << " =====" <<endl;
  cout << "Volume Obtido: " << v <<endl;
  cout << "Area obtida: " << a << endl <<endl;









  vtkSmartPointer<vtkPolyDataMapper> sphereMapper = vtkSmartPointer<vtkPolyDataMapper>::New();
  sphereMapper->SetInput( sphere->GetOutput() );

  vtkSmartPointer<vtkActor> sphereActor = vtkSmartPointer<vtkActor>::New();
  sphereActor->SetMapper( sphereMapper );


  vtkSmartPointer<vtkPolyDataMapper> fileMapper = vtkSmartPointer<vtkPolyDataMapper>::New();
  fileMapper->SetInput( reader->GetOutput() );

  vtkSmartPointer<vtkActor> fileActor = vtkSmartPointer<vtkActor>::New();
  fileActor->SetMapper( fileMapper );



  // A renderer and render window
  vtkSmartPointer<vtkRenderer> renderer = vtkSmartPointer<vtkRenderer>::New();
  vtkSmartPointer<vtkRenderWindow> renderWindow = vtkSmartPointer<vtkRenderWindow>::New();
  renderWindow->AddRenderer(renderer);

  // An interactor
  vtkSmartPointer<vtkRenderWindowInteractor> renderWindowInteractor = vtkSmartPointer<vtkRenderWindowInteractor>::New();
  renderWindowInteractor->SetRenderWindow(renderWindow);

  renderer->AddActor(fileActor);
  renderer->AddActor(sphereActor);
  renderer->SetBackground(0.3,0.35,0.5);

  renderWindow->SetSize(1024,800);
  renderWindow->Render();

  renderWindowInteractor->Start();


  return 0;
}



