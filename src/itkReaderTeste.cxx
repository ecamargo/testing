#include "itkImageSeriesReader.h"
#include "itkGDCMSeriesFileNames.h"
#include "itkGDCMImageIO.h"
#include "itkImage.h"
#include "itkImageToVTKImageFilter.h"
#include "itkVTKImageToImageFilter.h"

#include "vtkImageData.h"
#include "vtkImageViewer2.h"
#include "vtkRenderWindowInteractor.h"
#include "vtkRenderer.h"
#include "vtkRenderWindow.h"
#include "vtkImageActor.h"
#include "vtkPolyDataMapper.h"
#include "vtkActor.h"
#include "vtkLODActor.h"
#include "vtkOutlineFilter.h"
#include "vtkCamera.h"
#include "vtkStripper.h"
#include "vtkLookupTable.h"
#include "vtkImagePlaneWidget.h"
#include "vtkContourFilter.h"
#include "vtkProperty.h"
#include "vtkImageMapToColors.h"
#include "vtkPolyDataNormals.h"
#include "vtkDICOMImageReader.h"

#include "itkExtractImageFilter.h"
#include "itkImageFileWriter.h"
#include "itkImageFileReader.h"
#include "itkRescaleIntensityImageFilter.h"
#include "itkConnectedThresholdImageFilter.h"

#include "itkGradientMagnitudeRecursiveGaussianImageFilter.h"
#include "itkSigmoidImageFilter.h"
#include "itkFastMarchingImageFilter.h"
#include "itkBinaryThresholdImageFilter.h"
#include "itkGeodesicActiveContourLevelSetImageFilter.h"

using namespace std;

typedef itk::Image	<float, 3>				ImageType;
typedef itk::Image	<float, 2>				ImageType2D;
typedef itk::ImageToVTKImageFilter	<ImageType>		ConnectorType;
typedef itk::GDCMImageIO  ImageIOType;


void DICOMReader_ContourFilter(ConnectorType *connector);
void DICOMReader_IsoSurface(ConnectorType *connector);
void DICOMReader_Reslice(ConnectorType *connector, bool x, bool y, bool z);
void DICOMReader_ResliceWithPlanes(ConnectorType *connector);
void DICOMReader_ResliceWithPlanesMask(ConnectorType *connector, ConnectorType *connectorMask);
void DICOMViewer(ConnectorType *connector);
ImageType::Pointer DICOMReader(string path, ImageIOType *dicomIO);
int WriteDICOMSlice(ImageType2D *image, string outputFileName, ImageIOType *dicomIO);
ImageType2D::Pointer ImageReader(string path);


//-----------------------------------------------------------------------------
int main( int argc, char **argv )
{
  char path[256] = {"/home/eduardo/workspace/data_ImageLab/DICOM/Ricardo_head"};
//  char path_png[256] = {"/home/eduardo/workspace/data_ImageLab/DICOM/pppp/teste.png"};
//  char path_threshold[256] = {"/home/eduardo/workspace/data_ImageLab/DICOM/FUNCAOVE_7_threshold"};
  
  ImageIOType::Pointer dicomIO = ImageIOType::New();
  ImageType::Pointer image = DICOMReader(path, dicomIO);
//  ImageType::Pointer image_threshold = DICOMReader(path_threshold);]
//  ImageType2D::Pointer image_png = ImageReader(path_png);


  cout << "Input Image: " <<endl;
  cout << "\t Dimension: " << image->GetImageDimension() <<endl;
  cout << "\t size: ";
  for(unsigned int i=0; i < image->GetImageDimension(); i++)
    {
    cout << image->GetLargestPossibleRegion().GetSize().GetElement(i) << " ";
    }
  cout << endl;
  cout << ">>> Leitura efetuada!!" <<endl<<endl;


  ImageType::RegionType inputRegion = image->GetLargestPossibleRegion();
  ImageType::SizeType size = inputRegion.GetSize();
  size[2] = 0; // Setando o eixo de interesse.

  ImageType::IndexType start = inputRegion.GetIndex();
  const unsigned int sliceNumber = 105;
  start[2] = sliceNumber; // Setando o slice de interesse.

  ImageType::RegionType desiredRegion;
  desiredRegion.SetSize(  size  );
  desiredRegion.SetIndex( start );

  cout << ">>> ExtractImageFilter " <<endl;
  cout << "\t size: " << size[0] << " "
                      << size[1] << " "
                      << size[2] << endl;

  cout << "\t start: " << start[0] << " "
                       << start[1] << " "
                       << start[2] << endl;


  cout << ">>> ExtractImageFilter begin" << endl;
  typedef itk::ExtractImageFilter< ImageType, ImageType2D > ExtractFilterType;
  ExtractFilterType::Pointer ExtractFilter = ExtractFilterType::New();
  ExtractFilter->SetExtractionRegion( desiredRegion );
  ExtractFilter->SetInput( image );
  ExtractFilter->Update();
  cout << ">> ExtractImageFilter end" << endl;

  cout << "ITK extracted size: " <<endl;
  cout << "\t Dimension: " << ExtractFilter->GetOutput()->GetImageDimension() <<endl;
  cout << "\t size: ";
  for(unsigned int i=0; i < ExtractFilter->GetOutput()->GetImageDimension(); i++)
    {
    cout << ExtractFilter->GetOutput()->GetLargestPossibleRegion().GetSize().GetElement(i) << " ";
    }
  cout << endl;

  cout << ">> Extração efetuada!! " << endl<<endl;


	  
  //######################## Filtro
//    cout << "Aplicando filtros..." << endl;
	  
//  typedef   itk::GradientMagnitudeRecursiveGaussianImageFilter< ImageType, ImageType >  GradientFilterType;
//  GradientFilterType::Pointer  gradientMagnitude = GradientFilterType::New();
//		gradientMagnitude->SetInput( reader->GetOutput() );
//    gradientMagnitude->SetSigma(  0.8  );
//
//
//
////  typedef   itk::SigmoidImageFilter< ImageType, ImageType >  SigmoidFilterType;
////  SigmoidFilterType::Pointer sigmoid = SigmoidFilterType::New();
////  	sigmoid->SetInput( gradientMagnitude->GetOutput() );
////  	sigmoid->SetOutputMinimum(  0.0  );
////	  sigmoid->SetOutputMaximum(  1.0  );
////	  sigmoid->SetAlpha( -1.0 );
////	  sigmoid->SetBeta(  5.0 );
//
//
//
////  typedef  itk::FastMarchingImageFilter< ImageType, ImageType >    FastMarchingFilterType;
////  typedef FastMarchingFilterType::NodeContainer  NodeContainer;
////  typedef FastMarchingFilterType::NodeType       NodeType;
////  FastMarchingFilterType::Pointer  fastMarching = FastMarchingFilterType::New();
////  NodeContainer::Pointer seeds = NodeContainer::New();
////  ImageType::IndexType  seedPosition;
////
////  seedPosition[0] = 215;
////  seedPosition[1] = 448;
////  seedPosition[2] = 124;
////
////  NodeType node;
////
////  node.SetValue( -10.0 );
////  node.SetIndex( seedPosition );
////
////  seeds->Initialize();
////  seeds->InsertElement( 0, node );
////
////  fastMarching->SetTrialPoints( seeds );
////  fastMarching->SetSpeedConstant( 1.0 );
////  fastMarching->SetOutputSize( reader->GetOutput()->GetBufferedRegion().GetSize() );
//
//
//
//  
//  typedef  itk::GeodesicActiveContourLevelSetImageFilter< ImageType, ImageType > GeodesicActiveContourFilterType;
//  GeodesicActiveContourFilterType::Pointer geodesicActiveContour = GeodesicActiveContourFilterType::New();
//	  geodesicActiveContour->SetCurvatureScaling( 1.0 );
//	  geodesicActiveContour->SetPropagationScaling( 1.0 );
//	  geodesicActiveContour->SetAdvectionScaling( 0.5 );
//	  geodesicActiveContour->SetMaximumRMSError( 0.03 );
//	  geodesicActiveContour->SetNumberOfIterations( 30 );
////	  geodesicActiveContour->SetInput(  fastMarching->GetOutput() );
////	  geodesicActiveContour->SetFeatureImage( sigmoid->GetOutput() );
//	  geodesicActiveContour->SetInput(  reader->GetOutput() );
//	  geodesicActiveContour->SetFeatureImage( gradientMagnitude->GetOutput() );
//  
//
//
//
//
//  typedef itk::BinaryThresholdImageFilter< ImageType, ImageType    >    ThresholdingFilterType;
//  ThresholdingFilterType::Pointer thresholder = ThresholdingFilterType::New();
//	  thresholder->SetLowerThreshold( -1000.0 );
//	  thresholder->SetUpperThreshold( 0.0 );
//	  thresholder->SetOutsideValue( 255 );
//	  thresholder->SetInsideValue( 0 );
//	  thresholder->SetInput( geodesicActiveContour->GetOutput() );
//
//  try
//    {
//  	thresholder->Update();
//    }
//  catch( itk::ExceptionObject & excep )
//    {
//    std::cerr << "Exception caught !" << std::endl;
//    std::cerr << excep << std::endl;
//    }
//
//  // Print out some useful information
//  std::cout << std::endl;
//  std::cout << "Max. no. iterations: " << geodesicActiveContour->GetNumberOfIterations() << std::endl;
//  std::cout << "Max. RMS error: " << geodesicActiveContour->GetMaximumRMSError() << std::endl;
//  std::cout << std::endl;
//  std::cout << "No. elpased iterations: " << geodesicActiveContour->GetElapsedIterations() << std::endl;
//  std::cout << "RMS change: " << geodesicActiveContour->GetRMSChange() << std::endl;




	
  //######################## Conector para o Pipeline ITK => VTK	
  cout << "Conectando ITK ao VTK..." <<endl;
  ConnectorType::Pointer connector = ConnectorType::New();
//  connector->SetInput( thresholder->GetOutput() );
  connector->SetInput( image );
  


//  string outputFileName = "/home/eduardo/workspace/data_ImageLab/DICOM/pppp/1.dcm";
//  WriteDICOMSlice(ExtractFilter->GetOutput(), outputFileName, dicomIO);
  string outputFileName_png = "/home/eduardo/workspace/data_ImageLab/DICOM/pppp/teste.png";
  WriteDICOMSlice(ExtractFilter->GetOutput(), outputFileName_png, dicomIO);



//  ConnectorType::Pointer connector_threshold = ConnectorType::New();
//  connector_threshold->SetInput( image_threshold );

//  DICOMReader_Reslice(connector, true, false, false);
//  DICOMReader_ContourFilter(connector);
//  DICOMReader_IsoSurface(connector);
//  DICOMReader_Reslice(connector, true, false, false);
//  DICOMReader_Reslice(connector, false, true, false);
//  DICOMReader_Reslice(connector, false, false, true);
  DICOMReader_ResliceWithPlanes(connector );
//  DICOMReader_ResliceWithPlanesMask(connector, connector_threshold);
		
  return 0;
}

//-----------------------------------------------------------------------------
ImageType::Pointer DICOMReader(string path, ImageIOType *dicomIO)
{
  //######################## Leitura
  typedef itk::ImageSeriesReader  <ImageType>   ReaderType;
//  typedef itk::GDCMImageIO  ImageIOType;
  typedef itk::GDCMSeriesFileNames NamesGeneratorType;
  typedef std::vector <std::string> fileNamesContainer;

  // Classe de IO para imagens
//  ImageIOType::Pointer dicomIO = ImageIOType::New();

  // Gerador de séries para os nomes dos arquivos (conjunto DICOM)
  NamesGeneratorType::Pointer nameGenerator = NamesGeneratorType::New();
  nameGenerator->SetUseSeriesDetails(true);
  nameGenerator->SetInputDirectory(path);

  // Container para os nomes gerados
  fileNamesContainer fileNames;
  fileNames = nameGenerator->GetInputFileNames();

  // A leitura é feita neste ponto
  ReaderType::Pointer reader = ReaderType::New();
  reader->SetFileNames( fileNames );
  reader->SetImageIO( dicomIO );
  reader->Update();
  cout << "Leitura efetuada" << endl;

  typedef itk::Point<double, 3> pointType;
  pointType o;
  o = reader->GetOutput()->GetOrigin();

  cout << "ITK reader origin: " << o.GetElement(0) << " " << o.GetElement(1) << " " << o.GetElement(2) << endl;

  return reader->GetOutput();
}


//-----------------------------------------------------------------------------
ImageType2D::Pointer ImageReader(string path)
{
  typedef itk::ImageFileReader< ImageType2D >  ReaderType;
  ReaderType::Pointer itkReader;
  itkReader = ReaderType::New();
  itkReader->SetFileName( path.c_str() );

  try
    {
    itkReader->Update();
    }
  catch( itk::ExceptionObject & excp )
    {
    cout << "Reader Error!!!! " << excp.what()  <<endl;
    return NULL;
    }

  return itkReader->GetOutput();
}

//-----------------------------------------------------------------------------
void DICOMReader_Reslice(ConnectorType *connector, bool x, bool y, bool z)
{
	vtkRenderWindowInteractor *iren = vtkRenderWindowInteractor::New();		
		
  vtkImageViewer2 * viewer = vtkImageViewer2::New();
	  viewer->SetupInteractor( iren );
	  viewer->SetInput( connector->GetOutput() );
	  viewer->Render();
	  viewer->SetColorWindow( 256 );
	  viewer->SetColorLevel( 144 );
	  viewer->SetSlice(124);
  
  if(x)
  	{
  	viewer->SetSliceOrientationToXY(); // orientação em X
  	}
  else if(y)
  	{
    viewer->SetSliceOrientationToYZ(); // orientação em Y
  	}
  else if(z)
  	{
  	viewer->SetSliceOrientationToXZ(); // orientação em Z
  	}
 
  viewer->GetRenderer()->ResetCamera();
  
	iren->Start();
	
	iren->Delete();
	viewer->Delete();
}

//-----------------------------------------------------------------------------
void DICOMReader_ContourFilter(ConnectorType *connector)
{
  cout << "Aplicando vtkContourFilter..." <<endl;
  vtkRenderer *aRenderer = vtkRenderer::New();
  vtkRenderWindow *renWin = vtkRenderWindow::New();
	renWin->AddRenderer(aRenderer);
  vtkRenderWindowInteractor *iren = vtkRenderWindowInteractor::New();
    iren->SetRenderWindow(renWin);
	
	vtkContourFilter *skinExtractor = vtkContourFilter::New();
		skinExtractor->SetInput( connector->GetOutput());
		skinExtractor->SetNumberOfContours(6);
		skinExtractor->SetValue(0, -2040);
		skinExtractor->SetValue(1, -910);
		skinExtractor->SetValue(2, 228);
		skinExtractor->SetValue(3, 1366);
		skinExtractor->SetValue(4, 2504);
		skinExtractor->SetValue(5, 3642);
		//skinExtractor->GenerateValues(6, -2048, 3642);
		skinExtractor->ComputeNormalsOn();
		skinExtractor->ComputeGradientsOff();
		skinExtractor->UseScalarTreeOff();
		skinExtractor->Update();

	vtkPolyDataMapper *skinMapper = vtkPolyDataMapper::New();
		skinMapper->SetInput(skinExtractor->GetOutput());
		skinMapper->ScalarVisibilityOff();

	vtkLODActor *skin = vtkLODActor::New();
		skin->SetMapper(skinMapper);
		skin->GetProperty()->SetDiffuseColor(1, .49, .25);
		skin->GetProperty()->SetSpecular(.3);
		skin->GetProperty()->SetSpecularPower(20);
		skin->SetNumberOfCloudPoints(vtkImageData::SafeDownCast( skinExtractor->GetInput() )->GetNumberOfPoints()/100);


	  aRenderer->AddActor(skin);
	  aRenderer->SetBackground( 0.329412, 0.34902, 0.427451 ); //Paraview blue
	  cout << "Renderizando..." <<endl;
	  aRenderer->Render();
 
	  renWin->SetSize(1027, 768);
	  //renWin->GetInteractor()->SetDesiredUpdateRate( 5.0 );
	  iren->Initialize();
	  cout << "iren->Initialize()..." <<endl;
	  iren->Start();

	  // It is important to delete all objects created previously to prevent
	  // memory leaks. In this case, since the program is on its way to
	  // exiting, it is not so important. But in applications it is
	  // essential.
	  skinExtractor->Delete();
	  skinMapper->Delete();
	  skin->Delete();
	  aRenderer->Delete();
	  renWin->Delete();
	  iren->Delete();
}

//-----------------------------------------------------------------------------
void DICOMViewer(ConnectorType *connector)
{
	vtkRenderWindowInteractor *iren = vtkRenderWindowInteractor::New();		

  vtkImageViewer2 * viewer = vtkImageViewer2::New();
	  viewer->SetupInteractor( iren );
	  viewer->SetInput( connector->GetOutput() );
	  viewer->SetColorWindow( 255 );
	  viewer->SetColorLevel( 128 );	  
          viewer->SetSlice(146);
	  viewer->GetRenderer()->ResetCamera();
	  viewer->Render();    

	iren->Start();
	iren->Delete();
	viewer->Delete();
}

//-----------------------------------------------------------------------------
void DICOMReader_ResliceWithPlanes(ConnectorType *connector)
{
  /////////////////////////////////////////////////////////////////////////////
  //cria os planos do leitor itk
	vtkImagePlaneWidget *XPlane = vtkImagePlaneWidget::New();	
	  XPlane->SetInput( connector->GetOutput() );
	  XPlane->TextureInterpolateOn();
	  XPlane->SetTextureVisibility(1);
	  XPlane->RestrictPlaneToVolumeOn();
	  XPlane->SetResliceInterpolateToCubic();
	  XPlane->SetPlaneOrientationToXAxes();
	  XPlane->SetSliceIndex(256);
	  XPlane->SetWindowLevel(500,128);
	  XPlane->DisplayTextOn();
	  
	vtkImagePlaneWidget *YPlane = vtkImagePlaneWidget::New();	
		YPlane->SetInput( connector->GetOutput() );
		YPlane->TextureInterpolateOn();
		YPlane->SetTextureVisibility(1);
		YPlane->RestrictPlaneToVolumeOn();
		YPlane->SetResliceInterpolateToCubic();  
		YPlane->SetPlaneOrientationToYAxes();
		YPlane->SetSliceIndex(256);
		YPlane->SetWindowLevel(500,128);
		YPlane->DisplayTextOn();

	vtkImagePlaneWidget *ZPlane = vtkImagePlaneWidget::New();	
		ZPlane->SetInput( connector->GetOutput() );
		ZPlane->TextureInterpolateOn();
		ZPlane->SetTextureVisibility(1);
		ZPlane->RestrictPlaneToVolumeOn();
		ZPlane->SetResliceInterpolateToCubic();  
		ZPlane->SetPlaneOrientationToZAxes();
		ZPlane->SetSliceIndex(53);
		ZPlane->SetWindowLevel(500,128);
		ZPlane->DisplayTextOn();
			
	vtkOutlineFilter *outline = vtkOutlineFilter::New();
		outline->SetInput( connector->GetOutput() );
		outline->Update();
		
	vtkPolyDataMapper *outlineMapper = vtkPolyDataMapper::New();
		outlineMapper->SetInput( outline->GetOutput() );
		
	vtkActor *outlineActor = vtkActor::New();
		outlineActor->SetMapper( outlineMapper );			
		outlineActor->GetProperty()->SetColor(1,0,0);
	vtkRenderer *ren = vtkRenderer::New();	
		ren->AddActor( outlineActor );
		
	vtkRenderWindow *renWin = vtkRenderWindow::New();
		renWin->AddRenderer(ren);
		renWin->SetSize(1024, 768);

		
	vtkRenderWindowInteractor *iren = vtkRenderWindowInteractor::New();			
		iren->SetRenderWindow(renWin);
			
  XPlane->SetInteractor( iren );
  XPlane->On();  
  YPlane->SetInteractor( iren );
  YPlane->On();		
  ZPlane->SetInteractor( iren );
  ZPlane->On();
 
  /////////////////////////////////////////////////////////////////////////////
  //cria os planos do leitor vtk
//  vtkImagePlaneWidget *XPlaneVTK = vtkImagePlaneWidget::New();
//    XPlaneVTK->SetInput( image );
//    XPlaneVTK->TextureInterpolateOn();
//    XPlaneVTK->SetTextureVisibility(1);
//    XPlaneVTK->RestrictPlaneToVolumeOn();
//    XPlaneVTK->SetResliceInterpolateToCubic();
//    XPlaneVTK->SetPlaneOrientationToXAxes();
//    XPlaneVTK->SetSliceIndex(256);
//    XPlaneVTK->SetWindowLevel(500,128);
//    XPlaneVTK->DisplayTextOn();
//
//  vtkImagePlaneWidget *YPlaneVTK = vtkImagePlaneWidget::New();
//    YPlaneVTK->SetInput( image );
//    YPlaneVTK->TextureInterpolateOn();
//    YPlaneVTK->SetTextureVisibility(1);
//    YPlaneVTK->RestrictPlaneToVolumeOn();
//    YPlaneVTK->SetResliceInterpolateToCubic();
//    YPlaneVTK->SetPlaneOrientationToYAxes();
//    YPlaneVTK->SetSliceIndex(256);
//    YPlaneVTK->SetWindowLevel(500,128);
//    YPlaneVTK->DisplayTextOn();
//
//  vtkImagePlaneWidget *ZPlaneVTK = vtkImagePlaneWidget::New();
//    ZPlaneVTK->SetInput( image );
//    ZPlaneVTK->TextureInterpolateOn();
//    ZPlaneVTK->SetTextureVisibility(1);
//    ZPlaneVTK->RestrictPlaneToVolumeOn();
//    ZPlaneVTK->SetResliceInterpolateToCubic();
//    ZPlaneVTK->SetPlaneOrientationToZAxes();
//    ZPlaneVTK->SetSliceIndex(53);
//    ZPlaneVTK->SetWindowLevel(500,128);
//    ZPlaneVTK->DisplayTextOn();
//
//  vtkOutlineFilter *outlineVTK = vtkOutlineFilter::New();
//    outlineVTK->SetInput( image );
//    outlineVTK->Update();
//
//  vtkPolyDataMapper *outlineMapperVTK = vtkPolyDataMapper::New();
//    outlineMapperVTK->SetInput( outlineVTK->GetOutput() );
//
//  vtkActor *outlineActorVTK = vtkActor::New();
//    outlineActorVTK->SetMapper( outlineMapperVTK );
//    outlineActorVTK->GetProperty()->SetColor(0,1,0);
////  vtkRenderer *ren2 = vtkRenderer::New();
//    ren->AddActor( outlineActorVTK );
//
////  vtkRenderWindow *renWin = vtkRenderWindow::New();
////    renWin->AddRenderer(ren2);
////    renWin->SetSize(1024, 768);
//
////  ren->SetViewport(0,0,0.5,1);
////  ren2->SetViewport(0.5,0,1,1);
//
////  vtkRenderWindowInteractor *irenVTK = vtkRenderWindowInteractor::New();
////    irenVTK->SetRenderWindow(renWin);
////
////  XPlaneVTK->SetInteractor( irenVTK );
////  YPlaneVTK->SetInteractor( irenVTK );
////  ZPlaneVTK->SetInteractor( irenVTK );
//  XPlaneVTK->SetInteractor( iren );
//  YPlaneVTK->SetInteractor( iren );
//  ZPlaneVTK->SetInteractor( iren );
//
//  XPlaneVTK->On();
//  YPlaneVTK->On();
//  ZPlaneVTK->On();
//
	iren->Start();
////  irenVTK->Start();

	outline->Delete();
	outlineMapper->Delete();
	outlineActor->Delete();	
//  outlineVTK->Delete();
//  outlineMapperVTK->Delete();
//  outlineActorVTK->Delete();
	ren->Delete();
//	ren2->Delete();
	renWin->Delete();
	iren->Delete();
	XPlane->Delete();
	YPlane->Delete();
	ZPlane->Delete();	
//  XPlaneVTK->Delete();
//  YPlaneVTK->Delete();
//  ZPlaneVTK->Delete();
}

//-----------------------------------------------------------------------------
void DICOMReader_ResliceWithPlanesMask(ConnectorType *connector, ConnectorType *connectorMask)
{
  vtkLookupTable *lookupTable = vtkLookupTable::New();
  lookupTable->SetNumberOfTableValues(2);
  lookupTable->SetRange(0.0,1.0);
  lookupTable->SetTableValue( 0, 1.0, 0.0, 0.0, 0.3 ); //label 0 is opaque and red
  lookupTable->SetTableValue( 1, 0.0, 0.0, 0.0, 0.0 ); //label 1 is transparent
  lookupTable->Build();

  vtkImageMapToColors *mapTransparency = vtkImageMapToColors::New();
  mapTransparency->SetLookupTable(lookupTable);
  mapTransparency->PassAlphaToOutputOn();
#if VTK_MAJOR_VERSION <= 5
  mapTransparency->SetInput( connectorMask->GetOutput() );
#else
  mapTransparency->SetInputData( connectorMask->GetOutput() );
#endif



  /////////////////////////////////////////////////////////////////////////////
  //cria os planos do leitor itk
  vtkImagePlaneWidget *XPlane = vtkImagePlaneWidget::New();
    XPlane->SetInput( connector->GetOutput() );
    XPlane->TextureInterpolateOn();
    XPlane->SetTextureVisibility(1);
    XPlane->RestrictPlaneToVolumeOn();
    XPlane->SetResliceInterpolateToLinear();
    XPlane->SetPlaneOrientationToXAxes();
    XPlane->SetSliceIndex(256);
    XPlane->SetWindowLevel(500,128);
    XPlane->DisplayTextOn();

    vtkImagePlaneWidget *XPlaneMask = vtkImagePlaneWidget::New();
    XPlaneMask->SetInput( connectorMask->GetOutput() );
    XPlaneMask->TextureInterpolateOn();
    XPlaneMask->SetTextureVisibility(1);
    XPlaneMask->RestrictPlaneToVolumeOn();
    XPlaneMask->SetResliceInterpolateToLinear();
    XPlaneMask->SetPlaneOrientationToXAxes();
    XPlaneMask->SetSliceIndex(256);
    XPlaneMask->SetWindowLevel(500,128);
    XPlaneMask->SetColorMap( mapTransparency );
    XPlaneMask->DisplayTextOn();








  vtkImagePlaneWidget *YPlane = vtkImagePlaneWidget::New();
    YPlane->SetInput( connector->GetOutput() );
    YPlane->TextureInterpolateOn();
    YPlane->SetTextureVisibility(1);
    YPlane->RestrictPlaneToVolumeOn();
    YPlane->SetResliceInterpolateToCubic();
    YPlane->SetPlaneOrientationToYAxes();
    YPlane->SetSliceIndex(256);
    YPlane->SetWindowLevel(500,128);
    YPlane->DisplayTextOn();

  vtkImagePlaneWidget *ZPlane = vtkImagePlaneWidget::New();
    ZPlane->SetInput( connector->GetOutput() );
    ZPlane->TextureInterpolateOn();
    ZPlane->SetTextureVisibility(1);
    ZPlane->RestrictPlaneToVolumeOn();
    ZPlane->SetResliceInterpolateToCubic();
    ZPlane->SetPlaneOrientationToZAxes();
    ZPlane->SetSliceIndex(53);
    ZPlane->SetWindowLevel(500,128);
    ZPlane->DisplayTextOn();

  vtkOutlineFilter *outline = vtkOutlineFilter::New();
    outline->SetInput( connector->GetOutput() );
    outline->Update();

  vtkPolyDataMapper *outlineMapper = vtkPolyDataMapper::New();
    outlineMapper->SetInput( outline->GetOutput() );

  vtkActor *outlineActor = vtkActor::New();
    outlineActor->SetMapper( outlineMapper );
    outlineActor->GetProperty()->SetColor(1,0,0);
  vtkRenderer *ren = vtkRenderer::New();
    ren->AddActor( outlineActor );

  vtkRenderWindow *renWin = vtkRenderWindow::New();
    renWin->AddRenderer(ren);
    renWin->SetSize(1024, 768);

  vtkRenderWindowInteractor *iren = vtkRenderWindowInteractor::New();
    iren->SetRenderWindow(renWin);

  XPlane->SetInteractor( iren );
  XPlane->On();
  XPlaneMask->SetInteractor( iren );
  XPlaneMask->On();
//  YPlane->SetInteractor( iren );
//  YPlane->On();
//  ZPlane->SetInteractor( iren );
//  ZPlane->On();


  iren->Start();


  outline->Delete();
  outlineMapper->Delete();
  outlineActor->Delete();
  ren->Delete();
  renWin->Delete();
  iren->Delete();
  XPlane->Delete();
  XPlaneMask->Delete();
  YPlane->Delete();
  ZPlane->Delete();
  mapTransparency->Delete();
  lookupTable->Delete();
}

//-----------------------------------------------------------------------------
void DICOMReader_IsoSurface(ConnectorType *connector)
{
  vtkRenderer *aRenderer = vtkRenderer::New();
  vtkRenderWindow *renWin = vtkRenderWindow::New();
    renWin->AddRenderer(aRenderer);
  vtkRenderWindowInteractor *iren = vtkRenderWindowInteractor::New();
    iren->SetRenderWindow(renWin);
	
	
	// An isosurface, or contour value of 500 is known to correspond to
	  // the skin of the patient. Once generated, a vtkPolyDataNormals
	  // filter is is used to create normals for smooth surface shading
	  // during rendering.  The triangle stripper is used to create triangle
	  // strips from the isosurface; these render much faster on may
	  // systems.
	  vtkContourFilter *skinExtractor = vtkContourFilter::New();
	    skinExtractor->SetInput( connector->GetOutput());
	    skinExtractor->SetValue(0, 500);
	  vtkPolyDataNormals *skinNormals = vtkPolyDataNormals::New();
	    skinNormals->SetInputConnection(skinExtractor->GetOutputPort());
	    skinNormals->SetFeatureAngle(60.0);
	  vtkStripper *skinStripper = vtkStripper::New();
	    skinStripper->SetInputConnection(skinNormals->GetOutputPort());
	  vtkPolyDataMapper *skinMapper = vtkPolyDataMapper::New();
	    skinMapper->SetInputConnection(skinStripper->GetOutputPort());
	    skinMapper->ScalarVisibilityOff();
	  vtkActor *skin = vtkActor::New();
	    skin->SetMapper(skinMapper);
	    skin->GetProperty()->SetDiffuseColor(1, .49, .25);
	    skin->GetProperty()->SetSpecular(.3);
	    skin->GetProperty()->SetSpecularPower(20);

	  // An isosurface, or contour value of 1150 is known to correspond to
	  // the skin of the patient. Once generated, a vtkPolyDataNormals
	  // filter is is used to create normals for smooth surface shading
	  // during rendering.  The triangle stripper is used to create triangle
	  // strips from the isosurface; these render much faster on may
	  // systems.
	  vtkContourFilter *boneExtractor = vtkContourFilter::New();
//	    boneExtractor->SetInput( connector->GetOutput());
//	    boneExtractor->SetValue(0, 1150);
	  vtkPolyDataNormals *boneNormals = vtkPolyDataNormals::New();
//	    boneNormals->SetInputConnection(boneExtractor->GetOutputPort());
//	    boneNormals->SetFeatureAngle(60.0);
	  vtkStripper *boneStripper = vtkStripper::New();
//	    boneStripper->SetInputConnection(boneNormals->GetOutputPort());
	  vtkPolyDataMapper *boneMapper = vtkPolyDataMapper::New();
//	    boneMapper->SetInputConnection(boneStripper->GetOutputPort());
//	    boneMapper->ScalarVisibilityOff();
	  vtkActor *bone = vtkActor::New();
//	    bone->SetMapper(boneMapper);
//	    bone->GetProperty()->SetDiffuseColor(1, 1, .9412);

	  // An outline provides context around the data.
	  //
	  vtkOutlineFilter *outlineData = vtkOutlineFilter::New();
	    outlineData->SetInput( connector->GetOutput());
	  vtkPolyDataMapper *mapOutline = vtkPolyDataMapper::New();
	    mapOutline->SetInputConnection(outlineData->GetOutputPort());
	  vtkActor *outline = vtkActor::New();
	    outline->SetMapper(mapOutline);
	    outline->GetProperty()->SetColor(0,0,0);

	  // Now we are creating three orthogonal planes passing through the
	  // volume. Each plane uses a different texture map and therefore has
	  // different coloration.

	  // Start by creatin a black/white lookup table.
	  vtkLookupTable *bwLut = vtkLookupTable::New();
	    bwLut->SetTableRange (0, 2000);
	    bwLut->SetSaturationRange (0, 0);
	    bwLut->SetHueRange (0, 0);
	    bwLut->SetValueRange (0, 1);
	    bwLut->Build(); //effective built

	  // Now create a lookup table that consists of the full hue circle
	  // (from HSV).
	  vtkLookupTable *hueLut = vtkLookupTable::New();
	    hueLut->SetTableRange (0, 2000);
	    hueLut->SetHueRange (0, 1);
	    hueLut->SetSaturationRange (1, 1);
	    hueLut->SetValueRange (1, 1);
	    hueLut->Build(); //effective built

	  // Finally, create a lookup table with a single hue but having a range
	  // in the saturation of the hue.
	  vtkLookupTable *satLut = vtkLookupTable::New();
	    satLut->SetTableRange (0, 2000);
	    satLut->SetHueRange (.6, .6);
	    satLut->SetSaturationRange (0, 1);
	    satLut->SetValueRange (1, 1);
	    satLut->Build(); //effective built

	  // Create the first of the three planes. The filter vtkImageMapToColors
	  // maps the data through the corresponding lookup table created above.  The
	  // vtkImageActor is a type of vtkProp and conveniently displays an image on
	  // a single quadrilateral plane. It does this using texture mapping and as
	  // a result is quite fast. (Note: the input image has to be unsigned char
	  // values, which the vtkImageMapToColors produces.) Note also that by
	  // specifying the DisplayExtent, the pipeline requests data of this extent
	  // and the vtkImageMapToColors only processes a slice of data.
	  vtkImageMapToColors *saggitalColors = vtkImageMapToColors::New();
	    saggitalColors->SetInput( connector->GetOutput());
	    saggitalColors->SetLookupTable(bwLut);
	  vtkImageActor *saggital = vtkImageActor::New();
	    saggital->SetInput(saggitalColors->GetOutput());
	    saggital->SetDisplayExtent(32,32, 0,63, 0,92);

	  // Create the second (axial) plane of the three planes. We use the
	  // same approach as before except that the extent differs.
	  vtkImageMapToColors *axialColors = vtkImageMapToColors::New();
	    axialColors->SetInput( connector->GetOutput());
	    axialColors->SetLookupTable(hueLut);
	  vtkImageActor *axial = vtkImageActor::New();
	    axial->SetInput(axialColors->GetOutput());
	    axial->SetDisplayExtent(0,63, 0,63, 46,46);

	  // Create the third (coronal) plane of the three planes. We use 
	  // the same approach as before except that the extent differs.
	  vtkImageMapToColors *coronalColors = vtkImageMapToColors::New();
	    coronalColors->SetInput( connector->GetOutput());
	    coronalColors->SetLookupTable(satLut);
	  vtkImageActor *coronal = vtkImageActor::New();
	    coronal->SetInput(coronalColors->GetOutput());
	    coronal->SetDisplayExtent(0,63, 32,32, 0,92);

	  // It is convenient to create an initial view of the data. The
	  // FocalPoint and Position form a vector direction. Later on
	  // (ResetCamera() method) this vector is used to position the camera
	  // to look at the data in this direction.
	  vtkCamera *aCamera = vtkCamera::New();
	    aCamera->SetViewUp (0, 0, -1);
	    aCamera->SetPosition (0, 1, 0);
	    aCamera->SetFocalPoint (0, 0, 0);
	    aCamera->ComputeViewPlaneNormal();

	  // Actors are added to the renderer. 
	  aRenderer->AddActor(outline);
	  aRenderer->AddActor(saggital);
	  aRenderer->AddActor(axial);
	  aRenderer->AddActor(coronal);
//	  aRenderer->AddActor(axial);
//	  aRenderer->AddActor(coronal);
	  aRenderer->AddActor(skin);
	  aRenderer->AddActor(bone);

	  // Turn off bone for this example.
	  bone->VisibilityOff();
	  skin->VisibilityOn();

	  // Set skin to semi-transparent.
	  skin->GetProperty()->SetOpacity(0.5);

	  // An initial camera view is created.  The Dolly() method moves 
	  // the camera towards the FocalPoint, thereby enlarging the image.
	  aRenderer->SetActiveCamera(aCamera);
	  aRenderer->Render();
	  aRenderer->ResetCamera ();
	  aCamera->Dolly(1.5);

	  // Set a background color for the renderer and set the size of the
	  // render window (expressed in pixels).
//	  aRenderer->SetBackground(1,1,1);
	  aRenderer->SetBackground(0.2,0.2,0.2);
	  renWin->SetSize(1027, 768);

	  // Note that when camera movement occurs (as it does in the Dolly()
	  // method), the clipping planes often need adjusting. Clipping planes
	  // consist of two planes: near and far along the view direction. The 
	  // near plane clips out objects in front of the plane; the far plane
	  // clips out objects behind the plane. This way only what is drawn
	  // between the planes is actually rendered.
	  aRenderer->ResetCameraClippingRange ();

	  // interact with data
	  iren->Initialize();
	  iren->Start(); 

	  // It is important to delete all objects created previously to prevent
	  // memory leaks. In this case, since the program is on its way to
	  // exiting, it is not so important. But in applications it is
	  // essential.
	  skinExtractor->Delete();
	  skinNormals->Delete();
	  skinStripper->Delete();
	  skinMapper->Delete();
	  skin->Delete();
	  boneExtractor->Delete();
	  boneNormals->Delete();
	  boneStripper->Delete();
	  boneMapper->Delete();
	  bone->Delete();
	  outlineData->Delete();
	  mapOutline->Delete();
	  outline->Delete();
	  bwLut->Delete();
	  hueLut->Delete();
	  satLut->Delete();
	  saggitalColors->Delete();
	  saggital->Delete();
	  axialColors->Delete();
	  axial->Delete();
	  coronalColors->Delete();
	  coronal->Delete();
	  aCamera->Delete();
	  aRenderer->Delete();
	  renWin->Delete();
	  iren->Delete();
}


//-----------------------------------------------------------------------------
int WriteDICOMSlice(ImageType2D *image, string outputFileName, ImageIOType *dicomIO)
{
//  typedef itk::MinimumMaximumImageCalculator <ImageType2D> ImageCalculatorFilterType;
//
//  ImageCalculatorFilterType::Pointer imageCalculatorFilter = ImageCalculatorFilterType::New ();
//  imageCalculatorFilter->SetImage( image );
//  imageCalculatorFilter->Compute();
//
//
//
//  typedef itk::Image< char, 2 > OutputImageType;
//
//  typedef itk::RescaleIntensityImageFilter < ImageType2D, OutputImageType > RescaleFilterType;
//  RescaleFilterType::Pointer rescaleFilter = RescaleFilterType::New();
//  rescaleFilter->SetInput( image );
//  rescaleFilter->SetOutputMinimum( 255 );
//  rescaleFilter->SetOutputMaximum( -255 );
//  rescaleFilter->Update();


//  typedef itk::GDCMImageIO  ImageIOType;
//  ImageIOType::Pointer dicomIO;
//  dicomIO = ImageIOType::New();

//  typedef itk::ImageFileWriter< OutputImageType >  WriterType;
  typedef itk::ImageFileWriter< ImageType2D >  WriterType;
  WriterType::Pointer itkWriter;
  itkWriter = WriterType::New();
  itkWriter->SetFileName( outputFileName.c_str() );
//  itkWriter->SetInput( rescaleFilter->GetOutput() );
  itkWriter->SetInput( image );
  itkWriter->SetImageIO( dicomIO );
  itkWriter->UseInputMetaDataDictionaryOff();

  try
    {
    itkWriter->Update();
    }
  catch( itk::ExceptionObject & excp )
    {
    cout << "Writer Error!!!! " << excp.what()  <<endl;
    return 1;
    }

  return 0;
}

























//		ORIGINAL CODE
//==================================================================



	
//#include "itkImage.h"
//#include "itkImageFileReader.h"
//#include "itkImageToVTKImageFilter.h"
//#include "vtkImageViewer.h"
//#include "vtkRenderWindowInteractor.h"	
//	
//int main( int argc, char **argv ) {
//
//  typedef itk::Image< unsigned short, 2 >    	     ImageType;
//  typedef itk::ImageFileReader<ImageType> 	     ReaderType;
//  typedef itk::ImageToVTKImageFilter<	ImageType>   FilterType;
//
//  ReaderType::Pointer reader = ReaderType::New();
//  FilterType::Pointer connector = FilterType::New();
//  reader->SetFileName( argv[1] );
//  connector->SetInput( reader->GetOutput() );
//
//  vtkImageViewer * viewer = vtkImageViewer::New();
//  
//  vtkRenderWindowInteractor * renderWindowInteractor = 
//                                 vtkRenderWindowInteractor::New();
//
//  viewer->SetupInteractor( renderWindowInteractor );
//  viewer->SetInput( connector->GetOutput() );
//  viewer->Render();
//  viewer->SetColorWindow( 255 );
//  viewer->SetColorLevel( 128 );
//  renderWindowInteractor->Start();
//    
//  return 0;
//}
