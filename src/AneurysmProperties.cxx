#include "vtkRenderWindowInteractor.h"
#include "vtkRenderWindow.h"
#include "vtkRenderer.h"

#include "vtkSmartPointer.h"
#include "vtkPolyDataReader.h"
#include "vtkPolyData.h"
#include "vtkDataSetMapper.h"
#include "vtkActor.h"
#include "vtkProperty.h"
#include "vtkIdList.h"
#include "vtkCellData.h"
#include "vtkMath.h"
#include "vtkDoubleArray.h"
#include "vtkPolyDataNormals.h"
#include "vtkCell.h"
#include "vtkLineSource.h"
#include "vtkSphereSource.h"
#include "vtkPlane.h"
#include "vtkCutter.h"
#include "vtkPointData.h"
#include "vtkTriangle.h"
#include "vtkHull.h"
#include "vtkCleanPolyData.h"
#include "vtkTriangleFilter.h"
#include "vtkDataSetSurfaceFilter.h"
#include "vtkMassProperties.h"
#include "vtkCamera.h"
#include <vtkPolyDataCollection.h>


#include <list>
using namespace std;


class AverageDiameterOfParentArtery
{
public:
  //--------------------------------------------------------------------------------------------------
  AverageDiameterOfParentArtery()
  {
    NewPoint[0] = 0.0;
    NewPoint[1] = 0.0;
    NewPoint[2] = 0.0;

    NewRadius = -1.0;

    ProximalId = 0;
    PtId1 = -1;
    NeighborOfP1 = -1;

    DistalId = vtkSmartPointer<vtkIdList>::New();
    Centerline = vtkSmartPointer<vtkPolyData>::New();
  }

  //--------------------------------------------------------------------------------------------------
  ~AverageDiameterOfParentArtery(){}

  //--------------------------------------------------------------------------------------------------
  void SetInput( vtkSmartPointer<vtkPolyData> input )
  {
    Centerline->DeepCopy( input );
  }

  //--------------------------------------------------------------------------------------------------
  void SetProximalPoint( int pointId )
  {
    ProximalId = pointId;
  }

  //--------------------------------------------------------------------------------------------------
  void SetP1( int pointId )
  {
    PtId1 = pointId;
  }

  //--------------------------------------------------------------------------------------------------
  void GetP1( double p1[3] )
  {
    Centerline->GetPoint( PtId1, p1 );
  }

  //--------------------------------------------------------------------------------------------------
  int GetP1( )
  {
    return PtId1;
  }

  //--------------------------------------------------------------------------------------------------
  void SetDistalPoints( vtkSmartPointer<vtkIdList> distal )
  {
    DistalId->DeepCopy( distal );
  }

  //--------------------------------------------------------------------------------------------------
  double GetAverageDiameter()
  {
    return AverageDiameter;
  }

  //--------------------------------------------------------------------------------------------------
  void GetP2( double newPoint[3], double *newRadius )
  {
    newPoint[0] = NewPoint[0];
    newPoint[1] = NewPoint[1];
    newPoint[2] = NewPoint[2];

    *newRadius = NewRadius;
  }

  //--------------------------------------------------------------------------------------------------
  int GetNeighborOfP1()
  {
    return ( *VisitedPoints.rbegin() );
  }

  //--------------------------------------------------------------------------------------------------
  void Update()
  {
    FindPointNeighborOfP1( ProximalId );

    ComputeAverageDiameter();
  }

  //--------------------------------------------------------------------------------------------------
  list<int>* GetVisitedPoint()
  {
   return &VisitedPoints;
  }

  //--------------------------------------------------------------------------------------------------
  int GetProximalPoint()
  {
    return ProximalId;
  }

  //--------------------------------------------------------------------------------------------------
  vtkSmartPointer<vtkIdList> GetDistalPoints()
  {
    return DistalId;
  }

  //--------------------------------------------------------------------------------------------------
  vtkSmartPointer<vtkPolyData> GetInput()
  {
    return Centerline;
  }






protected:

  // Verifica se o id encontra-se na lista de pontos visitados
  //--------------------------------------------------------------------------------------------------
  bool WasVisited(int id)
  {
    list<int>::iterator it;

    bool ret = false;

    for(it = VisitedPoints.begin(); it != VisitedPoints.end(); ++it)
      {
      if( (*it) == id )
        {
  //      cout << "(*it): " << (*it) << "\t id: " << id <<endl;
        ret = true;
        }
      }

    return ret;
  }

  // Recupera um vtkIdList com os vizinhos do ponto ptId
  //--------------------------------------------------------------------------------------------------
  vtkSmartPointer<vtkIdList> GetPointNeighbors( int ptId )
  {
    vtkSmartPointer<vtkIdList> neighbors = vtkSmartPointer<vtkIdList>::New();

    vtkSmartPointer<vtkIdList> cellIds = vtkSmartPointer<vtkIdList>::New();

    Centerline->GetPointCells( ptId, cellIds );

    for(int i=0; i<cellIds->GetNumberOfIds(); ++i)
      {
      vtkSmartPointer<vtkIdList> points = vtkSmartPointer<vtkIdList>::New();
      Centerline->GetCellPoints( cellIds->GetId(i), points );

      if( points->GetNumberOfIds() > 2 )
        cout << "Error! Cell " << cellIds->GetId(i) << " with to many points." <<endl;


      for(int j=0; j < points->GetNumberOfIds(); ++j)
        {
        if( points->GetId(j) != ptId )
          {
  //        cout << points->GetId(j) << " é vizinho de " << ptId <<endl;
          neighbors->InsertNextId( points->GetId(j) );
          }
        }
      }


    return neighbors;
  }

  // Exclui os ids que foram visitados
  //--------------------------------------------------------------------------------------------------
  vtkSmartPointer<vtkIdList> GetNeighborsNotVisited( vtkSmartPointer<vtkIdList> neighbors )
  {
    vtkSmartPointer<vtkIdList> notVisited = vtkSmartPointer<vtkIdList>::New();

    for(int i=0; i < neighbors->GetNumberOfIds(); ++i)
      {
      int id = neighbors->GetId(i);

      if( !WasVisited( id ) ) // se o ponto ainda não foi visitado
        notVisited->InsertNextId( id );
      }

    return notVisited;
  }

  // Utiliza a equação paramétrica da reta r: xn = x0 + (x1 - x0)*t
  //                                          yn = y0 + (y1 - y0)*t
  //                                          zn = z0 + (z1 - z0)*t
  //
  // em conjunto a distância eucliana d² = (xn - x0)² + (yn - y0)² + (zn - z0)²
  // para interpolar point e radius.
  // Substituir xn, yn e zn da distância pelas equações das retas.
  // d² = (x1 - x0)² * t² + (y1 - y0)² * t² + (z1 - z0)² * t²
  // Isolando t e considerando apenas o valor positivo (vetor vai de p0 para pn para p1) encontra-se a equação
  // t = Distance2BetweenPoints(p0, p1)/ sqrt( (x1 - x0)² + (y1 - y0)² + (z1 - z0)² );
  //--------------------------------------------------------------------------------------------------
  void InterpolateNewPointAndRadius( double *point, double *radius )
  {
    double p1[3], p2[3];
    double r = Centerline->GetPointData()->GetArray(0)->GetTuple1( PtId1 );

    int lastElem = VisitedPoints.size() - 1;
    int j = PtId1;

    double distSum = 0.0;

    do
      {
      Centerline->GetPoint( j, p1 );
      Centerline->GetPoint( lastElem, p2 );

      double d = sqrt(vtkMath::Distance2BetweenPoints( p1, p2 ));

      // se distSum ainda não ultrapassou 2*r, continua percorrendo o segmento no sentido _ptId_1 -> Proximal e armazendo o distância total.
      if( (2*r) >= (distSum + d) )
        {
        distSum += d;

        --lastElem;
        --j;
        }
      else // se ultrapassou, interpola-se nova coordenada e raio
        {
        // cálculo do escalar t
        double t = d / sqrt( pow(p2[0]-p1[0], 2) + pow(p2[1]-p1[2], 2) + pow(p2[2]-p1[2], 2));

        // tendo p1, p2 e t, pode-se interpolar ponto e raio
        double x_n = p1[0] + (p2[0]-p1[0]) * t;
        double y_n = p1[1] + (p2[1]-p1[1]) * t;
        double z_n = p1[2] + (p2[2]-p1[2]) * t;

        point[0] = x_n;
        point[1] = y_n;
        point[2] = z_n;


        double r1 = Centerline->GetPointData()->GetArray(0)->GetTuple1( j );
        double r2 = Centerline->GetPointData()->GetArray(0)->GetTuple1( lastElem );
        *radius = r1 + (r2-r1) * t;

  //      cout << "j: "<< j <<endl;
  //      cout << "lastElem: "<< lastElem <<endl;
  //      cout << "p1: " << p1[0] << ", " << p1[1] << ", " << p1[2] << "\t radius: " << r1 << endl;
  //      cout << "p2: " << p2[0] << ", " << p2[1] << ", " << p2[2] << "\t radius: " << r2 << endl;
  //      cout << "point: " << point[0] << ", " << point[1] << ", " << point[2] << "\t radius: " << *radius << endl;

        distSum += d;
        }

      }while( (2*r) > distSum );
  }

  // Calcula o diâmetro médio usando a fórmula DM = 2*R1 + 2*R2)/2, onde:
  // R1 = raio do ponto _ptId_1; ponto informado inicialmente pelo usuário
  // R2 = raio do ponto newPoint = newRadius; calculados pelo método InterpolateNewPointAndRadius
  //--------------------------------------------------------------------------------------------------
  void ComputeAverageDiameter()
  {
    double p1[3], p2[3];
    double r1 = Centerline->GetPointData()->GetArray(0)->GetTuple1( PtId1 );
    double r2 = 0.0;

    Centerline->GetPoint( PtId1, p1 );
    InterpolateNewPointAndRadius( p2, &r2 );


    NewPoint[0] = p2[0];
    NewPoint[1] = p2[1];
    NewPoint[2] = p2[2];

    NewRadius = r2;

    AverageDiameter = (2*r1 + 2*r2)/2;
  }

  // Econtra o id do ponto vizinho a P1 no sentido P1 -> Proximal
  //--------------------------------------------------------------------------------------------------
  void FindPointNeighborOfP1( int ptInicial )
  {
    int p1;

    p1 = ptInicial; //ponto de busca inicial deverá ser passado o proximal.


    if(p1 == PtId1)
      return;


    // marca o ponto como visitado.
    VisitedPoints.push_back( p1 );


    NeighborOfP1 = p1;


    // recupera vizinhos de p1
    vtkSmartPointer<vtkIdList> neighbors = GetPointNeighbors( p1 );

    // lista dos vizinhos ainda não visitados
    vtkSmartPointer<vtkIdList> neighborsNotVisited = GetNeighborsNotVisited( neighbors );

    //loop recursivo para os vizinhos ainda não visitados
    for(int i=0; i < neighborsNotVisited->GetNumberOfIds(); ++i)
      {
      FindPointNeighborOfP1( neighborsNotVisited->GetId(i) );
      }
  }


  vtkSmartPointer<vtkPolyData> Centerline;

  list<int> VisitedPoints;

  double AverageDiameter;
  double NewPoint[3];
  double NewRadius;

  int ProximalId;                           // a linha de centro possui somente uma entrada de fluxo (ponto proximal).
  int PtId1;                                // ponto P1 selecionado pelo usuário, é assumido que o mesmo foi posicionado antes do aneurisma.
  int NeighborOfP1;
  vtkSmartPointer<vtkIdList> DistalId;      // a linha de centro pode possuir várias saídas de fluxo (pontos distais).
};




/* O pescoço deve ter os pontos de suas células ordenados de tal modo que cada normal irá apontar para o domo do aneurisma ("regra da mão direita");
 * Qualquer array nos inputs será eliminado.
 */
class AneurysmPropertiesFilter
{
public:

  //--------------------------------------------------------------------------------------------------
  AneurysmPropertiesFilter()
  {
    this->Neck = vtkSmartPointer<vtkPolyData>::New();
    this->Dome = vtkSmartPointer<vtkPolyData>::New();
    this->CutterPlane = vtkSmartPointer<vtkPolyData>::New();
    this->ConvexHull = vtkSmartPointer<vtkPolyData>::New();

    this->DiameterOfParentArtery = new AverageDiameterOfParentArtery;

    this->AneurysmHeight = 0.0;
    this->HigherAneurysmPoint[0] = 0.0;
    this->HigherAneurysmPoint[1] = 0.0;
    this->HigherAneurysmPoint[2] = 0.0;
    this->NeckCellId = -1;
    this->AneurysmAngle = 0;
    this->VesselAngle = 0;
    this->VerticePointOfVesselAngle[0] = 0.0;
    this->VerticePointOfVesselAngle[1] = 0.0;
    this->VerticePointOfVesselAngle[2] = 0.0;
    this->NeckArea = 0.0;
    this->AverageNeckDiameter = 0.0;
    this->AspectRatio = 0.0;
    this->RecursiveSpherePlanes = 4;
    this->VolumeConvexHull = 0.0;
    this->AreaConvexHull = 0.0;
    this->EllipticityIndex = 0.0;
    this->averageDiameterOfParentArtery = 0.0;
    this->AreaAneurysm = 0.0;
    this->VolumeAneurysm = 0.0;
    this->UndulationIndex = 0.0;
    this->NonsphericityIndex = 0.0;
    this->LongitudeAneurysm = 0.0;
    this->SizeRatio = 0.0;
    this->AneurysmDiameterMaximum = 0.0;
    this->BottleNeckEffect = 0.0;
  }

  //--------------------------------------------------------------------------------------------------
  ~AneurysmPropertiesFilter()
  {
    delete this->DiameterOfParentArtery;
  }

  //--------------------------------------------------------------------------------------------------
  void SetInputAneurysmNeck( vtkSmartPointer<vtkPolyData> neck )
  {
    this->Neck->DeepCopy( neck );
    this->Neck->Update();
  }

  //--------------------------------------------------------------------------------------------------
  vtkSmartPointer<vtkPolyData> GetInputAneurysmNeck( )
  {
    return this->Neck;
  }

  //--------------------------------------------------------------------------------------------------
  void SetInputAneurysmDome( vtkSmartPointer<vtkPolyData> dome )
  {
    this->Dome->DeepCopy( dome );
    this->Dome->Update();
  }

  //--------------------------------------------------------------------------------------------------
  vtkSmartPointer<vtkPolyData> GetInputAneurysmDome( )
  {
    return this->Dome;
  }

  //--------------------------------------------------------------------------------------------------
  double GetAneurysmHeight()
  {
    return this->AneurysmHeight;
  }

  //--------------------------------------------------------------------------------------------------
  double * GetHigherAneurysmPoint()
  {
    return this->HigherAneurysmPoint;
  }

  //--------------------------------------------------------------------------------------------------
  double GetAneurysmAngle()
  {
    return this->AneurysmAngle;
  }

  //--------------------------------------------------------------------------------------------------
  int GetNeckCellId()
  {
    return this->NeckCellId;
  }

  //--------------------------------------------------------------------------------------------------
  void Update( )
  {
    //removendo arrays não utilizados
    for(int i=0; i < this->Neck->GetCellData()->GetNumberOfArrays(); ++i)
      this->Neck->GetCellData()->RemoveArray( i );

    for(int i=0; i < this->Dome->GetCellData()->GetNumberOfArrays(); ++i)
      this->Dome->GetCellData()->RemoveArray( i );



    vtkSmartPointer<vtkMassProperties> mp = vtkSmartPointer<vtkMassProperties>::New();
    mp->SetInput( this->Dome );
    this->VolumeAneurysm = mp->GetVolume();
    this->AreaAneurysm = mp->GetSurfaceArea();


    //ATENÇÃO NA ORDEM DAS CHAMADAS ABAIXO (EXISTE DEPENDÊNCIA ENTRE ELAS)

    this->ComputeConvexHull();

    this->ComputeUndulationIndex();

    this->ComputeEllipticityIndex();

    this->ComputeNonsphericityIndex();

    this->ComputeLongitudeAneurysm();

    this->ComputeNeckArea();

    this->ComputeAverageNeckDiameter();

    this->ComputeAverageDiameterOfParentArtery();

    this->ComputeSizeRatio();

    this->ComputeNormals( this->Neck );

    this->FindNeckCellWithMaxDistanceToDome( &this->NeckCellId, &this->AneurysmHeight, this->HigherAneurysmPoint );

    this->ComputeAspectRatio();

    this->ComputeNeckPoints();

    this->ComputeInclinationAngle( this->NeckCellId, this->HigherAneurysmPoint );

    this->ComputeVesselAngle();

    this->ComputeAneurysmDiameterMaximum();

    this->ComputeBottleNeckEffect();
  }

  //--------------------------------------------------------------------------------------------------
  void ComputeExternalPoint( double distance, double barycenter[3], double normal[3], double *externalPoint )
  {
    double p[3];

    p[0] = normal[0] + barycenter[0];
    p[1] = normal[1] + barycenter[1];
    p[2] = normal[2] + barycenter[2];


    //forçando a localização de um ponto fora da bounding box
    double d = sqrt(vtkMath::Distance2BetweenPoints( barycenter, p ));
    double t = (distance/d) * 1.5;

    externalPoint[0] = barycenter[0] + t * ( p[0] - barycenter[0]);
    externalPoint[1] = barycenter[1] + t * ( p[1] - barycenter[1]);
    externalPoint[2] = barycenter[2] + t * ( p[2] - barycenter[2]);
  }

  //--------------------------------------------------------------------------------------------------
  vtkSmartPointer<vtkPolyData> GetCutterPlane()
  {
    return this->CutterPlane;
  }

  //--------------------------------------------------------------------------------------------------
  double * GetNeckPoint1()
  {
    return this->NeckPointP1;
  }

  //--------------------------------------------------------------------------------------------------
  double * GetNeckPoint2()
  {
    return this->NeckPointP2;
  }

  //--------------------------------------------------------------------------------------------------
  double GetVesselAngle()
  {
    return this->VesselAngle;
  }

  //--------------------------------------------------------------------------------------------------
  double * GetAverageDiameterP1()
  {
    return this->AverageDiameterP1;
  }

  //--------------------------------------------------------------------------------------------------
  double * GetAverageDiameterP2()
  {
    return this->AverageDiameterP2;
  }

  //--------------------------------------------------------------------------------------------------
  double * GetVerticePointOfVesselAngle()
  {
    return this->VerticePointOfVesselAngle;
  }

  //--------------------------------------------------------------------------------------------------
  void SetCenterline( vtkSmartPointer<vtkPolyData> centerline )
  {
    this->DiameterOfParentArtery->SetInput( centerline );
  }

  //--------------------------------------------------------------------------------------------------
  void SetProximalPoint( int proximalPointId )
  {
    this->DiameterOfParentArtery->SetProximalPoint( proximalPointId );
  }

  //--------------------------------------------------------------------------------------------------
  void SetDistalPoints( vtkSmartPointer<vtkIdList> distalIds )
  {
    this->DiameterOfParentArtery->SetDistalPoints( distalIds );
  }

  //--------------------------------------------------------------------------------------------------
  void SetUserSelectedPoint( int ptId )
  {
    this->DiameterOfParentArtery->SetP1( ptId );
  }

  //--------------------------------------------------------------------------------------------------
  double GetNeckArea()
  {
    return this->NeckArea;
  }

  //--------------------------------------------------------------------------------------------------
  double GetAverageNeckDiameter()
  {
    return this->AverageNeckDiameter;
  }

  //--------------------------------------------------------------------------------------------------
  double GetAspectRatio()
  {
    return this->AspectRatio;
  }

  //--------------------------------------------------------------------------------------------------
  vtkSmartPointer<vtkPolyData> GetConvexHull()
  {
    return this->ConvexHull;
  }

  //--------------------------------------------------------------------------------------------------
  double GetVolumeConvexHull()
  {
    return this->VolumeConvexHull;
  }

  //--------------------------------------------------------------------------------------------------
  double GetAreaConvexHull()
  {
    return this->AreaConvexHull;
  }

  //--------------------------------------------------------------------------------------------------
  double GetEllipticityIndex()
  {
    return this->EllipticityIndex;
  }

  //--------------------------------------------------------------------------------------------------
  double GetAverageDiameterOfParentArtery()
  {
    return this->averageDiameterOfParentArtery;
  }

  //---------------------------------------------------------------------------------------------
  double GetVolumeAneurysm()
  {
    return this->VolumeAneurysm;
  }

  //---------------------------------------------------------------------------------------------
  double GetAreaAneurysm()
  {
    return this->AreaAneurysm;
  }

  //---------------------------------------------------------------------------------------------
  double GetUndulationIndex()
  {
    return this->UndulationIndex;
  }

  //---------------------------------------------------------------------------------------------
  double GetNonsphericityIndex()
  {
    return this->NonsphericityIndex;
  }

  //---------------------------------------------------------------------------------------------
  double GetLongitudeAneurysm()
  {
    return this->LongitudeAneurysm;
  }

  //---------------------------------------------------------------------------------------------
  double GetSizeRatio()
  {
    return this->SizeRatio;
  }

  //---------------------------------------------------------------------------------------------
  double GetAneurysmDiameterMaximum()
  {
    return this->AneurysmDiameterMaximum;
  }

  //---------------------------------------------------------------------------------------------
  double GetBottleNeckEffect()
  {
    return this->BottleNeckEffect;
  }



  //para debug

  //---------------------------------------------------------------------------------------------
  vtkSmartPointer<vtkPolyDataCollection> GetCutterCollection()
  {
    return this->collection;
  }

  AverageDiameterOfParentArtery *GetAverageDiameterOfParentArteryFilter()
  {
    return this->DiameterOfParentArtery;
  }


protected:

  //--------------------------------------------------------------------------------------------------
  double ComputeEscalarProduct( double *u, double *v )
  {
  return (u[0] * v[0]) + (u[1] * v[1]) + (u[2] * v[2]);
  }

  //--------------------------------------------------------------------------------------------------
  void ComputeInclinationAngle( int neckCellId, double *heightPoint )
  {
  double userPoint[3], u[3], v[3], origin[3], e;

  this->Neck->GetCellData()->GetArray("Barycenters")->GetTuple( neckCellId, origin );
//  this->Neck->GetPoint( neckCellId, origin );

  double d1 = sqrt(vtkMath::Distance2BetweenPoints( this->AverageDiameterP1, this->NeckPointP1 ));
  double d2 = sqrt(vtkMath::Distance2BetweenPoints( this->AverageDiameterP1, this->NeckPointP2 ));

  if( d1 < d2 )
    {
    userPoint[0] = this->NeckPointP1[0];
    userPoint[1] = this->NeckPointP1[1];
    userPoint[2] = this->NeckPointP1[2];
    }
  else if( d2 < d1 )
    {
    userPoint[0] = this->NeckPointP2[0];
    userPoint[1] = this->NeckPointP2[1];
    userPoint[2] = this->NeckPointP2[2];
    }
  else if( d2 == d1 )
    {
    cout << "void ComputeInclinationAngle >> Check NeckPoints!!!!" <<endl;
    }


  u[0] = userPoint[0] - origin[0];
  u[1] = userPoint[1] - origin[1];
  u[2] = userPoint[2] - origin[2];

  v[0] = heightPoint[0] - origin[0];
  v[1] = heightPoint[1] - origin[1];
  v[2] = heightPoint[2] - origin[2];

  this->ComputeAngleBetweenTwoVectors( u, v , &e);

  this->AneurysmAngle = vtkMath::DegreesFromRadians( e );

//  cout << "\n" <<endl;
//  cout << "b: " << origin[0] << ", " << origin[1] << ", " << origin[2] <<endl;
//  cout << "s: "   << userPoint[0] << ", " << userPoint[1] << ", " << userPoint[2] <<endl;
//  cout << "i: " << heightPoint[0] << ", " << heightPoint[1] << ", " << heightPoint[2] <<endl;
//  cout << "u: " << u[0] << ", " << u[1] << ", " << u[2] <<endl;
//  cout << "v: " << v[0] << ", " << v[1] << ", " << v[2] <<endl;
//  cout << "u.v: " << prodEscalar <<endl;
//  cout << "|u|: " << norm_u <<endl;
//  cout << "|v|: " << norm_v <<endl;
//  cout << "d: "<< d << endl;
//  cout << "e: " << e <<endl;
//  cout << "this->AneurysmAngle: " << this->AneurysmAngle <<endl;
//  cout << "\n\n" <<endl;
  }

  //--------------------------------------------------------------------------------------------------
  void ComputeBarycenter( vtkSmartPointer<vtkCell> cell, double *barycenter )
  {
    if( cell->GetCellType() != VTK_TRIANGLE )
      return;

    vtkPoints *points;
    double p0[3], p1[3], p2[3];

    points = cell->GetPoints();

    points->GetPoint(0, p0);
    points->GetPoint(1, p1);
    points->GetPoint(2, p2);

    barycenter[0] = (p0[0] + p1[0] + p2[0])/3;
    barycenter[1] = (p0[1] + p1[1] + p2[1])/3;
    barycenter[2] = (p0[2] + p1[2] + p2[2])/3;
  }

  //--------------------------------------------------------------------------------------------------
  void ComputeCellNormal( vtkSmartPointer<vtkCell> cell, double barycenter[3], double *normal )
  {
    if( cell->GetCellType() != VTK_TRIANGLE )
      return;

    vtkPoints *points;
    double p1[3], p2[3], u[3], v[3];

    points = cell->GetPoints();

    points->GetPoint(0, p1);
    points->GetPoint(1, p2);

    u[0] = p1[0] - barycenter[0];
    u[1] = p1[1] - barycenter[1];
    u[2] = p1[2] - barycenter[2];

    v[0] = p2[0] - barycenter[0];
    v[1] = p2[1] - barycenter[1];
    v[2] = p2[2] - barycenter[2];

    this->ComputeVetorialProduct(u, v, normal);
  }

  //--------------------------------------------------------------------------------------------------
  double ComputeMaxBoundingDistance( double boudingBox[6] )
  {
  //  cout << "Xmin: " << boudingBox[0] << "\tXmax: " << boudingBox[1] << "\t" << boudingBox[1] - boudingBox[0]<< endl;
  //  cout << "Ymin: " << boudingBox[2] << "\tYmax: " << boudingBox[3] << "\t" << boudingBox[3] - boudingBox[2]<< endl;
  //  cout << "Zmin: " << boudingBox[4] << "\tZmax: " << boudingBox[5] << "\t" << boudingBox[5] - boudingBox[4]<< endl;

    double d = 0;
    double tmp;


    tmp = boudingBox[1] - boudingBox[0];  // Xmax - Xmin
    if( tmp > d )
      d = tmp;


    tmp = boudingBox[3] - boudingBox[2];  // Ymax - Ymin
    if( tmp > d )
      d = tmp;

    tmp = boudingBox[5] - boudingBox[4];  // Zmax - Zmin
    if( tmp > d )
      d = tmp;


    return d;
  }

  //--------------------------------------------------------------------------------------------------
  void FindNeckCellWithMaxDistanceToDome( int *neckCellId, double *height, double *higherAneurysmPoint )
  {
    vtkSmartPointer<vtkCell> neck_cell, dome_cell;

    *height = 0.0;

    double tolerance = 0.0001;
    double t; // Parametric coordinate of intersection (0 (corresponding to p1) to 1 (corresponding to p2))
    double pcoords[3];
    double x[3];
    int subId;

    double boudingBox[6];
    this->Dome->GetBounds( boudingBox );
    double boudingDistance = ComputeMaxBoundingDistance( boudingBox );

//    cout << "boudingDistance: " << boudingDistance <<endl;


    for(int i=0; i < this->Neck->GetNumberOfCells(); ++i)
      {
      double barycenter[3], normal[3], externalPoint[3];
      double tmpDistance;

      this->Neck->GetCellData()->GetArray("Normals")->GetTuple( i, normal );

      neck_cell = this->Neck->GetCell( i );

      this->Neck->GetCellData()->GetArray("Barycenters")->GetTuple( i, barycenter );

      this->ComputeExternalPoint( boudingDistance, barycenter, normal, externalPoint );


      for(int j=0; j < this->Dome->GetNumberOfCells(); ++j)
        {
        dome_cell = this->Dome->GetCell( j );

        vtkIdType iD = dome_cell->IntersectWithLine( barycenter, externalPoint, tolerance, t, x, pcoords, subId );

        if(iD)
          {
          tmpDistance = sqrt(vtkMath::Distance2BetweenPoints( barycenter, x ));

          if( tmpDistance > *height )
            {
            higherAneurysmPoint[0] = x[0];
            higherAneurysmPoint[1] = x[1];
            higherAneurysmPoint[2] = x[2];
            *height = tmpDistance;
            *neckCellId = i;
            }
          }
        }
      }
  }

  //--------------------------------------------------------------------------------------------------
  void ComputeNormals( vtkSmartPointer<vtkPolyData> neck )
  {
    vtkSmartPointer<vtkDoubleArray> normals = vtkSmartPointer<vtkDoubleArray>::New();
    normals->SetName("Normals");
    normals->SetNumberOfComponents(3);


    vtkSmartPointer<vtkDoubleArray> barycenter = vtkSmartPointer<vtkDoubleArray>::New();
    barycenter->SetName("Barycenters");
    barycenter->SetNumberOfComponents(3);

    vtkSmartPointer<vtkCell> cell;

    for(int i=0; i < neck->GetNumberOfCells(); ++i)
      {
      double n[3], c[3];

      cell = neck->GetCell( i );

      ComputeBarycenter( cell, c );

      ComputeCellNormal( cell, c, n );

      normals->InsertNextTuple( n );
      barycenter->InsertNextTuple( c );
      }


    neck->GetCellData()->AddArray( barycenter );
    neck->GetCellData()->AddArray( normals );
    neck->Update();
  }

  //--------------------------------------------------------------------------------------------------
  void ComputeVetorialProduct( double vec1[3], double vec2[3], double *normal )
  {
    double ux, uy, uz, vx, vy, vz;

    ux = vec1[0];
    uy = vec1[1];
    uz = vec1[2];

    vx = vec2[0];
    vy = vec2[1];
    vz = vec2[2];

    normal[0] = uy * vz - uz * vy;
    normal[1] = uz * vx - ux * vz;
    normal[2] = ux * vy - uy * vx;
  }

  //--------------------------------------------------------------------------------------------------
  void ComputeNeckPoints( )
  {
    double p1[3], p2[3], p3[3], u[3], v[3], normal[3];

    p2[0] = this->AverageDiameterP1[0];
    p2[1] = this->AverageDiameterP1[1];
    p2[2] = this->AverageDiameterP1[2];

    p1[0] = this->AverageDiameterP2[0];
    p1[1] = this->AverageDiameterP2[1];
    p1[2] = this->AverageDiameterP2[2];

    this->Neck->GetCellData()->GetArray("Barycenters")->GetTuple( this->NeckCellId, p3 );

    // vetores u e v partindo de p1
    u[0] = p2[0] - p1[0];
    u[1] = p2[1] - p1[1];
    u[2] = p2[2] - p1[2];

    v[0] = p3[0] - p1[0];
    v[1] = p3[1] - p1[1];
    v[2] = p3[2] - p1[2];


    // calculando a normal do plano usando vetores u e v
    this->ComputeVetorialProduct( u, v, normal);


    /*
     * Setando a origem e normal do plano de corte.
     * Origem fica tão afastada de p1 quanto os mínimos da bounding box.
     * O plano é defino pelos vetores u e v pois os pontos p1, p2 e p3 pertencem ao mesmo plano.
     * A normal do plano é usada pelo cutter.
     */
    double n[3], origin[3], bounds[6], boundMin[3];
    this->Neck->GetBounds( bounds );
    boundMin[0] = bounds[0];
    boundMin[1] = bounds[2];
    boundMin[2] = bounds[4];

    n[0] = p1[0] - p2[0];
    n[1] = p1[1] - p2[1];
    n[2] = p1[2] - p2[2];

    this->ComputeExternalPoint( sqrt(vtkMath::Distance2BetweenPoints( boundMin, p1 )), p2, n, origin);

    vtkSmartPointer<vtkPlane> plane = vtkSmartPointer<vtkPlane>::New();
    plane->SetOrigin( origin[0], origin[1], origin[2] );
    plane->SetNormal( normal[0], normal[1], normal[2] );


    // Create cutter
    vtkSmartPointer<vtkCutter> cutter = vtkSmartPointer<vtkCutter>::New();
    cutter->SetCutFunction( plane );
    cutter->SetInput( this->Neck );
    cutter->Update();

    this->CutterPlane->DeepCopy( cutter->GetOutput() );
    this->CutterPlane->Update();

//    double point[3];
    int ptId1, ptId2;
    ptId1 = -1;
    vtkSmartPointer<vtkIdList> cells = vtkSmartPointer<vtkIdList>::New();

    for(int i=0; i < this->CutterPlane->GetNumberOfPoints(); ++i)
      {
      this->CutterPlane->GetPointCells( i, cells );

      if( cells->GetNumberOfIds() == 1 )
        {
        if( ptId1 == -1 )
          {
          ptId1 = i;
          }
        else
          {
          ptId2 = i;
          }
        }
      }


    this->CutterPlane->GetPoint( ptId1, this->NeckPointP1 );
    this->CutterPlane->GetPoint( ptId2, this->NeckPointP2 );

  }

  //--------------------------------------------------------------------------------------------------
  void ComputeAverageDiameterOfParentArtery()
  {
//    // para teste - estes valores devem vir do método que calcula o diametro médio.
//    this->AverageDiameterP1[0] = -0.734;
//    this->AverageDiameterP1[1] = 1.0064;
//    this->AverageDiameterP1[2] = 0.00436;
//
//    this->AverageDiameterP2[0] = -0.6560632558;
//    this->AverageDiameterP2[1] = 0.6221252065;
//    this->AverageDiameterP2[2] = -0.0659;
//
//    this->averageDiameterOfParentArtery = 1.5;
//    //////////////////////////////////////////////





    this->DiameterOfParentArtery->Update();

    this->DiameterOfParentArtery->GetP1( this->AverageDiameterP1 );

    double p2Radius;
    this->DiameterOfParentArtery->GetP2( this->AverageDiameterP2, &p2Radius );

    this->averageDiameterOfParentArtery = this->DiameterOfParentArtery->GetAverageDiameter();
  }

  //--------------------------------------------------------------------------------------------------
  void ComputeAngleBetweenTwoVectors(double u[3], double v[3], double *angleInRadians)
  {
    double d, prodEscalar, norm_u, norm_v;

    prodEscalar = this->ComputeEscalarProduct(u, v);
    norm_u = vtkMath::Norm(u);
    norm_v = vtkMath::Norm(v);
    d = prodEscalar / (norm_u * norm_v);  // cos e = u.v / |u| * |v|
    *angleInRadians = acos( d ); // acos(d) = e; sendo "e" o angulo em radianos
  }

  //--------------------------------------------------------------------------------------------------
  void ComputeVesselAngle()
  {
    double u[3], v[3], angleInRadians;

    this->ComputeIntersectionPointOfLines(this->AverageDiameterP2, this->AverageDiameterP1, this->NeckPointP1, this->NeckPointP2, this->VerticePointOfVesselAngle);


    u[0] = this->VerticePointOfVesselAngle[0] - this->AverageDiameterP1[0];
    u[1] = this->VerticePointOfVesselAngle[1] - this->AverageDiameterP1[1];
    u[2] = this->VerticePointOfVesselAngle[2] - this->AverageDiameterP1[2];

    v[0] = this->VerticePointOfVesselAngle[0] - this->NeckPointP1[0];
    v[1] = this->VerticePointOfVesselAngle[1] - this->NeckPointP1[1];
    v[2] = this->VerticePointOfVesselAngle[2] - this->NeckPointP1[2];


//    cout << "VerticePointOfVesselAngle: " << VerticePointOfVesselAngle[0] << ", " << VerticePointOfVesselAngle[1] << ", " << VerticePointOfVesselAngle[2] <<endl;
//    cout << "AverageDiameterP1: " << AverageDiameterP1[0] << ", " << AverageDiameterP1[1] << ", " << AverageDiameterP1[2] <<endl;
//    cout << "AverageDiameterP2: " << AverageDiameterP2[0] << ", " << AverageDiameterP2[1] << ", " << AverageDiameterP2[2] <<endl;
//    cout << "NeckPointP1: " << NeckPointP1[0] << ", " << NeckPointP1[1] << ", " << NeckPointP1[2] <<endl;
//    cout << "NeckPointP2: " << NeckPointP2[0] << ", " << NeckPointP2[1] << ", " << NeckPointP2[2] <<endl;
//    cout << "u: " << u[0] << ", " << u[1] << ", " << u[2] <<endl;
//    cout << "v: " << v[0] << ", " << v[1] << ", " << v[2] <<endl;

    this->ComputeAngleBetweenTwoVectors( u, v, &angleInRadians );

    this->VesselAngle = vtkMath::DegreesFromRadians( angleInRadians );
  }

  // Calcula o ponto de interseção das linhas COPLANARES KL e MN
  //--------------------------------------------------------------------------------------------------
  void ComputeIntersectionPointOfLines( double k[3], double l[3], double m[3], double n[3], double *intersection )
  {
    double det, ix, iy, iz, kx, ky, kz, lx, ly, lz, mx, my, mz, nx, ny, nz, t, s;

    kx = k[0];   lx = l[0];
    ky = k[1];   ly = l[1];
    kz = k[2];   lz = l[2];

    mx = m[0];   nx = n[0];
    my = m[1];   ny = n[1];
    mz = m[2];   nz = n[2];


   if (((kx != lx) || (ky != ly))  &&
       ((mx != nx) || (my != ny)) )  /* se nao e' paralela ao plano XY*/
   {
    det = (nx - mx) * (ly - ky)  -  (ny - my) * (lx - kx);

    if (det == 0.0)
     return;

    s = ((nx - mx) * (my - ky) - (ny - my) * (mx - kx))/ det ;
    t = ((lx - kx) * (my - ky) - (ly - ky) * (mx - kx))/ det ;
   }





   if (((kx != lx) || (kz != lz))  &&
       ((mx != nx) || (mz != nz)) )  /* se nao e' paralela ao plano XZ*/
   {
    det = (nx - mx) * (lz - kz)  -  (nz - mz) * (lx - kx);

    if (det == 0.0)
      return;

    s = ((nx - mx) * (mz - kz) - (nz - mz) * (mx - kx))/ det ;
    t = ((lx - kx) * (mz - kz) - (lz - kz) * (mx - kx))/ det ;
   }




   if (((ky != ly) || (kz != lz))  &&
       ((my != ny) || (mz != nz)) )  /* se nao e' paralela ao plano YZ*/
   {
    det = (ny - my) * (lz - kz)  -  (nz - mz) * (ly - ky);

    if (det == 0.0)
      return;

    s = ((ny - my) * (mz - kz) - (nz - mz) * (my - ky))/ det ;
    t = ((ly - ky) * (mz - kz) - (lz - kz) * (my - ky))/ det ;
   }



//   this->VerticePointOfVesselAngle[0] = kx + (lx-kx)*s;
//   this->VerticePointOfVesselAngle[1] = ky + (ly-ky)*s;
//   this->VerticePointOfVesselAngle[2] = kz + (lz-kz)*s;

   intersection[0] = kx + (lx-kx)*s;
   intersection[1] = ky + (ly-ky)*s;
   intersection[2] = kz + (lz-kz)*s;
  }

  //--------------------------------------------------------------------------------------------------
  void ComputeNeckArea()
  {
    vtkSmartPointer<vtkTriangle> triangle;
    for(int i=0; i < this->Neck->GetNumberOfCells(); ++i)
      {
      triangle = vtkTriangle::SafeDownCast( this->Neck->GetCell( i ) );

      this->NeckArea += triangle->ComputeArea();
      }

//    cout << "this->NeckArea: " << this->NeckArea <<endl;
  }

  //--------------------------------------------------------------------------------------------------
  void ComputeAverageNeckDiameter()
  {
    this->AverageNeckDiameter = 2.0 * sqrt( this->NeckArea / vtkMath::DoublePi() );

//    cout << "this->AverageNeckDiameter: " << this->AverageNeckDiameter <<endl;
  }

  //--------------------------------------------------------------------------------------------------
  void ComputeAspectRatio()
  {
    this->AspectRatio = this->AneurysmHeight / this->AverageNeckDiameter;

//    cout << "this->AspectRatio: " << this->AspectRatio <<endl;
  }

  //--------------------------------------------------------------------------------------------------
  void ComputeConvexHull()
  {
    // Clean the polydata. This will remove duplicate points that may be present in the input data.
    vtkSmartPointer<vtkCleanPolyData> cleaner = vtkSmartPointer<vtkCleanPolyData>::New();
    cleaner->SetInput( this->Dome );

    vtkSmartPointer<vtkHull> hull = vtkSmartPointer<vtkHull>::New();
    hull->SetInputConnection( cleaner->GetOutputPort() );
    hull->AddRecursiveSpherePlanes( this->RecursiveSpherePlanes );

    vtkSmartPointer<vtkDataSetSurfaceFilter> UgridToPoly = vtkSmartPointer<vtkDataSetSurfaceFilter>::New();
    UgridToPoly->SetInputConnection( hull->GetOutputPort() );
    UgridToPoly->SetPieceInvariant(1);
    UgridToPoly->SetNonlinearSubdivisionLevel(0);

    vtkSmartPointer<vtkTriangleFilter> triangulate = vtkSmartPointer<vtkTriangleFilter>::New();
    triangulate->SetInputConnection( UgridToPoly->GetOutputPort() );
    triangulate->Update();

    vtkSmartPointer<vtkPolyData> out = vtkSmartPointer<vtkPolyData>::New();
    out->DeepCopy( triangulate->GetOutput() );
    out->Update();

    this->ConvexHull->DeepCopy( out );
    this->ConvexHull->Update();


    vtkSmartPointer<vtkMassProperties> mp = vtkSmartPointer<vtkMassProperties>::New();
    mp->SetInput( this->ConvexHull );
    this->VolumeConvexHull = mp->GetVolume();
    this->AreaConvexHull = mp->GetSurfaceArea();


//    cout << "this->VolumeConvexHull: " << this->VolumeConvexHull <<endl;
//    cout << "this->AreaConvexHull: " << this->AreaConvexHull <<endl;
  }

  //--------------------------------------------------------------------------------------------------
  void ComputeEllipticityIndex()
  {
    this->EllipticityIndex = 1.0 - pow( (18.0 * vtkMath::DoublePi()) , 1.0/3.0 ) * pow( this->VolumeConvexHull, 2.0/3.0 )  / this->AreaConvexHull;

//    cout << "this->EllipticityIndex: " << this->EllipticityIndex <<endl;
  }

  //--------------------------------------------------------------------------------------------------
  void ComputeUndulationIndex()
  {
    this->UndulationIndex = 1.0 - ( this->VolumeAneurysm / this->VolumeConvexHull );

//    cout << "this->UndulationIndex: " << this->UndulationIndex <<endl;
  }

  //--------------------------------------------------------------------------------------------------
  void ComputeNonsphericityIndex()
  {
    this->NonsphericityIndex = 1.0 - pow( (18.0 * vtkMath::DoublePi()) , 1.0/3.0 ) * pow( this->VolumeAneurysm, 2.0/3.0 )  / this->AreaAneurysm;

//    cout << "this->NonsphericityIndex: " << this->NonsphericityIndex <<endl;
  }

  //--------------------------------------------------------------------------------------------------
  void ComputeLongitudeAneurysm()
  {
//    this->LongitudeAneurysm = pow(1/((4.0/3.0*vtkMath::DoublePi())/this->VolumeAneurysm),1.0/3.0)*2.0;
    this->LongitudeAneurysm = pow(this->VolumeAneurysm/(4.0/3.0*vtkMath::DoublePi()), 1.0/3.0)*2.0;
//    cout << "this->LongitudeAneurysm: " << this->LongitudeAneurysm <<endl;
  }

  //--------------------------------------------------------------------------------------------------
  void ComputeSizeRatio()
  {
    this->SizeRatio = this->LongitudeAneurysm / this->averageDiameterOfParentArtery;

//    cout << "this->SizeRatio: " << this->SizeRatio <<endl;
  }

  //--------------------------------------------------------------------------------------------------
  void ComputeAneurysmDiameterMaximum()
  {
    this->collection = vtkSmartPointer<vtkPolyDataCollection>::New();

    double barycenter[3], basePoint[3], bounds[6], boundMin[3], boundMax[3], normal[3], *avgDiameter;
    int resolution = 20; // qtd de planos que irá cortar o domo


    this->Neck->GetCellData()->GetArray("Barycenters")->GetTuple( this->NeckCellId, barycenter );


    vtkSmartPointer<vtkLineSource> heigth = vtkSmartPointer<vtkLineSource>::New();
    heigth->SetPoint1( barycenter );
    heigth->SetPoint2( this->HigherAneurysmPoint );
    heigth->SetResolution( resolution-1 );
    heigth->Update();

    normal[0] = this->HigherAneurysmPoint[0] - barycenter[0];
    normal[1] = this->HigherAneurysmPoint[1] - barycenter[1];
    normal[2] = this->HigherAneurysmPoint[2] - barycenter[2];


    avgDiameter = new double[ heigth->GetOutput()->GetNumberOfPoints() ];


    for(int i=0; i < resolution; ++i)
      {
      heigth->GetOutput()->GetPoint( i, basePoint );

      vtkSmartPointer<vtkPlane> plane = vtkSmartPointer<vtkPlane>::New();
      plane->SetOrigin( basePoint[0], basePoint[1], basePoint[2] );
      plane->SetNormal( normal[0], normal[1], normal[2] );


      // Create cutter
      vtkSmartPointer<vtkCutter> cutter = vtkSmartPointer<vtkCutter>::New();
      cutter->SetCutFunction( plane );
      cutter->SetInput( this->Dome );
      cutter->Update();


      vtkSmartPointer<vtkPolyData> poly = vtkSmartPointer<vtkPolyData>::New();
      poly->DeepCopy( cutter->GetOutput() );
      poly->Update();

      collection->AddItem( poly );

      avgDiameter[i] = this->AverageDistanceBetweenPointsAndCenter( cutter->GetOutput(), basePoint );
      cout << "avgDiameter[" << i <<"]: " << avgDiameter[i] <<endl;
      }

    // encontrando o maior diametro
    this->AneurysmDiameterMaximum = avgDiameter[0];
    for(int i=1; i < resolution; ++i)
      {
      if( this->AneurysmDiameterMaximum < avgDiameter[i] )
        {
        this->AneurysmDiameterMaximum = avgDiameter[i];
        }
      }

    //até este ponto foi encontrado o raio máximo, agora multiplica-se por 2 para encontrar o diametro
    this->AneurysmDiameterMaximum *= 2;

    cout << "this->AneurysmDiameterMaximum:" << this->AneurysmDiameterMaximum <<endl;

    delete[] avgDiameter;


  }

  //--------------------------------------------------------------------------------------------------
  double AverageDistanceBetweenPointsAndCenter( vtkSmartPointer<vtkPolyData> poly, double *center )
  {
    double p[3], sum;

    sum = 0.0;

    for(int i=0; i < poly->GetNumberOfPoints(); ++i)
      {
      poly->GetPoint( i, p );

      sum += sqrt(vtkMath::Distance2BetweenPoints( center, p ));
      }

    double n = poly->GetNumberOfPoints();
    return sum / n ;
  }

  //--------------------------------------------------------------------------------------------------
  void ComputeBottleNeckEffect()
  {
    this->BottleNeckEffect = this->AneurysmDiameterMaximum / this->AverageNeckDiameter;
  }





  vtkSmartPointer<vtkPolyData> Neck;
  vtkSmartPointer<vtkPolyData> Dome;
  vtkSmartPointer<vtkPolyData> CutterPlane;
  vtkSmartPointer<vtkPolyData> ConvexHull;

  AverageDiameterOfParentArtery *DiameterOfParentArtery;

  int NeckCellId;                         // id da celula do pescoço usada para o calculo da altura do aneurisma
  int RecursiveSpherePlanes;              // usado no calculo do convexhull
  double AneurysmHeight;                  // altura do aneurisma
  double HigherAneurysmPoint[3];          // ponto do domo de onde foi calculada a altura do aneurisma
  double VerticePointOfVesselAngle[3];    // interseção entre a reta dos pontos do pescoço e a reta formada pelos pontos AverageDiameterP1 e AverageDiameterP2
  double AneurysmAngle;                   // angulo do aneurisma em graus
  double VesselAngle;                     // angulo do vaso em graus
  double AverageDiameterP1[3];            // ponto selecionado pelo usuário para calculo do diametro médio
  double AverageDiameterP2[3];            // ponto calculado no diametro médio
  double NeckPointP1[3], NeckPointP2[3];  // pontos de interseção do pescoço do aneurisma com o plano definido pelos pontos AverageDiameterP1, AverageDiameterP2 e o baricentro da celula NeckCellId
  double NeckArea;                        // somatório das áreas dos triangulos do pescoço (base) do aneurisma
  double AverageNeckDiameter;             // diametro médio do pescoço (base) - 2*sqrt(NeckArea/Pi)
  double AspectRatio;                     // MaximumHeight /  AverageNeckDiameter
  double VolumeConvexHull;                // Volume do convexHull
  double AreaConvexHull;                  // area do convexHull
  double EllipticityIndex;                // 1.0 - (18.0*Pi)^1.0/3.0 * VolumeConvexHull^2.0/3.0 / AreaConvexHull
  double averageDiameterOfParentArtery;   // diametro medio da arteria pai
  double VolumeAneurysm;                  // volume do aneurisma
  double AreaAneurysm;                    // area do aneurisma
  double UndulationIndex;                 // 1.0 - ( VolumeAneurysm / VolumeConvexHull )
  double NonsphericityIndex;              // 1.0 - ( 18.0 * Pi ) ^ 1.0/3.0 * VolumeAneurysm^2.0/3.0 / AreaAneurysm
  double LongitudeAneurysm;               // ( VolumeAneurysm/(4.0/3.0*vtkMath::DoublePi()) )^1.0/3.0)*2.0
  double SizeRatio;                       // LongitudeAneurysm / averageDiameterOfParentArtery
  double BottleNeckEffect;                // AneurysmDiameterMaximum / AverageNeckDiameter
  // Para calcular o valor de AneurysmDiameterMaximum o domo é cortado por planos perpediculares a linha definida por HigherAneurysmPoint e o baricentro da celula NeckCellId.
  // É verificada a distância entre o ponto central do plano (que esta na linha HigherAneurysmPoint- baricentro) e pontos da interseção entre plano e domo.
  // Procura-se a distância máxima * 2.
  double AneurysmDiameterMaximum;


  vtkSmartPointer<vtkPolyDataCollection> collection; // para debug
};





//--------------------------------------------------------------------------------------------------
void AddLineActorInRender( vtkSmartPointer<vtkPolyData> neck, vtkSmartPointer<vtkRenderer> renderer, int neckCellId,
                           double higherAneurysmPoint[3], double neckPoint[3], vtkSmartPointer<vtkPolyData> plane,
                           double * NeckPointP1, double *NeckPointP2, double *line_r1, double *line_r2,
                           double verticePointOfVesselAngle[3], double * AverageDiameterP1, double *AverageDiameterP2, vtkSmartPointer<vtkPolyDataCollection> collection)
{
  double p1[3];

  vtkSmartPointer<vtkCell> cell = neck->GetCell( neckCellId );

  neck->GetCellData()->GetArray("Barycenters")->GetTuple( neckCellId, p1 );

  double radius = 0.010;
  double lineWidth = 3;




//  //---------------
//  vtkPoints *points = cell->GetPoints();
//  double n1[3], n2[3], n3[3];
//  double normal[3], externalPoint[3];
//  points->GetPoint(0, n1);
//  points->GetPoint(1, n2);
//  points->GetPoint(2, n3);
//  neck->GetCellData()->GetArray("Normals")->GetTuple( neckCellId, normal );
//
//  AneurysmPropertiesFilter *filter = new AneurysmPropertiesFilter;
//  filter->ComputeExternalPoint( 1, p1, normal, externalPoint );
//  delete filter;
//
//
//  //----------------------------------------------------------------
//  cout << "neckCellId: " << neckCellId <<endl;
//  cout << "\tCell p1: " << n1[0] << ", " << n1[1] << ", " << n1[2] <<endl;
//  cout << "\tCell p2: " << n2[0] << ", " << n2[1] << ", " << n2[2] <<endl;
//  cout << "\tCell p3: " << n3[0] << ", " << n3[1] << ", " << n3[2] <<endl;
//  cout << "\tCell Barycenter: " << p1[0] << ", " << p1[1] << ", " << p1[2] <<endl;
//  cout << "\tCell normal vector: " << normal[0] << ", " << normal[1] << ", " << normal[2] <<endl;
//  cout << "externalPoint: " << externalPoint[0] << ", " << externalPoint[1] << ", " << externalPoint[2] <<endl;
//  cout << "higherAneurysmPoint: " << higherAneurysmPoint[0] << ", " << higherAneurysmPoint[1] << ", " << higherAneurysmPoint[2] <<endl;
//  cout << "verticePointOfVesselAngle: " << verticePointOfVesselAngle[0] << ", " << verticePointOfVesselAngle[1] << ", " << verticePointOfVesselAngle[2] <<endl;
//  cout << "line_r1: " << line_r1[0] << ", " << line_r1[1] << ", " << line_r1[2] << ", " << line_r1[3] << ", " << line_r1[4] << ", " << line_r1[5] <<endl;
//  cout << "line_r2: " << line_r2[0] << ", " << line_r2[1] << ", " << line_r2[2] << ", " << line_r2[3] << ", " << line_r2[4] << ", " << line_r2[5] <<endl;
//  cout << "neckPoint: " << neckPoint[0] << ", " << neckPoint[1] << ", " << neckPoint[2] <<endl;
//  cout << "NeckPointP1: " << NeckPointP1[0] << ", " << NeckPointP1[1] << ", " << NeckPointP1[2] <<endl;
//  cout << "NeckPointP2: " << NeckPointP2[0] << ", " << NeckPointP2[1] << ", " << NeckPointP2[2] <<endl;
//  cout << "AverageDiameterP1: " << AverageDiameterP1[0] << ", " << AverageDiameterP1[1] << ", " << AverageDiameterP1[2] <<endl;
//  cout << "AverageDiameterP2: " << AverageDiameterP2[0] << ", " << AverageDiameterP2[1] << ", " << AverageDiameterP2[2] <<endl;
//  cout << "\n" <<endl;
  //----------------------------------------------------------------




  //linha criada a partir do ponto pescoço mais próximo do ponto distal
  vtkSmartPointer<vtkLineSource> angleLine = vtkSmartPointer<vtkLineSource>::New();
  angleLine->SetPoint1( p1 );
  angleLine->SetPoint2( neckPoint );
  angleLine->SetResolution(10);
  angleLine->Update();

  vtkSmartPointer<vtkDataSetMapper> mapper_angleLine = vtkSmartPointer<vtkDataSetMapper>::New();
  mapper_angleLine->SetInput( angleLine->GetOutput() );

  vtkSmartPointer<vtkActor> actor_angleLine = vtkSmartPointer<vtkActor>::New();
  actor_angleLine->SetMapper( mapper_angleLine );
  actor_angleLine->GetProperty()->SetColor( 0.0, 1.0, 0.0 );
  actor_angleLine->GetProperty()->SetLineWidth(lineWidth);



  //altura
  vtkSmartPointer<vtkLineSource> heigth = vtkSmartPointer<vtkLineSource>::New();
  heigth->SetPoint1( p1 );
  heigth->SetPoint2( higherAneurysmPoint );
  heigth->SetResolution(10);
  heigth->Update();

  vtkSmartPointer<vtkDataSetMapper> mapper = vtkSmartPointer<vtkDataSetMapper>::New();
  mapper->SetInput( heigth->GetOutput() );

  vtkSmartPointer<vtkActor> actor_heigth = vtkSmartPointer<vtkActor>::New();
  actor_heigth->SetMapper( mapper );
  actor_heigth->GetProperty()->SetColor( 1.0, 0.0, 0.0 );
  actor_heigth->GetProperty()->SetLineWidth(lineWidth);




  // esfera base
  vtkSmartPointer<vtkSphereSource> sph1 = vtkSmartPointer<vtkSphereSource>::New();
  sph1->SetRadius( radius );
  sph1->SetCenter( p1 );
  sph1->SetThetaResolution( 30 );
  sph1->SetPhiResolution( 30 );

  vtkSmartPointer<vtkDataSetMapper> sph1_mapper = vtkSmartPointer<vtkDataSetMapper>::New();
  sph1_mapper->SetInput( sph1->GetOutput() );

  vtkSmartPointer<vtkActor> sph1_actor = vtkSmartPointer<vtkActor>::New();
  sph1_actor->SetMapper( sph1_mapper );
  sph1_actor->GetProperty()->SetColor( 0.0, 0.0, 1.0 ); // azul



  // esfera higherAneurysmPoint
  vtkSmartPointer<vtkSphereSource> sph2 = vtkSmartPointer<vtkSphereSource>::New();
  sph2->SetRadius( radius );
  sph2->SetCenter( higherAneurysmPoint );
  sph2->SetThetaResolution( 30 );
  sph2->SetPhiResolution( 30 );

  vtkSmartPointer<vtkDataSetMapper> sph2_mapper = vtkSmartPointer<vtkDataSetMapper>::New();
  sph2_mapper->SetInput( sph2->GetOutput() );

  vtkSmartPointer<vtkActor> sph2_actor = vtkSmartPointer<vtkActor>::New();
  sph2_actor->SetMapper( sph2_mapper );
  sph2_actor->GetProperty()->SetColor( 0.0, 1.0, 1.0 ); // ciano




  //cutter plane
  vtkSmartPointer<vtkDataSetMapper> cutterMapper = vtkSmartPointer<vtkDataSetMapper>::New();
  cutterMapper->SetInput( plane );
  cutterMapper->Update();

  // Create plane actor
  vtkSmartPointer<vtkActor> planeActor = vtkSmartPointer<vtkActor>::New();
  planeActor->GetProperty()->SetColor(1.0, 1.0, 0.0);
  planeActor->GetProperty()->SetLineWidth(lineWidth);
  planeActor->SetMapper(cutterMapper);




  // esfera neck p1
  vtkSmartPointer<vtkSphereSource> neck1 = vtkSmartPointer<vtkSphereSource>::New();
  neck1->SetRadius( radius );
  neck1->SetCenter( NeckPointP1 );
  neck1->SetThetaResolution( 30 );
  neck1->SetPhiResolution( 30 );

  vtkSmartPointer<vtkDataSetMapper> neck1_mapper = vtkSmartPointer<vtkDataSetMapper>::New();
  neck1_mapper->SetInput( neck1->GetOutput() );

  vtkSmartPointer<vtkActor> neck1_actor = vtkSmartPointer<vtkActor>::New();
  neck1_actor->SetMapper( neck1_mapper );
  neck1_actor->GetProperty()->SetColor( 1.0, 0.0, 0.0 ); // red


  // esfera neck p2
  vtkSmartPointer<vtkSphereSource> neck2 = vtkSmartPointer<vtkSphereSource>::New();
  neck2->SetRadius( radius );
  neck2->SetCenter( NeckPointP2 );
  neck2->SetThetaResolution( 30 );
  neck2->SetPhiResolution( 30 );

  vtkSmartPointer<vtkDataSetMapper> neck2_mapper = vtkSmartPointer<vtkDataSetMapper>::New();
  neck2_mapper->SetInput( neck2->GetOutput() );

  vtkSmartPointer<vtkActor> neck2_actor = vtkSmartPointer<vtkActor>::New();
  neck2_actor->SetMapper( neck2_mapper );
  neck2_actor->GetProperty()->SetColor( 1.0, 0.0, 0.0 ); // red


  //linha ligando os pontos do pescoço
  vtkSmartPointer<vtkLineSource> sourceNeckLine = vtkSmartPointer<vtkLineSource>::New();
  sourceNeckLine->SetPoint1( NeckPointP1 );
  sourceNeckLine->SetPoint2( NeckPointP2 );
  sourceNeckLine->SetResolution(10);
  sourceNeckLine->Update();

  vtkSmartPointer<vtkDataSetMapper> mapper_NeckLine = vtkSmartPointer<vtkDataSetMapper>::New();
  mapper_NeckLine->SetInput( sourceNeckLine->GetOutput() );

  vtkSmartPointer<vtkActor> actor_NeckLine = vtkSmartPointer<vtkActor>::New();
  actor_NeckLine->SetMapper( mapper_NeckLine );
  actor_NeckLine->GetProperty()->SetColor( 1.0, 1.0, 1.0 );
  actor_NeckLine->GetProperty()->SetLineWidth(7);
  actor_NeckLine->GetProperty()->SetOpacity(0.6);


  //linha ligando os pontos Average
  vtkSmartPointer<vtkLineSource> sourceAverageLine = vtkSmartPointer<vtkLineSource>::New();
  sourceAverageLine->SetPoint1( AverageDiameterP1 );
  sourceAverageLine->SetPoint2( AverageDiameterP2 );
  sourceAverageLine->SetResolution(10);
  sourceAverageLine->Update();

  vtkSmartPointer<vtkDataSetMapper> mapper_AverageLine = vtkSmartPointer<vtkDataSetMapper>::New();
  mapper_AverageLine->SetInput( sourceAverageLine->GetOutput() );

  vtkSmartPointer<vtkActor> actor_AverageLine = vtkSmartPointer<vtkActor>::New();
  actor_AverageLine->SetMapper( mapper_AverageLine );
  actor_AverageLine->GetProperty()->SetColor( 0.0, 1.0, 1.0 );
  actor_AverageLine->GetProperty()->SetLineWidth(7);
  actor_AverageLine->GetProperty()->SetOpacity(0.6);




  //linha para visualizar o ponto de interseção das linhas de onde vem o VesselAngle
  vtkSmartPointer<vtkLineSource> sourceLine_r1 = vtkSmartPointer<vtkLineSource>::New();
  sourceLine_r1->SetPoint1( line_r1[0], line_r1[1], line_r1[2] );
  sourceLine_r1->SetPoint2( line_r1[3], line_r1[4], line_r1[5] );
  sourceLine_r1->SetResolution(10);
  sourceLine_r1->Update();

  vtkSmartPointer<vtkDataSetMapper> mapper_line_r1 = vtkSmartPointer<vtkDataSetMapper>::New();
  mapper_line_r1->SetInput( sourceLine_r1->GetOutput() );

  vtkSmartPointer<vtkActor> actor_line_r1 = vtkSmartPointer<vtkActor>::New();
  actor_line_r1->SetMapper( mapper_line_r1 );
  actor_line_r1->GetProperty()->SetColor( 0.0, 0.0, 1.0 );
  actor_line_r1->GetProperty()->SetLineStipplePattern(0xf0f0);
  actor_line_r1->GetProperty()->SetLineStippleRepeatFactor(1);
  actor_line_r1->GetProperty()->SetLineWidth(lineWidth);

  vtkSmartPointer<vtkLineSource> sourceLine_r2 = vtkSmartPointer<vtkLineSource>::New();
  sourceLine_r2->SetPoint1( line_r2[0], line_r2[1], line_r2[2] );
  sourceLine_r2->SetPoint2( line_r2[3], line_r2[4], line_r2[5] );
  sourceLine_r2->SetResolution(10);
  sourceLine_r2->Update();

  vtkSmartPointer<vtkDataSetMapper> mapper_line_r2 = vtkSmartPointer<vtkDataSetMapper>::New();
  mapper_line_r2->SetInput( sourceLine_r2->GetOutput() );

  vtkSmartPointer<vtkActor> actor_line_r2 = vtkSmartPointer<vtkActor>::New();
  actor_line_r2->SetMapper( mapper_line_r2 );
  actor_line_r2->GetProperty()->SetColor( 1.0, 0.0, 0.0 );
  actor_line_r2->GetProperty()->SetLineStipplePattern(0xf0f0);
  actor_line_r2->GetProperty()->SetLineStippleRepeatFactor(1);
  actor_line_r2->GetProperty()->SetLineWidth(lineWidth);



  // esfera intersectPoint
  vtkSmartPointer<vtkSphereSource> sph_intersectPoint = vtkSmartPointer<vtkSphereSource>::New();
  sph_intersectPoint->SetRadius( radius );
  sph_intersectPoint->SetCenter( line_r2[3], line_r2[4], line_r2[5] );
  sph_intersectPoint->SetThetaResolution( 30 );
  sph_intersectPoint->SetPhiResolution( 30 );

  vtkSmartPointer<vtkDataSetMapper> intersectPoint_mapper = vtkSmartPointer<vtkDataSetMapper>::New();
  intersectPoint_mapper->SetInput( sph_intersectPoint->GetOutput() );

  vtkSmartPointer<vtkActor> intersectPoint_actor = vtkSmartPointer<vtkActor>::New();
  intersectPoint_actor->SetMapper( intersectPoint_mapper );
  intersectPoint_actor->GetProperty()->SetColor( 0.0, 1.0, 0.0 ); // green


  // esferas Average
  vtkSmartPointer<vtkSphereSource> sph_AverageDiameterP1 = vtkSmartPointer<vtkSphereSource>::New();
  sph_AverageDiameterP1->SetRadius( radius );
  sph_AverageDiameterP1->SetCenter( AverageDiameterP1 );
  sph_AverageDiameterP1->SetThetaResolution( 30 );
  sph_AverageDiameterP1->SetPhiResolution( 30 );

  vtkSmartPointer<vtkDataSetMapper> mapper_AverageDiameterP1 = vtkSmartPointer<vtkDataSetMapper>::New();
  mapper_AverageDiameterP1->SetInput( sph_AverageDiameterP1->GetOutput() );

  vtkSmartPointer<vtkActor> actor_AverageDiameterP1 = vtkSmartPointer<vtkActor>::New();
  actor_AverageDiameterP1->SetMapper( mapper_AverageDiameterP1 );
  actor_AverageDiameterP1->GetProperty()->SetColor( 1.0, 1.0, 0.0 );

  vtkSmartPointer<vtkSphereSource> sph_AverageDiameterP2 = vtkSmartPointer<vtkSphereSource>::New();
  sph_AverageDiameterP2->SetRadius( radius );
  sph_AverageDiameterP2->SetCenter( AverageDiameterP2 );
  sph_AverageDiameterP2->SetThetaResolution( 30 );
  sph_AverageDiameterP2->SetPhiResolution( 30 );

  vtkSmartPointer<vtkDataSetMapper> mapper_AverageDiameterP2 = vtkSmartPointer<vtkDataSetMapper>::New();
  mapper_AverageDiameterP2->SetInput( sph_AverageDiameterP2->GetOutput() );

  vtkSmartPointer<vtkActor> actor_AverageDiameterP2 = vtkSmartPointer<vtkActor>::New();
  actor_AverageDiameterP2->SetMapper( mapper_AverageDiameterP2 );
  actor_AverageDiameterP2->GetProperty()->SetColor( 0.0, 0.0, 0.0 );


  for(int i=0; i < collection->GetNumberOfItems(); ++i)
    {
    vtkSmartPointer<vtkDataSetMapper> cutter_planeMapper = vtkSmartPointer<vtkDataSetMapper>::New();
    cutter_planeMapper->SetInput( vtkPolyData::SafeDownCast( collection->GetItemAsObject(i) ) );

    // Create plane actor
    vtkSmartPointer<vtkActor> cutter_planeActor = vtkSmartPointer<vtkActor>::New();
    cutter_planeActor->GetProperty()->SetColor(1.0, 0.0, 0.0);
    cutter_planeActor->GetProperty()->SetLineWidth(2);
    cutter_planeActor->SetMapper( cutter_planeMapper );


    renderer->AddActor( cutter_planeActor );
    }




  renderer->AddActor( planeActor );
  renderer->AddActor( intersectPoint_actor );
  renderer->AddActor( actor_line_r1 );
  renderer->AddActor( actor_line_r2 );
  renderer->AddActor( actor_NeckLine );
  renderer->AddActor( actor_AverageLine );
  renderer->AddActor( neck1_actor );
  renderer->AddActor( neck2_actor );
  renderer->AddActor( actor_heigth );
  renderer->AddActor( actor_angleLine );
  renderer->AddActor( sph1_actor );
  renderer->AddActor( sph2_actor );
  renderer->AddActor( actor_AverageDiameterP1 );
  renderer->AddActor( actor_AverageDiameterP2 );
}




//--------------------------------------------------------------------------------------------------
int main( int argc, char **argv )
{
  double backgroundColor[] = {0.8, 0.8, 0.8};
  double domoColor[] = {0.0, 0.0, 0.5};

  string fileNameDomo;
  string fileNameNeck;
  string fileNameCenterline;

//  fileNameDomo.append("/home/eduardo/workspace/data_HeMoLab/Aneurisma_2_domo.vtk");
//  fileNameNeck.append("/home/eduardo/workspace/data_HeMoLab/Aneurisma_2_base.vtk");
//  fileNameCenterline.append("/home/eduardo/workspace/data_HeMoLab/Aneurisma_2_centerline.vtk");

  fileNameDomo.append("/home/eduardo/workspace/data_HeMoLab/Aneurisk/c0001/aneurism_SAC_C0001.vtk");
  fileNameNeck.append("/home/eduardo/workspace/data_HeMoLab/Aneurisk/c0001/aneurism_BASE_C0001_PolyData_InvertedNormals.vtk");
  fileNameCenterline.append("/home/eduardo/workspace/data_HeMoLab/Aneurisk/c0001/centerLine_WF_C0001_new.vtk");
  //---------------------------------


  vtkSmartPointer<vtkPolyDataReader> readerDomo = vtkSmartPointer<vtkPolyDataReader>::New();
  readerDomo->SetFileName( fileNameDomo.c_str() );
  readerDomo->Update();


  vtkSmartPointer<vtkPolyDataReader> readerNeck = vtkSmartPointer<vtkPolyDataReader>::New();
  readerNeck->SetFileName( fileNameNeck.c_str() );
  readerNeck->Update();


  vtkSmartPointer<vtkPolyData> domo = vtkSmartPointer<vtkPolyData>::New();
  domo->DeepCopy( readerDomo->GetOutput() );
  domo->Update();


  vtkSmartPointer<vtkPolyData> neck = vtkSmartPointer<vtkPolyData>::New();
  neck->DeepCopy( readerNeck->GetOutput() );
  neck->Update();


  vtkSmartPointer<vtkDataSetMapper> mapperdomo = vtkSmartPointer<vtkDataSetMapper>::New();
  mapperdomo->SetInput( domo );

  vtkSmartPointer<vtkActor> actordomo = vtkSmartPointer<vtkActor>::New();
  actordomo->SetMapper( mapperdomo );
  actordomo->GetProperty()->SetColor( domoColor );
  actordomo->GetProperty()->SetOpacity(0.4);


  vtkSmartPointer<vtkDataSetMapper> mapperneck = vtkSmartPointer<vtkDataSetMapper>::New();
  mapperneck->SetInput( neck );

  vtkSmartPointer<vtkActor> actorneck = vtkSmartPointer<vtkActor>::New();
  actorneck->SetMapper( mapperneck );
  actorneck->GetProperty()->SetOpacity(0.3);
  actorneck->GetProperty()->EdgeVisibilityOn();




  vtkSmartPointer<vtkPolyDataReader> readercenterline = vtkSmartPointer<vtkPolyDataReader>::New();
  readercenterline->SetFileName( fileNameCenterline.c_str() );
  readercenterline->Update();

  vtkSmartPointer<vtkPolyData> centerline = vtkSmartPointer<vtkPolyData>::New();
  centerline->DeepCopy( readercenterline->GetOutput() );
  centerline->Update();

  vtkSmartPointer<vtkDataSetMapper> mappercenterline = vtkSmartPointer<vtkDataSetMapper>::New();
  mappercenterline->SetInput( centerline );

  vtkSmartPointer<vtkActor> actorcenterline = vtkSmartPointer<vtkActor>::New();
  actorcenterline->SetMapper( mappercenterline );
  actorcenterline->GetProperty()->SetColor(1.0, 1.0, 0.0);




  vtkSmartPointer<vtkRenderer> renderer = vtkSmartPointer<vtkRenderer>::New();
  renderer->AddActor( actordomo );
  renderer->AddActor( actorneck );
  renderer->AddActor( actorcenterline );
  renderer->SetBackground( backgroundColor );
//  renderer->GetActiveCamera()->SetPosition( 0,0,3 );
//  renderer->GetActiveCamera()->SetViewUp( 1,0,0 );





  // *********** input teste 2 - informando Distal
  vtkSmartPointer<vtkIdList> DistalId = vtkSmartPointer<vtkIdList>::New(); //ponto(s) de saída de fluxo na centerline, pode(m) ser informado mas não é essencial

  int ProximalId = 0;     //ponto de entrada de fluxo na centerline
  int ptId_1 = 20;        //ponto selecionado pelo usuário - P1
  DistalId->InsertNextId(107);


  AneurysmPropertiesFilter *filter = new AneurysmPropertiesFilter;
  filter->SetInputAneurysmNeck( neck );
  filter->SetInputAneurysmDome( domo );
  filter->SetCenterline( centerline );
  filter->SetProximalPoint( ProximalId );
  filter->SetDistalPoints( DistalId );
  filter->SetUserSelectedPoint( ptId_1 );
  filter->Update();

  int neckCellId =                      filter->GetNeckCellId();
  double *higherAneurysmPoint =         filter->GetHigherAneurysmPoint();
  double *NeckPoint2 =                  filter->GetNeckPoint2();
  double *p1 =                          filter->GetAverageDiameterP1();
  double *p2 =                          filter->GetAverageDiameterP2();
  double *verticePointOfVesselAngle =   filter->GetVerticePointOfVesselAngle();

  double line_r1[6];
  line_r1[0] = NeckPoint2[0];
  line_r1[1] = NeckPoint2[1];
  line_r1[2] = NeckPoint2[2];
  line_r1[3] = verticePointOfVesselAngle[0];
  line_r1[4] = verticePointOfVesselAngle[1];
  line_r1[5] = verticePointOfVesselAngle[2];

  double line_r2[6];
  line_r2[0] = p1[0];
  line_r2[1] = p1[1];
  line_r2[2] = p1[2];
  line_r2[3] = verticePointOfVesselAngle[0];
  line_r2[4] = verticePointOfVesselAngle[1];
  line_r2[5] = verticePointOfVesselAngle[2];


  AddLineActorInRender( filter->GetInputAneurysmNeck(), renderer, neckCellId, higherAneurysmPoint,
                        filter->GetNeckPoint2(), filter->GetCutterPlane(), filter->GetNeckPoint1(),
                        filter->GetNeckPoint2(), line_r1, line_r2, filter->GetVerticePointOfVesselAngle(), p1, p2, filter->GetCutterCollection() );



  cout << "Volume Aneurysm: "                     << filter->GetVolumeAneurysm() <<endl;
  cout << "Area Aneurysm: "                       << filter->GetAreaAneurysm() <<endl;
  cout << "Aneurysm Height: "                     << filter->GetAneurysmHeight() <<endl;
  cout << "Neck Area: "                           << filter->GetNeckArea() <<endl;
  cout << "Average Neck Diameter: "               << filter->GetAverageNeckDiameter() <<endl;
  cout << "Aspect Ratio: "                        << filter->GetAspectRatio() <<endl;
  cout << "Average Diameter Of Parent Artery: "   << filter->GetAverageDiameterOfParentArtery() <<endl;
  cout << "Volume of Convex Hull: "               << filter->GetVolumeConvexHull() <<endl;
  cout << "Area of Convex Hull: "                 << filter->GetAreaConvexHull() <<endl;
  cout << "Undulation Index: "                    << filter->GetUndulationIndex() <<endl;
  cout << "Nonsphericity Index: "                 << filter->GetNonsphericityIndex() <<endl;
  cout << "Ellipticity Index: "                   << filter->GetEllipticityIndex() <<endl;
  cout << "VesselAngle: "                         << filter->GetVesselAngle() <<endl;
  cout << "AneurysmAngle: "                       << filter->GetAneurysmAngle() <<endl;
  cout << "LongitudeAneurysm: "                   << filter->GetLongitudeAneurysm() <<endl;
  cout << "SizeRatio: "                           << filter->GetSizeRatio() <<endl;
  cout << "AneurysmDiameterMaximum: "             << filter->GetAneurysmDiameterMaximum() <<endl;
  cout << "BottleNeckEffect: "                    << filter->GetBottleNeckEffect() <<endl;


  double AverageDiameter;
  double newPoint[3];
  double newRadius;
  int NeighborOfP1;


  AverageDiameterOfParentArtery *DiameterOfParentArtery = filter->GetAverageDiameterOfParentArteryFilter();

  AverageDiameter = DiameterOfParentArtery->GetAverageDiameter();
  DiameterOfParentArtery->GetP2( newPoint, &newRadius );
  NeighborOfP1 = DiameterOfParentArtery->GetNeighborOfP1();
  ptId_1 = DiameterOfParentArtery->GetP1();


  cout << "ProximalId: " << DiameterOfParentArtery->GetProximalPoint() <<endl;
  for(int i=0; i< DiameterOfParentArtery->GetDistalPoints()->GetNumberOfIds(); ++i)
    {
    cout << "DiameterOfParentArtery->GetDistalPoints()->GetId("<<i<<"): " << DiameterOfParentArtery->GetDistalPoints()->GetId(i) <<endl;
    }
  double d[3];
  DiameterOfParentArtery->GetInput()->GetPoint( ptId_1, d );
  cout << "Selected point (ptId_1): " << ptId_1 << "\t" << d[0]<< ", " << d[1] << ", " << d[2] <<endl;
  cout << "Neighbor of selected point (SENTIDO _ptId_1 -> Proximal): " << NeighborOfP1 <<endl;

  cout << "Average diameter: " << AverageDiameter << "\t newPoint: "<< newPoint[0]<< ", " << newPoint[1] << ", " << newPoint[2] << "\t newRadius: "<< newRadius << endl;







  delete filter;



  vtkSmartPointer<vtkRenderWindow> renWin = vtkSmartPointer<vtkRenderWindow>::New();
  renWin->AddRenderer(renderer);

  vtkSmartPointer<vtkRenderWindowInteractor> iren = vtkSmartPointer<vtkRenderWindowInteractor>::New();
  iren->SetRenderWindow(renWin);
  renWin->SetSize(1024, 768);


  renderer->Render();
  iren->Initialize();
  iren->Start();



  return 0;
}
