#include "itkImageSeriesReader.h"
#include "itkGDCMSeriesFileNames.h"
#include "itkGDCMImageIO.h"
#include "itkImage.h"
#include "itkImageToVTKImageFilter.h"
#include "itkVTKImageToImageFilter.h"

#include "vtkImageData.h"
#include "vtkImageViewer2.h"
#include "vtkRenderWindowInteractor.h"
#include "vtkRenderer.h"
#include "vtkRenderWindow.h"
#include "vtkImageActor.h"
#include "vtkPolyDataMapper.h"
#include "vtkActor.h"
#include "vtkLODActor.h"
#include "vtkOutlineFilter.h"
#include "vtkCamera.h"
#include "vtkStripper.h"
#include "vtkLookupTable.h"
#include "vtkImagePlaneWidget.h"
#include "vtkContourFilter.h"
#include "vtkProperty.h"
#include "vtkImageMapToColors.h"
#include "vtkPolyDataNormals.h"
#include "vtkDICOMImageReader.h"

#include "itkExtractImageFilter.h"


#include "itkSobelEdgeDetectionImageFilter.h"
#include "itkMedianImageFilter.h"

using namespace std;

typedef itk::Image  <float, 3>        ImageType;
typedef itk::Image  <float, 2>        ImageType2D;
typedef itk::ImageToVTKImageFilter  <ImageType2D>   ConnectorType;


//-----------------------------------------------------------------------------
int main( int argc, char **argv )
{
  //######################## Leitura
  typedef itk::ImageSeriesReader  <ImageType>   ReaderType;
  typedef itk::GDCMImageIO  ImageIOType;
  typedef itk::GDCMSeriesFileNames NamesGeneratorType;
  typedef std::vector <std::string> fileNamesContainer;

//  char path[256] = {"/mnt/Backup/Backup_Pivello/mpivello/work/DICOM/OSIRIX_repo/coronarias/TOUTATIX/TOUTATIX/Cardiac 1CTA_CORONARY_ARTERIES_TESTBOLUS (Adult)/Heart w-o  1.5  B25f  55%"};
  char path[256] = {"/home/workspace/DICOM_TEST/heart_ED"};

  if ( argc > 1 )
    {
    cout << "Mudando o diretorio para: " << argv[1] << endl;
    strcpy(path, argv[1]);
    }

  vtkDICOMImageReader* readerVTK =  vtkDICOMImageReader::New();
  readerVTK->SetDirectoryName(path);
  readerVTK->Update();

  double bds[6];
  double origin[3];
  readerVTK->GetOutput()->GetBounds(bds);
  readerVTK->GetOutput()->GetOrigin(origin);

  cout << "VTK Bounds: " << bds[0] << " " << bds[1] << " " << bds[2]
       << " " << bds[3] << " " << bds[4] << " " << bds[5] << endl;
  cout << "VTK origin: " << origin[0] << " " << origin[1] << " " << origin[2] << endl;
  // Classe de IO para imagens
  ImageIOType::Pointer dicomIO = ImageIOType::New();

  // Gerador de séries para os nomes dos arquivos (conjunto DICOM)
  NamesGeneratorType::Pointer nameGenerator = NamesGeneratorType::New();
  nameGenerator->SetUseSeriesDetails(true);
  nameGenerator->SetInputDirectory(path);

  // Container para os nomes gerados
  fileNamesContainer fileNames;
  fileNames = nameGenerator->GetInputFileNames();

  // A leitura é feita neste ponto
  ReaderType::Pointer reader = ReaderType::New();
  reader->SetFileNames( fileNames );
  reader->SetImageIO( dicomIO );
  reader->Update();
  cout << "Leitura efetuada" << endl;

  //######################## Selecionando um slice
  ImageType::RegionType inputRegion = reader->GetOutput()->GetLargestPossibleRegion();
  ImageType::SizeType size = inputRegion.GetSize();
  size[2] = 0; // Setando o eixo de interesse.

  ImageType::IndexType start = inputRegion.GetIndex();
//  const unsigned int sliceNumber = 200;
  start[2] = 4; // Setando o slice de interesse.

   ImageType::RegionType desiredRegion;
    desiredRegion.SetSize(  size  );
      desiredRegion.SetIndex( start );

  typedef itk::ExtractImageFilter< ImageType, ImageType2D > ExtractFilterType;
  ExtractFilterType::Pointer ExtractFilter = ExtractFilterType::New();
  ExtractFilter->SetExtractionRegion( desiredRegion );
  ExtractFilter->SetInput( reader->GetOutput() );
  ExtractFilter->Update();

  //######################## Conector para o Pipeline ITK => VTK
  cout << "Conectando ITK ao VTK..." <<endl;
  ConnectorType::Pointer connector = ConnectorType::New();
//  connector->SetInput( thresholder->GetOutput() );
  connector->SetInput( ExtractFilter->GetOutput() );
  connector->Update();

  vtkRenderer *ren1 = vtkRenderer::New();
  vtkRenderer* ren2 = vtkRenderer::New();
  vtkRenderer* ren3 = vtkRenderer::New();
  vtkRenderWindow *renWin = vtkRenderWindow::New();
  renWin->AddRenderer(ren1);
  renWin->AddRenderer(ren2);
  renWin->AddRenderer(ren3);

  vtkRenderWindowInteractor *iren = vtkRenderWindowInteractor::New();
  iren->SetRenderWindow(renWin);

  vtkImagePlaneWidget* planeWidgetZ = vtkImagePlaneWidget::New();
  planeWidgetZ->SetInteractor( iren);
  planeWidgetZ->SetCurrentRenderer(ren1);
  planeWidgetZ->SetKeyPressActivationValue('z');
//  planeWidgetZ->SetPicker(picker);
  planeWidgetZ->GetPlaneProperty()->SetColor(0,0,1);
//  planeWidgetZ->SetTexturePlaneProperty(ipwProp);
  planeWidgetZ->TextureInterpolateOn();
  planeWidgetZ->SetResliceInterpolateToCubic();
  planeWidgetZ->SetInput(connector->GetOutput());
  planeWidgetZ->SetPlaneOrientationToZAxes();
  planeWidgetZ->SetSliceIndex(53);
//  planeWidgetZ->SetLookupTable( planeWidgetX->GetLookupTable());
  planeWidgetZ->DisplayTextOn();
  planeWidgetZ->On();
  planeWidgetZ->InteractionOff();


  ///////////////////////////////////////////////////////////////////////////////////
  //Filtro sobel com median
  typedef itk::MedianImageFilter<ImageType2D, ImageType2D> MedianImageFilterType;

  MedianImageFilterType::Pointer medianFilter = MedianImageFilterType::New();
  medianFilter->SetInput( ExtractFilter->GetOutput() );

  ImageType2D::SizeType indexRadius;
  indexRadius[0] = 3;
  indexRadius[1] = 3;

  medianFilter->SetRadius( indexRadius );
  medianFilter->Update();

  typedef itk::SobelEdgeDetectionImageFilter<ImageType2D, ImageType2D>   EdgeFilterType;

  EdgeFilterType::Pointer edgeFilter = EdgeFilterType::New();
  edgeFilter->SetInput( medianFilter->GetOutput() );
  edgeFilter->Update();

  ConnectorType::Pointer connector2 = ConnectorType::New();
  connector2->SetInput( edgeFilter->GetOutput() );
  connector2->Update();



  vtkImagePlaneWidget* planeWidget = vtkImagePlaneWidget::New();
  planeWidget->SetInteractor( iren);
  planeWidget->SetCurrentRenderer(ren2);
  planeWidget->SetKeyPressActivationValue('z');
//  planeWidgetZ->SetPicker(picker);
  planeWidget->GetPlaneProperty()->SetColor(0,0,1);
//  planeWidgetZ->SetTexturePlaneProperty(ipwProp);
  planeWidget->TextureInterpolateOn();
  planeWidget->SetResliceInterpolateToCubic();
  planeWidget->SetInput(connector2->GetOutput());
  planeWidget->SetPlaneOrientationToZAxes();
  planeWidget->SetSliceIndex(53);
//  planeWidgetZ->SetLookupTable( planeWidgetX->GetLookupTable());
  planeWidget->DisplayTextOn();
  planeWidget->On();
  planeWidget->InteractionOff();


  //////////////////////////////////////////////////////////////////////////////
  //Filtro sobel
  EdgeFilterType::Pointer edgeFilter2 = EdgeFilterType::New();
  edgeFilter2->SetInput( ExtractFilter->GetOutput() );
  edgeFilter2->Update();

  ConnectorType::Pointer connector3 = ConnectorType::New();
  connector3->SetInput( edgeFilter2->GetOutput() );
  connector3->Update();

  vtkImagePlaneWidget* planeWidget2 = vtkImagePlaneWidget::New();
  planeWidget2->SetInteractor( iren);
  planeWidget2->SetCurrentRenderer(ren3);
  planeWidget2->SetKeyPressActivationValue('z');
//  planeWidgetZ->SetPicker(picker);
  planeWidget2->GetPlaneProperty()->SetColor(0,0,1);
//  planeWidgetZ->SetTexturePlaneProperty(ipwProp);
  planeWidget2->TextureInterpolateOn();
  planeWidget2->SetResliceInterpolateToCubic();
  planeWidget2->SetInput(connector3->GetOutput());
  planeWidget2->SetPlaneOrientationToZAxes();
  planeWidget2->SetSliceIndex(53);
//  planeWidgetZ->SetLookupTable( planeWidgetX->GetLookupTable());
  planeWidget2->DisplayTextOn();
  planeWidget2->On();
  planeWidget2->InteractionOff();


  // Add the actors to the renderer, set the background and size
  //
  ren1->SetBackground(0.2, 0.2, 0.2);
  ren2->SetBackground(0.1, 0.1, 0.1);
  renWin->SetSize(1000, 400);

  ren1->SetViewport(0,0,0.333,1);
  ren2->SetViewport(0.333,0,0.666,1);
  ren3->SetViewport(0.666,0,1,1);
  ren1->ResetCamera();
  ren2->ResetCamera();
  ren3->ResetCamera();

  // render the image
  //
  iren->Initialize();
  renWin->Render();

  iren->Start();


  iren->Delete();
  renWin->Delete();
  ren1->Delete();
  ren2->Delete();
  ren3->Delete();

  planeWidget2->Delete();
  planeWidget->Delete();
  planeWidgetZ->Delete();

  readerVTK->Delete();

  return 0;
}
