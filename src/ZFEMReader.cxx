#include "vtkUnstructuredGridReader.h"
#include "vtkDataSetMapper.h"


#include "vtkDoubleArray.h"
#include "vtkPoints.h"
#include "vtkIdList.h"
#include "vtkUnstructuredGrid.h"
#include "vtkPointData.h"
#include "vtkCellType.h"





#include "vtkRenderWindowInteractor.h"
#include "vtkRenderWindow.h"
#include "vtkRenderer.h"

#include <sstream>
#include <iostream>

using namespace std;



int ReadZFEM(string fileName, vtkUnstructuredGrid *inputGrid)
{
  char caux[256];

  FILE *fp = fopen(fileName.c_str(), "r");
  if(!fp)
    {
    printf("\nError Reading File!!!\n");
    return 1;
    }

  //  cout << "Lendo cabeçalho.." <<endl;
  fscanf(fp, "%s", caux);
  //  cout << caux <<endl;
  fscanf(fp, "%s", caux);
  //  cout << caux <<endl;
  fscanf(fp, "%s", caux);
  //  cout << caux <<endl;

  int numberOfPoints;
  fscanf(fp, "%d", &numberOfPoints);
  //  cout << numberOfPoints <<endl;

  vtkPoints *points = vtkPoints::New();

  float point[3];
  for(int i=0; i < numberOfPoints; ++i)
    {
    fscanf(fp, "%g %g %g", &point[0], &point[1], &point[2]);
    //    cout << point[0] << " " << point[1] << " " << point[2] <<endl;

    points->InsertNextPoint( point );
    }

  vtkUnstructuredGrid *outputGrid = vtkUnstructuredGrid::New();

  outputGrid->SetPoints( points );





  //  cout << "Lendo propriedades dos pontos.." <<endl;
  fscanf(fp, "%s", caux);
  //  cout << caux <<endl;
  fscanf(fp, "%s", caux);
  //  cout << caux <<endl;

  vtkDoubleArray *array = vtkDoubleArray::New();
  array->SetName( caux );
  //  cout << "array->GetName(): " << array->GetName() <<endl;

  int i;
  fscanf(fp, "%d", &i);
  //  cout << i <<endl;
  array->SetNumberOfComponents( i );
  //  cout << "array->GetNumberOfComponents(): " << array->GetNumberOfComponents() <<endl;

  float *tuple = new float[i];
  for(int j=0; j < numberOfPoints; ++j)
    {
    for(int k=0; k < i; ++k)
      {
      fscanf(fp, "%g", &tuple[k]);
//      cout << tuple[k] << endl;
      }
    array->InsertNextTuple( tuple );
    }
//  cout << "array->GetNumberOfTuples(): " << array->GetNumberOfTuples() <<endl;

  delete [] tuple;

  outputGrid->GetPointData()->AddArray( array );





//  cout << "Lendo conectividade dos pontos.." <<endl;
  fscanf(fp, "%s", caux);
//  cout << caux <<endl;
  fscanf(fp, "%s", caux);
//  cout << caux <<endl;
  fscanf(fp, "%s", caux);
//  cout << caux <<endl;
  fscanf(fp, "%d", &i);
//  cout << i <<endl;

  vtkIdList *idList = vtkIdList::New();
  idList->SetNumberOfIds( 2 );

  int p0, p1;
  for(int j=0; j < i; ++j)
    {
    fscanf(fp, "%d %d", &p0, &p1);

    // os índices de cada ponto nos arquivos começam em 1, apenas ajustando.
    idList->SetId(0, p0-1);
    idList->SetId(1, p1-1);

//    cout << "idList->GetId(0): " << idList->GetId(0) <<endl;
//    cout << "idList->GetId(1): " << idList->GetId(1) <<endl;

    outputGrid->InsertNextCell(VTK_LINE, idList);
    }

  inputGrid->DeepCopy( outputGrid );
  inputGrid->Update();


  idList->Delete();
  array->Delete();
  points->Delete();
  outputGrid->Delete();

  fclose(fp);

  return 0;
}



//string imGUIDICOMDIROpenInterface::GetInitials(const char *name)
//{
//  if (this->DebugMode)
//    imInfo("  >> \"const char * imGUIDICOMDIROpenInterface::GetInitials(const char *name)\"");
//
//  string str = name;
//  string initials;
//
//
//  // remove espaços em branco no final do nome
//  string c;
//  c = str.at(str.size()-1);
//  while( c.compare(" ") == 0 )
//    {
//    str.resize(str.size()-1);
//    c = str.at(str.size()-1);
//    }
//
//
//  // inicial do primeiro nome
//  initials = str.at(0);
//  initials += ".";
//
//
//  // inicial do último nome
//  for(unsigned int i = str.size()-1; i > 0; i--)
//    {
//    string current, control;
//    current = str.at(i);
//
//    if( current.compare(" ") == 0)
//      {
//      char c = str.at(i+1);
//
//      initials += c;
//      initials += ".";
//      break;
//      }
//    }
//
//
//
////  for(unsigned int i=1; i < str.size(); i++)
////    {
////    string current, control;
////    current = str.at(i);
////
////    if( current.compare(" ") == 0)
////      {
////      control = str.at(i+2);
////
////      if(control.compare(".") == 0) // nome abreviado
////        {
////        char c = str.at(i+1);
////        initials += c;
////        initials += ".";
////        i += 2;
////        continue;
////        }
////
////
////      control.clear();
////      control = str.at(i+3);
////      if( control.compare(" ") == 0 ) // preposição encontrada
////        {
////        i += 2;
////        continue;
////        }
////
////
////      char c = str.at(i+1);
////      initials += c;
////      initials += ".";
////      }
////    }
//
//  return initials;
//}





//-----------------------------------------------------------------------------
int main( int argc, char **argv )
{
//  string fileName = "/home/eduardo/workspace/data_HeMoLab/Cebral/BH0033_original/spln/acom_scaled.swc_spln.zfem";
//  string fileName = "/home/eduardo/workspace/data_HeMoLab/Cebral/BH0033_original/spln/A_tree.swc_spln.zfem";
//  string fileName = "/home/eduardo/workspace/data_HeMoLab/Cebral/BH0033_original/spln/B_tree.swc_spln.zfem";
//  string fileName = "/home/eduardo/workspace/data_HeMoLab/Cebral/BH0033_original/spln/C_tree.swc_spln.zfem";
//  string fileName = "/home/eduardo/workspace/data_HeMoLab/Cebral/BH0033_original/spln/D_tree.swc_spln.zfem";
//  string fileName = "/home/eduardo/workspace/data_HeMoLab/Cebral/BH0033_original/spln/E_tree.swc_spln.zfem";
//  string fileName = "/home/eduardo/workspace/data_HeMoLab/Cebral/BH0033_original/spln/F_tree.swc_spln.zfem";
  string fileName = "/home/eduardo/workspace/data_HeMoLab/Cebral/BH0033_original/spln/tree.swc_spln.zfem";

  vtkUnstructuredGrid *grid = vtkUnstructuredGrid::New();

  if( ReadZFEM( fileName, grid ) )
    {
    cout << "Verificar leitor." <<endl;

    grid->Delete();

    return 1;
    }


  vtkDataSetMapper *mapper = vtkDataSetMapper::New();
  mapper->SetInput( grid );


  vtkActor *actor = vtkActor::New();
  actor->SetMapper( mapper );


  vtkRenderer *renderer = vtkRenderer::New();
  renderer->AddActor( actor );
//    renderer->SetBackground(0.4,0.4,0.5);

  vtkRenderWindow *renWin = vtkRenderWindow::New();
  renWin->AddRenderer(renderer);

  vtkRenderWindowInteractor *iren = vtkRenderWindowInteractor::New();
  iren->SetRenderWindow(renWin);
  renWin->SetSize(1024, 768);

  renderer->Render();

  iren->Initialize();
  iren->Start();


  mapper->Delete();
  actor->Delete();
  renderer->Delete();
  renWin->Delete();
  iren->Delete();


  cout << "-- Used parameters --" << endl;
  cout << "File: " << fileName <<endl;

  cout << "\n-- Built grid --" << endl;
  cout << *grid <<endl;


  grid->Delete();


  return 0;
}
