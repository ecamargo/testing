#include "vtkRenderWindowInteractor.h"
#include "vtkRenderWindow.h"
#include "vtkRenderer.h"

#include "vtkSmartPointer.h"
#include "vtkMath.h"
#include "vtkSphereSource.h"
#include "vtkPolyData.h"
#include "vtkPolyDataMapper.h"
#include "vtkActor.h"
#include "vtkProperty.h"
#include "vtkIdList.h"
#include "vtkCellArray.h"

using namespace std;



//-----------------------------------------------------------------------------
int main( int argc, char **argv )
{
  double backgroundColor[] = {0.8, 0.8, 0.8};

  // criando o cubo com os vertices dados
  vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();

  //seta os pontos no cubo
  vtkSmartPointer<vtkPolyData> cube = vtkSmartPointer<vtkPolyData>::New();
  cube->SetPoints( points );
  points->Delete();


  cube->GetPoints()->InsertNextPoint( 4.10604, -159.783, -446.84 );
  cube->GetPoints()->InsertNextPoint( 97.6315, -162.907, -448.467 );
  cube->GetPoints()->InsertNextPoint( 101.103, -63.4827, -439.764 );
  cube->GetPoints()->InsertNextPoint( 7.57801, -60.3592, -438.136 );
  cube->GetPoints()->InsertNextPoint( 4.74339, -163.665, -402.757 );
  cube->GetPoints()->InsertNextPoint( 98.2689, -166.788, -404.384 );
  cube->GetPoints()->InsertNextPoint( 101.741, -67.3642, -395.68 );
  cube->GetPoints()->InsertNextPoint( 8.21536, -64.2406, -394.053 );


  // cria cada uma das 6 faces do cubo
  vtkSmartPointer<vtkCellArray> cells = vtkSmartPointer<vtkCellArray>::New();
  vtkSmartPointer<vtkIdList> idList = vtkSmartPointer<vtkIdList>::New();

  idList->InsertNextId(0);
  idList->InsertNextId(1);
  idList->InsertNextId(2);
  idList->InsertNextId(3);
  cells->InsertNextCell( idList );
  idList->Reset();

  idList->InsertNextId(0);
  idList->InsertNextId(3);
  idList->InsertNextId(7);
  idList->InsertNextId(4);
  cells->InsertNextCell( idList );
  idList->Reset();

  idList->InsertNextId(0);
  idList->InsertNextId(1);
  idList->InsertNextId(5);
  idList->InsertNextId(4);
  cells->InsertNextCell( idList );
  idList->Reset();

  idList->InsertNextId(3);
  idList->InsertNextId(2);
  idList->InsertNextId(6);
  idList->InsertNextId(7);
  cells->InsertNextCell( idList );
  idList->Reset();

  idList->InsertNextId(1);
  idList->InsertNextId(2);
  idList->InsertNextId(6);
  idList->InsertNextId(5);
  cells->InsertNextCell( idList );
  idList->Reset();

  idList->InsertNextId(4);
  idList->InsertNextId(5);
  idList->InsertNextId(6);
  idList->InsertNextId(7);
  cells->InsertNextCell( idList );
  idList->Reset();

  cube->SetPolys( cells );
  cube->Update();

  double center[3];
  cube->GetCenter( center );

  double radius = 0;
  for(int i=0; i < 8; ++i)
    {
    double tmp = sqrt( vtkMath::Distance2BetweenPoints( center,  cube->GetPoints()->GetPoint(i) ) );
    cout << "tmp: " << tmp << " \tradius: " << radius <<endl;

    if(tmp > radius) radius = tmp;
    }


  vtkSmartPointer<vtkSphereSource> sph = vtkSmartPointer<vtkSphereSource>::New();
  sph->SetCenter( center );
  sph->SetRadius( radius );
  sph->SetThetaResolution(100);
  sph->SetPhiResolution(100);
  sph->Update();




  vtkSmartPointer<vtkPolyDataMapper> mapperCube = vtkSmartPointer<vtkPolyDataMapper>::New();
  mapperCube->SetInput( cube );

  vtkSmartPointer<vtkActor> actorCube = vtkSmartPointer<vtkActor>::New();
  actorCube->SetMapper( mapperCube );
  actorCube->GetProperty()->SetRepresentationToWireframe();
  actorCube->GetProperty()->SetColor( 1.0, 0.0, 0.0 );
//  actorCube->GetProperty()->SetOpacity(0.1);


  vtkSmartPointer<vtkPolyDataMapper> mapperSphere = vtkSmartPointer<vtkPolyDataMapper>::New();
  mapperSphere->SetInput( sph->GetOutput() );

  vtkSmartPointer<vtkActor> actorSphere = vtkSmartPointer<vtkActor>::New();
  actorSphere->SetMapper( mapperSphere );
  actorSphere->GetProperty()->SetRepresentationToWireframe();
  actorSphere->GetProperty()->SetOpacity(0.1);
    




  vtkSmartPointer<vtkRenderer> renderer = vtkSmartPointer<vtkRenderer>::New();
  renderer->AddActor( actorCube );
  renderer->AddActor( actorSphere );


  renderer->SetBackground( backgroundColor );

  vtkSmartPointer<vtkRenderWindow> renWin = vtkSmartPointer<vtkRenderWindow>::New();
  renWin->AddRenderer(renderer);

  vtkSmartPointer<vtkRenderWindowInteractor> iren = vtkSmartPointer<vtkRenderWindowInteractor>::New();
  iren->SetRenderWindow(renWin);
  renWin->SetSize(1024, 768);


  renderer->Render();
  iren->Initialize();
  iren->Start();
}

