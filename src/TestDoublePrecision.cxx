
#include <vtkSmartPointer.h>
#include <vtkPolyData.h>
#include <vtkPolyDataReader.h>
#include <vtkPolyDataWriter.h>
#include <vtkPolyDataMapper.h>
#include <vtkActor.h>
#include <vtkRenderWindow.h>
#include <vtkRenderer.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkProperty.h>
#include <vtkPointData.h>
#include <limits>

#include <iostream>
#include <float.h>
using namespace std;

//-----------------------------------------------------------------------------
int main( int argc, char **argv )
{
  string lineFile = "/home/eduardo/workspace/data_HeMoLab/testPrecision.vtk";


  //Reading centerline
  vtkSmartPointer<vtkPolyDataReader> lineReader = vtkSmartPointer<vtkPolyDataReader>::New();
  lineReader->SetFileName( lineFile.c_str() );
  lineReader->Update();

  vtkSmartPointer<vtkPolyData> centerline = vtkSmartPointer<vtkPolyData>::New();
  centerline->DeepCopy( lineReader->GetOutput() );
  centerline->Update();

  typedef std::numeric_limits< double > dbl;
  cout << std::setprecision(dbl::digits10);

  double p[3];
  centerline->GetPoints()->GetPoint(0, p);
  cout << p[0] << ", " << p[1] << ", " << p[2] << endl;

  char str[1024];
  double d = -0.5012345678901234;
  sprintf (str, "%.16f", d);
  printf("%s\n", str);

  printf("%lf, %lg, %lg \n", p[0], p[1], p[2]);


  centerline->GetPoints()->GetPoint(1, p);
  cout << p[0] << ", " << p[1] << ", " << p[2] << endl;


  //Setting output
  vtkSmartPointer<vtkPolyDataMapper> mapperLine = vtkSmartPointer<vtkPolyDataMapper>::New();
  mapperLine->SetInput( lineReader->GetOutput() );

  vtkSmartPointer<vtkActor> actorLine = vtkSmartPointer<vtkActor>::New();
  actorLine->SetMapper( mapperLine );


  // Create a renderer, render window, and interactor
  vtkSmartPointer<vtkRenderer> renderer = vtkSmartPointer<vtkRenderer>::New();
  vtkSmartPointer<vtkRenderWindow> renderWindow = vtkSmartPointer<vtkRenderWindow>::New();
  renderWindow->AddRenderer(renderer);
  renderWindow->SetSize(1024, 768);

  vtkSmartPointer<vtkRenderWindowInteractor> renderWindowInteractor = vtkSmartPointer<vtkRenderWindowInteractor>::New();
  renderWindowInteractor->SetRenderWindow(renderWindow);
 
  // Add the actor to the scene
  renderer->AddActor(actorLine);

  // Render and interact
  renderWindow->Render();
  renderWindowInteractor->Start();


  return 0;
}


