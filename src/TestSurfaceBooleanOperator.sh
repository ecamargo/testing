#BOOLEAN_OPERATOR

ANEURISK_DIR=/home/eduardo/workspace/data_HeMoLab/Aneurisk

CASE_NAME=c0023
WARP_VALUE=0.01

ORIGINAL=$ANEURISK_DIR/$CASE_NAME/surface/original_clipped_refined.vtk
PARENTVESSEL=$ANEURISK_DIR/$CASE_NAME/$WARP_VALUE/parentVesselWarp.vtk
OUTPUT_DIR=$ANEURISK_DIR/$CASE_NAME/

cd /home/eduardo/workspace/Testing/build/

#echo $ORIGINAL
#echo $PARENTVESSEL
#echo $OUTPUT_DIR

./bin/TestSurfaceBooleanOperators $ORIGINAL $PARENTVESSEL $OUTPUT_DIR


