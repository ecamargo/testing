#include "itkImageSeriesReader.h"
#include "itkGDCMSeriesFileNames.h"
#include "itkGDCMImageIO.h"
#include "itkImage.h"
#include "itkImageToVTKImageFilter.h"
#include "itkVTKImageToImageFilter.h"

#include "vtkImageViewer2.h"
#include "vtkRenderWindowInteractor.h"
#include "vtkRenderWindow.h"
#include "vtkRenderer.h"

#include "itkBinaryThresholdImageFilter.h"


using namespace std;

typedef itk::Image	<float, 3>	ImageType;
typedef itk::ImageToVTKImageFilter	<ImageType>		ConnectorType;



//-----------------------------------------------------------------------------
void Reslice(ConnectorType *connector, bool x, bool y, bool z)
{
  vtkRenderWindowInteractor *iren = vtkRenderWindowInteractor::New();

  vtkImageViewer2 * viewer = vtkImageViewer2::New();
    viewer->SetupInteractor( iren );
    viewer->SetInput( connector->GetOutput() );
    viewer->GetRenderWindow()->SetSize(800,600);
    viewer->Render();
    viewer->SetColorWindow( 256 );
    viewer->SetColorLevel( 144 );
    viewer->SetSlice(124);

  if(x)
    {
    viewer->SetSliceOrientationToXY(); // orientação em X
    }
  else if(y)
    {
    viewer->SetSliceOrientationToYZ(); // orientação em Y
    }
  else if(z)
    {
    viewer->SetSliceOrientationToXZ(); // orientação em Z
    }

  viewer->GetRenderer()->ResetCamera();

  iren->Start();

  iren->Delete();
  viewer->Delete();
}


//-----------------------------------------------------------------------------
int main( int argc, char **argv )
{
  if ( argc != 3 )
  {
    cerr << "Usage: " << argv[0] << "Input directory with DICOM files. " << "Input axis to view (0 to X, 1 to Y or 2 to Z)." << endl;
    return EXIT_FAILURE;
  }
  
  char path[256];
  stringstream s(argv[1]);
  s >> path;
  
  int axis; 
  stringstream ss(argv[2]);
  ss >> axis;
  
  cout << "-- Used parameters --" << endl;
  cout << "Path: " << path <<endl;
  cout << "Axis to view: " << axis <<endl;
  cout << "\n" <<endl;
  
  
  //######################## Leitura
  typedef itk::ImageSeriesReader	<ImageType>		ReaderType;
  typedef itk::GDCMImageIO	ImageIOType;
  typedef itk::GDCMSeriesFileNames NamesGeneratorType;
  typedef std::vector <std::string> fileNamesContainer;
  

  
  // Classe de IO para imagens
  ImageIOType::Pointer dicomIO = ImageIOType::New();
	
  // Gerador de séries para os nomes dos arquivos (conjunto DICOM)	
  NamesGeneratorType::Pointer nameGenerator = NamesGeneratorType::New();
  nameGenerator->SetUseSeriesDetails(true);
  nameGenerator->SetInputDirectory(path);
	
  // Container para os nomes gerados	
  fileNamesContainer fileNames;
  fileNames = nameGenerator->GetInputFileNames();

  // A leitura é feita neste ponto 
  ReaderType::Pointer reader = ReaderType::New();
  reader->SetFileNames( fileNames );
  reader->SetImageIO( dicomIO );
  reader->Update();
  cout << "Leitura efetuada" << endl;
 
	  
  //######################## Filtro
    cout << "Aplicando filtros..." << endl;
	  
  typedef itk::BinaryThresholdImageFilter< ImageType, ImageType    >    ThresholdingFilterType;  
  ThresholdingFilterType::Pointer thresholder = ThresholdingFilterType::New();                   
	  thresholder->SetLowerThreshold( -1000.0 );
	  thresholder->SetUpperThreshold( 0.0 );
	  thresholder->SetOutsideValue( 255 );
	  thresholder->SetInsideValue( 0 );
    thresholder->SetInput( reader->GetOutput() );

  try
    {
  	thresholder->Update();
    }
  catch( itk::ExceptionObject & excep )
    {
    std::cerr << "Exception caught !" << std::endl;
    std::cerr << excep << std::endl;
    }
 
	
  //######################## Conector para o Pipeline ITK => VTK	
  cout << "Conectando ITK ao VTK..." <<endl;
  ConnectorType::Pointer connector = ConnectorType::New();
  connector->SetInput( thresholder->GetOutput() );
  
    
  //######################## Escolhe o eixo de visualização   
  if(axis==0)
    {
//     cout << "Visualizando em X.\n" <<endl;
    Reslice(connector, true, false, false); 
    }
    else if(axis==1)
    {
//     cout << "Visualizando em Y.\n" <<endl;      
    Reslice(connector, false, true, false); 
    }
    else if(axis==2)
    {
//     cout << "Visualizando em Z.\n" <<endl;      
    Reslice(connector, false, false, true); 
    }
    else
    {
    printf("Eixo inválido. Configurando em X\n");
    Reslice(connector, true, false, false); 
    }
       
    
  return 0;
}


