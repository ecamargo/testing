
#include <iostream>
#include <fstream>


using namespace std;


int OpenFile( string fileName,  ofstream *xmlFile )
{
  xmlFile->open ( fileName.c_str(), ios::trunc);

  if( !xmlFile->is_open() )
    return 1;

  xmlFile->setf(ios::fixed,ios::floatfield);
  *xmlFile << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" <<endl;

  return 0;
}

void CloseFile( ofstream *xmlFile )
{
  xmlFile->close();
}

void OpenRoot( ofstream *xmlFile, string rootName )
{
  *xmlFile << "<" << rootName << ">" <<endl;
}

void CloseRoot( ofstream *xmlFile, string rootName )
{
  *xmlFile << "</" << rootName << ">" <<endl;
}

void OpenChild( ofstream *xmlFile, string rootName, int indent )
{
  string idt;
  for(int i=0; i < indent; ++i)
    {
    idt.append("\t");
    }

  *xmlFile << idt << "<" << rootName << ">" <<endl;
}

void CloseChild( ofstream *xmlFile, string rootName, int indent )
{
  string idt;
  for(int i=0; i < indent; ++i)
    {
    idt.append("\t");
    }

  *xmlFile << idt << "</" << rootName << ">" <<endl;
}

void WriteTag( ofstream *xmlFile, int indent, string tagName, double value )
{
  string idt;
  for(int i=0; i < indent; ++i)
    {
    idt.append("\t");
    }

  *xmlFile << idt <<"<" << tagName << ">" << value << "</" << tagName << ">" <<endl;
}

void WriteTag( ofstream *xmlFile, int indent, string tagName, string value )
{
  string idt;
  for(int i=0; i < indent; ++i)
    {
    idt.append("\t");
    }

  *xmlFile << idt <<"<" << tagName << ">" << value << "</" << tagName << ">" <<endl;
}

//-----------------------------------------------------------------------------
int main( int argc, char **argv )
{
  string fileName ="/home/eduardo/workspace";

  //identificando a barra que separa o nome do caso
  int pos;
  for (int i=fileName.length(); i > 0; --i)
    {
    if( (fileName.compare(i,1,"/") == 0) ||  (fileName.compare(i,1,"\\") == 0) )
      {
      pos = i+1;
      break;
      }
    }

  // recuperando o nome do caso
  string caseName = fileName.substr(pos,fileName.length());

  fileName.append( "/HMAneurysmPropertiesFilter.xml" );

  ofstream XMLFile;
       
  if( OpenFile( fileName, &XMLFile ) )
    return 1;


  string root = "Case";
  OpenRoot( &XMLFile, root );


  int level = 1;
  WriteTag( &XMLFile, level, "Name", caseName  );
  WriteTag( &XMLFile, level, "Description", "description text"  );


  level = 2;
  string parameters = "Parameters";
  OpenChild( &XMLFile, parameters, level-1 );

  WriteTag( &XMLFile, level, "AneurysmSize", 0.1  );
  WriteTag( &XMLFile, level, "AverageAspectRatio", 0.1  );
  WriteTag( &XMLFile, level, "MaximumAspectRatio", 0.1  );
  WriteTag( &XMLFile, level, "MinimumAspectRatio", 0.1  );
  WriteTag( &XMLFile, level, "InputAneurysmToVesselSizeRatio", 0.1  );
  WriteTag( &XMLFile, level, "OutputAneurysmToVesselSizeRatio", 0.1  );
  WriteTag( &XMLFile, level, "AneurysmUndulationIndex", 0.1  );
  WriteTag( &XMLFile, level, "AneurysmEllipticityIndex", 0.1  );
  WriteTag( &XMLFile, level, "AneurysmNonsphericityIndex", 0.1  );
  WriteTag( &XMLFile, level, "AneurysmVesselAngle", 0.1  );
  WriteTag( &XMLFile, level, "AneurysmInclinationAngle", 0.1  );
  WriteTag( &XMLFile, level, "WallThinningRatio", 0.1  );


  if( 1 )
    {
    level = 3;
    string auxiliarParameters = "AuxiliarParameters";
    OpenChild( &XMLFile, auxiliarParameters, level-1 );

    WriteTag( &XMLFile, level, "AneurysmVolume", 0.1  );
    WriteTag( &XMLFile, level, "VolumeOfConvexHull", 0.1  );
    WriteTag( &XMLFile, level, "AreaOfConvexHull", 0.1  );
    WriteTag( &XMLFile, level, "AneurysmSackSurface", 0.1  );
    WriteTag( &XMLFile, level, "AneurysmNeckSurface", 0.1  );
    WriteTag( &XMLFile, level, "AneurysmSurface", 0.1  );
    WriteTag( &XMLFile, level, "AverageNeckDiameter", 0.1  );
    WriteTag( &XMLFile, level, "MaximumNeckRadius", 0.1  );
    WriteTag( &XMLFile, level, "MinimumNeckRadius", 0.1  );
    WriteTag( &XMLFile, level, "MaximumHeight", 0.1  );
    WriteTag( &XMLFile, level, "DiameterD1", 0.1  );
    WriteTag( &XMLFile, level, "DiameterD2", 0.1  );
    WriteTag( &XMLFile, level, "DiameterD3", 0.1  );
    WriteTag( &XMLFile, level, "DiameterD4", 0.1  );
    WriteTag( &XMLFile, level, "DiameterD1", 0.1  );
    WriteTag( &XMLFile, level, "DiameterD2", 0.1  );
    WriteTag( &XMLFile, level, "DiameterD3", 0.1  );
    WriteTag( &XMLFile, level, "DiameterD4", 0.1  );

    CloseChild( &XMLFile, auxiliarParameters, 2 );
    }

  CloseChild( &XMLFile, parameters, 1 );

  CloseRoot( &XMLFile, root );

  CloseFile( &XMLFile );


  return 0;
}



