#include <vtkPolyDataMapper.h>
#include <vtkActor.h>
#include <vtkRenderWindow.h>
#include <vtkRenderer.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkPolyData.h>
#include <vtkSmartPointer.h>
#include <vtkSphereSource.h>
#include <vtkCaptionWidget.h>
#include <vtkCaptionRepresentation.h>
#include <vtkCaptionActor2D.h>
#include <vtkTextActor.h>
#include <vtkTextProperty.h>
#include <vtkPointHandleRepresentation3D.h>
#include <vtkProperty2D.h>


int main(int, char *[])
{
  // Sphere
  vtkSmartPointer<vtkSphereSource> sphereSource =
    vtkSmartPointer<vtkSphereSource>::New();
  sphereSource->Update();

  vtkSmartPointer<vtkPolyDataMapper> mapper =
    vtkSmartPointer<vtkPolyDataMapper>::New();
  mapper->SetInputConnection(sphereSource->GetOutputPort());

  vtkSmartPointer<vtkActor> actor =
    vtkSmartPointer<vtkActor>::New();
  actor->SetMapper(mapper);

  // A renderer and render window
  vtkSmartPointer<vtkRenderer> renderer =
    vtkSmartPointer<vtkRenderer>::New();
  vtkSmartPointer<vtkRenderWindow> renderWindow =
    vtkSmartPointer<vtkRenderWindow>::New();
  renderWindow->AddRenderer(renderer);

  // An interactor
  vtkSmartPointer<vtkRenderWindowInteractor> renderWindowInteractor =
    vtkSmartPointer<vtkRenderWindowInteractor>::New();
  renderWindowInteractor->SetRenderWindow(renderWindow);


  std::string s = "1-(AV/Vch) ";
  std::string s1= "ABCDEFGHIJ ";

  double color[3] = {1.0, 0.0, 0.0};

  vtkSmartPointer<vtkTextProperty> captionProperty = vtkSmartPointer<vtkTextProperty>::New();
  captionProperty->SetColor( color );
  captionProperty->SetBold(0);
  captionProperty->SetItalic(0);
  captionProperty->SetShadow(0);
  captionProperty->SetFontSize(100);
  captionProperty->SetFontFamily(VTK_ARIAL);
  captionProperty->SetJustification(VTK_TEXT_CENTERED);
  captionProperty->SetVerticalJustification(VTK_TEXT_CENTERED);

  // Create the widget and its representation
  vtkSmartPointer<vtkCaptionRepresentation> captionRepresentation = vtkSmartPointer<vtkCaptionRepresentation>::New();
  captionRepresentation->GetCaptionActor2D()->SetCaption( s.c_str() );
//  captionRepresentation->GetCaptionActor2D()->SetCaptionTextProperty( captionProperty );

//  captionRepresentation->GetCaptionActor2D()->GetCaptionTextProperty()->SetColor( color );
  captionRepresentation->GetCaptionActor2D()->GetCaptionTextProperty()->SetBold(0);
  captionRepresentation->GetCaptionActor2D()->GetCaptionTextProperty()->SetItalic(0);
  captionRepresentation->GetCaptionActor2D()->GetCaptionTextProperty()->SetShadow(0);
  captionRepresentation->GetCaptionActor2D()->GetCaptionTextProperty()->SetFontSize(20);
  captionRepresentation->GetCaptionActor2D()->GetCaptionTextProperty()->SetFontFamily(VTK_ARIAL);
  captionRepresentation->GetCaptionActor2D()->GetCaptionTextProperty()->SetJustification(VTK_TEXT_CENTERED);
  captionRepresentation->GetCaptionActor2D()->GetCaptionTextProperty()->SetVerticalJustification(VTK_TEXT_CENTERED);

  captionRepresentation->GetCaptionActor2D()->LeaderOn();
//  captionRepresentation->GetCaptionActor2D()->SetLeaderGlyphSize(0.1);
  captionRepresentation->GetCaptionActor2D()->SetAttachmentPoint( sphereSource->GetCenter() );
  captionRepresentation->GetCaptionActor2D()->GetProperty()->SetColor( color );
  captionRepresentation->SetPosition( 0.01, 0.5);





//  double *pos, size[2], bound[6];
//
//  sphereSource->GetOutput()->GetBounds( bound );
//  cout << "bound: " << bound[0] << ", " << bound[1] << ", " << bound[2] << ", "
//                    << bound[3] << ", " << bound[4] << ", " << bound[5] <<endl;
//
//  pos = captionRepresentation->GetPosition();
//  cout << "pos: " << pos[0] << ", " << pos[1]  <<endl;
//
//  pos = captionRepresentation->GetPosition2();
//  cout << "pos2: " << pos[0] << ", " << pos[1]  <<endl;
//
//  captionRepresentation->GetSize( size );
//  cout << "size: " << size[0] << ", " << size[1]  <<endl;

//  cout << *captionRepresentation <<endl;
  cout << captionRepresentation->GetCaptionActor2D()->GetLeaderGlyphSize() <<endl;




  vtkSmartPointer<vtkCaptionWidget> captionWidget = vtkSmartPointer<vtkCaptionWidget>::New();
  captionWidget->SetInteractor(renderWindowInteractor);
  captionWidget->SetRepresentation(captionRepresentation);



  // Add the actors to the scene
  renderer->AddActor(actor);
  renderer->SetBackground(0.1,0.2,0.4);

  renderWindow->SetSize(800,600);
  renderWindow->Render();


  captionWidget->On();
  captionWidget->Render();


  // Begin mouse interaction
  renderWindowInteractor->Start();

  return EXIT_SUCCESS;
}
