#BOOLEAN_OPERATOR

ANEURISK_DIR=/home/nfs/aneurysm_project/morphIndexes/1287259/26896/ANGIOGRAFIA_5/volumetricImage_1/segmentationID_1/meshID_1/morphIdexes/massTests

ORIGINAL=$ANEURISK_DIR/aneurysmNeck.vtk
PARENTVESSEL=$ANEURISK_DIR/sack_more_triangle2.vtk
OUTPUT_DIR=$ANEURISK_DIR/

cd ../build/bin
./TestSurfaceBooleanOperators $ORIGINAL $PARENTVESSEL $OUTPUT_DIR
cd -

