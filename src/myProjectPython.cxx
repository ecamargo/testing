#include <Python.h>
#include <string>



//-----------------------------------------------------------------------------
int initialTest( int argc, char **argv )
{
  setenv("PYTHONPATH","/home/eduardo/workspace/Testing/src/",1);

  PyObject *pName, *pModule, *pDict, *pClass, *pInstance, *pValue;
  int i, arg[8];


  if (argc < 4)
    {
    fprintf(stderr,"Usage: call python_filename class_name function_name\n");
    return 1;
    }
  for (i = 0; i < argc; i++)
    {
    printf("%s\n", argv[i]);
    }

  Py_Initialize();

  // Build the name object
  if ((pName = PyString_FromString(argv[1])) == NULL)
    {
    printf("Error: PyString_FromString\n");
    return -1;
    }


  // Load the module object
  if ((pModule = PyImport_Import(pName)) == NULL)
    {
    printf("Error: PyImport_Import\n");
    return -1;
    }

  // pDict is a borrowed reference
  if ((pDict = PyModule_GetDict(pModule))==NULL)
    {
    printf("Error: PyModule_GetDict\n");
    return -1;
    }


  // Build the name of a callable class
  pClass = PyDict_GetItemString(pDict, argv[2]);

  // Create an instance of the class
  if (PyCallable_Check(pClass))
    {
    pInstance = PyObject_CallObject(pClass, NULL);
    }

  // Build parameter list
  if( argc > 4 )
    {
      for (i = 0; i < argc - 4; i++)
      {
      arg[i] = atoi(argv[i + 4]);
      }
    // Call a method of the class with two parameters
    pValue = PyObject_CallMethod(pInstance, argv[3], "(ii)", arg[0], arg[1]);

    }
  else
    {
    // Call a method of the class with no parameters
    pValue = PyObject_CallMethod(pInstance, argv[3], NULL);
    }

  if (pValue != NULL)
    {
    printf("Return of call : %ld\n", PyInt_AsLong(pValue));
    Py_DECREF(pValue);
    }
  else
    {
    PyErr_Print();
    }
  
  // Clean up
  Py_DECREF(pModule);
  Py_DECREF(pName);
  Py_Finalize();

  return 0;
}

//-----------------------------------------------------------------------------
int scriptTest( std::string scritpName, std::string parameters )
{
//  setenv("PYTHONPATH","/home/eduardo/workspace/HeMoLab3D/build/utilities/vmtk-1.0.1/vmtkScripts/",1);

  Py_Initialize();

//  scritpName = "vmtkmeshgenerator";
//  parameters = "-ifile /home/eduardo/workspace/data_HeMoLab/modelos/3D/carotida_furada.vtp -ofile /home/eduardo/workspace/data_HeMoLab/modelos/3D/carotida_furada_teste.vtu -edgelength 0.5";
//
//  std::string head = "import os\nos.system(\"/home/eduardo/workspace/libs/vmtk/build/Install/bin/";
//  std::string  script = head + scritpName + " " + parameters + "\")\n";
//  int result = PyRun_SimpleString(script.c_str());

  const char* pythonScript = "import os\nos.system(\"/home/eduardo/workspace/libs/vmtk/build/Install/bin/vmtkmeshgenerator -ifile /home/eduardo/workspace/data_HeMoLab/modelos/3D/carotida_furada.vtp -ofile /home/eduardo/workspace/data_HeMoLab/modelos/3D/carotida_furada_teste.vtu -edgelength 0.5\")\n";
  int result = PyRun_SimpleString(pythonScript);

  Py_Finalize();

  printf("%s", pythonScript);
//  printf("%s", script.c_str());

  return result;
}




//-----------------------------------------------------------------------------
int main( int argc, char **argv )
{
//  int ret = initialTest( argc, argv );
//  printf("\t >>>> initialTest end!!\n\n");

  int ret = scriptTest( "","" );


  return ret;
}


