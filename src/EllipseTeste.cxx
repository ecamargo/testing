/*
 * EllipseTeste.cxx
 *
 *  Created on: 16/11/2010
 *      Author: igor
 */

#include "vtkRenderer.h"
#include "vtkRenderWindow.h"
#include "vtkRenderWindowInteractor.h"
#include "vtkDataSetMapper.h"
#include "vtkProperty.h"
#include "vtkDICOMImageReader.h"
#include "vtkImageData.h"
#include "vtkImageOrthoPlanes.h"
#include "vtkImagePlaneWidget.h"
#include "vtkOutlineFilter.h"
#include "vtkPolyDataMapper.h"
#include "vtkCellPicker.h"
#include "vtkPolyData.h"
#include "vtkParametricEllipsoid.h"
#include "vtkParametricFunctionSource.h"
#include "vtkTransform.h"

#include "vtkSuperquadricSource.h"
#include "vtkCommand.h"
#include "vtkBoxWidget.h"

#include <string.h>

using namespace std;


class BoxWidgetCallback : public vtkCommand
{
public:
  static BoxWidgetCallback *New()
    { return new BoxWidgetCallback; }
  virtual void Execute(vtkObject *caller, unsigned long, void*)
    {
    double bds[6];
    vtkPolyData *pd = vtkPolyData::New();
    this->box->GetPolyData(pd);
    pd->GetBounds(bds);

    double size[3];
    size[0] = bds[1]-bds[0];
    size[1] = bds[3]-bds[2];
    size[2] = bds[5]-bds[4];

//    this->ellipsoid->SetXRadius(size[0]/2);
//    this->ellipsoid->SetYRadius(size[1]/2);
//    this->ellipsoid->SetZRadius(size[2]/2);

    this->superquadricSource->SetCenter(pd->GetCenter());
    this->superquadricSource->SetScale(size[0]/2, size[1]/2, size[2]/2);;

    pd->Delete();
    }
  BoxWidgetCallback(): box(0), superquadricSource(0) {}

  vtkBoxWidget *box;
//  vtkParametricEllipsoid *ellipsoid;
  vtkSuperquadricSource *superquadricSource;
};


int main( int argc, char **argv )
{
  char fname[] = {"/home/workspace/DICOM_TEST/FUNCAOVE_7/"};
  if ( argc > 1 )
    {
    cout << "Mudando o diretorio para" << argv[1] << endl;
    strcpy(fname, argv[1]);
    }

  vtkDICOMImageReader* reader =  vtkDICOMImageReader::New();

  reader->SetDirectoryName(fname);

  reader->Update();

  vtkOutlineFilter* outline = vtkOutlineFilter::New();
    outline->SetInput(reader->GetOutput());

  vtkPolyDataMapper* outlineMapper = vtkPolyDataMapper::New();
    outlineMapper->SetInput(outline->GetOutput());

  vtkActor* outlineActor =  vtkActor::New();
    outlineActor->SetMapper( outlineMapper);

  vtkRenderer *ren = vtkRenderer::New();

  vtkRenderWindow *renWin = vtkRenderWindow::New();
  renWin->AddRenderer(ren);
  renWin->SetSize(800, 600);

  vtkRenderWindowInteractor *iren = vtkRenderWindowInteractor::New();
  iren->SetRenderWindow(renWin);

  vtkCellPicker* picker = vtkCellPicker::New();
    picker->SetTolerance(0.005);

  vtkProperty* ipwProp = vtkProperty::New();

  vtkImagePlaneWidget* planeWidgetX = vtkImagePlaneWidget::New();
    planeWidgetX->SetInteractor( iren);
    planeWidgetX->SetKeyPressActivationValue('x');
    planeWidgetX->SetPicker(picker);
    planeWidgetX->RestrictPlaneToVolumeOn();
    planeWidgetX->GetPlaneProperty()->SetColor(1,0,0);
    planeWidgetX->SetTexturePlaneProperty(ipwProp);
    planeWidgetX->TextureInterpolateOff();
    planeWidgetX->SetResliceInterpolateToNearestNeighbour();
    planeWidgetX->SetInput(reader->GetOutput());
    planeWidgetX->SetPlaneOrientationToXAxes();
    planeWidgetX->SetSliceIndex(52);
    planeWidgetX->DisplayTextOn();
    planeWidgetX->On();
    planeWidgetX->InteractionOff();
//    planeWidgetX->InteractionOn();

  vtkImagePlaneWidget* planeWidgetY = vtkImagePlaneWidget::New();
    planeWidgetY->SetInteractor( iren);
    planeWidgetY->SetKeyPressActivationValue('y');
    planeWidgetY->SetPicker(picker);
    planeWidgetY->GetPlaneProperty()->SetColor(1,1,0);
    planeWidgetY->SetTexturePlaneProperty(ipwProp);
    planeWidgetY->TextureInterpolateOn();
    planeWidgetY->SetResliceInterpolateToLinear();
    planeWidgetY->SetInput(reader->GetOutput());
    planeWidgetY->SetPlaneOrientationToYAxes();
    planeWidgetY->SetSliceIndex(256);
    planeWidgetY->SetLookupTable( planeWidgetX->GetLookupTable());
    planeWidgetY->DisplayTextOff();
    planeWidgetY->UpdatePlacement();
    planeWidgetY->On();
    planeWidgetY->InteractionOff();

  vtkImagePlaneWidget* planeWidgetZ = vtkImagePlaneWidget::New();
    planeWidgetZ->SetInteractor( iren);
    planeWidgetZ->SetKeyPressActivationValue('z');
    planeWidgetZ->SetPicker(picker);
    planeWidgetZ->GetPlaneProperty()->SetColor(0,0,1);
    planeWidgetZ->SetTexturePlaneProperty(ipwProp);
    planeWidgetZ->TextureInterpolateOn();
    planeWidgetZ->SetResliceInterpolateToCubic();
    planeWidgetZ->SetInput(reader->GetOutput());
    planeWidgetZ->SetPlaneOrientationToZAxes();
    planeWidgetZ->SetSliceIndex(256);
    planeWidgetZ->SetLookupTable( planeWidgetX->GetLookupTable());
    planeWidgetZ->DisplayTextOn();
    planeWidgetZ->On();
    planeWidgetZ->InteractionOff();

  vtkImageOrthoPlanes *orthoPlanes = vtkImageOrthoPlanes::New();
    orthoPlanes->SetPlane(0, planeWidgetX);
    orthoPlanes->SetPlane(1, planeWidgetY);
    orthoPlanes->SetPlane(2, planeWidgetZ);
    orthoPlanes->ResetPlanes();

  vtkSuperquadricSource *superquadricSource = vtkSuperquadricSource::New();
    superquadricSource->ToroidalOff();
    superquadricSource->SetPhiResolution(20);
    superquadricSource->SetThetaResolution(20);
    superquadricSource->SetCenter(reader->GetOutput()->GetCenter());
    superquadricSource->SetSize(1);

  double spc[3], size[3];
  int dim[3];
  reader->GetOutput()->GetSpacing(spc);
  reader->GetOutput()->GetDimensions(dim);

  size[0] = dim[0]*spc[0];
  size[1] = dim[1]*spc[1];
  size[2] = dim[2]*spc[2];

  cout << "spacing: " << spc[0] << " " << spc[1] << " " << spc[2] << endl;
  cout << "dimension: " << dim[0] << " " << dim[1] << " " << dim[2] << endl;
  cout << "size: " << size[0] << " " << size[1] << " " << size[2] << endl;
  cout << "size 2: " << size[0]/2 << " " << size[1]/2 << " " << size[2]/2 << endl;

  superquadricSource->SetScale(size[0]/2, size[1]/2, size[2]/2);

  //# ------------------------------------------------------------
  //# Create an ellipsoidal surface
  //# ------------------------------------------------------------
  vtkParametricEllipsoid *ellipsoid = vtkParametricEllipsoid::New();
  ellipsoid->SetXRadius(size[0]/2);
  ellipsoid->SetYRadius(size[1]/2);
  ellipsoid->SetZRadius(size[2]/2);

  vtkParametricFunctionSource *ellipsoidSource = vtkParametricFunctionSource::New();
  ellipsoidSource->SetParametricFunction(ellipsoid);
  ellipsoidSource->SetScalarModeToZ();

  vtkPolyDataMapper *squadMapper = vtkPolyDataMapper::New();
  squadMapper->SetInput(superquadricSource->GetOutput());
//  squadMapper->SetInput(ellipsoidSource->GetOutput());
//  squadMapper->ScalarVisibilityOff();

  vtkActor *squadActor = vtkActor::New();
  squadActor->SetMapper(squadMapper);
//    actor SetTexture atext
  squadActor->GetProperty()->SetColor(1, 1, 0);
  squadActor->GetProperty()->SetOpacity(0.6);


//  cout << "ellipsoid resolution: " << ellipsoid->GetMaximumU() << " " << ellipsoid->GetMaximumV()
//      << " " << ellipsoid->GetMaximumW() << endl;
//  cout << "parametric resolution: " << ellipsoidSource->GetUResolution() << " " << ellipsoidSource->GetVResolution()
//      << " " << ellipsoidSource->GetWResolution() << endl;

  vtkBoxWidget *box = vtkBoxWidget::New();
  box->SetPlaceFactor(1);
  box->SetInput(reader->GetOutput());
  box->PlaceWidget();
  box->SetInteractor(iren);
//  box->RotationEnabledOff();
  box->On();

  vtkPolyData *pd = vtkPolyData::New();
  box->GetPolyData(pd);

  double c[3];
  pd->GetCenter(c);

  vtkTransform *trans = vtkTransform::New();
  trans->Identity();
  trans->Translate(c);
//  squadActor->SetUserTransform(trans);

  BoxWidgetCallback *boxCallback = BoxWidgetCallback::New();
  boxCallback->box = box;
//  boxCallback->ellipsoid = ellipsoid;
  boxCallback->superquadricSource = superquadricSource;

  box->AddObserver(vtkCommand::InteractionEvent, boxCallback, 0.0);

  ren->AddActor( outlineActor);
  ren->AddActor( squadActor);

  iren->Initialize();
  renWin->Render();

  iren->Start();

  pd->Delete();

  squadMapper->Delete();
  squadActor->Delete();
  superquadricSource->Delete();

  ellipsoidSource->Delete();
  ellipsoid->Delete();

  trans->Delete();
  boxCallback->Delete();
  ipwProp->Delete();
  orthoPlanes->Delete();
  planeWidgetX->Delete();
  planeWidgetY->Delete();
  planeWidgetZ->Delete();
  outlineActor->Delete();
  outlineMapper->Delete();
  outline->Delete();
  picker->Delete();
  box->Delete();
  iren->Delete();
  renWin->Delete();
  ren->Delete();
  reader->Delete();

  return 0;
}
