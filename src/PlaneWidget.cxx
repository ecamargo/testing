#include <vtkPlaneWidget.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkSmartPointer.h>
 
int main(int, char *[])
{  
  vtkSmartPointer<vtkRenderer> renderer = 
    vtkSmartPointer<vtkRenderer>::New();
 
  vtkSmartPointer<vtkRenderWindow> renderWindow = 
    vtkSmartPointer<vtkRenderWindow>::New();
  renderWindow->AddRenderer(renderer);
 
  vtkSmartPointer<vtkRenderWindowInteractor> renderWindowInteractor = 
    vtkSmartPointer<vtkRenderWindowInteractor>::New();
  renderWindowInteractor->SetRenderWindow(renderWindow);
 
  vtkSmartPointer<vtkPlaneWidget> planeWidget = 
      vtkSmartPointer<vtkPlaneWidget>::New();
  planeWidget->SetInteractor(renderWindowInteractor);
 
  cout << planeWidget->GetHandleSize() <<endl;
  planeWidget->SetHandleSize(0.005);
  cout << planeWidget->GetHandleSize() <<endl;
//  planeWidget->SizeHandles();
  planeWidget->PlaceWidget();
  planeWidget->On();
//  planeWidget->HandlesOff();

  renderWindowInteractor->Initialize();
 
  renderer->ResetCamera();
  renderWindow->SetSize(1024, 768);
  renderWindow->Render();
  renderWindowInteractor->Start();
 
  return EXIT_SUCCESS;
}
