/*
 * EllipsoidParametricTeste.cxx
 *
 *  Created on: 18/11/2010
 *      Author: igor
 */

#include "vtkRenderer.h"
#include "vtkRenderWindow.h"
#include "vtkRenderWindowInteractor.h"
#include "vtkDataSetMapper.h"
#include "vtkProperty.h"
#include "vtkDICOMImageReader.h"
#include "vtkImageData.h"
#include "vtkImageOrthoPlanes.h"
#include "vtkImagePlaneWidget.h"
#include "vtkOutlineFilter.h"
#include "vtkPolyDataMapper.h"
#include "vtkCellPicker.h"
#include "vtkProperty.h"

#include "vtkParametricEllipsoid.h"
#include "vtkParametricFunctionSource.h"


int main( int argc, char **argv )
{

//vtkParametricSuperEllipsoid superEllipsoid
//superEllipsoid SetXRadius 1.25
//superEllipsoid SetYRadius 1.5
//superEllipsoid SetZRadius 1.0
//superEllipsoid SetN1 1.1
//superEllipsoid SetN2 1.75
//vtkParametricFunctionSource superEllipsoidSource
//  superEllipsoidSource SetParametricFunction superEllipsoid
//  superEllipsoidSource SetScalarModeToV
//
//vtkPolyDataMapper superEllipsoidMapper
//  superEllipsoidMapper SetInputConnection [superEllipsoidSource GetOutputPort]
//  superEllipsoidMapper SetScalarRange 0 3.14
//
//vtkActor superEllipsoidActor
//  superEllipsoidActor SetMapper superEllipsoidMapper
//  superEllipsoidActor SetPosition 8 4 0
//
//vtkTextMapper superEllipsoidTextMapper
//    superEllipsoidTextMapper SetInput "Super Ellipsoid"
//    [superEllipsoidTextMapper GetTextProperty] SetJustificationToCentered
//    [superEllipsoidTextMapper GetTextProperty] SetVerticalJustificationToCentered
//    [superEllipsoidTextMapper GetTextProperty] SetColor 1 0 0
//    [superEllipsoidTextMapper GetTextProperty] SetFontSize 14
//vtkActor2D superEllipsoidTextActor
//    superEllipsoidTextActor SetMapper superEllipsoidTextMapper
//    [superEllipsoidTextActor GetPositionCoordinate] SetCoordinateSystemToWorld
//    [superEllipsoidTextActor GetPositionCoordinate] SetValue 8 1.5 0




  ///////////////////////////////////////////////////////////////////////////////
  //# ------------------------------------------------------------
  //# Create an ellipsoidal surface
  //# ------------------------------------------------------------
  vtkParametricEllipsoid *ellipsoid = vtkParametricEllipsoid::New();
  ellipsoid->SetXRadius(168);
  ellipsoid->SetYRadius(168);
  ellipsoid->SetZRadius(106);

  vtkParametricFunctionSource *ellipsoidSource = vtkParametricFunctionSource::New();
  ellipsoidSource->SetParametricFunction(ellipsoid);
//  ellipsoidSource->SetScalarModeToZ();

  vtkPolyDataMapper *ellipsoidMapper = vtkPolyDataMapper::New();
  ellipsoidMapper->SetInput(ellipsoidSource->GetOutput());
//  ellipsoidMapper->SetScalarRange(-0.5, 0.5);

  vtkActor *ellipsoidActor = vtkActor::New();
  ellipsoidActor->SetMapper(ellipsoidMapper);
  ellipsoidActor->GetProperty()->SetColor(1,1,0);
  ellipsoidActor->GetProperty()->SetOpacity(0.25);
//  ellipsoidActor->SetPosition(8, -12, 0);
//  ellipsoidActor->SetScale( 1.5, 1.5, 1.5);

//  vtkTextMapper ellipsoidTextMapper
//  ellipsoidTextMapper SetInput "Ellipsoid"
//  [ellipsoidTextMapper GetTextProperty] SetJustificationToCentered
//  [ellipsoidTextMapper GetTextProperty] SetVerticalJustificationToCentered
//  [ellipsoidTextMapper GetTextProperty] SetColor 1 0 0
//  [ellipsoidTextMapper GetTextProperty] SetFontSize 14
//  vtkActor2D ellipsoidTextActor
//  ellipsoidTextActor SetMapper ellipsoidTextMapper
//  [ellipsoidTextActor GetPositionCoordinate] SetCoordinateSystemToWorld
//  [ellipsoidTextActor GetPositionCoordinate] SetValue 8 -14.5 0


  vtkRenderer *ren = vtkRenderer::New();

  vtkRenderWindow *renWin = vtkRenderWindow::New();
  renWin->AddRenderer(ren);
  renWin->SetSize(800, 600);

  vtkRenderWindowInteractor *iren = vtkRenderWindowInteractor::New();
  iren->SetRenderWindow(renWin);

  ren->AddActor( ellipsoidActor);

  iren->Initialize();
  renWin->Render();

  iren->Start();

  ellipsoidMapper->Delete();
  ellipsoidSource->Delete();
  ellipsoid->Delete();
  ellipsoidActor->Delete();
  iren->Delete();
  renWin->Delete();
  ren->Delete();



  return 0;
}
