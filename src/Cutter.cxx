#include <vtkSmartPointer.h>
#include <vtkCubeSource.h>
#include <vtkPolyDataMapper.h>
#include <vtkPlane.h>
#include <vtkCutter.h>
#include <vtkProperty.h>
#include <vtkActor.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>




#include <vtkSphereSource.h>
#include <vtkMath.h>
#include <vtkPlaneSource.h>
#include <vtkPolyDataReader.h>
#include <vtkLineSource.h>
#include <vtkPolyDataCollection.h>
#include <vtkMaskPoints.h>


using namespace std;



//--------------------------------------------------------------------------------------------------
void BuildVector(double base[3], double vertex[3], double aperture[3], double *u, double *v)
{
  u[0] = base[0] - vertex[0];
  u[1] = base[1] - vertex[1];
  u[2] = base[2] - vertex[2];

  v[0] = aperture[0] - vertex[0];
  v[1] = aperture[1] - vertex[1];
  v[2] = aperture[2] - vertex[2];
}

//--------------------------------------------------------------------------------------------------
double ComputeEscalarProduct( double *u, double *v )
{
return (u[0] * v[0]) + (u[1] * v[1]) + (u[2] * v[2]);
}

//--------------------------------------------------------------------------------------------------
void ComputeAngleBetweenTwoVectors(double u[3], double v[3], double *angleInRadians)
{
  double d, prodEscalar, norm_u, norm_v;

  prodEscalar = ComputeEscalarProduct(u, v);
  norm_u = vtkMath::Norm(u);
  norm_v = vtkMath::Norm(v);
  d = prodEscalar / (norm_u * norm_v);  // cos e = u.v / |u| * |v|
  *angleInRadians = acos( d ); // acos(d) = e; sendo "e" o angulo em radianos
}

//--------------------------------------------------------------------------------------------------
void ComputeTriangleOrthocenter( double A[3], double B[3], double C[3], double *Orthocenter)
{
  double angleInA, angleInB, angleInC, u[3], v[3];
  double x1, x2, x3;
  double y1, y2, y3;
  double z1, z2, z3;

  BuildVector(B, A, C, u, v);
  ComputeAngleBetweenTwoVectors( u, v, &angleInA );

  BuildVector(A, B, C, u, v);
  ComputeAngleBetweenTwoVectors( u, v, &angleInB );

  BuildVector(B, C, A, u, v);
  ComputeAngleBetweenTwoVectors( u, v, &angleInC );


//  cout << "A: " << A[0] << ", " << A[1] << ", " << A[2] <<endl;
//  cout << "B: " << B[0] << ", " << B[1] << ", " << B[2] <<endl;
//  cout << "C: " << C[0] << ", " << C[1] << ", " << C[2] <<endl;
//
//
//  cout << "angleInA em radianos: " << angleInA << endl;
//  cout << "angleInB em radianos: " << angleInB << endl;
//  cout << "angleInC em radianos: " << angleInC << endl;


  x1 = A[0];
  y1 = A[1];
  z1 = A[2];

  x2 = B[0];
  y2 = B[1];
  z2 = B[2];

  x3 = C[0];
  y3 = C[1];
  z3 = C[2];


  Orthocenter[0] = (x1*tan(angleInA) + x2*tan(angleInB) + x3*tan(angleInC)) / (tan(angleInA)+tan(angleInB)+tan(angleInC));
  Orthocenter[1] = (y1*tan(angleInA) + y2*tan(angleInB) + y3*tan(angleInC)) / (tan(angleInA)+tan(angleInB)+tan(angleInC));
  Orthocenter[2] = (z1*tan(angleInA) + z2*tan(angleInB) + z3*tan(angleInC)) / (tan(angleInA)+tan(angleInB)+tan(angleInC));


//  cout << "Orthocenter: " << Orthocenter[0] << ", " << Orthocenter[1] << ", " << Orthocenter[2] <<endl;
}

//--------------------------------------------------------------------------------------------------
double AverageDistanceBetweenPointsAndCenter( vtkSmartPointer<vtkPolyData> poly, double *center )
{
  double p[3], sum;

  sum = 0.0;

  for(int i=0; i < poly->GetNumberOfPoints(); ++i)
    {
    poly->GetPoint( i, p );

    sum += vtkMath::Distance2BetweenPoints( center, p );
    }

  double n = poly->GetNumberOfPoints();
  return sum / n ;
}

// Calcula o ponto de interseção das linhas KL e MN
//--------------------------------------------------------------------------------------------------
void ComputeIntersectionPointOfLines( double k[3], double l[3], double m[3], double n[3], double *intersection )
{
  double det, ix, iy, iz, kx, ky, kz, lx, ly, lz, mx, my, mz, nx, ny, nz, t, s;

  kx = k[0];   lx = l[0];
  ky = k[1];   ly = l[1];
  kz = k[2];   lz = l[2];

  mx = m[0];   nx = n[0];
  my = m[1];   ny = n[1];
  mz = m[2];   nz = n[2];



  cout << "k: " << k[0] << ", " << k[1] << ", " << k[2] <<endl;
  cout << "l: " << l[0] << ", " << l[1] << ", " << l[2] <<endl;
  cout << "m: " << m[0] << ", " << m[1] << ", " << m[2] <<endl;
  cout << "n: " << n[0] << ", " << n[1] << ", " << n[2] <<endl;



 if (((kx != lx) || (ky != ly))  &&
     ((mx != nx) || (my != ny)) )  /* se nao e' paralela ao plano XY*/
 {
  det = (nx - mx) * (ly - ky)  -  (ny - my) * (lx - kx);

  if (det == 0.0)
   return;

  s = ((nx - mx) * (my - ky) - (ny - my) * (mx - kx))/ det ;
  t = ((lx - kx) * (my - ky) - (ly - ky) * (mx - kx))/ det ;
 }





 if (((kx != lx) || (kz != lz))  &&
     ((mx != nx) || (mz != nz)) )  /* se nao e' paralela ao plano XZ*/
 {
  det = (nx - mx) * (lz - kz)  -  (nz - mz) * (lx - kx);

  if (det == 0.0)
    return;

  s = ((nx - mx) * (mz - kz) - (nz - mz) * (mx - kx))/ det ;
  t = ((lx - kx) * (mz - kz) - (lz - kz) * (mx - kx))/ det ;
 }




 if (((ky != ly) || (kz != lz))  &&
     ((my != ny) || (mz != nz)) )  /* se nao e' paralela ao plano YZ*/
 {
  det = (ny - my) * (lz - kz)  -  (nz - mz) * (ly - ky);

  if (det == 0.0)
    return;

  s = ((ny - my) * (mz - kz) - (nz - mz) * (my - ky))/ det ;
  t = ((ly - ky) * (mz - kz) - (lz - kz) * (my - ky))/ det ;
 }



// this->VerticePointOfVesselAngle[0] = kx + (lx-kx)*s;
// this->VerticePointOfVesselAngle[1] = ky + (ly-ky)*s;
// this->VerticePointOfVesselAngle[2] = kz + (lz-kz)*s;

 intersection[0] = kx + (lx-kx)*s;
 intersection[1] = ky + (ly-ky)*s;
 intersection[2] = kz + (lz-kz)*s;
}


//--------------------------------------------------------------------------------------------------
double ComputeMaxDiameter(vtkSmartPointer<vtkPolyData> domo, vtkSmartPointer<vtkPolyDataCollection> collection,
                          double *barycenter, double *HigherAneurysmPoint, double *NeckPointP1, double *NeckPointP2)
{
  double basePoint[3], normal[3], *avgDiameter;
  int resolution = 5;


  vtkSmartPointer<vtkLineSource> heigth = vtkSmartPointer<vtkLineSource>::New();
  heigth->SetPoint1( barycenter );
  heigth->SetPoint2( HigherAneurysmPoint );
  heigth->SetResolution( resolution-1 );
  heigth->Update();

  normal[0] = HigherAneurysmPoint[0] - barycenter[0];
  normal[1] = HigherAneurysmPoint[1] - barycenter[1];
  normal[2] = HigherAneurysmPoint[2] - barycenter[2];


  avgDiameter = new double[ heigth->GetOutput()->GetNumberOfPoints() ];


  for(int i=0; i < resolution; ++i)
    {
    heigth->GetOutput()->GetPoint( i, basePoint );

    vtkSmartPointer<vtkPlane> plane = vtkSmartPointer<vtkPlane>::New();
    plane->SetOrigin( basePoint[0], basePoint[1], basePoint[2] );
    plane->SetNormal( normal[0], normal[1], normal[2] );


    // Create cutter
    vtkSmartPointer<vtkCutter> cutter = vtkSmartPointer<vtkCutter>::New();
    cutter->SetCutFunction( plane );
    cutter->SetInput( domo );
    cutter->Update();


    vtkSmartPointer<vtkPolyData> poly = vtkSmartPointer<vtkPolyData>::New();
    poly->DeepCopy( cutter->GetOutput() );
    poly->Update();

    collection->AddItem( poly );

    avgDiameter[i] = AverageDistanceBetweenPointsAndCenter( cutter->GetOutput(), basePoint );
    cout << "avgDiameter[" << i <<"]: " << avgDiameter[i] <<endl;
    }

  // encontrando o maior diametro
  double AneurysmDiameterMaximum = avgDiameter[0];
  int pos = 0;          //para debug
  for(int i=1; i < resolution; ++i)
    {
    if( AneurysmDiameterMaximum < avgDiameter[i] )
      {
      AneurysmDiameterMaximum = avgDiameter[i];
      pos = i;
      }
    }

  //até este ponto foi encontrado o raio máximo, agora multiplica-se por 2 para encontrar o diametro
  AneurysmDiameterMaximum *= 2;


  delete[] avgDiameter;



  cout << "AneurysmDiameterMaximum:" << AneurysmDiameterMaximum <<endl;
  return AneurysmDiameterMaximum;
}




//--------------------------------------------------------------------------------------------------
void ContoursFromPolyData()
{
  string fileNameDomo;
  fileNameDomo.append("/home/eduardo/workspace/data_HeMoLab/Aneurisma_2_domo.vtk");

  vtkSmartPointer<vtkPolyDataReader> readerDomo = vtkSmartPointer<vtkPolyDataReader>::New();
  readerDomo->SetFileName( fileNameDomo.c_str() );
  readerDomo->Update();


  double barycenter[3], HigherAneurysmPoint[3], NeckPointP1[3], NeckPointP2[3];
  vtkSmartPointer<vtkPolyDataCollection> collection = vtkSmartPointer<vtkPolyDataCollection>::New();

  HigherAneurysmPoint[0] = 0.469769;
  HigherAneurysmPoint[1] = -0.0896911;
  HigherAneurysmPoint[2] = 0.14142;

  barycenter[0] = -0.115392;
  barycenter[1] = -0.0370282;
  barycenter[2] = -0.0088294;

  NeckPointP1[0] = 0.469769;
  NeckPointP1[1] = -0.0896911;
  NeckPointP1[2] = 0.14142;

  NeckPointP2[0] = -0.343494;
  NeckPointP2[1] = 0.360565;
  NeckPointP2[2] = -0.000480653;




  ComputeMaxDiameter( readerDomo->GetOutput(), collection,  barycenter, HigherAneurysmPoint, NeckPointP1, NeckPointP2 );



  vtkSmartPointer<vtkSphereSource> sph_HigherAneurysm = vtkSmartPointer<vtkSphereSource>::New();
  sph_HigherAneurysm->SetRadius( 0.015 );
  sph_HigherAneurysm->SetCenter( HigherAneurysmPoint );
  sph_HigherAneurysm->SetThetaResolution( 30 );
  sph_HigherAneurysm->SetPhiResolution( 30 );

  vtkSmartPointer<vtkPolyDataMapper> HigherAneurysmPointMapper = vtkSmartPointer<vtkPolyDataMapper>::New();
  HigherAneurysmPointMapper->SetInput( sph_HigherAneurysm->GetOutput() );

  vtkSmartPointer<vtkActor> HigherAneurysmActor = vtkSmartPointer<vtkActor>::New();
  HigherAneurysmActor->SetMapper( HigherAneurysmPointMapper );
  HigherAneurysmActor->GetProperty()->SetColor(0.0, 1.0, 0.0);


  vtkSmartPointer<vtkSphereSource> sph_intersectionPoint = vtkSmartPointer<vtkSphereSource>::New();
  sph_intersectionPoint->SetRadius( 0.015 );
  sph_intersectionPoint->SetCenter( barycenter );
  sph_intersectionPoint->SetThetaResolution( 30 );
  sph_intersectionPoint->SetPhiResolution( 30 );

  vtkSmartPointer<vtkPolyDataMapper> intersectionPointMapper = vtkSmartPointer<vtkPolyDataMapper>::New();
  intersectionPointMapper->SetInput( sph_intersectionPoint->GetOutput() );

  vtkSmartPointer<vtkActor> intersectionPointActor = vtkSmartPointer<vtkActor>::New();
  intersectionPointActor->SetMapper( intersectionPointMapper );
  intersectionPointActor->GetProperty()->SetColor(0.0, 0.0, 1.0);



  vtkSmartPointer<vtkLineSource> heigth = vtkSmartPointer<vtkLineSource>::New();
  heigth->SetPoint1( barycenter );
  heigth->SetPoint2( HigherAneurysmPoint );
  heigth->SetResolution( 10 );
  heigth->Update();

  vtkSmartPointer<vtkPolyDataMapper> heigthMapper = vtkSmartPointer<vtkPolyDataMapper>::New();
  heigthMapper->SetInput( heigth->GetOutput() );

  vtkSmartPointer<vtkActor> heightActor = vtkSmartPointer<vtkActor>::New();
  heightActor->SetMapper( heigthMapper );



  vtkSmartPointer<vtkPolyDataMapper> domeMapper = vtkSmartPointer<vtkPolyDataMapper>::New();
  domeMapper->SetInput( readerDomo->GetOutput() );

  // Create plane actor
  vtkSmartPointer<vtkActor> domeActor = vtkSmartPointer<vtkActor>::New();
  domeActor->SetMapper( domeMapper );
  domeActor->GetProperty()->SetOpacity(0.3);


  vtkSmartPointer<vtkRenderer> renderer = vtkSmartPointer<vtkRenderer>::New();

  for(int i=0; i < collection->GetNumberOfItems(); ++i)
    {
    vtkSmartPointer<vtkPolyDataMapper> planeMapper = vtkSmartPointer<vtkPolyDataMapper>::New();
    planeMapper->SetInput( vtkPolyData::SafeDownCast( collection->GetItemAsObject(i) ) );

    // Create plane actor
    vtkSmartPointer<vtkActor> planeActor = vtkSmartPointer<vtkActor>::New();
    planeActor->GetProperty()->SetColor(1.0, 0.0, 0.0);
    planeActor->GetProperty()->SetLineWidth(2);
    planeActor->SetMapper( planeMapper );


    renderer->AddActor( planeActor );
    }


  renderer->AddActor(domeActor);
  renderer->AddActor(heightActor);
  renderer->AddActor(HigherAneurysmActor);
  renderer->AddActor(intersectionPointActor);


  // Add renderer to renderwindow and render
  vtkSmartPointer<vtkRenderWindow> renderWindow = vtkSmartPointer<vtkRenderWindow>::New();
  renderWindow->AddRenderer(renderer);
  renderWindow->SetSize(600, 600);

  vtkSmartPointer<vtkRenderWindowInteractor> interactor = vtkSmartPointer<vtkRenderWindowInteractor>::New();
  interactor->SetRenderWindow(renderWindow);
  renderer->SetBackground(1,1,1);
  renderWindow->Render();

  interactor->Start();
}

double ComputePointSize( vtkSmartPointer<vtkPolyData> poly )
{
  double p0[3], p1[3];
  double dist = 0;
  for(int i=0; i < poly->GetNumberOfPoints()-1; ++i)
    {
    poly->GetPoint( i, p0 );
    poly->GetPoint( i+1, p1 );

    dist += sqrt(vtkMath::Distance2BetweenPoints( p0, p1 ));
    }

  return dist * 0.8;
}


//--------------------------------------------------------------------------------------------------
void MaximizeLinePoints()
{
  string fileNameLine;
  fileNameLine.append("/home/eduardo/workspace/data_HeMoLab/Aneurisma_2_centerline.vtk");

  vtkSmartPointer<vtkPolyDataReader> reader = vtkSmartPointer<vtkPolyDataReader>::New();
  reader->SetFileName( fileNameLine.c_str() );
  reader->Update();

  vtkSmartPointer<vtkPolyDataMapper> mapperLine = vtkSmartPointer<vtkPolyDataMapper>::New();
  mapperLine->SetInput( reader->GetOutput() );

  vtkSmartPointer<vtkActor> actorLine = vtkSmartPointer<vtkActor>::New();
  actorLine->SetMapper( mapperLine );


  vtkSmartPointer<vtkMaskPoints> maskPoints = vtkSmartPointer<vtkMaskPoints>::New();
  maskPoints->SetInput( reader->GetOutput() );
  maskPoints->GenerateVerticesOn();
  maskPoints->SetOnRatio(0);
  maskPoints->Update();

  vtkSmartPointer<vtkPolyDataMapper> mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
  mapper->SetInput( maskPoints->GetOutput() );

  double pointSize = ComputePointSize( maskPoints->GetOutput() );
  cout << "pointSize: " << pointSize <<endl;

  vtkSmartPointer<vtkActor> actor = vtkSmartPointer<vtkActor>::New();
  actor->SetMapper( mapper );
  actor->GetProperty()->SetRepresentationToPoints();
  actor->GetProperty()->SetPointSize( pointSize );
  actor->GetProperty()->SetColor(0.0, 1.0, 0.0);

  vtkSmartPointer<vtkRenderer> renderer = vtkSmartPointer<vtkRenderer>::New();
  renderer->AddActor(actor);
  renderer->AddActor(actorLine);

  vtkSmartPointer<vtkRenderWindow> renderWindow = vtkSmartPointer<vtkRenderWindow>::New();
  renderWindow->AddRenderer(renderer);
  renderWindow->SetSize(600, 600);

  vtkSmartPointer<vtkRenderWindowInteractor> interactor = vtkSmartPointer<vtkRenderWindowInteractor>::New();
  interactor->SetRenderWindow(renderWindow);
  renderer->SetBackground(0,0,0);
  renderWindow->Render();

  interactor->Start();
}


int main(int, char *[])
{
//  ContoursFromPolyData();
  MaximizeLinePoints();





  //  vtkSmartPointer<vtkCubeSource> cube =
  //    vtkSmartPointer<vtkCubeSource>::New();
  //  cube->SetXLength(40);
  //  cube->SetYLength(30);
  //  cube->SetZLength(20);
  //  vtkSmartPointer<vtkPolyDataMapper> cubeMapper =
  //    vtkSmartPointer<vtkPolyDataMapper>::New();
  //  cubeMapper->SetInputConnection(cube->GetOutputPort());
  //
  //  // Create a plane to cut,here it cuts in the XZ direction (xz normal=(1,0,0);XY =(0,0,1),YZ =(0,1,0)
  //  vtkSmartPointer<vtkPlane> plane =
  //    vtkSmartPointer<vtkPlane>::New();
  //  plane->SetOrigin(10,0,0);
  //  plane->SetNormal(1,0,0);
  //
  //  // Create cutter
  //  vtkSmartPointer<vtkCutter> cutter =
  //    vtkSmartPointer<vtkCutter>::New();
  //  cutter->SetCutFunction(plane);
  //  cutter->SetInputConnection(cube->GetOutputPort());
  //  cutter->Update();
  //
  //  vtkSmartPointer<vtkPolyDataMapper> cutterMapper =
  //    vtkSmartPointer<vtkPolyDataMapper>::New();
  //  cutterMapper->SetInputConnection( cutter->GetOutputPort());
  //
  //  // Create plane actor
  //  vtkSmartPointer<vtkActor> planeActor =
  //    vtkSmartPointer<vtkActor>::New();
  //  planeActor->GetProperty()->SetColor(1.0, 0.0, 0.0);
  //  planeActor->GetProperty()->SetLineWidth(2);
  //  planeActor->SetMapper(cutterMapper);
  //
  //  // Create cube actor
  //  vtkSmartPointer<vtkActor> cubeActor =
  //    vtkSmartPointer<vtkActor>::New();
  //  cubeActor->GetProperty()->SetColor(0.5,1,0.5);
  //  cubeActor->GetProperty()->SetOpacity(0.5);
  //  cubeActor->SetMapper(cubeMapper);
  //
  //  // Create renderers and add actors of plane and cube
  //  vtkSmartPointer<vtkRenderer> renderer =
  //    vtkSmartPointer<vtkRenderer>::New();
  //  renderer->AddActor(planeActor); //display the rectangle resulting from the cut
  //  renderer->AddActor(cubeActor); //display the cube
  //
  //  // Add renderer to renderwindow and render
  //  vtkSmartPointer<vtkRenderWindow> renderWindow =
  //    vtkSmartPointer<vtkRenderWindow>::New();
  //  renderWindow->AddRenderer(renderer);
  //  renderWindow->SetSize(600, 600);
  //
  //  vtkSmartPointer<vtkRenderWindowInteractor> interactor =
  //    vtkSmartPointer<vtkRenderWindowInteractor>::New();
  //  interactor->SetRenderWindow(renderWindow);
  //  renderer->SetBackground(0,0,0);
  //  renderWindow->Render();
  //
  //  interactor->Start();
  //
  //  return EXIT_SUCCESS;
}
