/*
 * PlotTeste.cxx
 *
 *  Created on: Aug 31, 2011
 *      Author: igor
 */

//#include "vtkRenderer.h"
//#include "vtkRenderWindow.h"
//#include "vtkSmartPointer.h"
//#include "vtkChartXY.h"
//#include "vtkPlot.h"
//#include "vtkTable.h"
//#include "vtkFloatArray.h"
//#include "vtkContextView.h"
//#include "vtkContextScene.h"
//#include "vtkRenderWindowInteractor.h"
////#include "vtkRegressionTestImage.h"



#include "vtkQtLineChartView.h"
#include "vtkQtChartRepresentation.h"
#include "vtkQtTableView.h"

#include "vtkSphereSource.h"
#include "vtkDataObjectToTable.h"
#include "vtkTable.h"
#include "vtkSmartPointer.h"

//#include "QTestApp.h"
#include <QWidget>
#include <qapplication.h>

#include "stdlib.h"
#include "string.h"

#include "PlotTesteVTK.h"

#define VTK_CREATE(type, name) \
  vtkSmartPointer<type> name = vtkSmartPointer<type>::New()

//----------------------------------------------------------------------------
//int TestLinePlot( int argc, char * argv [] )
int main( int argc, char **argv )
{
//  cout << "create view" << endl;
//  // Set up a 2D scene, add an XY chart to it
//  VTK_CREATE(vtkContextView, view);
//  cout << "set background" << endl;
//  view->GetRenderer()->SetBackground(1.0, 1.0, 1.0);
//  cout << "set size" << endl;
//  view->GetRenderWindow()->SetSize(400, 300);
//  cout << "create chart" << endl;
//  VTK_CREATE(vtkChartXY, chart);
//  cout << "Add Item" << endl;
//  view->GetScene()->AddItem(chart);
//  cout << "create table" << endl;
//  // Create a table with some points in it...
//  VTK_CREATE(vtkTable, table);
//  VTK_CREATE(vtkFloatArray, arrX);
//  arrX->SetName("X Axis");
//  table->AddColumn(arrX);
//  VTK_CREATE(vtkFloatArray, arrC);
//  arrC->SetName("Cosine");
//  table->AddColumn(arrC);
//  VTK_CREATE(vtkFloatArray, arrS);
//  arrS->SetName("Sine");
//  table->AddColumn(arrS);
//  VTK_CREATE(vtkFloatArray, arrS2);
//  arrS2->SetName("Sine2");
//  table->AddColumn(arrS2);
//  // Test charting with a few more points...
//  int numPoints = 69;
//  float inc = 7.5 / (numPoints-1);
//  table->SetNumberOfRows(numPoints);
//  for (int i = 0; i < numPoints; ++i)
//    {
//    table->SetValue(i, 0, i * inc);
//    table->SetValue(i, 1, cos(i * inc) + 0.0);
//    table->SetValue(i, 2, sin(i * inc) + 0.0);
//    table->SetValue(i, 3, sin(i * inc) + 0.5);
//    }
//
//  // Add multiple line plots, setting the colors etc
//  vtkPlot *line = chart->AddPlot(vtkChart::LINE);
//  line->SetInput(table, 0, 1);
//  line->SetColor(0, 255, 0, 255);
//  line->SetWidth(1.0);
//  line = chart->AddPlot(vtkChart::LINE);
//  line->SetInput(table, 0, 2);
//  line->SetColor(255, 0, 0, 255);
//  line->SetWidth(5.0);
//  line = chart->AddPlot(vtkChart::LINE);
//  line->SetInput(table, 0, 3);
//  line->SetColor(0, 0, 255, 255);
//  line->SetWidth(4.0);
//
//  //Finally render the scene and compare the image to a reference image
//  view->GetRenderWindow()->SetMultiSamples(0);
////  int retVal = vtkRegressionTestImageThreshold(view->GetRenderWindow(), 25);
//  //int retVal = vtkRegressionTestImage(view->GetRenderWindow());
////  if(retVal == vtkRegressionTester::DO_INTERACTOR)
////    {
//    view->GetInteractor()->Start();
////    }


//  QApplication app( argc, argv );
//
////  QTestApp app(argc, argv);
//
//  // Create a sphere and create a vtkTable from its point data (normal vectors)
//  VTK_CREATE(vtkSphereSource, sphereSource);
//  VTK_CREATE(vtkDataObjectToTable, tableConverter);
//  tableConverter->SetInput(sphereSource->GetOutput());
//  tableConverter->SetFieldType(vtkDataObjectToTable::POINT_DATA);
//  tableConverter->Update();
//  vtkTable* pointTable = tableConverter->GetOutput();
//
//  // Create a line chart view
//  vtkSmartPointer<vtkQtLineChartView> chartView =
//    vtkSmartPointer<vtkQtLineChartView>::New();
//  chartView->SetupDefaultInteractor();
//
//  // Set the chart title
//  chartView->SetTitle("Sphere Normals");
//
//  // Add the table to the view
//  vtkDataRepresentation* dataRep = chartView->AddRepresentationFromInput(pointTable);
//
//  // You can downcast to get the chart representation:
//  vtkQtChartRepresentation* chartRep =
//    vtkQtChartRepresentation::SafeDownCast(dataRep);
//  if (!chartRep)
//    {
//    cerr << "Failed to get chart table representation." << endl;
//    return 1;
//    }
//
//  // TODO-
//  // The user shouldn't be required to call Update().
//  // The view should handle updates automatically.
//  chartView->Update();
//
//  // Show the view's qt widget
//  chartView->Show();

//  // Show the table in a vtkQtTableView with split columns off
//  VTK_CREATE(vtkQtTableView, tableView);
//  tableView->SetSplitMultiComponentColumns(false);
//  tableView->AddRepresentationFromInput(pointTable);
//  tableView->Update();
//  tableView->GetWidget()->show();
//
//  // Show the table in a vtkQtTableView with split column on
//  VTK_CREATE(vtkQtTableView, tableView2);
//  tableView2->SetSplitMultiComponentColumns(true);
//  tableView2->AddRepresentationFromInput(pointTable);
//  tableView2->Update();
//  tableView2->GetWidget()->show();



  QApplication app( argc, argv );

  PlotTesteVTK *plotTeste = new PlotTesteVTK();

  plotTeste->Create();

  plotTeste->show();

  app.exec();

  delete plotTeste;

  return 0;
}
