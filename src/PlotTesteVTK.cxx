/*
 * PlotTesteVTK.cxx
 *
 *  Created on: Sep 1, 2011
 *      Author: igor
 */

#include "PlotTesteVTK.h"
#include "vtkSmartPointer.h"
#include "vtkRenderer.h"
#include "vtkRenderWindow.h"
#include "vtkTable.h"
#include "vtkFloatArray.h"
#include "vtkContextView.h"
#include "vtkContextScene.h"
#include "vtkRenderWindowInteractor.h"
#include "vtkAxis.h"


#define VTK_CREATE(type, name) \
  vtkSmartPointer<type> name = vtkSmartPointer<type>::New()

//-----------------------------------------------------------------------------
PlotTesteVTK::PlotTesteVTK()
{
  setupUi(this);

  this->Widget = new QVTKWidget();

  this->View = vtkContextView::New();

  this->gridLayout_2->addWidget(this->Widget);
}

//-----------------------------------------------------------------------------
PlotTesteVTK::~PlotTesteVTK()
{
  this->View->Delete();

  delete this->Widget;
}

void PlotTesteVTK::Create()
{
  this->View->SetInteractor(this->Widget->GetInteractor());
  this->Widget->SetRenderWindow(this->View->GetRenderWindow());
  this->View->GetRenderer()->SetBackground(1.0, 1.0, 1.0);
  this->View->GetRenderWindow()->SetSize(600, 500);

  VTK_CREATE(vtkChartXY, chart);
  chart->SetTitle("Heart Curve");
  chart->SetShowLegend(true);

  this->View->GetScene()->AddItem(chart);

//  // Create a table with some points in it...
//  VTK_CREATE(vtkTable, table);
//  VTK_CREATE(vtkFloatArray, arrX);
//  arrX->SetName("X Axis");
//  table->AddColumn(arrX);
//  VTK_CREATE(vtkFloatArray, arrC);
//  arrC->SetName("Cosine");
//  table->AddColumn(arrC);
//  VTK_CREATE(vtkFloatArray, arrS);
//  arrS->SetName("Sine");
//  table->AddColumn(arrS);
//  VTK_CREATE(vtkFloatArray, arrS2);
//  arrS2->SetName("Sine2");
//  table->AddColumn(arrS2);
//  // Test charting with a few more points...
//  int numPoints = 69;
//  float inc = 7.5 / (numPoints-1);
//  table->SetNumberOfRows(numPoints);
//  for (int i = 0; i < numPoints; ++i)
//    {
//    cout << "i: " << i << "\ti*inc: " << i*inc << "\tcos: " << cos(i*inc +0) << endl;
//    table->SetValue(i, 0, i * inc);
//    table->SetValue(i, 1, cos(i * inc) + 0.0);
//    table->SetValue(i, 2, sin(i * inc) + 0.0);
//    table->SetValue(i, 3, sin(i * inc) + 0.5);
//    }
//
//  // Add multiple line plots, setting the colors etc
//  vtkPlot *line = chart->AddPlot(vtkChart::LINE);
//  line->SetInput(table, 0, 1);
//  line->SetColor(0, 255, 0, 255);
//  line->SetWidth(1.0);
//  line = chart->AddPlot(vtkChart::LINE);
//  line->SetInput(table, 0, 2);
//  line->SetColor(255, 0, 0, 255);
//  line->SetWidth(5.0);
//  line = chart->AddPlot(vtkChart::LINE);
//  line->SetInput(table, 0, 3);
//  line->SetColor(0, 0, 255, 255);
//  line->SetWidth(4.0);


  double time[] = {0, 0.017544, 0.052632, 0.06316, 0.075088, 0.17544, 0.2, 0.24772, 0.26032, 0.2912, 0.30176, 0.8};
  double p[] = {0, 0, 51973181.8, 52755163.6, 52354636.4, 24508454.5, 16068772.7, -5683672.73, 811544.545, -381454.545, 0, 0};

  // Create a table with some points in it...
  VTK_CREATE(vtkTable, table);
  VTK_CREATE(vtkFloatArray, arrX);
  arrX->SetName("Time [sec]");
  table->AddColumn(arrX);
  VTK_CREATE(vtkFloatArray, arrC);
  arrC->SetName("Pressure");
  table->AddColumn(arrC);

  // Test charting with a few more points...
  int numPoints = 12;
  table->SetNumberOfRows(numPoints);
  for (int i = 0; i < numPoints; ++i)
    {
//    cout << "i: " << i << "\ttime i: " << time[i] << "\t\tp: " << p[i] << endl;
    table->SetValue(i, 0, time[i]);
    table->SetValue(i, 1, p[i]);
    }

  // Add multiple line plots, setting the colors etc
  vtkPlot *line = chart->AddPlot(vtkChart::LINE);
  line->SetLabel("pressure");
  line->SetInput(table, 0, 1);
  line->SetColor(255, 0, 0, 255);
  line->SetWidth(2.0);

//  VTK_CREATE(vtkAxix, xAxis);
//  VTK_CREATE(vtkAxix, yAxis);
  vtkAxis *axis = chart->GetAxis(0);
  axis->SetTitle("Pressure");
  axis->SetGridVisible(false);
//  if (axis)
//    cout << "Axis 0\n" << *axis << endl;



  axis = chart->GetAxis(1);
  axis->SetTitle("Time [sec]");
  axis->SetGridVisible(false);
//  if (axis)
//    cout << "Axis 1\n" << *axis << endl;

//  axis = chart->GetAxis(2);
//  if (axis)
//    cout << "Axis 2\n" << *axis << endl;
//
//  axis = chart->GetAxis(3);
//  if (axis)
//    cout << "Axis 3\n" << *axis << endl;

  this->View->GetRenderWindow()->SetMultiSamples(0);

  this->Widget->show();
}
