#include "vtkRenderWindowInteractor.h"
#include "vtkRenderWindow.h"
#include "vtkRenderer.h"

#include "vtkPolyDataReader.h"
#include "vtkPolyData.h"
#include "vtkPolyDataMapper.h"
#include "vtkActor.h"
#include "vtkProperty.h"
#include "vtkPointData.h"


#include <vtkAlgorithmOutput.h>

#include <string.h>


//--------------------------------------------------------------------------------------------------
int main( int argc, char **argv )
{
  std::string fileName = "/home/eduardo/Downloads/diff_mesh_con.vtk";

  vtkPolyDataReader *reader = vtkPolyDataReader::New();
  reader->SetFileName(fileName.c_str());
  reader->Update();


  vtkPolyData *poly = vtkPolyData::New();
  poly->DeepCopy( reader->GetOutput() );
  cout << *poly <<endl;

  vtkPolyDataMapper *mapper = vtkPolyDataMapper::New();
  mapper->SetInput( poly );
  mapper->SetScalarModeToUsePointFieldData();
  mapper->SetColorModeToMapScalars();
  mapper->ScalarVisibilityOn();
  mapper->SetScalarRange( poly->GetPointData()->GetArray("RegionId")->GetRange() );
  mapper->SelectColorArray("RegionId");


  vtkActor *actor = vtkActor::New();
  actor->SetMapper( mapper );

  vtkRenderer *renderer = vtkRenderer::New();

  renderer->AddActor( actor );
  renderer->SetBackground(0.5,0.5,0.5);

  vtkRenderWindow *renWin = vtkRenderWindow::New();
  renWin->AddRenderer(renderer);

  vtkRenderWindowInteractor *iren = vtkRenderWindowInteractor::New();
  iren->SetRenderWindow(renWin);
  renWin->SetSize(1024, 768);

  renderer->Render();

  iren->Initialize();
  iren->Start();



  poly->Delete();
  reader->Delete();
  mapper->Delete();
  actor->Delete();
  renderer->Delete();
  renWin->Delete();
  iren->Delete();

  return 0;
}
