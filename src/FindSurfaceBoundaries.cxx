#include "vtkRenderWindowInteractor.h"
#include "vtkRenderWindow.h"
#include "vtkRenderer.h"

#include "vtkSmartPointer.h"
#include "vtkPolyDataReader.h"
#include "vtkPolyDataWriter.h"
#include "vtkPolyData.h"
#include "vtkPolyDataMapper.h"
#include "vtkActor.h"
#include "vtkProperty.h"
#include "vtkIdList.h"
#include "vtkCellArray.h"
#include "vtkMaskPoints.h"
#include "vtkCleanPolyData.h"

#include <map>

using namespace std;

vtkSmartPointer<vtkIdList> visitedCells;
vtkSmartPointer<vtkIdList> visitedPoints;
std::map<int,int> SackPointsMap;

//-----------------------------------------------------------------------------------------------------------
bool IsBorderCell( int cellId, vtkSmartPointer<vtkPolyData> mesh )
{
  vtkSmartPointer<vtkIdList> ptList = vtkSmartPointer<vtkIdList>::New();
  vtkSmartPointer<vtkIdList> edgeNeighbors = vtkSmartPointer<vtkIdList>::New();

  mesh->GetCellPoints( cellId, ptList );

  mesh->GetCellEdgeNeighbors( cellId, ptList->GetId(0), ptList->GetId(1), edgeNeighbors);
  if( edgeNeighbors->GetNumberOfIds() == 0) return true;

  mesh->GetCellEdgeNeighbors( cellId, ptList->GetId(0), ptList->GetId(2), edgeNeighbors);
  if( edgeNeighbors->GetNumberOfIds() == 0) return true;

  mesh->GetCellEdgeNeighbors( cellId, ptList->GetId(1), ptList->GetId(2), edgeNeighbors);
  if( edgeNeighbors->GetNumberOfIds() == 0) return true;

  return false;
}

//-----------------------------------------------------------------------------------------------------------
void InsertEdgeInBorder( int *edge, vtkSmartPointer<vtkPolyData> mesh, vtkSmartPointer<vtkPolyData> border )
{
  double p0[3], p1[3];
  int id0, id1;

  mesh->GetPoint( edge[0], p0 );
  mesh->GetPoint( edge[1], p1 );

  std::map<int,int>::iterator it0 = SackPointsMap.find( edge[0] );
  std::map<int,int>::iterator it1 = SackPointsMap.find( edge[1] );

  if( it0 != SackPointsMap.end() ) id0 = it0->second;
  if( it1 != SackPointsMap.end() ) id1 = it1->second;


  if( it0 == SackPointsMap.end() ) // se o ponto ainda não foi inserido em border, insira
    {
    id0 = border->GetPoints()->InsertNextPoint( p0 );
    SackPointsMap.insert( std::pair<int,int>(edge[0],id0) );
    }

  if( it1 == SackPointsMap.end() )// se o ponto ainda não foi inserido em border, insira
    {
    id1 = border->GetPoints()->InsertNextPoint( p1 );
    SackPointsMap.insert( std::pair<int,int>(edge[1],id1) );
    }


  vtkSmartPointer<vtkIdList> pts = vtkSmartPointer<vtkIdList>::New();
  pts->InsertNextId( id0 );
  pts->InsertNextId( id1 );

  border->InsertNextCell( VTK_LINE, pts );
  border->Update();
}

//-----------------------------------------------------------------------------------------------------------
void FindBorderList( std::map<int,int> *borderList, int controlPoint, vtkSmartPointer<vtkPolyData> mesh )
{
  vtkSmartPointer<vtkIdList> cells = vtkSmartPointer<vtkIdList>::New();
  vtkSmartPointer<vtkIdList> ptList = vtkSmartPointer<vtkIdList>::New();
  vtkSmartPointer<vtkIdList> edgeNeighbors = vtkSmartPointer<vtkIdList>::New();

  mesh->GetPointCells( controlPoint, cells );

//  cout << "controlPoint: " << controlPoint  <<endl;


  for(int i=0; i < cells->GetNumberOfIds(); ++i)
    {
    int cellid = cells->GetId(i);
//    cout << "cellid: " << cellid  <<endl;
    mesh->GetCellPoints( cellid, ptList );

    if( ptList->GetNumberOfIds() > 3 )
      cout << "Verify cells points. Cell " << cellid << " has " << ptList->GetNumberOfIds() << " points." <<endl;


    for(int j=0; j < ptList->GetNumberOfIds(); ++j)
      {
      int id = ptList->GetId(j);
//      cout << "\t ptId: " << id  <<endl;
      if( id == controlPoint)
        continue;

      std::map<int,int>::iterator it = SackPointsMap.find( id );

      mesh->GetCellEdgeNeighbors( cellid, id, controlPoint, edgeNeighbors);

      if( (edgeNeighbors->GetNumberOfIds() == 0) && (it == SackPointsMap.end()) ) // se a aresta não possui células vizinhas e ainda não foi inserida no polydata de saida
        {
        borderList->insert( std::pair<int,int>( id, controlPoint) );
//        cout << "Borda que será adicionada: " << id << ", " << controlPoint <<endl;
        }
      }
    }
}

//-----------------------------------------------------------------------------------------------------------
void FindSurfaceBorder( int cellID, int controlPoint, vtkSmartPointer<vtkPolyData> mesh, vtkSmartPointer<vtkPolyData> border )
{
  vtkSmartPointer<vtkIdList> edge0_Neighbors = vtkSmartPointer<vtkIdList>::New();
  vtkSmartPointer<vtkIdList> edge1_Neighbors = vtkSmartPointer<vtkIdList>::New();
  vtkSmartPointer<vtkIdList> edge2_Neighbors = vtkSmartPointer<vtkIdList>::New();
  vtkSmartPointer<vtkIdList> cellNeighbors = vtkSmartPointer<vtkIdList>::New();
  vtkSmartPointer<vtkIdList> ptList = vtkSmartPointer<vtkIdList>::New();
  int edge[2];


  if( controlPoint == -1 )
    {
    mesh->GetCellPoints( cellID, ptList );

    // encontra a primeira borda sem vizinhos
    mesh->GetCellEdgeNeighbors( cellID, ptList->GetId(0), ptList->GetId(1), edge0_Neighbors);
    mesh->GetCellEdgeNeighbors( cellID, ptList->GetId(0), ptList->GetId(2), edge1_Neighbors);
    mesh->GetCellEdgeNeighbors( cellID, ptList->GetId(1), ptList->GetId(2), edge2_Neighbors);


    if( edge0_Neighbors->GetNumberOfIds() == 0 ) // borda 0
      {
      edge[0] = ptList->GetId(0);
      edge[1] = ptList->GetId(1);

      cout << "\t >> Borda na aresta de pontos: " << edge[0] << ", " << edge[1] <<endl;
      }
    else if( edge1_Neighbors->GetNumberOfIds() == 0 ) // borda 1
      {
      edge[0] = ptList->GetId(0);
      edge[1] = ptList->GetId(2);

      cout << "\t >> Borda na aresta de pontos: " << edge[0] << ", " << edge[1] <<endl;
      }
    else if( edge2_Neighbors->GetNumberOfIds() == 0 ) // borda 2
      {
      edge[0] = ptList->GetId(1);
      edge[1] = ptList->GetId(2);

      cout << "\t >> Borda na aresta de pontos: " << edge[0] << ", " << edge[1] <<endl;
      }


    InsertEdgeInBorder( edge, mesh, border );
    controlPoint = edge[1];
    FindSurfaceBorder( cellID, controlPoint, mesh, border );
    }
  else
    {
    std::map<int,int> borderList;
    std::map<int,int>::iterator it;

    FindBorderList( &borderList, controlPoint, mesh );

    it = borderList.begin();
    edge[0] = it->first;
    edge[1] = it->second;





    std::map<int,int>::iterator it0 = SackPointsMap.find( edge[0] );
    std::map<int,int>::iterator it1 = SackPointsMap.find( edge[1] );

    if( it0 != SackPointsMap.end() )
      {
//      cout << "\t it0: " << it0->first <<endl;
//      cout << "\t it0: " << it0->second <<endl;
      if( it1 != SackPointsMap.end() )
        {
//        cout << "\t it1: " << it1->first <<endl;
//        cout << "\t it1: " << it1->second <<endl;
        return;
        }
      }




    InsertEdgeInBorder( edge, mesh, border );
    cout << "\t >> Nova aresta: " << edge[0] << ", " << edge[1] <<endl;


    if( controlPoint == edge[0] )
      controlPoint = edge[1];
    else if( controlPoint == edge[1] )
      controlPoint = edge[0];

//    controlPoint = edge[1];

    FindSurfaceBorder( -1, controlPoint, mesh, border );
    }
}

//-----------------------------------------------------------------------------
int main( int argc, char **argv )
{
  double backgroundColor[] = {0.8, 0.8, 0.8};

//  string fileNameMesh = "/home/eduardo/workspace/data_HeMoLab/Aneurisk/c0003/ANEURISM_BASE_C0003.vtk";
  string fileNameMesh = "/home/eduardo/workspace/data_HeMoLab/Aneurisk/c0023/Sack.vtk";
//  string fileNameMesh = "/home/nfs/aneurysm_project/morphIndexes/1287259/26896/ANGIOGRAFIA_5/volumetricImage_1/segmentationID_1/meshID_1/morphIdexes/indexes/pre-processing/multiblock/multiblock_1_0.vtp";"



  vtkSmartPointer<vtkPolyDataReader> readermesh = vtkSmartPointer<vtkPolyDataReader>::New();
  readermesh->SetFileName( fileNameMesh.c_str() );
  readermesh->Update();


  vtkSmartPointer<vtkCleanPolyData> clean = vtkSmartPointer<vtkCleanPolyData>::New();
  clean->SetInput( readermesh->GetOutput() );
  clean->Update();

  vtkSmartPointer<vtkPolyData> mesh = vtkSmartPointer<vtkPolyData>::New();
  mesh->DeepCopy( clean->GetOutput() );
  mesh->Update();

  vtkSmartPointer<vtkIdList> ptList = vtkSmartPointer<vtkIdList>::New();
  for(int i=0; i<mesh->GetNumberOfCells(); ++i)
    {
    mesh->GetCellPoints( i, ptList );

    if(ptList->GetNumberOfIds() > 3)
      {
      cout << "check cell points. CellId: " << i <<endl;
      return 1;
      }
    }

  vtkSmartPointer<vtkPolyData> border = vtkSmartPointer<vtkPolyData>::New();
  border->SetPoints( vtkSmartPointer<vtkPoints>::New() );
  border->SetLines( vtkSmartPointer<vtkCellArray>::New() );
  border->Update();


  visitedCells = vtkSmartPointer<vtkIdList>::New();
  mesh->BuildLinks();
  mesh->Update();

  // garanto a visita em todas as células. Pode existir uma malha descontínua (2, ou mais, conjuntos de células desconexos).
  // Isso buscará a borda de cada conjunto de células.
  for(int i=0; i < mesh->GetNumberOfCells(); ++i)
    {
    if( (visitedCells->IsId(i) == -1) && IsBorderCell(i, mesh) ) // célula ainda não visitada. célula de borda (possui ao menos 1 aresta sem vizinhos)
      {      cout << "Visitando célula: " << i <<endl;
      visitedCells->InsertUniqueId( i ); // marca célula como visitada.
      FindSurfaceBorder( i, -1, mesh, border );
      }
    }


  vtkSmartPointer<vtkPolyDataMapper> mappermesh = vtkSmartPointer<vtkPolyDataMapper>::New();
  mappermesh->SetInput( mesh );

  vtkSmartPointer<vtkActor> actormesh = vtkSmartPointer<vtkActor>::New();
  actormesh->SetMapper( mappermesh );
  actormesh->GetProperty()->SetRepresentationToWireframe();
  actormesh->GetProperty()->SetOpacity(0.1);




  vtkSmartPointer<vtkMaskPoints> maskPoints = vtkSmartPointer<vtkMaskPoints>::New();
  maskPoints->SetInput( border );
  maskPoints->GenerateVerticesOn();
  maskPoints->SetOnRatio(0);
  maskPoints->Update();

  vtkSmartPointer<vtkPolyDataMapper> mapperBorderPoints = vtkSmartPointer<vtkPolyDataMapper>::New();
  mapperBorderPoints->SetInput( maskPoints->GetOutput() );

  vtkSmartPointer<vtkActor> actorBorderPoints = vtkSmartPointer<vtkActor>::New();
  actorBorderPoints->SetMapper( mapperBorderPoints );
  actorBorderPoints->GetProperty()->SetColor(0,0,1);
  actorBorderPoints->GetProperty()->SetRepresentationToPoints();
  actorBorderPoints->GetProperty()->SetPointSize( 4 );
    



  vtkSmartPointer<vtkPolyDataMapper> mapperBorderLine = vtkSmartPointer<vtkPolyDataMapper>::New();
  mapperBorderLine->SetInput( border );

  vtkSmartPointer<vtkActor> actorBorderLine = vtkSmartPointer<vtkActor>::New();
  actorBorderLine->SetMapper( mapperBorderLine );
  actorBorderLine->GetProperty()->SetColor(1,0,0);
  actorBorderLine->GetProperty()->SetLineWidth(2);


  vtkSmartPointer<vtkRenderer> renderer = vtkSmartPointer<vtkRenderer>::New();
  renderer->AddActor( actormesh );
  renderer->AddActor( actorBorderPoints );
  renderer->AddActor( actorBorderLine );

  renderer->SetBackground( backgroundColor );

  vtkSmartPointer<vtkRenderWindow> renWin = vtkSmartPointer<vtkRenderWindow>::New();
  renWin->AddRenderer(renderer);

  vtkSmartPointer<vtkRenderWindowInteractor> iren = vtkSmartPointer<vtkRenderWindowInteractor>::New();
  iren->SetRenderWindow(renWin);
  renWin->SetSize(1024, 768);


  renderer->Render();
  iren->Initialize();
  iren->Start();
}

