#include "vtkRenderWindowInteractor.h"
#include "vtkRenderWindow.h"
#include "vtkRenderer.h"

#include "vtkSmartPointer.h"
#include "vtkPolyDataReader.h"
#include "vtkPolyDataWriter.h"
#include "vtkPolyData.h"
#include "vtkPolyDataMapper.h"
#include "vtkActor.h"
#include "vtkProperty.h"

#include "vtkHull.h"
#include "vtkCleanPolyData.h"
#include "vtkTriangleFilter.h"
#include "vtkDataSetSurfaceFilter.h"
#include "vtkMassProperties.h"

#include "vtkIdList.h"
#include "vtkPoints.h"
#include "vtkCellArray.h"
#include "vtkUnsignedCharArray.h"
#include "vtkDoubleArray.h"
#include "vtkCellData.h"

using namespace std;


//---------------------------------------------------------------------------------------------
vtkSmartPointer<vtkPolyData> ConvexHull(vtkSmartPointer<vtkPolyData> input, int RecursiveSpherePlanes)
{
  // Clean the polydata. This will remove duplicate points that may be present in the input data.
  vtkSmartPointer<vtkCleanPolyData> cleaner = vtkSmartPointer<vtkCleanPolyData>::New();
  cleaner->SetInput(input);

  vtkSmartPointer<vtkHull> hull = vtkSmartPointer<vtkHull>::New();
  hull->SetInputConnection( cleaner->GetOutputPort() );
  hull->AddRecursiveSpherePlanes( RecursiveSpherePlanes );

  vtkSmartPointer<vtkDataSetSurfaceFilter> UgridToPoly = vtkSmartPointer<vtkDataSetSurfaceFilter>::New();
  UgridToPoly->SetInputConnection( hull->GetOutputPort() );
  UgridToPoly->SetPieceInvariant(1);
  UgridToPoly->SetNonlinearSubdivisionLevel(0);

  vtkSmartPointer<vtkTriangleFilter> triangulate = vtkSmartPointer<vtkTriangleFilter>::New();
  triangulate->SetInputConnection( UgridToPoly->GetOutputPort() );
  triangulate->Update();

  vtkSmartPointer<vtkPolyData> out = vtkSmartPointer<vtkPolyData>::New();
  out->DeepCopy( triangulate->GetOutput() );
  out->Update();

  return out;
}

//---------------------------------------------------------------------------------------------
void VolumeCH(vtkSmartPointer<vtkPolyData> input, double *volume, double *area)
{
  vtkSmartPointer<vtkMassProperties> mp = vtkSmartPointer<vtkMassProperties>::New();
  mp->SetInput( input );
  *volume = mp->GetVolume();
  *area = mp->GetSurfaceArea();
}

//--------------------------------------------------------------------------------------------------
int main( int argc, char **argv )
{
  double backgroundColor[] = {0.8, 0.8, 0.8};
  double hullColor[] = {1, 0, 0};
  double meshColor[] = {0.0, 0.0, 0.5};
  int RecursiveSpherePlanes = 4;

  string fileNameMesh = "/home/eduardo/Downloads/aneurismSac2.vtk";
//  string fileNameMesh = "/home/eduardo/workspace/data_HeMoLab/Aneurisma.vtk";
//  string fileNameMesh = "/home/eduardo/workspace/data_HeMoLab/sphere.vtk";
//  string fileNameMesh = "/home/eduardo/workspace/data_HeMoLab/cone_truncado.vtk";
//  string fileNameMesh = "/home/eduardo/workspace/data_HeMoLab/sphere_cells_duplicadas.vtk";
//  string fileNameMesh = "/home/eduardo/workspace/data_HeMoLab/sphere_points_duplicados.vtk";
//  string fileNameMesh = "/home/eduardo/workspace/data_HeMoLab/cone_truncado_points_duplicados.vtk";
//  string fileNameMesh = "/home/eduardo/workspace/data_HeMoLab/cone_truncado_cell_duplicadas.vtk";



  vtkSmartPointer<vtkPolyDataReader> readermesh = vtkSmartPointer<vtkPolyDataReader>::New();
  readermesh->SetFileName( fileNameMesh.c_str() );
  readermesh->Update();

  vtkSmartPointer<vtkPolyData> mesh = vtkSmartPointer<vtkPolyData>::New();
  mesh->DeepCopy( readermesh->GetOutput() );
  mesh->Update();




  unsigned char yellow[3] = {255, 255, 0};
  unsigned char red[3] = {255, 0, 0};
  vtkSmartPointer<vtkUnsignedCharArray> color = vtkSmartPointer<vtkUnsignedCharArray>::New();
  color->SetNumberOfComponents(3);
  color->SetName("Cell_Colors");

  for(int i=0; i < mesh->GetNumberOfCells(); ++i)
    {
    if( (i%2) == 0 )
      color->InsertNextTupleValue( yellow );
    else
      color->InsertNextTupleValue( red );
    }

  mesh->GetCellData()->SetScalars(color);
  mesh->Update();








  vtkSmartPointer<vtkPolyDataMapper> mappermesh = vtkSmartPointer<vtkPolyDataMapper>::New();
  mappermesh->SetInput( mesh );

  vtkSmartPointer<vtkActor> actormesh = vtkSmartPointer<vtkActor>::New();
  actormesh->SetMapper( mappermesh );
  actormesh->GetProperty()->SetOpacity(1);
//  actormesh->GetProperty()->SetRepresentationToWireframe();
  actormesh->GetProperty()->SetColor( meshColor );



  vtkSmartPointer<vtkPolyData> convexHull = ConvexHull( mesh, RecursiveSpherePlanes );

  double Vch, Sch;
  VolumeCH( convexHull, &Vch, &Sch );
  cout << "Volume: " << Vch <<endl;
  cout << "Area: " << Sch <<endl;

  vtkSmartPointer<vtkPolyDataWriter> writer = vtkSmartPointer<vtkPolyDataWriter>::New();
  writer->SetInput( convexHull );
  writer->SetFileName("/home/eduardo/Downloads/ConvexHull.vtk");
  writer->Update();



  vtkSmartPointer<vtkPolyDataMapper> mapperHull = vtkSmartPointer<vtkPolyDataMapper>::New();
  mapperHull->SetInput( convexHull );

  vtkSmartPointer<vtkActor> actorHull = vtkSmartPointer<vtkActor>::New();
  actorHull->SetMapper( mapperHull );
  actorHull->GetProperty()->SetRepresentationToWireframe();
  actorHull->GetProperty()->SetOpacity(0.5);
  actorHull->GetProperty()->SetColor( hullColor );


  vtkSmartPointer<vtkRenderer> renderer = vtkSmartPointer<vtkRenderer>::New();
  renderer->AddActor( actormesh );
//  renderer->AddActor( actorHull );
  renderer->SetBackground( backgroundColor );

  vtkSmartPointer<vtkRenderWindow> renWin = vtkSmartPointer<vtkRenderWindow>::New();
  renWin->AddRenderer(renderer);

  vtkSmartPointer<vtkRenderWindowInteractor> iren = vtkSmartPointer<vtkRenderWindowInteractor>::New();
  iren->SetRenderWindow(renWin);
  renWin->SetSize(1024, 768);


  renderer->Render();
  iren->Initialize();
  iren->Start();

  return 0;
}
