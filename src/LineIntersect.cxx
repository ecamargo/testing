#define EPS 0.000000000001

#include <iostream>
#include <math.h>

using namespace std;

typedef struct {
   double x,y,z;
} XYZ;



bool ComputeIntersectionPointOfLines2( double p0[3], double p1[3], double n0[3], double n1[3], double *intersection )
{
  double a, b, c, d, e, f, g, h, i, j, k, l, s;
  double L0, M0, N0, L1, M1, N1;
  double x, y ,z;


  a = p0[0];   d = p1[0];
  b = p0[1];   e = p1[1];
  c = p0[2];   f = p1[2];

  g = n0[0];   j = n1[0];
  h = n0[1];   k = n1[1];
  i = n0[2];   l = n1[2];

  L0 = d-a;   M0 = e-b;   N0 = f-c;
  L1 = j-g;   M1 = k-h;   N1 = l-i;


  cout << "p0: " << a << ", " << b << ", " << c <<endl;
  cout << "p1: " << d << ", " << e << ", " << f <<endl;
  cout << "\t >> L0: " << L0 << endl;
  cout << "\t >> M0: " << M0 << endl;
  cout << "\t >> N0: " << N0 << endl;

  cout << endl;
  cout << "n0: " << g << ", " << h << ", " << i <<endl;
  cout << "n1: " << j << ", " << k << ", " << l <<endl;
  cout << "\t >> L1: " << L1 << endl;
  cout << "\t >> M1: " << M1 << endl;
  cout << "\t >> N1: " << N1 << endl;

  // se forem paralelas
  if( L0/L1 == M0/M1 &&
      M0/M1 == N0/N1  )
    {
    return false;
    }

  if( fabs(-M0*L1 + M1*L0) > 0.0 )
    {
    y = ( M1*M0*(g-a) - M0*L1*h + M1*L0*b ) / (-M0*L1 + M1*L0);

    x = a + L0 * ( (y-b)/M0 );

    z = ( (y-h)/M1 )*N1 + i;



    intersection[0] = x;
    intersection[1] = y;
    intersection[2] = z;

    cout << "intersection: " << intersection[0] << ", " << intersection[1] << ", " << intersection[2] <<endl;

    return true;
    }

  return false;
}


void ComputeIntersectionPointOfLines( double k[3], double l[3], double m[3], double n[3], double *intersection )
{
  double det, kx, ky, kz, lx, ly, lz, mx, my, mz, nx, ny, nz, s;
  //  double t;

  kx = k[0];   lx = l[0];
  ky = k[1];   ly = l[1];
  kz = k[2];   lz = l[2];

  mx = m[0];   nx = n[0];
  my = m[1];   ny = n[1];
  mz = m[2];   nz = n[2];


  cout << "k: " << kx << ", " << ky << ", " << kz <<endl;
  cout << "l: " << lx << ", " << ly << ", " << lz <<endl;
  cout << "m: " << mx << ", " << my << ", " << mz <<endl;
  cout << "n: " << nx << ", " << ny << ", " << nz <<endl;


  if (((kx != lx) || (ky != ly))  &&
      ((mx != nx) || (my != ny)) )  /* se nao e' paralela ao plano XY*/
    {
    cout << "primeiro if" <<endl;

    det = (nx - mx) * (ly - ky)  -  (ny - my) * (lx - kx);

    if (det != 0.0)
      {
      s = ((nx - mx) * (my - ky) - (ny - my) * (mx - kx))/ det ;
//      t = ((lx - kx) * (my - ky) - (ly - ky) * (mx - kx))/ det ;
      }
    }





  if (((kx != lx) || (kz != lz))  &&
      ((mx != nx) || (mz != nz)) )  /* se nao e' paralela ao plano XZ*/
    {
    cout << "segundo if" <<endl;

    det = (nx - mx) * (lz - kz)  -  (nz - mz) * (lx - kx);

    if (det != 0.0)
      {
      s = ((nx - mx) * (mz - kz) - (nz - mz) * (mx - kx))/ det ;
  //  t = ((lx - kx) * (mz - kz) - (lz - kz) * (mx - kx))/ det ;
      }

    }




  if (((ky != ly) || (kz != lz))  &&
      ((my != ny) || (mz != nz)) )  /* se nao e' paralela ao plano YZ*/
    {
    cout << "terceiro if" <<endl;

    det = (ny - my) * (lz - kz)  -  (nz - mz) * (ly - ky);

    if (det != 0.0)
      {
      s = ((ny - my) * (mz - kz) - (nz - mz) * (my - ky))/ det ;
//      t = ((ly - ky) * (mz - kz) - (lz - kz) * (my - ky))/ det ;
      }

    }


  intersection[0] = kx + (lx-kx)*s;
  intersection[1] = ky + (ly-ky)*s;
  intersection[2] = kz + (lz-kz)*s;

  cout << "intersection1: " << intersection[0] << ", " << intersection[1] << ", " << intersection[2] <<endl;
}

/*
   Calculate the line segment PaPb that is the shortest route between
   two lines P1P2 and P3P4. Calculate also the values of mua and mub where
      Pa = P1 + mua (P2 - P1)
      Pb = P3 + mub (P4 - P3)
   Return FALSE if no solution exists.
*/
int LineLineIntersect(
   XYZ p1,XYZ p2,XYZ p3,XYZ p4,XYZ *pa,XYZ *pb,
   double *mua, double *mub)
{
   XYZ p13,p43,p21;
   double d1343,d4321,d1321,d4343,d2121;
   double numer,denom;

   p13.x = p1.x - p3.x;
   p13.y = p1.y - p3.y;
   p13.z = p1.z - p3.z;
   p43.x = p4.x - p3.x;
   p43.y = p4.y - p3.y;
   p43.z = p4.z - p3.z;

   if( fabs(p43.x) < EPS && fabs(p43.y) < EPS && fabs(p43.z) < EPS )
      return 1;
   p21.x = p2.x - p1.x;
   p21.y = p2.y - p1.y;
   p21.z = p2.z - p1.z;
   if (fabs(p21.x) < EPS && fabs(p21.y) < EPS && fabs(p21.z) < EPS)
      return 1;

   d1343 = p13.x * p43.x + p13.y * p43.y + p13.z * p43.z;
   d4321 = p43.x * p21.x + p43.y * p21.y + p43.z * p21.z;
   d1321 = p13.x * p21.x + p13.y * p21.y + p13.z * p21.z;
   d4343 = p43.x * p43.x + p43.y * p43.y + p43.z * p43.z;
   d2121 = p21.x * p21.x + p21.y * p21.y + p21.z * p21.z;

   denom = d2121 * d4343 - d4321 * d4321;
   if (fabs(denom) < EPS)
      return 1;
   numer = d1343 * d4321 - d1321 * d4343;

   *mua = numer / denom;
   *mub = (d1343 + d4321 * (*mua)) / d4343;

   pa->x = p1.x + *mua * p21.x;
   pa->y = p1.y + *mua * p21.y;
   pa->z = p1.z + *mua * p21.z;
   pb->x = p3.x + *mub * p43.x;
   pb->y = p3.y + *mub * p43.y;
   pb->z = p3.z + *mub * p43.z;

   return 0;
}



int main( int argc, char *argv[] )
{
//  XYZ p1, p2, p3, p4, pa, pb;
//  double mua, mub;


  double n0[3], n1[3], p0[3], p1[3], intersection[3];

//  p0[0] = -0.00487998;
//  p0[1] = 1.36237;
//  p0[2] = -0.00181009;
//
//  p1[0] = 0.000286288;
//  p1[1] = 2.3307;
//  p1[2] = -0.00000164104;
//
//  n0[0] = 0.350361;
//  n0[1] = 1.2813;
//  n0[2] = 0.355536;
//
//  n1[0] = 0.348014;
//  n1[1] = -0.291858;
//  n1[2] = 0.35774;


  p0[0] = -0.00539553;
  p0[1] = -0.272568;
  p0[2] = -0.00376492;

  p1[0] = -0.000274913;
  p1[1] = -1.24947;
  p1[2] = 0.0000957706;

  n0[0] = 0.348697;
  n0[1] = 1.28137;
  n0[2] = 0.357099;

  n1[0] = 0.349792;
  n1[1] = -0.292015;
  n1[2] = 0.35607;
//  vertice = 0.404156, -78.406, 0.304983


//  p0[0] = 2.8961;
//  p0[1] = 6.2174;
//  p0[2] = 1;
//
//  p1[0] = 8.6282;
//  p1[1] = 0.9521;
//  p1[2] = 1;
//
//
//  n0[0] = -5.0805;
//  n0[1] = -9.7528;
//  n0[2] = 1;
//
//  n1[0] = 5.1091;
//  n1[1] = -9.1635;
//  n1[2] = 1;


  cout << ComputeIntersectionPointOfLines2( p0, p1, n0, n1, intersection ) <<endl;

  return 0;
}

