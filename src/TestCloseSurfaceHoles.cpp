/*=========================================================================
 Module    : Aneurysm Applications
 File      :
 Copyright : (C)opyright 2013++
 See COPYRIGHT statement in top level directory.
 Authors   : D. Millan
 Modified  :
 Purpose   : close holes mehs from the "Global Processing" pipeline
 Date      :
 Version   :
 Changes   :

 This software is distributed WITHOUT ANY WARRANTY; without even
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the above copyright notices for more information.
 =========================================================================*/

//VTK lib
#include <vtkstd/exception>
#include <vtkPolyDataReader.h>
#include <vtkFeatureEdges.h>
#include <vtkIdList.h>
#include <vtkPolyDataConnectivityFilter.h>
#include <vtkCleanPolyData.h>
#include <vtkTriangleFilter.h>
#include <vtkAppendPolyData.h>
#include <vtkPolyDataCollection.h>
#include <vtkPolyData.h>
#include <vtkPolyDataWriter.h>

#include <vtkPolyDataMapper.h>
#include <vtkActor.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkProperty.h>
#include <vtkCamera.h>

#include <vtkPoints.h>
#include <vtkCellArray.h>

//Standard
#include <iostream>
#include <vector>
#include<vtkFillHolesFilter.h>

using namespace std;
void closeSurfaceHolesByVtk( vtkPolyData *surfaceWithHoles, vtkPolyData *surfaceWithoutHoles, double holeSize );
void closeSurfaceHoles( vtkPolyData *surfaceWithHoles, vtkPolyData *surfaceWithoutHoles, int method, int bRender, char *output_name );
void writePolyData( vtkPolyData *meshToWrite, const char *nameFile );

void usage()
{
    cerr << "Usage: closeholes [input] [output] <option>	           			" << endl;
    cerr << "The input/output are Poly Data meshes of triangles					" << endl;
    cerr << "Options:															" << endl;
    cerr << " -v, --verbose    Details the readed/writed file proporties		" << endl;
    cerr << " -nr              No Rendering                      				" << endl;
    cerr << " -method <int>    0: A polygonal cell is created and triangulated (default)\n"
            << "                  1: A point is added in the center and after a \n" << "                     triangulation is performed.              		"
            << endl;
    exit( 1 );
}

int main( int argc, char *argv[] )
{
    if( argc < 3 )
    {
        usage( );
    }

    const char * input_name = NULL, *output_name = NULL;
    input_name = argv[1];
    argc--;
    argv++;
    output_name = argv[1];
    argc--;
    argv++;
    
    bool bRender = true;
    bool ok;
    bool verbose = false;
    //unsigned int method = 0;
    unsigned int sizehole = 0;

    while ( argc > 1 )
    {
        ok = false;
        //Option:
        if( ( ok == false ) && ( strcmp( argv[1], "-nr" ) == 0 ) )
        {
            argc--;
            argv++;
            bRender = false;
            ok = true;
        }
        if( ( ok == false ) && ( ( strcmp( argv[1], "-v" ) == 0 ) || ( strcmp( argv[1], "--verbose" ) == 0 ) ) )
        {
            argc--;
            argv++;
            verbose = true;
            ok = true;
        }
        if( ( ok == false ) && ( strcmp( argv[1], "-sizehole" ) == 0 ) )
        {
            argc--;
            argv++;
            if( argv[1] == NULL )
            {
                cout << "ERROR:: method IS NULL" << endl;
                usage( );
            }
            sizehole = atoi( argv[1] );
            argc--;
            argv++;
            ok = true;
        }
        if( ok == false )
        {
            cerr << "Can not parse argument " << argv[1] << endl;
            usage( );
        }
    }

    vtkPolyDataReader *reader = vtkPolyDataReader::New( );
    reader->SetFileName( input_name );
    try
    {
        reader->Update( );
    }
    catch ( vtkstd::exception& e )
    {
        cout << "ExceptionObject caught !" << endl;
        cout << e.what( ) << endl;
        return -1;
    }

    if( verbose )
    {
        cout << "Reading PolyData input:  " << input_name << endl;
        cout << "Original surface has:\n\t" << reader->GetOutput( )->GetNumberOfPoints( ) << " Points\n\t" << reader->GetOutput( )->GetNumberOfCells( )
                << " Cells	 " << "\n\t\tverts : " << reader->GetOutput( )->GetNumberOfVerts( ) << "\n\t\tlines : " << reader->GetOutput( )->GetNumberOfLines( )
                << "\n\t\tpolys : " << reader->GetOutput( )->GetNumberOfPolys( ) << "\n\t\tstrips: " << reader->GetOutput( )->GetNumberOfStrips( ) << endl;
    }
    vtkPolyData *outPolyData = vtkPolyData::New( );
    closeSurfaceHolesByVtk( reader->GetOutput( ), outPolyData, sizehole );
    writePolyData( outPolyData, output_name );
    if( verbose )
        {
            cout << "\n Reading PolyData input:  " << input_name << endl;
            cout << "Surface without Holes has:\n\t" << outPolyData->GetNumberOfPoints( ) << " Points\n\t" << outPolyData->GetNumberOfCells( )
                    << " Cells   " << "\n\t\tverts : " << outPolyData->GetNumberOfVerts( ) << "\n\t\tlines : " << outPolyData->GetNumberOfLines( )
                    << "\n\t\tpolys : " << outPolyData->GetNumberOfPolys( ) << "\n\t\tstrips: " << outPolyData->GetNumberOfStrips( ) << endl;
        }
    
    if( bRender )
    {
        vtkPolyDataMapper* mapper = vtkPolyDataMapper::New( );
        mapper->SetInput( outPolyData );
        mapper->ScalarVisibilityOff( );

        vtkActor* actor = vtkActor::New( );
        actor->SetMapper( mapper );
        actor->GetProperty( )->SetColor( 1.0000, 0.3882, 0.2784 );
        actor->RotateX( 30.0 );
        actor->RotateY( 60.0 );
        actor->RotateZ( 30.0 );

        vtkRenderer *ren = vtkRenderer::New( );
        vtkRenderWindow *renWin = vtkRenderWindow::New( );
        renWin->AddRenderer( ren );
        vtkRenderWindowInteractor *iren = vtkRenderWindowInteractor::New( );
        iren->SetRenderWindow( renWin );
        ren->AddActor( actor );
        ren->SetBackground( 1., 1., 1. );
        renWin->SetSize( 700, 700 );

        // We'll zoom in a little by accessing the camera and invoking a "Zoom"
        // method on it.
        renWin->Render( );

        // This starts the event loop and as a side effect causes an initial render.
        iren->Start( );

        // Exiting from here, we have to delete all the instances that
        // have been created.
        mapper->Delete( );
        actor->Delete( );
        ren->Delete( );
        renWin->Delete( );
        iren->Delete( );
    }
    reader->Delete( );
    outPolyData->Delete();
    //writer->Delete( );
}
void writePolyData( vtkPolyData *meshToWrite, const char *nameFile )
{
    vtkPolyDataWriter *writer = vtkPolyDataWriter::New( );
    writer->SetInput( meshToWrite );
    writer->SetFileName( nameFile );
    writer->Write( );
    writer->SetFileTypeToBinary( );
    writer->Update( );
    writer->Delete();
}

void closeSurfaceHolesByVtk( vtkPolyData *surfaceWithHoles, vtkPolyData *surfaceWithoutHoles, double holeSize )
{
    vtkFillHolesFilter *fillSurfaceHoles = vtkFillHolesFilter::New( );
    fillSurfaceHoles->SetInput( surfaceWithHoles );
    fillSurfaceHoles->SetHoleSize( holeSize );
    fillSurfaceHoles->Update( );
    surfaceWithoutHoles->DeepCopy( fillSurfaceHoles->GetOutput( ) );
    fillSurfaceHoles->Delete();

}

void closeSurfaceHoles( vtkPolyData *surfaceWithHoles, vtkPolyData *surfaceWithoutHoles, int method, int bRender, char *output_name )
{

    vtkFeatureEdges * edges = vtkFeatureEdges::New( );
    edges->SetInput( surfaceWithHoles );
    edges->BoundaryEdgesOn( );
    edges->FeatureEdgesOff( );
    edges->ManifoldEdgesOff( );
    edges->NonManifoldEdgesOff( );
    edges->Update( );

    //PolyDataConnectivityFilter: each hole is identified as a region
    vtkPolyDataConnectivityFilter *connec = vtkPolyDataConnectivityFilter::New( );
    connec->SetInput( edges->GetOutput( ) );
    connec->SetExtractionModeToSpecifiedRegions( );
    connec->Update( );

    int numEdgesInRegion = 0;
    int numRegions = connec->GetNumberOfExtractedRegions( );
    vtkIdList* idList = vtkIdList::New( );
    idList->SetNumberOfIds( numRegions );

    vtkCleanPolyData * clean = vtkCleanPolyData::New( );
    vector<vtkPolyData*> poly( numRegions );

    //For each hole (region) the points are stored and sorted
    for ( int idRegion = 0; idRegion < numRegions; idRegion++ )
    {

        connec->AddSpecifiedRegion( idRegion );
        connec->Update( );
        numEdgesInRegion = connec->GetOutput( )->GetNumberOfLines( );

        clean->SetInput( connec->GetOutput( ) );
        clean->Update( );

        //By each hole are created the point and the connectivity lists (not sorted)
        vtkIdList * pts = vtkIdList::New( );
        vtkIdList * cells = vtkIdList::New( );

        pts->InsertNextId( 0 );
        cells->InsertNextId( 0 );

        int npts = clean->GetOutput( )->GetNumberOfPoints( );
        clean->GetOutput( )->BuildLinks( ); //in order to can sort
        clean->GetOutput( )->BuildCells( );
        clean->GetOutput( )->Update( );

        //now the edges and points are sorted
        int ptId = 0, cellId = 0;
        for ( int id = 0; id < npts; id++ )
        {
            vtkIdList * ptIds = vtkIdList::New( );
            vtkIdList * cellIds = vtkIdList::New( );

            clean->GetOutput( )->GetPointCells( pts->GetId( id ), cellIds );
            if( cellIds->GetId( 0 ) == cells->GetId( id ) )
                cellId = cellIds->GetId( 1 );
            else
                cellId = cellIds->GetId( 0 );

            //By each cell are stored the edge cells in a sorted way
            cells->InsertNextId( cellId );

            clean->GetOutput( )->GetCellPoints( cells->GetId( id + 1 ), ptIds );
            if( ptIds->GetId( 0 ) == pts->GetId( id ) )
                ptId = ptIds->GetId( 1 );
            else
                ptId = ptIds->GetId( 0 );

            //By each cell are stored the points in a sorted way
            pts->InsertNextId( ptId );
        }

        vtkCellArray * polygon = vtkCellArray::New( );
        //Add the center point if this method is sets

        if( method ) //npts triangles are created with a point in the centre of the hole
        {
            double centre[3] = { 0.0, 0.0, 0.0 };
            for ( int id = 0; id < npts; id++ )
            {
                centre[0] += clean->GetOutput( )->GetPoint( id )[0];
                centre[1] += clean->GetOutput( )->GetPoint( id )[1];
                centre[2] += clean->GetOutput( )->GetPoint( id )[2];
            }
            centre[0] = centre[0] / ( (double) npts );
            centre[1] = centre[1] / ( (double) npts );
            centre[2] = centre[2] / ( (double) npts );
            clean->GetOutput( )->GetPoints( )->InsertNextPoint( centre );

            for ( int id = 0; id < npts; id++ )
            {
                vtkIdType *ptIds = new vtkIdType[3];
                ptIds[0] = pts->GetId( id + 0 );
                if( id < npts - 1 ) //last triangle
                    ptIds[1] = pts->GetId( id + 1 );
                else
                    ptIds[1] = pts->GetId( 0 );
                ptIds[2] = npts;
                polygon->InsertNextCell( 3, ptIds );
                if( ptIds )
                    delete[] ptIds;
            }
        }
        else //A polygon is created
        {
            polygon->InsertNextCell( pts );
        }

        //Each sorted hole is stored
        poly[idRegion] = vtkPolyData::New( );
        poly[idRegion]->SetPoints( clean->GetOutput( )->GetPoints( ) );
        poly[idRegion]->SetPolys( polygon );
        poly[idRegion]->Update( );
        connec->DeleteSpecifiedRegion( idRegion );
    }

    vtkAppendPolyData *append = vtkAppendPolyData::New( );
    for ( int idRegion = 0; idRegion < numRegions; idRegion++ )
    {
        if( method == 0 )
        {
            vtkTriangleFilter* triang = vtkTriangleFilter::New( );
            triang->SetInput( poly[idRegion] );
            triang->Update( );
            append->AddInput( triang->GetOutput( ) );
        }
        else
        {
            append->AddInput( poly[idRegion] );
        }
    }
    append->Update( );

    vtkAppendPolyData *append2 = vtkAppendPolyData::New( );
    append2->AddInput( append->GetOutput( ) );
    append2->AddInput( surfaceWithHoles );
    append2->Update( );

    vtkCleanPolyData * clean2 = vtkCleanPolyData::New( );
    clean2->SetInput( append2->GetOutput( ) );
    clean2->Update( );

    vtkPolyDataWriter *writer = vtkPolyDataWriter::New( );
    writer->SetInput( clean2->GetOutput( ) );
    writer->SetFileName( output_name );
    writer->Write( );
    writer->Update( );

    if( bRender )
    {
        vtkPolyDataMapper* mapper = vtkPolyDataMapper::New( );
        mapper->SetInput( clean2->GetOutput( ) );
        mapper->ScalarVisibilityOff( );

        vtkActor* actor = vtkActor::New( );
        actor->SetMapper( mapper );
        actor->GetProperty( )->SetColor( 1.0000, 0.3882, 0.2784 );
        actor->RotateX( 30.0 );
        actor->RotateY( 60.0 );
        actor->RotateZ( 30.0 );

        vtkRenderer *ren = vtkRenderer::New( );
        vtkRenderWindow *renWin = vtkRenderWindow::New( );
        renWin->AddRenderer( ren );
        vtkRenderWindowInteractor *iren = vtkRenderWindowInteractor::New( );
        iren->SetRenderWindow( renWin );
        ren->AddActor( actor );
        ren->SetBackground( 1., 1., 1. );
        renWin->SetSize( 700, 700 );

        // We'll zoom in a little by accessing the camera and invoking a "Zoom"
        // method on it.
        renWin->Render( );

        // This starts the event loop and as a side effect causes an initial render.
        iren->Start( );

        // Exiting from here, we have to delete all the instances that
        // have been created.
        mapper->Delete( );
        actor->Delete( );
        ren->Delete( );
        renWin->Delete( );
        iren->Delete( );
    }
    //reader->Delete( );
    writer->Delete( );
}
