#include "vtkRenderWindowInteractor.h"
#include "vtkRenderWindow.h"
#include "vtkRenderer.h"

#include "vtkSmartPointer.h"
#include "vtkPolyDataReader.h"
#include "vtkPolyDataWriter.h"
#include "vtkPolyData.h"
#include "vtkPolyDataMapper.h"
#include "vtkActor.h"
#include "vtkProperty.h"
#include "vtkIdList.h"
#include "vtkPointData.h"
#include "vtkMath.h"
#include "vtkSphereSource.h"
#include "vtkDoubleArray.h"


#include <list>
using namespace std;



// para Testes

// Imprime os ids dos pontos que formam o segmento Proximal -> _NeighborOfP1
//--------------------------------------------------------------------------------------------------
void PrintList(list<int> *l)
{
	list<int>::iterator it;

	cout << "Num elem da lista: " << l->size() << endl;
	for(it = l->begin(); it != l->end(); ++it)
		{
		cout << "\t (*it): " << (*it) << endl;
		}

}

//--------------------------------------------------------------------------------------------------
void SetSpherePoints( vtkSmartPointer<vtkPolyData> centerline, vtkSmartPointer<vtkRenderer> renderer,
		int userId, double *xyz )
{
	int phi, theta;
	double radius = 0.1;
	phi = theta = 30;

	vtkSmartPointer<vtkSphereSource> sph_userId = vtkSmartPointer<vtkSphereSource>::New();
	sph_userId->SetCenter( centerline->GetPoint(userId) );
	sph_userId->SetRadius( radius );
	sph_userId->SetThetaResolution( theta );
	sph_userId->SetPhiResolution( phi );

	vtkSmartPointer<vtkPolyDataMapper> mapper_sph_userId = vtkSmartPointer<vtkPolyDataMapper>::New();
	mapper_sph_userId->SetInput( sph_userId->GetOutput() );

	vtkSmartPointer<vtkActor> actor_sph_userId = vtkSmartPointer<vtkActor>::New();
	actor_sph_userId->SetMapper( mapper_sph_userId );
	actor_sph_userId->GetProperty()->SetColor(0, 1, 0);

	renderer->AddActor( actor_sph_userId );




	vtkSmartPointer<vtkSphereSource> sph_newPoint = vtkSmartPointer<vtkSphereSource>::New();
	sph_newPoint->SetCenter( xyz );
	sph_newPoint->SetRadius( radius );
	sph_newPoint->SetThetaResolution( theta );
	sph_newPoint->SetPhiResolution( phi );

	vtkSmartPointer<vtkPolyDataMapper> mapper_newPoint = vtkSmartPointer<vtkPolyDataMapper>::New();
	mapper_newPoint->SetInput( sph_newPoint->GetOutput() );

	vtkSmartPointer<vtkActor> actor_newPoint = vtkSmartPointer<vtkActor>::New();
	actor_newPoint->SetMapper( mapper_newPoint );
	actor_newPoint->GetProperty()->SetColor(1, 0, 0);

	renderer->AddActor( actor_newPoint );
}

//--------------------------------------------------------------------------------------------------
void MarkPoints( vtkSmartPointer<vtkPolyData> centerline, vtkSmartPointer<vtkRenderer> renderer )
{
	int phi, theta;
	double radius = 0.1;
	phi = theta = 30;

	for(int i=0; i < centerline->GetNumberOfPoints(); ++i)
		{
		vtkSmartPointer<vtkSphereSource> sph_userId = vtkSmartPointer<vtkSphereSource>::New();
		sph_userId->SetCenter( centerline->GetPoint(i) );
		sph_userId->SetRadius( radius );
		sph_userId->SetThetaResolution( theta );
		sph_userId->SetPhiResolution( phi );

		vtkSmartPointer<vtkPolyDataMapper> mapper_sph_userId = vtkSmartPointer<vtkPolyDataMapper>::New();
		mapper_sph_userId->SetInput( sph_userId->GetOutput() );

		vtkSmartPointer<vtkActor> actor_sph_userId = vtkSmartPointer<vtkActor>::New();
		actor_sph_userId->SetMapper( mapper_sph_userId );
		//    actor_sph_userId->GetProperty()->SetColor(0, 0, 1);
		actor_sph_userId->GetProperty()->SetOpacity(0.5);

		renderer->AddActor( actor_sph_userId );
		}
}

//******************************************

void AddDs( vtkSmartPointer<vtkRenderer> renderer, vtkSmartPointer<vtkPolyData> D)
{
	int phi, theta;
	double radius = 0.1;
	phi = theta = 30;
	double p[3], r;

	for(int i=0; i < D->GetNumberOfPoints(); ++i)
		{
		D->GetPoint(i, p);
		r = D->GetPointData()->GetArray(0)->GetTuple1(i);

		cout << "p: " << p[0] << ", " << p[1] << ", " << p[2] << "\t r: " << r << endl;

		vtkSmartPointer<vtkSphereSource> sph_userId = vtkSmartPointer<vtkSphereSource>::New();
		sph_userId->SetCenter( p );
		sph_userId->SetRadius( radius );
		sph_userId->SetThetaResolution( theta );
		sph_userId->SetPhiResolution( phi );


		vtkSmartPointer<vtkPolyDataMapper> mapper_sph_userId = vtkSmartPointer<vtkPolyDataMapper>::New();
		mapper_sph_userId->SetInput( sph_userId->GetOutput() );

		vtkSmartPointer<vtkActor> actor_sph_userId = vtkSmartPointer<vtkActor>::New();
		actor_sph_userId->SetMapper( mapper_sph_userId );
		actor_sph_userId->GetProperty()->SetColor(0, 0, 1);
		actor_sph_userId->GetProperty()->SetOpacity(0.5);

		renderer->AddActor( actor_sph_userId );
		}
}



class AverageDiameterOfParentArtery
{
	public:
		//--------------------------------------------------------------------------------------------------
		AverageDiameterOfParentArtery()
		{
		NewPoint[0] = 0.0;
		NewPoint[1] = 0.0;
		NewPoint[2] = 0.0;

		NewRadius = -1.0;

		ProximalId = 0;
		PtId1 = 1;
		NeighborOfP1 = -1;

		DistalId = vtkSmartPointer<vtkIdList>::New();
		Centerline = vtkSmartPointer<vtkPolyData>::New();
		}

		//--------------------------------------------------------------------------------------------------
		~AverageDiameterOfParentArtery(){}

		//--------------------------------------------------------------------------------------------------
		void SetInput( vtkSmartPointer<vtkPolyData> input )
		{
			Centerline->DeepCopy( input );
		}

		//--------------------------------------------------------------------------------------------------
		void SetProximalPoint( int pointId )
		{
			ProximalId = pointId;
		}

		//--------------------------------------------------------------------------------------------------
		void SetP1( int pointId )
		{
			PtId1 = pointId;
		}

		//--------------------------------------------------------------------------------------------------
		void SetDistalPoints( vtkSmartPointer<vtkIdList> distal )
		{
			DistalId->DeepCopy( distal );
		}

		//--------------------------------------------------------------------------------------------------
		double GetAverageDiameter()
		{
			return AverageDiameter;
		}

		//--------------------------------------------------------------------------------------------------
		void GetP2( double newPoint[3], double *newRadius )
		{
			newPoint[0] = NewPoint[0];
			newPoint[1] = NewPoint[1];
			newPoint[2] = NewPoint[2];

			*newRadius = NewRadius;
		}

		//--------------------------------------------------------------------------------------------------
		int GetNeighborOfP1()
		{
			return ( *VisitedPoints.rbegin() );
		}

		//--------------------------------------------------------------------------------------------------
		void Update()
		{
			FindPointNeighborOfP1( ProximalId );

			ComputeAverageDiameter();
			ComputeD1(0.6);
		}

		//--------------------------------------------------------------------------------------------------
		list<int>* GetVisitedPoint()
  					{
			return &VisitedPoints;
  					}

		vtkSmartPointer<vtkPolyData> GetD1()
  					{
			return D_points;
  					}


	protected:

		// Verifica se o id encontra-se na lista de pontos visitados
		//--------------------------------------------------------------------------------------------------
		bool WasVisited(int id)
		{
		list<int>::iterator it;

		bool ret = false;

		for(it = VisitedPoints.begin(); it != VisitedPoints.end(); ++it)
			{
			if( (*it) == id )
				{
				//      cout << "(*it): " << (*it) << "\t id: " << id <<endl;
				ret = true;
				}
			}

		return ret;
		}

		// Recupera um vtkIdList com os vizinhos do ponto ptId
		//--------------------------------------------------------------------------------------------------
		vtkSmartPointer<vtkIdList> GetPointNeighbors( int ptId )
  					{
			vtkSmartPointer<vtkIdList> neighbors = vtkSmartPointer<vtkIdList>::New();

			vtkSmartPointer<vtkIdList> cellIds = vtkSmartPointer<vtkIdList>::New();

			Centerline->GetPointCells( ptId, cellIds );

			for(int i=0; i<cellIds->GetNumberOfIds(); ++i)
				{
				vtkSmartPointer<vtkIdList> points = vtkSmartPointer<vtkIdList>::New();
				Centerline->GetCellPoints( cellIds->GetId(i), points );

				if( points->GetNumberOfIds() > 2 )
					cout << "Error! Cell " << cellIds->GetId(i) << " with to many points." <<endl;


				for(int j=0; j < points->GetNumberOfIds(); ++j)
					{
					if( points->GetId(j) != ptId )
						{
						//        cout << points->GetId(j) << " é vizinho de " << ptId <<endl;
						neighbors->InsertNextId( points->GetId(j) );
						}
					}
				}


			return neighbors;
  					}

		// Exclui os ids que foram visitados
		//--------------------------------------------------------------------------------------------------
		vtkSmartPointer<vtkIdList> GetNeighborsNotVisited( vtkSmartPointer<vtkIdList> neighbors )
  					{
			vtkSmartPointer<vtkIdList> notVisited = vtkSmartPointer<vtkIdList>::New();

			for(int i=0; i < neighbors->GetNumberOfIds(); ++i)
				{
				int id = neighbors->GetId(i);

				if( !WasVisited( id ) ) // se o ponto ainda não foi visitado
					notVisited->InsertNextId( id );
				}

			return notVisited;
  					}

		// Utiliza a equação paramétrica da reta r: xn = x0 + (x1 - x0)*t
		//                                          yn = y0 + (y1 - y0)*t
		//                                          zn = z0 + (z1 - z0)*t
		//
		// em conjunto a distância eucliana d² = (xn - x0)² + (yn - y0)² + (zn - z0)²
		// para interpolar point e radius.
		// Substituir xn, yn e zn da distância pelas equações das retas.
		// d² = (x1 - x0)² * t² + (y1 - y0)² * t² + (z1 - z0)² * t²
		// Isolando t e considerando apenas o valor positivo (vetor vai de p0 para pn para p1) encontra-se a equação
		// t = Distance2BetweenPoints(p0, p1)/ sqrt( (x1 - x0)² + (y1 - y0)² + (z1 - z0)² );
		//--------------------------------------------------------------------------------------------------
		//  void InterpolateNewPointAndRadius( double *point, double *radius )
		void InterpolateNewPointAndRadius( double maxDistance, double *point, double *radius )
		{
			double p1[3], p2[3];
			//    double r = Centerline->GetPointData()->GetArray(0)->GetTuple1( PtId1 );

			int lastElem = VisitedPoints.size() - 1;
			int j = PtId1;

			double distSum = 0.0;

			do
				{
				Centerline->GetPoint( j, p1 );
				Centerline->GetPoint( lastElem, p2 );

				double d = sqrt(vtkMath::Distance2BetweenPoints( p1, p2 ));

				// se distSum ainda não ultrapassou 2*r, continua percorrendo o segmento no sentido _ptId_1 -> Proximal e armazendo o distância total.
				//      if( (2*r) >= (distSum + d) )
				if( maxDistance >= (distSum + d) && j > 1 )
					{
					distSum += d;

					--lastElem;
					--j;
					}
				else // se ultrapassou, interpola-se nova coordenada e raio
					{
					// cálculo do escalar t
					//        double t = d / sqrt( pow(p2[0]-p1[0], 2) + pow(p2[1]-p1[2], 2) + pow(p2[2]-p1[2], 2));
					//        double t = (2*r-distSum) / sqrt(d);
					double t = (maxDistance - distSum) / sqrt(d);

					// tendo p1, p2 e t, pode-se interpolar ponto e raio
					double x_n = p1[0] + (p2[0]-p1[0]) * t;
					double y_n = p1[1] + (p2[1]-p1[1]) * t;
					double z_n = p1[2] + (p2[2]-p1[2]) * t;

					point[0] = x_n;
					point[1] = y_n;
					point[2] = z_n;


					double r1 = Centerline->GetPointData()->GetArray(0)->GetTuple1( j );
					double r2 = Centerline->GetPointData()->GetArray(0)->GetTuple1( lastElem );
					*radius = r1 + (r2-r1) * t;

					cout << "j: "<< j <<endl;
					cout << "lastElem: "<< lastElem <<endl;
					//								        cout << "p1: " << p1[0] << ", " << p1[1] << ", " << p1[2] << "\t radius: " << r1 << endl;
					//								        cout << "p2: " << p2[0] << ", " << p2[1] << ", " << p2[2] << "\t radius: " << r2 << endl;
					//								        cout << "point: " << point[0] << ", " << point[1] << ", " << point[2] << "\t radius: " << *radius << endl;

					distSum += d;
					}

				//      }while( (2*r) > distSum );
				}while( maxDistance > distSum );
		}

		//--------------------------------------------------------------------------------------------------
		void ComputeD1( double distance )
		{
			double point[3], radius;

			vtkSmartPointer<vtkDoubleArray> arr = vtkSmartPointer<vtkDoubleArray>::New();
			vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();

			arr->SetNumberOfComponents(1);
			arr->SetName("Radius");

			for(int i=1; i<=4; ++i)
				{
				int id;
				InterpolateNewPointAndRadius( i*distance, point, &radius );
				id = points->InsertNextPoint( point[0], point[1], point[2] );
				arr->InsertNextTuple( &radius );

				cout << "point: " << point[0] << ", " << point[1] << ", " << point[2] << "\t radius: " << radius << endl;
				cout << "pt id: " <<  id <<endl;
				}

			D_points = vtkSmartPointer<vtkPolyData>::New();
			D_points->SetPoints( points );
			D_points->GetPointData()->AddArray( arr );

			cout << *D_points <<endl;
		}


		// Calcula o diâmetro médio usando a fórmula DM = 2*R1 + 2*R2)/2, onde:
		// R1 = raio do ponto _ptId_1; ponto informado inicialmente pelo usuário
		// R2 = raio do ponto newPoint = newRadius; calculados pelo método InterpolateNewPointAndRadius
		//--------------------------------------------------------------------------------------------------
		void ComputeAverageDiameter()
		{
			double p1[3], p2[3];
			double r1 = Centerline->GetPointData()->GetArray(0)->GetTuple1( PtId1 );
			double r2 = 0.0;

			Centerline->GetPoint( PtId1, p1 );
			//    InterpolateNewPointAndRadius( p2, &r2 );
			double r = Centerline->GetPointData()->GetArray(0)->GetTuple1( PtId1 );

			InterpolateNewPointAndRadius( 2*r, p2, &r2 );


			NewPoint[0] = p2[0];
			NewPoint[1] = p2[1];
			NewPoint[2] = p2[2];

			NewRadius = r2;

			AverageDiameter = (2*r1 + 2*r2)/2;
		}

		// Econtra o id do ponto vizinho a P1 no sentido P1 -> Proximal
		//--------------------------------------------------------------------------------------------------
		void FindPointNeighborOfP1( int ptInicial )
		{
			int p1;

			p1 = ptInicial; //ponto de busca inicial deverá ser passado o proximal.


			if(p1 == PtId1)
				return;


			// marca o ponto como visitado.
			VisitedPoints.push_back( p1 );


			NeighborOfP1 = p1;


			// recupera vizinhos de p1
			vtkSmartPointer<vtkIdList> neighbors = GetPointNeighbors( p1 );

			// lista dos vizinhos ainda não visitados
			vtkSmartPointer<vtkIdList> neighborsNotVisited = GetNeighborsNotVisited( neighbors );

			//loop recursivo para os vizinhos ainda não visitados
			for(int i=0; i < neighborsNotVisited->GetNumberOfIds(); ++i)
				{
				FindPointNeighborOfP1( neighborsNotVisited->GetId(i) );
				}
		}

		vtkSmartPointer<vtkPolyData> Centerline;

		list<int> VisitedPoints;

		double AverageDiameter;
		double NewPoint[3];
		double NewRadius;

		int ProximalId;                           // a linha de centro possui somente uma entrada de fluxo (ponto proximal).
		int PtId1;                                // ponto P1 selecionado pelo usuário, é assumido que o mesmo foi posicionado antes do aneurisma.
		int NeighborOfP1;
		vtkSmartPointer<vtkIdList> DistalId;      // a linha de centro pode possuir várias saídas de fluxo (pontos distais).

		vtkSmartPointer<vtkPolyData> D_points;
};




//--------------------------------------------------------------------------------------------------
int main( int argc, char **argv )
{
	double backgroundColor[] = {0.8, 0.8, 0.8};
	double lineColor[] = {0.0, 0.0, 0.5};

	double AverageDiameter;
	double newPoint[3];
	double newRadius;

	int ProximalId; //ponto de entrada de fluxo na centerline
	int ptId_1;
	int NeighborOfP1;


	vtkSmartPointer<vtkIdList> DistalId = vtkSmartPointer<vtkIdList>::New(); //ponto(s) de saída de fluxo na centerline, pode(m) ser informado mas não é essencial

	string fileNameline;
	string fileNamemesh;


	// *********** input teste 1 - sem informar Distal
	ProximalId = 0;
	ptId_1 = 20;
	NeighborOfP1 = -1;
	fileNameline.append("/home/eduardo/workspace/data_HeMoLab/reuniao/ParentVesselReconstruction/id1/centerline_id1_model.vtk");
	fileNamemesh.append("/home/eduardo/workspace/data_HeMoLab/reuniao/ParentVesselReconstruction/id1/id1_model.vtk");
	//---------------------------------

	// *********** input teste 2 - informando Distal
	//  ProximalId = 0;
	//  ptId_1 = 18;
	//  NeighborOfP1 = -1;
	//  DistalId->InsertNextId(64);
	//  DistalId->InsertNextId(37);
	//  fileNameline.append("/home/eduardo/workspace/data_HeMoLab/reuniao/ParentVesselReconstruction/id2/centerline_id2_model.vtk");
	//  fileNamemesh.append("/home/eduardo/workspace/data_HeMoLab/reuniao/ParentVesselReconstruction/id2/id2_model.vtk");
	//---------------------------------


	vtkSmartPointer<vtkPolyDataReader> readerline = vtkSmartPointer<vtkPolyDataReader>::New();
	readerline->SetFileName( fileNameline.c_str() );
	readerline->Update();


	vtkSmartPointer<vtkPolyDataReader> readermesh = vtkSmartPointer<vtkPolyDataReader>::New();
	readermesh->SetFileName( fileNamemesh.c_str() );
	readermesh->Update();


	vtkSmartPointer<vtkPolyData> line = vtkSmartPointer<vtkPolyData>::New();
	line->DeepCopy( readerline->GetOutput() );
	line->Update();

	AverageDiameterOfParentArtery *avg = new AverageDiameterOfParentArtery;
	avg->SetInput( line );
	avg->SetProximalPoint( ProximalId );
	avg->SetDistalPoints( DistalId );
	avg->SetP1( ptId_1 );

	avg->Update();

	AverageDiameter = avg->GetAverageDiameter();
	avg->GetP2( newPoint, &newRadius );
	NeighborOfP1 = avg->GetNeighborOfP1();





	//  cout << "ProximalId: " << ProximalId <<endl;
	//  for(int i=0; i< DistalId->GetNumberOfIds(); ++i)
	//    {
	//    cout << "DistalId->GetId("<<i<<"): " << DistalId->GetId(i) <<endl;
	//    }
	//  double d[3];
	//  line->GetPoint( ptId_1, d );
	//  cout << "Selected point (ptId_1): " << ptId_1 << "\t" << d[0]<< ", " << d[1] << ", " << d[2] <<endl;
	//  cout << "Neighbor of selected point (SENTIDO _ptId_1 -> Proximal): " << NeighborOfP1 <<endl;
	//
	//  cout << "Average diameter: " << AverageDiameter << "\t newPoint: "<< newPoint[0]<< ", " << newPoint[1] << ", " << newPoint[2] << "\t newRadius: "<< newRadius << endl;

	//  PrintList( avg->GetVisitedPoint() );







	vtkSmartPointer<vtkPolyDataMapper> mapperline = vtkSmartPointer<vtkPolyDataMapper>::New();
	mapperline->SetInput( line );

	vtkSmartPointer<vtkActor> actorline = vtkSmartPointer<vtkActor>::New();
	actorline->SetMapper( mapperline );
	actorline->GetProperty()->SetColor( lineColor );


	vtkSmartPointer<vtkPolyDataMapper> mappermesh = vtkSmartPointer<vtkPolyDataMapper>::New();
	mappermesh->SetInput( readermesh->GetOutput() );

	vtkSmartPointer<vtkActor> actormesh = vtkSmartPointer<vtkActor>::New();
	actormesh->SetMapper( mappermesh );
	actormesh->GetProperty()->SetOpacity(0.3);


	vtkSmartPointer<vtkRenderer> renderer = vtkSmartPointer<vtkRenderer>::New();
	renderer->AddActor( actorline );
	renderer->AddActor( actormesh );
	renderer->SetBackground( backgroundColor );


	SetSpherePoints( line, renderer, ptId_1, newPoint );
	MarkPoints( line, renderer );

	vtkSmartPointer<vtkPolyData> points = avg->GetD1();
	cout << *points <<endl;
	AddDs( renderer, points );


	vtkSmartPointer<vtkRenderWindow> renWin = vtkSmartPointer<vtkRenderWindow>::New();
	renWin->AddRenderer(renderer);

	vtkSmartPointer<vtkRenderWindowInteractor> iren = vtkSmartPointer<vtkRenderWindowInteractor>::New();
	iren->SetRenderWindow(renWin);
	renWin->SetSize(1024, 768);


	delete avg;

	renderer->Render();
	iren->Initialize();
	iren->Start();



	return 0;
}
