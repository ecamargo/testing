

#include "vtkPolyDataReader.h"
#include "vtkPolyDataWriter.h"
#include "vtkPolyData.h"
#include "vtkSmartPointer.h"
#include <vtkActor.h>
#include <vtkRenderWindow.h>
#include <vtkRenderer.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkPolyDataMapper.h>
#include <vtkIntArray.h>
#include <vtkCell.h>
#include <vtkCellArray.h>
#include <vtkIdList.h>
#include <vtkFeatureEdges.h>
#include <vtkSortDataArray.h>

#include <string.h>
#include <map>
#include <vector>

using namespace std;


vtkSmartPointer<vtkIdList> meshSackBorderPointList;
vtkSmartPointer<vtkIdList> visitedCells;
vtkSmartPointer<vtkIdList> insertedPoints;
std::map<int,int> pointsMap;


//-----------------------------------------------------------------------------------------------------------
void WritePolyData( vtkSmartPointer<vtkPolyData> poly, string fullName)
{
  vtkSmartPointer<vtkPolyDataWriter> writer = vtkSmartPointer<vtkPolyDataWriter>::New();
  writer->SetFileName( fullName.c_str() );
  writer->SetInput( poly );
  writer->Update();
}


//-----------------------------------------------------------------------------------------------------------
bool HasOnlyTriangles( vtkSmartPointer<vtkPolyData> poly )
{
  vtkSmartPointer<vtkIdList> ptList = vtkSmartPointer<vtkIdList>::New();
  for(int i=0; i < poly->GetNumberOfCells(); ++i)
    {
    poly->GetCellPoints( i, ptList );

    if(ptList->GetNumberOfIds() != 3)
      {
      cout << "CellId: " << i << "is not triangle." <<endl;
      return false;
      }
    }

  return true;
}

//-----------------------------------------------------------------------------------------------------------
vtkSmartPointer<vtkPolyData> CreateBorder( vtkSmartPointer<vtkPolyData> mesh, vtkSmartPointer<vtkPolyData> featureEdges )
{
  double p0[3], p1[3];

  vtkSmartPointer<vtkPolyData> border = vtkSmartPointer<vtkPolyData>::New();
  border->SetPoints( vtkSmartPointer<vtkPoints>::New() );
  border->SetLines( vtkSmartPointer<vtkCellArray>::New() );

  std::map<int,int>::iterator it0;
  std::map<int,int>::iterator it1;
  std::map<int,int>::reverse_iterator rit;


  vtkSmartPointer<vtkIdList> line = vtkSmartPointer<vtkIdList>::New();

  it0 = pointsMap.begin();
  it1 = pointsMap.begin();
  rit = pointsMap.rbegin();

  for(it0 = pointsMap.begin(); it0 != pointsMap.end(); ++it0)
    {
    if( (it0->first) == (rit->first) && (it0->second) == (rit->second) )
      it1 = pointsMap.begin();
    else
      ++it1;

    mesh->GetPoint( it0->second, p0);
    mesh->GetPoint( it1->second, p1);

    line->InsertNextId( border->GetPoints()->InsertNextPoint( p0 ) );
    line->InsertNextId( border->GetPoints()->InsertNextPoint( p1 ) );

    cout << "ligando: " << it0->second << " " << it1->second << endl;
    border->InsertNextCell( VTK_LINE, line );

    border->Update();

    line->Reset();
    }


  return border;
}

//-----------------------------------------------------------------------------------------------------------
vtkSmartPointer<vtkPolyData> BuildListOfPoinsInBorder( vtkSmartPointer<vtkPolyData> mesh, vtkSmartPointer<vtkPolyData> featureEdges )
{
  double point[3];
  int meshId;
  std::map<int,int>::iterator it;

  vtkSmartPointer<vtkPolyData> border = vtkSmartPointer<vtkPolyData>::New();
  border->SetPoints( vtkSmartPointer<vtkPoints>::New() );
  border->SetLines( vtkSmartPointer<vtkCellArray>::New() );


  for(int i=0; i < featureEdges->GetNumberOfPoints(); ++i)
    {
    featureEdges->GetPoint(i, point);
    meshId = mesh->FindPoint(point);

    border->GetPoints()->InsertNextPoint( point );

    pointsMap.insert( std::pair<int, int>(i, meshId) );
//    cout << "i: " << i << " meshId: " << meshId <<endl;
    }

  border->Update();


  vtkSmartPointer<vtkIdList> ptIds = vtkSmartPointer<vtkIdList>::New();
  for(int i=0; i < featureEdges->GetNumberOfCells(); ++i)
    {
    vtkSmartPointer<vtkCell> cell = featureEdges->GetCell(i);
    featureEdges->GetCellPoints(i, ptIds);

    vtkSmartPointer<vtkIdList> line = vtkSmartPointer<vtkIdList>::New();
    for(int j=0; j < ptIds->GetNumberOfIds(); ++j)
      {
      line->InsertNextId( ptIds->GetId(j) );
      }

    border->InsertNextCell( VTK_LINE, line );
    }

  border->Update();

  return border;
}

//-----------------------------------------------------------------------------------------------------------
vtkSmartPointer<vtkIdList> GetNeighborsNotVisitedCells( int pointId, vtkSmartPointer<vtkPolyData> featureEdges )
{
  vtkSmartPointer<vtkIdList> cellIds = vtkSmartPointer<vtkIdList>::New();
  vtkSmartPointer<vtkIdList> NeighborsNotVisitedCells = vtkSmartPointer<vtkIdList>::New();

  featureEdges->GetPointCells( pointId, cellIds );

  for(int i=0; i < cellIds->GetNumberOfIds(); ++i)
    {
    if( visitedCells->IsId( cellIds->GetId(i) ) == -1 )
      NeighborsNotVisitedCells->InsertNextId( cellIds->GetId(i) );
    }

  return NeighborsNotVisitedCells;
}

//-----------------------------------------------------------------------------------------------------------
void ReorderingPoints( int cellId, vtkSmartPointer<vtkPolyData> featureEdges, vtkSmartPointer<vtkPolyData> border )
{
  int id0, id1;
  double p0[3], p1[3];
  vtkSmartPointer<vtkCell> cell;


  if( visitedCells->IsId(cellId) != -1 )
    return;


  if(cellId == 0)
    {
    border->SetPoints( vtkSmartPointer<vtkPoints>::New() );
    border->SetLines( vtkSmartPointer<vtkCellArray>::New() );

    visitedCells->InsertUniqueId(0);
    cell = featureEdges->GetCell(0);
    }
  else
    {
    visitedCells->InsertUniqueId(cellId);
    cell = featureEdges->GetCell(cellId);
    }


  id0 = cell->GetPointId(0);
  id1 = cell->GetPointId(1);

  featureEdges->GetPoint(id0, p0);
  featureEdges->GetPoint(id1, p1);

  if( insertedPoints->GetId(id0) == -1)
    {
    insertedPoints->SetId(id0, border->GetPoints()->InsertNextPoint(p0) );
    }

  if( insertedPoints->GetId(id1) == -1)
    {
    insertedPoints->SetId(id1, border->GetPoints()->InsertNextPoint(p1) );
    }


  vtkSmartPointer<vtkIdList> line = vtkSmartPointer<vtkIdList>::New();
  line->InsertNextId( insertedPoints->GetId(id0) );
  line->InsertNextId( insertedPoints->GetId(id1) );
  cout << "Inserindo cell: " << border->GetNumberOfCells() << " -> " << id0 << " " << id1 << endl;
  border->InsertNextCell( VTK_LINE, line );


  vtkSmartPointer<vtkIdList> neighbors = GetNeighborsNotVisitedCells( id1, featureEdges );
  for(int i=0; i < neighbors->GetNumberOfIds(); ++i)
    {
    ReorderingPoints( neighbors->GetId(i), featureEdges, border );
    }
}

//-----------------------------------------------------------------------------------------------------------
vtkSmartPointer<vtkPolyData> ComputeFeatureEdge( vtkSmartPointer<vtkPolyData> mesh )
{
  vtkSmartPointer<vtkFeatureEdges> featureEdges = vtkSmartPointer<vtkFeatureEdges>::New();
  featureEdges->SetInput( mesh );
  featureEdges->SetBoundaryEdges(1);
  featureEdges->SetFeatureEdges(0);
  featureEdges->SetNonManifoldEdges(0);
  featureEdges->SetManifoldEdges(0);
  featureEdges->SetColoring(0);
  featureEdges->SetFeatureAngle(30);
  featureEdges->Update();

  return featureEdges->GetOutput();
}

//-----------------------------------------------------------------------------------------------------------
int GetNextPoint( int finalPoint, int ptId, vtkSmartPointer<vtkIdList> ptIds )
{
  if( ptId != ptIds->GetId(0) && finalPoint != ptIds->GetId(0) )
    {
    return ptIds->GetId(0);
    }
  else if( ptId != ptIds->GetId(1) && finalPoint != ptIds->GetId(1) )
    {
    return ptIds->GetId(1);
    }

  return -1;
}

//-----------------------------------------------------------------------------------------------------------
void RemoveGreaterLoop( std::vector< vtkSmartPointer<vtkIdList> > *loopVector )
{
  int max = 0;
  for(unsigned int j=0; j<loopVector->size(); ++j)
    {
    int n = loopVector->at(j)->GetNumberOfIds();
    if( n > max)
      max = n;
    }

  std::vector< vtkSmartPointer<vtkIdList> >::iterator it = loopVector->begin();
  while( it != loopVector->end() )
    {
    if( (*it)->GetNumberOfIds() == max)
      {
      loopVector->erase( it );
      it = loopVector->begin();
      }
    ++it;
    }
}

//-----------------------------------------------------------------------------------------------------------
void RemoveDuplicateLoop( std::vector< vtkSmartPointer<vtkIdList> > *loopVector )
{
  vtkSmartPointer<vtkIntArray> keyArray = vtkSmartPointer<vtkIntArray>::New();

  for(unsigned int i=0; i<loopVector->size(); ++i)
    {
    vtkSmartPointer<vtkIdList> loop = loopVector->at(i);
    for(unsigned int j=0; j < loop->GetNumberOfIds(); ++j)
      {
      keyArray->InsertNextValue(i);
      }

    // Sort the array
    vtkSmartPointer<vtkSortDataArray> sortDataArray = vtkSmartPointer<vtkSortDataArray>::New();
    sortDataArray->Sort(keyArray, loop);

    keyArray->Reset();
    }

  vtkSmartPointer<vtkIdList> toRemove = vtkSmartPointer<vtkIdList>::New();

  for(unsigned int i=0; i<loopVector->size(); ++i)
    {
    vtkSmartPointer<vtkIdList> loop = loopVector->at(i);
    for(unsigned int j=i+1; j<loopVector->size(); ++j)
      {
      vtkSmartPointer<vtkIdList> loop1 = loopVector->at(j);

      int allSame = 1;

      if( loop->GetNumberOfIds() == loop1->GetNumberOfIds() )
        {
        for( int z=0; z < loop->GetNumberOfIds(); ++z )
          {
          if(loop->GetId(z) != loop1->GetId(z) )
            allSame = 0;
          }
        }

      if( !allSame ) toRemove->InsertUniqueId(j);
      }
    }


  std::vector< vtkSmartPointer<vtkIdList> > vector;
  for(unsigned int i=0; i<loopVector->size(); ++i)
    {
    vtkSmartPointer<vtkIdList> loop = loopVector->at(i);
    if( toRemove->IsId(i) == -1 )
      vector.push_back( loop );
    }

  loopVector->clear();

  for(unsigned int i=0; i<vector.size(); ++i)
    {
    vtkSmartPointer<vtkIdList> loop = vector.at(i);
    loopVector->push_back( loop );
    }
}

//-----------------------------------------------------------------------------------------------------------
void GetPointsLoop( vtkSmartPointer<vtkIdList> ptsLoop, int ptId, int cellId, vtkSmartPointer<vtkPolyData> border )
{
  vtkSmartPointer<vtkIdList> visitedCells = vtkSmartPointer<vtkIdList>::New();
  for(int i=0; i<border->GetNumberOfCells();++i)
    visitedCells->InsertNextId(-1);



  int ptid  = ptId;
  int cellid = cellId;

  int nextPoint = ptId;
  int finalPoint = ptId;

  while(nextPoint != -1)
    {
    ptsLoop->InsertNextId( nextPoint );
    vtkSmartPointer<vtkIdList> ptIds = vtkSmartPointer<vtkIdList>::New();
    border->GetCellPoints( cellid, ptIds );
    visitedCells->SetId( cellid, 1); //marca como visitada

    nextPoint = GetNextPoint( finalPoint, ptid, ptIds );
    ptid  = nextPoint;

    if(ptid == -1) break;

    vtkSmartPointer<vtkIdList> cells = vtkSmartPointer<vtkIdList>::New();
    border->GetPointCells( ptid, cells );
    for(int i=0; i<cells->GetNumberOfIds(); ++i)
      {
      if( visitedCells->GetId( cells->GetId(i) ) == -1 ) //pega a célula q não foi visitada
        cellid = cells->GetId(i);
      }
    }

}

//-----------------------------------------------------------------------------------------------------------
vtkSmartPointer<vtkPolyData> RemovePendentCells( vtkSmartPointer<vtkPolyData> mesh )
{
  vtkSmartPointer<vtkPolyData> poly = vtkSmartPointer<vtkPolyData>::New();

  vtkSmartPointer<vtkPolyData> featureEdge = ComputeFeatureEdge( mesh );

  visitedCells->Reset();
  insertedPoints->Reset();

  for(int i=0; i < featureEdge->GetNumberOfPoints(); ++i)
    insertedPoints->InsertNextId(-1);

  vtkSmartPointer<vtkPolyData> border = vtkSmartPointer<vtkPolyData>::New();
  WritePolyData( featureEdge, "/home/nfs/aneurysm_project/morphIndexes/1287259/26896/ANGIOGRAFIA_5/volumetricImage_1/segmentationID_1/meshID_1/morphIdexes/massTests/featureEdge.vtk" );
  ReorderingPoints( 0, featureEdge, border );
  WritePolyData( border, "/home/nfs/aneurysm_project/morphIndexes/1287259/26896/ANGIOGRAFIA_5/volumetricImage_1/segmentationID_1/meshID_1/morphIdexes/massTests/border.vtk" );





  vtkSmartPointer<vtkIdList> ptsIn3Cells = vtkSmartPointer<vtkIdList>::New();
  // identifica os pontos em border que estão em mais de 2 células
  for(int i=0; i < border->GetNumberOfPoints(); ++i)
    {
    vtkSmartPointer<vtkIdList> cellIds = vtkSmartPointer<vtkIdList>::New();
    border->GetPointCells(i, cellIds);
    if( cellIds->GetNumberOfIds() > 2 )
      ptsIn3Cells->InsertUniqueId(i);
    }


  vtkSmartPointer<vtkIdList> ptdIds = vtkSmartPointer<vtkIdList>::New();
  for(int i=0; i < ptsIn3Cells->GetNumberOfIds(); ++i)
    {
    vtkSmartPointer<vtkIdList> cellIds = vtkSmartPointer<vtkIdList>::New();
    border->GetPointCells( ptsIn3Cells->GetId(i), cellIds ); //lista de células que usam o ponto que está em mais de 2 células

    std::vector< vtkSmartPointer<vtkIdList> > loopVector;

//    for(int j=0; j<cellIds->GetNumberOfIds(); ++j)
//      {
//      vtkSmartPointer<vtkIdList> ptsLoop = vtkSmartPointer<vtkIdList>::New();
//      GetPointsLoop( ptsLoop, ptsIn3Cells->GetId(i), cellIds->GetId(j), border );
//      loopVector.push_back( ptsLoop );
//      }
    }


  std::vector< vtkSmartPointer<vtkIdList> > loopVector;
  vtkSmartPointer<vtkIdList> ptsLoop = vtkSmartPointer<vtkIdList>::New();
  ptsLoop->InsertNextId(70);
  ptsLoop->InsertNextId(73);
  ptsLoop->InsertNextId(72);
  ptsLoop->InsertNextId(71);
  ptsLoop->InsertNextId(63);
  ptsLoop->InsertNextId(21);
  ptsLoop->InsertNextId(22);
  ptsLoop->InsertNextId(33);
  loopVector.push_back( ptsLoop );

  vtkSmartPointer<vtkIdList> ptsLoop1 = vtkSmartPointer<vtkIdList>::New();
  ptsLoop1->InsertNextId(80);
  ptsLoop1->InsertNextId(63);
  ptsLoop1->InsertNextId(55);
  ptsLoop1->InsertNextId(91);
  loopVector.push_back( ptsLoop1 );

  vtkSmartPointer<vtkIdList> ptsLoop2 = vtkSmartPointer<vtkIdList>::New();
  ptsLoop2->InsertNextId(70);
  ptsLoop2->InsertNextId(73);
  ptsLoop2->InsertNextId(72);
  ptsLoop2->InsertNextId(71);
  ptsLoop->InsertNextId(63);
  ptsLoop->InsertNextId(21);
  ptsLoop->InsertNextId(22);
  ptsLoop->InsertNextId(33);
  loopVector.push_back( ptsLoop2 );

  vtkSmartPointer<vtkIdList> ptsLoop3 = vtkSmartPointer<vtkIdList>::New();
  ptsLoop3->InsertNextId(10);
  ptsLoop3->InsertNextId(15);
  ptsLoop3->InsertNextId(8);
  ptsLoop3->InsertNextId(7);
  loopVector.push_back( ptsLoop3 );

  vtkSmartPointer<vtkIdList> ptsLoop4 = vtkSmartPointer<vtkIdList>::New();
  ptsLoop4->InsertNextId(80);
  ptsLoop4->InsertNextId(63);
  ptsLoop4->InsertNextId(55);
  ptsLoop4->InsertNextId(91);
  loopVector.push_back( ptsLoop4 );

  vtkSmartPointer<vtkIdList> ptsLoop5 = vtkSmartPointer<vtkIdList>::New();
  ptsLoop5->InsertNextId(10);
  ptsLoop5->InsertNextId(15);
  ptsLoop5->InsertNextId(8);
  ptsLoop5->InsertNextId(7);
  loopVector.push_back( ptsLoop5 );

  std::vector< vtkSmartPointer<vtkIdList> >::iterator it = loopVector.begin();
  while( it != loopVector.end() )
    {
    cout << "NumberOfIds: " << (*it)->GetNumberOfIds() <<endl;
    for(int z=0; z<(*it)->GetNumberOfIds(); z++)
      cout <<"\t " << (*it)->GetId(z) <<endl;

    ++it;
    }

  RemoveGreaterLoop( &loopVector );
  RemoveDuplicateLoop( &loopVector );

  it = loopVector.begin();
  while( it != loopVector.end() )
    {
    cout << "NumberOfIds: " << (*it)->GetNumberOfIds() <<endl;
    for(int z=0; z<(*it)->GetNumberOfIds(); z++)
      cout <<"\t " << (*it)->GetId(z) <<endl;

    ++it;
    }



  return poly;
}

////-----------------------------------------------------------------------------------------------------------
//vtkSmartPointer<vtkPolyData> RemovePendentTriangles( vtkSmartPointer<vtkPolyData> mesh )
//{
//  vtkSmartPointer<vtkPolyData> input = vtkSmartPointer<vtkPolyData>::New();
//  input->DeepCopy( mesh );
//  input->BuildLinks();
//  input->Update();
//
//  vtkSmartPointer<vtkPolyData> poly = vtkSmartPointer<vtkPolyData>::New();
//  poly->SetPoints( input->GetPoints() );
//  poly->SetPolys( vtkSmartPointer<vtkCellArray>::New()  );
//  poly->Update();
//
//  for(int i=0; i<input->GetNumberOfCells(); ++i)
//    {
//    vtkSmartPointer<vtkIdList> ptIds = vtkSmartPointer<vtkIdList>::New();
//
//    input->GetCellPoints(i, ptIds);
//    poly->InsertNextCell( VTK_TRIANGLE, ptIds );
//    }
//
//
//
//  vtkSmartPointer<vtkPolyData> featureEdge = ComputeFeatureEdge( input );
//
//  visitedCells->Reset();
//  insertedPoints->Reset();
//  for(int i=0; i < featureEdge->GetNumberOfPoints(); ++i)
//    insertedPoints->InsertNextId(-1);
//
//  vtkSmartPointer<vtkPolyData> border = vtkSmartPointer<vtkPolyData>::New();
////WritePolyData( featureEdge, "/home/nfs/aneurysm_project/morphIndexes/1287259/26896/ANGIOGRAFIA_5/volumetricImage_1/segmentationID_1/meshID_1/morphIdexes/massTests/featureEdge.vtk" );
//  ReorderingPoints( 0, featureEdge, border );
////  WritePolyData( border, "/home/nfs/aneurysm_project/morphIndexes/1287259/26896/ANGIOGRAFIA_5/volumetricImage_1/segmentationID_1/meshID_1/morphIdexes/massTests/border.vtk" );
//
//  for(int i=0; i < border->GetNumberOfPoints(); ++i)
//    {
//    vtkSmartPointer<vtkIdList> cellIds = vtkSmartPointer<vtkIdList>::New();
//    border->GetPointCells(i, cellIds);
//
//    if( cellIds->GetNumberOfIds() > 2 ) // ponto que participa de mais de 2 células. Usando este, seu antecessor e seu posterior para criar um novo triangulo
//      {
//      double point[3];
//      int v0, v1, v2;
//
//      border->GetPoint(i, point);
//      v0 = input->FindPoint(point);
//
//      border->GetPoint(i+1, point);
//      v1 = input->FindPoint(point);
//
//      border->GetPoint(i-1, point);
//      v2 = input->FindPoint(point);
//
//      vtkSmartPointer<vtkIdList> ptIds = vtkSmartPointer<vtkIdList>::New();
//      ptIds->InsertNextId(v0);
//      ptIds->InsertNextId(v1);
//      ptIds->InsertNextId(v2);
//
//      poly->InsertNextCell( VTK_TRIANGLE, ptIds );
//      }
//    }
//
//  insertedPoints->Reset();
//  visitedCells->Reset();
//
//  return poly;
//}

//-----------------------------------------------------------------------------------------------------------
int main( int argc, char **argv )
{
  visitedCells = vtkSmartPointer<vtkIdList>::New();
  insertedPoints = vtkSmartPointer<vtkIdList>::New();

  string path = "/home/nfs/aneurysm_project/morphIndexes/1287259/26896/ANGIOGRAFIA_5/volumetricImage_1/segmentationID_1/meshID_1/morphIdexes/massTests/";
  string sack = "sack_more_triangle2.vtk";
  string neck = "aneurysmNeck.vtk";


  string sackToRead = path + sack;
  string neckToRead = path + neck;

  vtkSmartPointer<vtkPolyDataReader> readerSack = vtkSmartPointer<vtkPolyDataReader>::New();
  readerSack->SetFileName( sackToRead.c_str() );
  readerSack->Update();

  vtkSmartPointer<vtkPolyData> meshSack = RemovePendentCells( readerSack->GetOutput() );
  WritePolyData( meshSack, "/home/nfs/aneurysm_project/morphIndexes/1287259/26896/ANGIOGRAFIA_5/volumetricImage_1/segmentationID_1/meshID_1/morphIdexes/massTests/meshSack.vtk" );
//  vtkSmartPointer<vtkPolyData> meshSack = vtkSmartPointer<vtkPolyData>::New();
//  meshSack->DeepCopy( readerSack->GetOutput() );
  meshSack->BuildLinks();
  meshSack->Update();

  if( !HasOnlyTriangles( meshSack ) )
    {
    cout << "Sack does not have only triangles." <<endl;
    return 1;
    }

  vtkSmartPointer<vtkPolyDataMapper> mapperSack = vtkSmartPointer<vtkPolyDataMapper>::New();
  mapperSack->SetInput( meshSack );

  vtkSmartPointer<vtkActor> actorSack = vtkSmartPointer<vtkActor>::New();
  actorSack->SetMapper(mapperSack);




  vtkSmartPointer<vtkPolyData> featureEdgesPoly = ComputeFeatureEdge( meshSack );
  featureEdgesPoly->BuildLinks();

  insertedPoints->Reset();
  visitedCells->Reset();

  for(int i=0; i < featureEdgesPoly->GetNumberOfPoints(); ++i)
    insertedPoints->InsertNextId(-1);


  vtkSmartPointer<vtkPolyData> border = vtkSmartPointer<vtkPolyData>::New();
  WritePolyData( featureEdgesPoly, "/home/nfs/aneurysm_project/morphIndexes/1287259/26896/ANGIOGRAFIA_5/volumetricImage_1/segmentationID_1/meshID_1/morphIdexes/massTests/featureEdgesPoly.vtk" );
  ReorderingPoints( 0, featureEdgesPoly, border );
  WritePolyData( border, "/home/nfs/aneurysm_project/morphIndexes/1287259/26896/ANGIOGRAFIA_5/volumetricImage_1/segmentationID_1/meshID_1/morphIdexes/massTests/border.vtk" );

  BuildListOfPoinsInBorder( meshSack, border );

  std::map<int,int>::iterator it;
  it = pointsMap.begin();
  for(it = pointsMap.begin(); it != pointsMap.end(); ++it)
    {
    cout << "it->first: " << it->first << ",  it->second: " << it->second <<endl;
    }






  vtkSmartPointer<vtkPolyData> sackBorder = CreateBorder( meshSack, border );

  vtkSmartPointer<vtkPolyDataMapper> mapperSackBorder = vtkSmartPointer<vtkPolyDataMapper>::New();
  mapperSackBorder->SetInput( sackBorder );

  vtkSmartPointer<vtkActor> actorSackBorder = vtkSmartPointer<vtkActor>::New();
  actorSackBorder->SetMapper( mapperSackBorder );




  vtkSmartPointer<vtkPolyDataReader> readerNeck = vtkSmartPointer<vtkPolyDataReader>::New();
  readerNeck->SetFileName( neckToRead.c_str() );
  readerNeck->Update();

  vtkSmartPointer<vtkPolyData> meshNeck = vtkSmartPointer<vtkPolyData>::New();
  meshNeck->DeepCopy( readerNeck->GetOutput() );
  meshNeck->BuildLinks();
  meshNeck->Update();

  if( !HasOnlyTriangles( meshNeck ) )
    {
    cout << "Neck does not have only triangles." <<endl;
    return 1;
    }

  vtkSmartPointer<vtkPolyDataMapper> mapperNeck = vtkSmartPointer<vtkPolyDataMapper>::New();
  mapperNeck->SetInput( meshNeck );

  vtkSmartPointer<vtkActor> actorNeck = vtkSmartPointer<vtkActor>::New();
  actorNeck->SetMapper(mapperNeck);







  // A renderer and render window
  vtkSmartPointer<vtkRenderer> renderer = vtkSmartPointer<vtkRenderer>::New();
  vtkSmartPointer<vtkRenderWindow> renderWindow = vtkSmartPointer<vtkRenderWindow>::New();
  renderWindow->AddRenderer(renderer);

  // An interactor
  vtkSmartPointer<vtkRenderWindowInteractor> renderWindowInteractor = vtkSmartPointer<vtkRenderWindowInteractor>::New();
  renderWindowInteractor->SetRenderWindow(renderWindow);

  renderer->AddActor(actorSack);
  renderer->AddActor(actorSackBorder);
//  renderer->AddActor(actorNeck);
  renderer->SetBackground(0.3,0.35,0.5);

  renderWindow->SetSize(1024,800);
  renderWindow->Render();

  renderWindowInteractor->Start();

  return 0;
}



