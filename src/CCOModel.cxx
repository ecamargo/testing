#include "vtkRenderWindowInteractor.h"
#include "vtkRenderWindow.h"
#include "vtkRenderer.h"

#include "vtkPolyDataReader.h"
#include "vtkPolyData.h"
#include "vtkPolyDataMapper.h"
#include "vtkActor.h"

#include "vtkCell.h"
#include "vtkIdList.h"
#include "vtkPoints.h"
#include "vtkCellData.h"
#include "vtkPointData.h"


#include <math.h>
#include <vector>
#include <list>
#include <stack>

using namespace std;


typedef enum
{
  INFLOW = 0,
  TERMINAL = 1,
  BIFURCATION = 2
}NodeType;

typedef std::list<vtkIdType> ListOfInt;
typedef std::vector<ListOfInt> VectorOfIntList;
ListOfInt VisitedList;
ListOfInt::iterator VisitListIt;

std::stack<int> ControlStack;








class CCONode
{
  public:
    //-------------------------------------------------
    CCONode()
    {
//    cout << " CCONode()" << endl;
    }

    //-------------------------------------------------
    ~CCONode()
    {
//      cout << " ~CCONode()" << endl;
    }

    //-------------------------------------------------
    void SetPosition(double *d)
    {
//      cout << " void SetPosition(double *d)" << endl;

      this->m_position[0] = d[0];
      this->m_position[1] = d[1];
      this->m_position[2] = d[2];
    }

    //-------------------------------------------------
    double * GetPosition()
    {
//      cout << " double * GetPosition()" << endl;

      return this->m_position;
    }

    //-------------------------------------------------
    void SetId(int Id)
    {
//      cout << " NODE void SetId(int Id): " << Id << endl;

      this->m_id = Id;
    }

    //-------------------------------------------------
    int GetId()
    {
//      cout << " int GetId()" << endl;

      return this->m_id;
    }

    //-------------------------------------------------
    void SetType(NodeType type)
    {
//      cout << " void SetType(NodeType type): " << type << endl;

      this->m_type = type;
    }

    //-------------------------------------------------
    NodeType GetType()
    {
//      cout << " NodeType GetType()" << endl;

      return this->m_type;
    }

  protected:
    int m_id;
    double m_position[3];
    NodeType m_type;
};


//---------------------------------------------------------------------------------------------
class CCOElement
{
  public:

    //-------------------------------------------------
    CCOElement()
    {
//      cout << " CCOElement()" << endl;

    this->m_nodesIds = vtkIdList::New();
    this->m_childs.clear();
    this->m_radius = 0.0;
    this->m_length = 0.0;
    }

    //-------------------------------------------------
    ~CCOElement()
    {
//      cout << " ~CCOElement()" << endl;

      this->m_nodesIds->Delete();

      vector<CCOElement *>::iterator it;
      for( it = this->m_childs.begin(); it != this->m_childs.end(); ++it )
        {
        delete (*it);
        }
      this->m_childs.clear();
    }

    //-------------------------------------------------
    void AddChild(CCOElement *parent, CCOElement *element)
    {
//      cout << " void AddChild(CCOElement *parent, CCOElement *element)" << endl;

      parent->m_childs.push_back( element );
    }

    //-------------------------------------------------
    int GetFirstNode()
    {
//      cout << " CCONode * GetFirstNode()" << endl;

      return this->m_nodesIds->GetId(0);
    }

    //-------------------------------------------------
    int GetLastNode()
    {
//      cout << " CCONode * GetLastNode()" << endl;

      return this->m_nodesIds->GetId( this->m_nodesIds->GetNumberOfIds()-1 );
    }

    //-------------------------------------------------
     void AddNode(int node)
     {
//       cout << " void AddNode(CCONode *node)" << endl;

       this->m_nodesIds->InsertNextId( node );
     }

    //-------------------------------------------------
    vector<CCOElement *> GetChilds()
    {
//      cout << " vector<CCOElement *> GetChilds()" << endl;

      return this->m_childs;
    }

    //-------------------------------------------------
    void SetId(int Id)
    {
//      cout << " ELEMENT void SetId(int Id): " << Id << endl;

      this->m_id = Id;
    }

    //-------------------------------------------------
    int GetId()
    {
//      cout << " int GetId()" << endl;

      return this->m_id;
    }

    //-------------------------------------------------
    vtkIdList * GetListOfNodes()
    {
//      cout << " vtkIdList * GetListOfNodes()" << endl;

      return this->m_nodesIds;
    }

    //-------------------------------------------------
    void SetRadius(double arg)
    {
//      cout << " void SetRadius(double arg)" << endl;

      this->m_radius = arg;
    }

    //-------------------------------------------------
    double GetRadius()
    {
//      cout << " double GetRadius()" << endl;

      return this->m_radius;
    }

    //-------------------------------------------------
    double GetLength()
    {
//      cout << " double GetLength()" << endl;

      return this->m_length;
    }

    //-------------------------------------------------
    void SetLength(double arg)
    {
//      cout << " void SetLength(double arg)" << endl;

      this->m_length = arg;
    }


  protected:
    int m_id;
    vtkIdList * m_nodesIds;
    vector<CCOElement *> m_childs;
    double m_radius;
    double m_length;
};


//---------------------------------------------------------------------------------------------
class CCOModel
{
  public:

    //-------------------------------------------------
    CCOModel()
    {
//      cout << " CCOModel()" << endl;

    this->m_tree = NULL;

    this->m_nodes.clear();
    this->m_incidenceList.clear();
    this->m_3nodesList.clear();
    this->m_1nodesList.clear();
    }

    //-------------------------------------------------
    ~CCOModel()
    {
//      cout << " ~CCOModel()" << endl;

      if(this->m_tree != NULL)
        {
        delete this->m_tree;
        this->m_tree = NULL;
        }


      vector<CCONode *>::iterator nodeIt;
      for(nodeIt = this->m_nodes.begin(); nodeIt != this->m_nodes.end(); ++nodeIt)
      {
        delete (*nodeIt);
      }
      this->m_nodes.clear();


      vector<vtkIdList *>::iterator incidenceIt;
      for(incidenceIt = this->m_incidenceList.begin(); incidenceIt != this->m_incidenceList.end(); ++incidenceIt)
        {
        (*incidenceIt)->Delete();
        }
      this->m_incidenceList.clear();


      this->m_3nodesList.clear();
      this->m_1nodesList.clear();
    }

    //-------------------------------------------------
    CCOElement * GetRoot()
    {
//      cout << " CCOElement * GetRoot()" << endl;

      if( this->m_tree == NULL )
        {
        this->m_tree = new CCOElement();
        }

      return this->m_tree;
    }

    //-------------------------------------------------
    CCOElement * GetElement(int id)
    {
//      cout << " CCOElement * GetElement(int id)" << endl;

      CCOElement * e;

      if( id == this->GetRoot()->GetId() )
        {
        e = this->GetRoot();
        }
      else
        {
        e = this->GetElement( this->GetRoot(), id);
        }

      return e;
    }

    //-------------------------------------------------
    void PrintTree(CCOElement *element)
    {
//      cout << " void PrintTree()\n" << endl;

      if(element == NULL)
        {
        this->PrintElement( this->GetRoot() );

        vector<CCOElement *> childs = this->GetRoot()->GetChilds();

        for(unsigned int i=0; i < childs.size(); ++i)
          {
          this->PrintTree( childs.at(i) );
          }
        }
      else
        {
        this->PrintElement( element );

        vector<CCOElement *> childs = element->GetChilds();

        for(unsigned int i=0; i < childs.size(); ++i)
          {
          this->PrintTree( childs.at(i) );
          }
        }
      }

    //-------------------------------------------------
    void PrintElement(CCOElement *element)
    {
//      cout << " void PrintTree()\n" << endl;
      double *p;

      if( element == NULL )
        return;

      cout << "Element Id: " << element->GetId() << endl;
      cout << "Radius: " << element->GetRadius() << endl;
      cout << "Length: " << element->GetLength() << endl;
      cout << "Number Of Childs: " << element->GetChilds().size() << endl;
      cout << "Childs: ";
      for(unsigned int i=0; i<element->GetChilds().size(); ++i)
        {
        cout << element->GetChilds().at(i)->GetId() << ";  ";
        }

      cout << endl;
        cout << "\tNode 1" << endl;
          cout << "\t\t Id: " << element->GetFirstNode() << endl;
          p = this->GetNode( element->GetFirstNode() )->GetPosition();
          cout << "\t\t Position: " << p[0] << " >> " << p[1] << " >> " << p[2] << endl;
          cout << "\t\t Type: " << this->GetNode( element->GetFirstNode() )->GetType() << endl;
        cout << "\tNode 2" << endl;
          cout << "\t\t Id: " << element->GetLastNode() << endl;
          p = this->GetNode( element->GetLastNode() )->GetPosition();
          cout << "\t\t Position: " << p[0] << " >> " << p[1] << " >> " << p[2] << endl;
          cout << "\t\t Type: " << this->GetNode( element->GetLastNode() )->GetType() << endl;
    }

    //-------------------------------------------------
    int InsertNextNode(CCONode *node)
    {
//      cout << " int AddNode(CCOElement *element)\n" << endl;

      // O índice dos nós sempre começa do 1 (um) e não do zero.
      // Por isso, insere-se um nó vazio na primeira posição
      if( this->m_nodes.size() == 0 )
        {
        this->m_nodes.push_back( new CCONode() );
        }

      this->m_nodes.push_back( node );

      int size = this->m_nodes.size();

      return (size-1);
    }

    //-------------------------------------------------
    CCONode * GetNode(int id)
    {
//      cout << " CCONode * GetNode(int id)\n" << endl;

      return this->m_nodes.at(id);
    }

    //-------------------------------------------------
    int GetNumberOfNodes()
    {
//      cout << " int GetNumberOfNodes()\n" << endl;

      // O índice dos nós sempre começa do 1 (um) e não do zero.
      int n = 0;
      if( this->m_nodes.size() != 0 )
        {
        n = this->m_nodes.size()-1;
        }

      return n;
    }

    //-------------------------------------------------
    vector<vtkIdList *> GetIncidenceList()
    {
      if( this->m_incidenceList.size() <= 0 )
        {
        this->MakeIncidenceList( NULL );

        vector<vtkIdList *>::iterator it;

        for(it = this->m_2nodesList.begin(); it != this->m_2nodesList.end(); ++it)
          {
          this->m_incidenceList.push_back( (*it) );
          }

        for(it = this->m_3nodesList.begin(); it != this->m_3nodesList.end(); ++it)
          {
          this->m_incidenceList.push_back( (*it) );
          }

        for(it = this->m_1nodesList.begin(); it != this->m_1nodesList.end(); ++it)
          {
          this->m_incidenceList.push_back( (*it) );
          }
        }


      return this->m_incidenceList;
    }



  protected:

    // cria 3 vetores de vtkIdList contendo, respectivamente, ids dos elementos de 2 nodes, 3 nodes e 1 node.
    //-------------------------------------------------
     void MakeIncidenceList(CCOElement *element)
     {
//     cout << " void MakeIncidenceList(CCOElement *element)\n" << endl;

       vtkIdList *idList = NULL;
       vtkIdList *idList_3nodes = NULL;
       vtkIdList *idList_1nodes = NULL;
       CCOElement *aux;


       if(element == NULL)
         {
         aux = this->GetRoot();

         idList_1nodes = vtkIdList::New();
         idList_1nodes->InsertNextId( aux->GetFirstNode() ); // inserindo nó INFLOW

         this->m_1nodesList.push_back( idList_1nodes );
         }
       else
         {
         aux = element;
         }

       // elementos de 2 nós
       idList = vtkIdList::New();
       idList->DeepCopy( aux->GetListOfNodes() );
       this->m_2nodesList.push_back( idList );

       vector<CCOElement *> childs = aux->GetChilds();

       // elementos de 3 nós
       if(childs.size() == 2) // bifurcação
         {
         idList_3nodes = vtkIdList::New();
         idList_3nodes->InsertNextId( aux->GetLastNode() ); // inserindo nó distal (pai)

         idList_3nodes->InsertNextId( childs.at(0)->GetFirstNode() ); // inserindo nó proximal do primeiro filho
         idList_3nodes->InsertNextId( childs.at(1)->GetFirstNode() ); // inserindo nó proximal do segundo filho

         this->m_3nodesList.push_back( idList_3nodes );
         }
       else // atingiu nó folha - elementos de 1 nó
         {
         idList_1nodes = vtkIdList::New();
         idList_1nodes->InsertNextId( aux->GetLastNode() ); // inserindo nó distal (terminal)

         this->m_1nodesList.push_back( idList_1nodes );
         }

       for(unsigned int i = 0; i < childs.size(); ++i)
         {
         this->MakeIncidenceList( childs.at(i) );
         }
     }

     //-------------------------------------------------
     CCOElement * GetElement(CCOElement *element, int id)
     {
 //      cout << " CCOElement * GetElement(CCOElement *root, int id)" << endl;

      CCOElement *e = NULL;

      vector<CCOElement *>::iterator it;
      vector<CCOElement *> childs = element->GetChilds();




//      for(it = childs.begin(); it != childs.end(); it++)
//        {
//        cout << "elemet: " << element->GetId() << "\tchild: " << (*it)->GetId() <<endl;
//        }




      for(it = childs.begin(); it != childs.end(); it++)
        {
//        cout << "(*it)->GetId(): " << (*it)->GetId() << "\tid: " << id <<endl;

        if( id == (*it)->GetId() )
          {
          e = (*it);
//          cout << "\treturn -> (*it)->GetId(): " << (*it)->GetId() << "\tid: " << id <<endl;
//          return e;
          }
        else
          {
          e = this->GetElement( (*it), id );
//          cout << "\treturn -> this->GetElement( (*it), id ): " << (*it)->GetId() << "\tid: " << id <<endl;
//          return e;
          }


        if(e != NULL)
          return e;
        }
       return e;
     }


  private:

    CCOElement *m_tree;
    vector<CCONode *> m_nodes;
    vector<vtkIdList *> m_incidenceList;
    vector<vtkIdList *> m_3nodesList;
    vector<vtkIdList *> m_2nodesList;
    vector<vtkIdList *> m_1nodesList;
};




//---------------------------------------------------------------------------------------------
class IniWriter
{
  public:

    //-------------------------------------------------
    int Write()
    {
//      cout << " int Write()" << endl;

    int nodeFlow, nodePressure;

    nodeFlow = nodePressure = 0;

    if( this->m_Model->GetNumberOfNodes() <= 0)
      {
      cout <<"\nError! Tree has zero nodes. Check IniWriter.\n\n" << endl;
      return 1;
      }

    char fileName[1024];
    strcpy( fileName, this->m_OutPutPath );
    strcat( fileName, "IniFile.txt" );


    FILE *fp = fopen( fileName, "w" );
    if(!fp)
      {
      cout << "\nError Opening IniFile.txt!!!\n\n" << endl;
      return 1;
      }


    fprintf(fp, "*Initial Conditions\n");
    for(int i=0; i < this->m_Model->GetNumberOfNodes(); ++i)
      {
      fprintf(fp, "%d\n", nodeFlow);
      fprintf(fp, "%d\n", nodePressure);
      }


    fprintf(fp, "\n*Time [ T , DT ]\n");
    fprintf(fp, "0.0000000E+00    0.1000000E+01\n");


    fclose(fp);

    return 0;
    }

    //-------------------------------------------------
    void SetOutPutPath(const char * path)
    {
//      cout << " void SetOutPutPath(const char * path)" << endl;

      this->m_OutPutPath = path;
    }

    //-------------------------------------------------
    const char * GetOutPutPath()
    {
//      cout << " onst char * GetOutPutPath()" << endl;

      return this->m_OutPutPath;
    }

    //-------------------------------------------------
    void SetModel(CCOModel *model)
    {
//      cout << " void SetModel(CCOModel *model)" << endl;

      this->m_Model = model;
    }

  protected:
    const char *m_OutPutPath;
    CCOModel *m_Model;
};


//---------------------------------------------------------------------------------------------
class MeshWriter
{
  public:
    //-------------------------------------------------
    int Write()
    {
//      cout << " int Write()" << endl;

    if( this->m_Model->GetNumberOfNodes() <= 0)
      {
      cout <<"\nError! Tree has zero nodes. Check WriteMeshFile() method \n\n" << endl;
      return 1;
      }

    char fileName[1024];
    strcpy( fileName, this->m_OutPutPath );
    strcat( fileName, "Mesh.txt" );


    FILE *fp = fopen( fileName, "w" );
    if(!fp)
      {
      cout << "\nError Opening Mesh.txt!!!\n\n" << endl;
      return 1;
      }


    fprintf(fp, "*NODAL DOFs\n");
    fprintf(fp, "2\n");


    fprintf(fp, "\n*DIMEN\n");
    fprintf(fp, "3\n");


    fprintf(fp, "\n*COORDINATES\n");
    double *pos;
    fprintf(fp, "%d\n", this->m_Model->GetNumberOfNodes() );
    for(int i=1; i <= this->m_Model->GetNumberOfNodes(); ++i)
      {
      pos = this->m_Model->GetNode(i)->GetPosition();
      fprintf(fp, "%E %E %E\n", pos[0], pos[1], pos[2]);
      }



    vector<vtkIdList *>::iterator it;
    vector<vtkIdList *> vec = this->m_Model->GetIncidenceList();


    fprintf(fp, "\n*ELEMENT GROUPS\n");
    fprintf(fp, "1\n");
    fprintf(fp, "1 %d Generic\n", (int)vec.size());

    for(it = vec.begin(); it != vec.end(); ++it)
      {
      fprintf(fp, "%d\n", (int)(*it)->GetNumberOfIds() );
      }


    fprintf(fp, "\n*INCIDENCE\n");
    vec = this->m_Model->GetIncidenceList();
    for(it = vec.begin(); it != vec.end(); ++it)
      {
      for(int i = 0; i < (*it)->GetNumberOfIds(); ++i)
        {
        fprintf(fp, "%d ", (int)(*it)->GetId(i) );
        }
      fprintf(fp, "\n");
      }


    fprintf(fp, "\n*ELEMENT TYPE\n");
    bool inflow = true;
    vec = this->m_Model->GetIncidenceList();
    for(it = vec.begin(); it != vec.end(); ++it)
      {
      if( (*it)->GetNumberOfIds() == 2)
        {
        fprintf(fp, "1\n" );
        }
      else if( (*it)->GetNumberOfIds() == 3)
        {
        fprintf(fp, "2\n" );
        }
      else if( ( (*it)->GetNumberOfIds() == 1) && inflow )
        {
        fprintf(fp, "3\n" );
        inflow = false;
        }
      else
        {
        fprintf(fp, "4\n" );
        }
      }


    fprintf(fp, "\n*ELEMENT MAT\n");
    int count = 1;
    vec = this->m_Model->GetIncidenceList();
    for(it = vec.begin(); it != vec.end(); ++it)
      {
      if( (*it)->GetNumberOfIds() == 2)
        {
        fprintf(fp, "%d\n", count);
        ++count;
        }
      else if( (*it)->GetNumberOfIds() == 3)
        {
        fprintf(fp, "%d\n", count);
        }
      else if( (*it)->GetNumberOfIds() == 1)
        {
        fprintf(fp, "%d\n", count+1);
        }
      }


    fprintf(fp, "\n*DIRICHLET CONDITIONS\n");
    int value = 0;
    for(int i=0; i < (this->m_Model->GetNumberOfNodes()*2); ++i)
      {
      fprintf(fp, "%d\n", value);
      }

    fprintf(fp, "\n");

    for(int i=0; i < (this->m_Model->GetNumberOfNodes()*2); ++i)
      {
      fprintf(fp, "%d\n", value);
      }



    fclose(fp);

    return 0;
    }

    //-------------------------------------------------
    void SetOutPutPath(const char * path)
    {
//      cout << " void SetOutPutPath(const char * path)" << endl;

      this->m_OutPutPath = path;
    }

    //-------------------------------------------------
    const char * GetOutPutPath()
    {
//      cout << " onst char * GetOutPutPath()" << endl;

      return this->m_OutPutPath;
    }

    //-------------------------------------------------
    void SetModel(CCOModel *model)
    {
//      cout << " void SetModel(CCOModel *model)" << endl;

      this->m_Model = model;
    }

  protected:
    const char *m_OutPutPath;
    CCOModel *m_Model;
};


//---------------------------------------------------------------------------------------------
class ParamWriter
{
  public:
    //-------------------------------------------------
    int Write()
    {
//      cout << " int Write()" << endl;

    if( this->m_Model->GetNumberOfNodes() <= 0)
      {
      cout <<"\nError! Tree has zero nodes. Check WriteParamFile() method \n\n" << endl;
      return 1;
      }

    char fileName[1024];
    strcpy( fileName, this->m_OutPutPath );
    strcat( fileName, "Param.txt" );


    FILE *fp = fopen( fileName, "w" );
    if(!fp)
      {
      cout << "\nError Opening Param.txt!!!\n\n" << endl;
      return 1;
      }


    fprintf(fp, "*Parameter Groups\n");
    vector<vtkIdList *> vec;
    vector<vtkIdList *>::iterator it;
    int count = 1;
    vec = this->m_Model->GetIncidenceList();
    for(it = vec.begin(); it != vec.end(); ++it)
      {
      if( (*it)->GetNumberOfIds() == 2)
        {
        ++count;
        }
      }
    fprintf(fp, "%d\n", count+1);


    fprintf(fp, "\n*Real Parameters\n");
    for(int i = 0; i < (this->m_Model->GetNumberOfNodes()/2); ++i)
      {
      fprintf(fp, "%d\n", 2);
      }
    fprintf(fp, "%d\n", 0);
    fprintf(fp, "%d\n", 0);

    fprintf(fp, "\n");

    CCOElement *element = NULL;
    for(int i = 0; i < (this->m_Model->GetNumberOfNodes()/2); ++i)
      {
//      cout << "Solicitando ElementId: " << i << endl;
      element = this->m_Model->GetElement(i);

      if( element == NULL)
        {
        cout << "\nError in Param Writer. Check Number of Elements and Nodes!!!\n\n" << endl;
        }

      fprintf(fp, "%E %E\n", element->GetLength(), element->GetRadius() );
      }
    fprintf(fp, "\n");


    fprintf(fp, "*Integer Parameters\n");
    for(int i = 0; i < (count+1); ++i)
      {
      fprintf(fp, "%d\n", 0);
      }



    fclose(fp);

    return 0;
    }

    //-------------------------------------------------
    void SetOutPutPath(const char * path)
    {
//      cout << " void SetOutPutPath(const char * path)" << endl;

      this->m_OutPutPath = path;
    }

    //-------------------------------------------------
    const char * GetOutPutPath()
    {
//      cout << " onst char * GetOutPutPath()" << endl;

      return this->m_OutPutPath;
    }

    //-------------------------------------------------
    void SetModel(CCOModel *model)
    {
//      cout << " void SetModel(CCOModel *model)" << endl;

      this->m_Model = model;
    }

  protected:
    const char *m_OutPutPath;
    CCOModel *m_Model;
};





// ===== métodos auxiliares

//-------------------------------------------------
double ComputeEuclidianDistance(double *p1, double *p2)
{
  double euclidian_distance = sqrt( pow(p1[0] - p2[0], 2) + pow(p1[1] - p2[1], 2) + pow(p1[2] - p2[2], 2) );

  return euclidian_distance;
}

//---------------------------------------------------------------------------------------------
bool CellVisited(int cellid)
{
//  cout << " bool CellVisited(int cellid)\n" << endl;

  for (VisitListIt = VisitedList.begin(); VisitListIt != VisitedList.end(); ++VisitListIt)
  {
  if (*(VisitListIt) == cellid)
   return true;
  }
  return false;

}

//---------------------------------------------------------------------------------------------
vtkIdList *GetChildList(vtkPolyData *poly, int cellId)
{
//  cout << " vtkIdList *GetChildList(vtkPolyData *poly, int cellId)\n" << endl;

  vtkIdList *childs = vtkIdList::New();

  vtkIdList *ptids = vtkIdList::New();
  ptids->SetNumberOfIds(1);

  vtkIdList *cellids = vtkIdList::New();

  vtkCell* cell = poly->GetCell(cellId);

  int pt1 = cell->GetPointIds()->GetId(0);
  int pt2 = cell->GetPointIds()->GetId(1);

  ptids->SetId(0, pt1);


  // passa o id da celula que se quer descobrir os vizinhos - cellId
  // ponto que faz parte da celula atual e tambem parte dos vizinhos
  // os ids dos vizinhos vem em  vtkIdList *cellids
  poly->GetCellNeighbors(cellId, ptids, cellids);


  if (cellids->GetNumberOfIds()) // se celula tem vizinhos
    {
//    cout << "retornando id filho " << cellids->GetId(0) << endl;

    bool Visited = false;

    for (int i = 0; i < cellids->GetNumberOfIds() ; ++i)
      {
      if ( CellVisited(cellids->GetId(i)) )
        Visited = true; // se vizinho ja foi visitado, entao esse vizinho não é filho da celula dada por CellId
      }

    if (!Visited) // se vizinho nao foi visitado, entao é filho
      {
      for (int i = 0; i < cellids->GetNumberOfIds() ; ++i)
        {
        childs->InsertNextId(cellids->GetId(i)); // insere na lista de filhos
//        cout << "Filhos obtido:  "<< cellids->GetId(i) << endl;
        }
      }
    }

  ptids->SetId(0, pt2); // repete o mesmo processo para o outro ponto da celula

  poly->GetCellNeighbors(cellId, ptids, cellids);
  if (cellids->GetNumberOfIds() )
    {
//    cout << "retornando id filho " << cellids->GetId(0) << endl;

    bool Visited = false;

    for (int i = 0; i < cellids->GetNumberOfIds() ; ++i)
      {
      if ( CellVisited(cellids->GetId(i)))
        Visited = true;
      }

    if (!Visited)
      {
      for (int i = 0; i < cellids->GetNumberOfIds() ; ++i)
        {
        childs->InsertNextId(cellids->GetId(i));
//        cout << "Filhos obtido:  "<< cellids->GetId(i) << endl;
        }
      }
    }

  // deleta vtkidlists auxiliares
  ptids->Delete();
  cellids->Delete();

  return childs;
}

//---------------------------------------------------------------------------------------------
void CreateCCOModel( vtkPolyData *poly, CCOModel *ccoModel, CCOElement *parent )
{
//  cout << " void CreateCCOModel( vtkPolyData *poly, CCOModel *ccoModel, CCOElement *parent )\n" << endl;

  vtkCell *cell;
  vtkPoints *points;
  vtkIdList * childs = NULL;
  int ptId = ccoModel->GetNumberOfNodes();
  int cellid;


  if(parent == NULL)     // adicionando célula root - sempre começa pela célula zero
    {
    cellid = 0;
    cell = poly->GetCell( cellid );
    points = cell->GetPoints();

    VisitedList.push_front( cellid );

    CCONode *node1 = new CCONode;
    node1->SetPosition( points->GetPoint( 0 ) );
    node1->SetType( INFLOW );
    ptId++;
    node1->SetId( ptId );

    CCONode *node2 = new CCONode;
    node2->SetPosition( points->GetPoint( 1 ) );
    node2->SetType( TERMINAL );
    ptId++;
    node2->SetId( ptId );

    CCOElement *root = ccoModel->GetRoot();
    root->AddNode( ccoModel->InsertNextNode(node1) );
    root->AddNode( ccoModel->InsertNextNode(node2) );
    root->SetId( cellid );

    root->SetRadius( poly->GetCellData()->GetArray(0)->GetTuple1( cellid ) );
    root->SetLength( ComputeEuclidianDistance( node1->GetPosition(), node2->GetPosition() ) );


    // buscando filhos
    if(childs)
      {
      childs->Delete();
      }

    childs = GetChildList( poly, cellid );

    for(int i = 0; i < childs->GetNumberOfIds(); ++i)
      {
//      cout << "adicionando célula " << childs->GetId(i) << " na pilha "<< endl;
      ControlStack.push( childs->GetId(i) );

      if( !CellVisited( childs->GetId(i) ) )
        {
        CreateCCOModel( poly, ccoModel, root );
        }
      }
    }
  else
    {
    do
      {
      cellid = ControlStack.top(); // pega referência do último id da célula na pilha de controle

      cell = poly->GetCell( cellid );
      points = cell->GetPoints();

      ControlStack.pop(); // retira último id da célula da pilha de controle
//      cout << "\t --> retirando célula " << cellid << " da pilha "<< endl;
      VisitedList.push_front( cellid ); // marca a célula como visitada

      CCONode *n1 = new CCONode;
      n1->SetPosition( points->GetPoint( 0 ) );
      ptId++;
      n1->SetId( ptId );

      CCONode *n2 = new CCONode;
      n2->SetPosition( points->GetPoint( 1 ) );
      ptId++;
      n2->SetId( ptId );

      CCOElement *element = new CCOElement;
      element->AddNode( ccoModel->InsertNextNode( n1 ) );
      element->AddNode( ccoModel->InsertNextNode( n2 ) );
      element->SetId( cellid ); // cada elemento possui o mesmo id da célula vtk que da qual teve origem

      element->SetRadius( poly->GetCellData()->GetArray(0)->GetTuple1( cellid ) );
      element->SetLength( ComputeEuclidianDistance( n1->GetPosition(), n2->GetPosition() ) );


      ccoModel->GetNode( parent->GetLastNode() )->SetType( BIFURCATION );
      ccoModel->GetNode( element->GetFirstNode() )->SetType( BIFURCATION );
      ccoModel->GetNode( element->GetLastNode() )->SetType( TERMINAL );

      parent->AddChild( parent, element );

      // buscando filhos
      if(childs)
        {
        childs->Delete();
        }

      childs = GetChildList( poly, cellid );

      for(int i = 0; i < childs->GetNumberOfIds(); ++i)
        {
//        cout << "adicionando célula " << childs->GetId(i) << " na pilha "<< endl;
        ControlStack.push( childs->GetId(i) ); // adiciona filho na pilha de controle

        if( !CellVisited( childs->GetId(i) ) ) // se a célula NÃO FOI visitada
          {
          CreateCCOModel( poly, ccoModel, element ); // chamada recursiva tendo o elemento recém criado como root
          }
        }
      }while( !ControlStack.empty() );
    }

  if(childs)
    {
    childs->Delete();
    }
}





//-----------------------------------------------------------------------------
int main( int argc, char **argv )
{
//  const char *fileName = "/home/eduardo/workspace/data_HeMoLab/CCO_SolverGP/Caso1_Nterm2/Arvore_Nterm2.vtk";
//  const char *fileName = "/home/eduardo/workspace/data_HeMoLab/CCO_SolverGP/Caso2_Nterm3/Arvore_Nterm3.vtk";
//  const char *fileName = "/home/eduardo/workspace/data_HeMoLab/CCO_SolverGP/Caso3_Nterm5/Arvore_Nterm5.vtk";
//  const char *fileName = "/home/eduardo/workspace/data_HeMoLab/CCO_SolverGP/Caso4_Nterm10/Arvore10.vtk";
//  const char *fileName = "/home/eduardo/workspace/data_HeMoLab/CCO_SolverGP/Caso5_Nterm20/Arvore20_New.vtk";
  const char *fileName = "/home/eduardo/workspace/data_HeMoLab/CCO_SolverGP/Caso6_Nterm250/Arvore250.vtk";
  

//  const char *outputPath = "/home/eduardo/workspace/data_HeMoLab/CCO_SolverGP/testes/Caso1_Nterm2/";
//  const char *outputPath = "/home/eduardo/workspace/data_HeMoLab/CCO_SolverGP/testes/Caso2_Nterm3/";
//  const char *outputPath = "/home/eduardo/workspace/data_HeMoLab/CCO_SolverGP/testes/Caso3_Nterm5/";
//  const char *outputPath = "/home/eduardo/workspace/data_HeMoLab/CCO_SolverGP/testes/Caso4_Nterm10/";
//  const char *outputPath = "/home/eduardo/workspace/data_HeMoLab/CCO_SolverGP/testes/Caso5_Nterm20/";
  const char *outputPath = "/home/eduardo/workspace/data_HeMoLab/CCO_SolverGP/testes/Caso6_Nterm250/";





//  cout << "\n-- Used parameters --" << endl;
//  cout << "fileName --> " << fileName <<endl;
//  cout << "outputPath --> " << outputPath <<endl;
//  cout << "\n" <<endl;
    



  vtkPolyDataReader *reader = vtkPolyDataReader::New();
  reader->SetFileName(fileName);
  reader->Update();

  vtkPolyData *poly = vtkPolyData::New();
  poly->ShallowCopy( reader->GetOutput() );
  poly->Update();

  CCOModel *ccoModel = new CCOModel;

  CreateCCOModel( poly, ccoModel, NULL );





  cout << "\n-- Printing tree -- \n" << endl;
  ccoModel->PrintTree( NULL );





  cout << "\n-- Printing Incidence List --" << endl;
  vector<vtkIdList *>::iterator it;
  vector<vtkIdList *> vec = ccoModel->GetIncidenceList();
  cout << "Number of Elements: " << vec.size() << endl;
  for(it = vec.begin(); it != vec.end(); ++it)
    {
    for(int i = 0; i < (*it)->GetNumberOfIds(); ++i)
      {
      cout << (*it)->GetId(i) << "\t";
      }
    cout << endl;
    }







  IniWriter *iniWriter = new IniWriter();
  iniWriter->SetModel( ccoModel );
  iniWriter->SetOutPutPath( outputPath );
  iniWriter->Write();
  delete iniWriter;


  MeshWriter *meshWriter = new MeshWriter();
  meshWriter->SetModel( ccoModel );
  meshWriter->SetOutPutPath( outputPath );
  meshWriter->Write();
  delete meshWriter;


  ParamWriter *paramWriter = new ParamWriter();
  paramWriter->SetModel( ccoModel );
  paramWriter->SetOutPutPath( outputPath );
  paramWriter->Write();
  delete paramWriter;


  delete ccoModel;
  poly->Delete();


  vtkPolyDataMapper *mapper = vtkPolyDataMapper::New();
  mapper->SetInput( reader->GetOutput() );

  vtkActor *actor = vtkActor::New();
  actor->SetMapper( mapper );

  vtkRenderer *renderer = vtkRenderer::New();
  renderer->AddActor( actor );
  renderer->SetBackground(0.3,0.3,0.5);

  vtkRenderWindow *renWin = vtkRenderWindow::New();
  renWin->AddRenderer(renderer);

  vtkRenderWindowInteractor *iren = vtkRenderWindowInteractor::New();
  iren->SetRenderWindow(renWin);
  renWin->SetSize(1024, 768);

  renderer->Render();

  iren->Initialize();
  iren->Start();

  mapper->Delete();
  actor->Delete();
  renderer->Delete();
  renWin->Delete();
  iren->Delete();
  reader->Delete();


  return 0;
}

