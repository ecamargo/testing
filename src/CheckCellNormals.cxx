#include "vtkRenderWindowInteractor.h"
#include "vtkRenderWindow.h"
#include "vtkRenderer.h"

#include "vtkSmartPointer.h"
#include "vtkPolyDataReader.h"
#include "vtkPolyDataWriter.h"
#include "vtkPolyData.h"
#include "vtkPolyDataMapper.h"
#include "vtkActor.h"
#include "vtkProperty.h"
#include "vtkMath.h"
#include "vtkIdList.h"


using namespace std;



//-----------------------------------------------------------------------------
int main( int argc, char **argv )
{
  double backgroundColor[] = {0.8, 0.8, 0.8};


  string fileNameMesh = "/home/eduardo/workspace/data_HeMoLab/Aneurisk/c0001/aneurism_BASE_C0001_PolyData.vtk";



  vtkSmartPointer<vtkPolyDataReader> readermesh = vtkSmartPointer<vtkPolyDataReader>::New();
  readermesh->SetFileName( fileNameMesh.c_str() );
  readermesh->Update();

  vtkSmartPointer<vtkPolyData> mesh = vtkSmartPointer<vtkPolyData>::New();
  mesh->DeepCopy( readermesh->GetOutput() );
  mesh->Update();

  vtkSmartPointer<vtkIdList> ptList = vtkSmartPointer<vtkIdList>::New();
  for(int i=0; i<mesh->GetNumberOfCells(); ++i)
    {
    mesh->GetCellPoints( i, ptList );

    if(ptList->GetNumberOfIds() > 3)
      {
      cout << "check cell points. CellId: " << i <<endl;
      return 1;
      }

    double p0[3], p1[3], p2[3];

    mesh->GetPoint( ptList->GetId(0), p0);
    mesh->GetPoint( ptList->GetId(1), p1);
    mesh->GetPoint( ptList->GetId(2), p2);

    double det = vtkMath::Determinant3x3( p0, p1, p2 );

    cout << "Cell: " << i << "\t Det: " << det <<endl;

    if( det <= 0.0 )
      cout << "\t >> check Cell: " << i << "\t Det: " << det <<endl;
    }





  vtkSmartPointer<vtkPolyDataMapper> mappermesh = vtkSmartPointer<vtkPolyDataMapper>::New();
  mappermesh->SetInput( mesh );

  vtkSmartPointer<vtkActor> actormesh = vtkSmartPointer<vtkActor>::New();
  actormesh->SetMapper( mappermesh );
    
  vtkSmartPointer<vtkRenderer> renderer = vtkSmartPointer<vtkRenderer>::New();
  renderer->AddActor( actormesh );

  renderer->SetBackground( backgroundColor );

  vtkSmartPointer<vtkRenderWindow> renWin = vtkSmartPointer<vtkRenderWindow>::New();
  renWin->AddRenderer(renderer);

  vtkSmartPointer<vtkRenderWindowInteractor> iren = vtkSmartPointer<vtkRenderWindowInteractor>::New();
  iren->SetRenderWindow(renWin);
  renWin->SetSize(1024, 768);


  renderer->Render();
  iren->Initialize();
  iren->Start();
}

