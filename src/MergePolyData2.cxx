

#include "vtkPolyDataReader.h"
#include "vtkPolyDataWriter.h"
#include "vtkPolyData.h"
#include "vtkSmartPointer.h"
#include <vtkActor.h>
#include <vtkRenderWindow.h>
#include <vtkRenderer.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkPolyDataMapper.h>
#include <vtkBooleanOperationPolyDataFilter.h>
#include <vtkIdList.h>

#include <string.h>

using namespace std;



//-----------------------------------------------------------------------------------------------------------
bool HasOnlyTriangles( vtkSmartPointer<vtkPolyData> poly )
{
  vtkSmartPointer<vtkIdList> ptList = vtkSmartPointer<vtkIdList>::New();
  for(int i=0; i < poly->GetNumberOfCells(); ++i)
    {
    poly->GetCellPoints( i, ptList );

    if(ptList->GetNumberOfIds() != 3)
      {
      cout << "CellId: " << i << "is not triangle." <<endl;
      return false;
      }
    }

  return true;
}

//-----------------------------------------------------------------------------------------------------------
void WritePolyData( vtkSmartPointer<vtkPolyData> poly, string fullName)
{
  vtkSmartPointer<vtkPolyDataWriter> writer = vtkSmartPointer<vtkPolyDataWriter>::New();
  writer->SetFileName( fullName.c_str() );
  writer->SetInput( poly );
  writer->Update();
}

//-----------------------------------------------------------------------------------------------------------
int main( int argc, char **argv )
{

  string path = "/home/nfs/aneurysm_project/morphIndexes/1287259/26896/ANGIOGRAFIA_5/volumetricImage_1/segmentationID_1/meshID_1/morphIdexes/massTests/";
  string sack = "sack_more_triangle2.vtk";
  string neck = "aneurysmNeck.vtk";


  string sackToRead = path + sack;
  string neckToRead = path + neck;

  vtkSmartPointer<vtkPolyDataReader> readerSack = vtkSmartPointer<vtkPolyDataReader>::New();
  readerSack->SetFileName( sackToRead.c_str() );
  readerSack->Update();

  vtkSmartPointer<vtkPolyData> meshSack = vtkSmartPointer<vtkPolyData>::New();
  meshSack->DeepCopy( readerSack->GetOutput() );
  meshSack->BuildLinks();
  meshSack->Update();

  if( !HasOnlyTriangles( meshSack ) )
    {
    cout << "Sack does not have only triangles." <<endl;
    return 1;
    }

  vtkSmartPointer<vtkPolyDataMapper> mapperSack = vtkSmartPointer<vtkPolyDataMapper>::New();
  mapperSack->SetInput( meshSack );

  vtkSmartPointer<vtkActor> actorSack = vtkSmartPointer<vtkActor>::New();
  actorSack->SetMapper(mapperSack);





  vtkSmartPointer<vtkPolyDataReader> readerNeck = vtkSmartPointer<vtkPolyDataReader>::New();
  readerNeck->SetFileName( neckToRead.c_str() );
  readerNeck->Update();

  vtkSmartPointer<vtkPolyData> meshNeck = vtkSmartPointer<vtkPolyData>::New();
  meshNeck->DeepCopy( readerNeck->GetOutput() );
  meshNeck->BuildLinks();
  meshNeck->Update();

  if( !HasOnlyTriangles( meshNeck ) )
    {
    cout << "Neck does not have only triangles." <<endl;
    return 1;
    }

  vtkSmartPointer<vtkPolyDataMapper> mapperNeck = vtkSmartPointer<vtkPolyDataMapper>::New();
  mapperNeck->SetInput( meshNeck );

  vtkSmartPointer<vtkActor> actorNeck = vtkSmartPointer<vtkActor>::New();
  actorNeck->SetMapper(mapperNeck);



//  vtkSmartPointer<vtkPolyDataConnectivityFilter> connectivity = vtkSmartPointer<vtkPolyDataConnectivityFilter>::New();
//  connectivity->SetInput();
//  connectivity->ScalarConnectivityOn()




  vtkSmartPointer<vtkBooleanOperationPolyDataFilter> boolean = vtkSmartPointer<vtkBooleanOperationPolyDataFilter>::New();
  boolean->SetOperation( 1 );
  boolean->SetInput(0, meshSack);
  boolean->SetInput(1, meshNeck);
  boolean->Update();

  WritePolyData( boolean->GetOutput(), "/home/nfs/aneurysm_project/morphIndexes/1287259/26896/ANGIOGRAFIA_5/volumetricImage_1/segmentationID_1/meshID_1/morphIdexes/massTests/boolean.vtk");


  vtkSmartPointer<vtkPolyDataMapper> mapperBoolean = vtkSmartPointer<vtkPolyDataMapper>::New();
  mapperBoolean->SetInput( boolean->GetOutput() );

  vtkSmartPointer<vtkActor> actorBoolean = vtkSmartPointer<vtkActor>::New();
  actorBoolean->SetMapper( mapperBoolean );




  // A renderer and render window
  vtkSmartPointer<vtkRenderer> renderer = vtkSmartPointer<vtkRenderer>::New();
  vtkSmartPointer<vtkRenderWindow> renderWindow = vtkSmartPointer<vtkRenderWindow>::New();
  renderWindow->AddRenderer(renderer);

  // An interactor
  vtkSmartPointer<vtkRenderWindowInteractor> renderWindowInteractor = vtkSmartPointer<vtkRenderWindowInteractor>::New();
  renderWindowInteractor->SetRenderWindow(renderWindow);

//  renderer->AddActor(actorSack);
//  renderer->AddActor(actorNeck);
  renderer->AddActor(actorBoolean);
  renderer->SetBackground(0.3,0.35,0.5);

  renderWindow->SetSize(1024,800);
  renderWindow->Render();

  renderWindowInteractor->Start();

  return 0;
}



