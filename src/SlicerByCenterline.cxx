
#include <vtkSmartPointer.h>
#include <vtkLine.h>
#include <vtkCellArray.h>
#include <vtkTubeFilter.h>
#include <vtkLineSource.h>
#include <vtkPolyData.h>
#include <vtkPolyDataReader.h>
#include <vtkPolyDataWriter.h>
#include <vtkPolyDataMapper.h>
#include <vtkUnstructuredGrid.h>
#include <vtkUnstructuredGridReader.h>
#include <vtkDataSetSurfaceFilter.h>
#include <vtkGeometryFilter.h>
#include <vtkActor.h>
#include <vtkRenderWindow.h>
#include <vtkRenderer.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkProperty.h>
#include <vtkPointData.h>
#include <vtkPlane.h>
#include <vtkCutter.h>
#include <vtkSplineFilter.h>


//#include "stdio.h"

#include <iostream>
using namespace std;




////--------------------------------------------------------------------------------------------------
//void ComputeExternalPoint( double distance, double pointCoord[3], double normal[3], double *externalPoint )
//{
//  double p[3];
//
//  p[0] = normal[0] + pointCoord[0];
//  p[1] = normal[1] + pointCoord[1];
//  p[2] = normal[2] + pointCoord[2];
//
//
//  //forçando a localização de um ponto fora da bounding box
//  double d = sqrt(vtkMath::Distance2BetweenPoints( pointCoord, p ));
//  double t = (distance/d) * 1.5;
//
//  externalPoint[0] = pointCoord[0] + t * ( p[0] - pointCoord[0]);
//  externalPoint[1] = pointCoord[1] + t * ( p[1] - pointCoord[1]);
//  externalPoint[2] = pointCoord[2] + t * ( p[2] - pointCoord[2]);
//}

//-----------------------------------------------------------------------------
vtkIdList * GetPointNeighbors( vtkIdType pointId, vtkPolyData *input )
{
  vtkIdList *neighbors = vtkIdList::New();

  input->BuildLinks();

  vtkIdList *cellIds = vtkIdList::New();
  input->GetPointCells(pointId, cellIds);

//  cout << "ponto: " << pointId <<endl;

  for(int i=0; i<cellIds->GetNumberOfIds(); ++i)
    {
    vtkIdList *ptIds = vtkIdList::New();
    input->GetCellPoints(cellIds->GetId(i), ptIds);

//    cout << "\tcell: " << cellIds->GetId(i) <<endl;

    int pos = ptIds->IsId(pointId);

    if(pos == 0)
      {
      neighbors->InsertUniqueId( ptIds->GetId(pos+1) );
//      cout << "\t\tvizinho: " << ptIds->GetId(pos+1) <<endl;
      }
    else if(pos == ptIds->GetNumberOfIds()-1)
      {
      neighbors->InsertUniqueId( ptIds->GetId(pos-1) );
//      cout << "\t\tvizinho: " << ptIds->GetId(pos-1) <<endl;
      }
    else
      {
      neighbors->InsertUniqueId( ptIds->GetId(pos+1) );
      neighbors->InsertUniqueId( ptIds->GetId(pos-1) );
//      cout << "\t\tvizinho: " << ptIds->GetId(pos+1) <<endl;
//      cout << "\t\tvizinho: " << ptIds->GetId(pos-1) <<endl;
      }

    ptIds->Delete();
    }

  cellIds->Delete();

  return neighbors;
}


//------------------------------------------------------------------------------------
void InsertPointsInCell(vtkIdType ptId, vtkPolyData *input, vtkIdList *visitedPoints, vtkIdList *finalCell)
{
  vtkIdList *neighbors = GetPointNeighbors(ptId, input);
  finalCell->InsertUniqueId(ptId);
  visitedPoints->InsertUniqueId(ptId);


  for(int i=0; i<neighbors->GetNumberOfIds();++i)
    {
    vtkIdType id = neighbors->GetId(i);
    if( visitedPoints->IsId(id) == -1 )
      InsertPointsInCell( id, input, visitedPoints, finalCell );
    }

  neighbors->Delete();
}

//------------------------------------------------------------------------------------
vtkPolyData * MergeAllLines(vtkPolyData *input)
{
  // input é uma linha de centro produzida pelo HeMoLab e, por isso, o ponto 0 (zero) é o ponto de entrada do fluxo.

  vtkIdList *visitedPoints = vtkIdList::New();
  vtkIdList *finalCell = vtkIdList::New();

  vtkPolyData *output = vtkPolyData::New();
  output->Allocate();

  output->SetPoints( input->GetPoints() );
  output->GetPointData()->DeepCopy( input->GetPointData() );

  InsertPointsInCell( 0, input, visitedPoints, finalCell );


  for(int i=0; i<finalCell->GetNumberOfIds();++i)
    {
    cout << finalCell->GetId(i) <<endl;
    }

  output->InsertNextCell(VTK_POLY_LINE, finalCell);
  output->Update();

  visitedPoints->Delete();
  finalCell->Delete();

  return output;
}

//-----------------------------------------------------------------------------
int main( int argc, char **argv )
{
  string centerlineFile = "/home/eduardo/workspace/data_HeMoLab/TesteSlicerByCenterline/centerlineToTest.vtk";
  string volumeFile = "/home/eduardo/workspace/data_HeMoLab/TesteSlicerByCenterline/volumeToTest.vtk";


  //Reading centerline
  vtkSmartPointer<vtkPolyDataReader> centerlineReader = vtkSmartPointer<vtkPolyDataReader>::New();
  centerlineReader->SetFileName( centerlineFile.c_str() );
  centerlineReader->Update();

  vtkSmartPointer<vtkPolyData> centerline = vtkSmartPointer<vtkPolyData>::New();
  centerline->DeepCopy( centerlineReader->GetOutput() );
  centerline->Update();


  //Reading volume
  vtkSmartPointer<vtkUnstructuredGridReader> volumeReader = vtkSmartPointer<vtkUnstructuredGridReader>::New();
  volumeReader->SetFileName( volumeFile.c_str() );
  volumeReader->Update();

//  vtkSmartPointer<vtkGeometryFilter> UgridToPoly = vtkSmartPointer<vtkGeometryFilter>::New();
//  UgridToPoly->SetInput( volumeReader->GetOutput() );
//  UgridToPoly->Update();

  vtkSmartPointer<vtkUnstructuredGrid> volume = vtkSmartPointer<vtkUnstructuredGrid>::New();
  volume->DeepCopy( volumeReader->GetOutput() );
  volume->Update();




  //Slicing - vtkCutter
  double coordA[3], coordB[3], normal[3], origin[3];
//  double radius;

  int pointA = 1;
  int pointB = 0;
  centerline->GetPoint( pointA, coordA );
//  radius = centerline->GetPointData()->GetArray("Radius")->GetTuple1( pointA );


    pointB = pointA+1;
    centerline->GetPoint( pointB, coordB );

    normal[0] = coordB[0] - coordA[0];
    normal[1] = coordB[1] - coordA[1];
    normal[2] = coordB[2] - coordA[2];

    cout << "coordA: " << coordA[0] << ", " << coordA[1] << ", " << coordA[2] << endl;
    cout << "coordB: " << coordB[0] << ", " << coordB[1] << ", " << coordB[2] << endl;
    cout << "normal: " << normal[0] << ", " << normal[1] << ", " << normal[2] << endl;



    if( pointA == centerline->GetNumberOfPoints()-1 )
    {
    pointB = pointA-1;
    centerline->GetPoint( pointB, coordB );

    normal[0] = coordA[0] - coordB[0];
    normal[1] = coordA[1] - coordB[1];
    normal[2] = coordA[2] - coordB[2];


    cout << "coordA: " << coordA[0] << ", " << coordA[1] << ", " << coordA[2] << endl;
    cout << "coordB: " << coordB[0] << ", " << coordB[1] << ", " << coordB[2] << endl;
    cout << "normal: " << normal[0] << ", " << normal[1] << ", " << normal[2] << endl;
    }


  origin[0] = coordA[0];
  origin[1] = coordA[1];
  origin[2] = coordA[2];







  origin[0] = 1.46495;
  origin[1] = -17.4385;
  origin[2] = 33.4407;

  normal[0] = 0.926956;
  normal[1] = 0.0240383;
  normal[2] = -0.370408;

  cout << "origin: " << origin[0] << ", " << origin[1] << ", " << origin[2] << endl;
  cout << "normal: " << normal[0] << ", " << normal[1] << ", " << normal[2] << endl;




  vtkSmartPointer<vtkPlane> plane = vtkSmartPointer<vtkPlane>::New();
  plane->SetOrigin( origin[0], origin[1], origin[2] );
  plane->SetNormal( normal[0], normal[1], normal[2] );

  // Create cutter
  vtkSmartPointer<vtkCutter> cutter = vtkSmartPointer<vtkCutter>::New();
  cutter->SetCutFunction( plane );
  cutter->SetInput( volume );
  cutter->Update();
  
  vtkSmartPointer<vtkPolyData> cutterPlane = vtkSmartPointer<vtkPolyData>::New();
  cutterPlane->DeepCopy( cutter->GetOutput() );
  cutterPlane->Update();
  


  vtkSmartPointer<vtkPolyData> tmp = vtkSmartPointer<vtkPolyData>::New();
  tmp->DeepCopy( MergeAllLines(centerline) );
  tmp->Update();



  vtkSmartPointer<vtkSplineFilter> sf = vtkSmartPointer<vtkSplineFilter>::New();
  sf->SetInput( centerline );
  sf->SetSubdivideToLength();
  sf->SetLength( 0.05 );



  vtkSmartPointer<vtkPolyDataWriter> w = vtkSmartPointer<vtkPolyDataWriter>::New();
  w->SetInput( tmp );
  w->SetFileName("/home/eduardo/workspace/data_HeMoLab/TesteSlicerByCenterline/centerlineToTestOneCell.vtk");
  w->Write();



  //Setting output
  vtkSmartPointer<vtkPolyDataMapper> mapperCenterLine = vtkSmartPointer<vtkPolyDataMapper>::New();
  mapperCenterLine->SetInput( centerlineReader->GetOutput() );

  vtkSmartPointer<vtkPolyDataMapper> mapperCutter = vtkSmartPointer<vtkPolyDataMapper>::New();
  mapperCutter->SetInput( cutterPlane );

  vtkSmartPointer<vtkActor> actorCenterLine = vtkSmartPointer<vtkActor>::New();
  actorCenterLine->SetMapper( mapperCenterLine );

  vtkSmartPointer<vtkActor> actorCutter = vtkSmartPointer<vtkActor>::New();
  actorCutter->SetMapper( mapperCutter );


  // Create a renderer, render window, and interactor
  vtkSmartPointer<vtkRenderer> renderer = vtkSmartPointer<vtkRenderer>::New();
  vtkSmartPointer<vtkRenderWindow> renderWindow = vtkSmartPointer<vtkRenderWindow>::New();
  renderWindow->AddRenderer(renderer);
  renderWindow->SetSize(1024, 768);

  vtkSmartPointer<vtkRenderWindowInteractor> renderWindowInteractor = vtkSmartPointer<vtkRenderWindowInteractor>::New();
  renderWindowInteractor->SetRenderWindow(renderWindow);
 
  // Add the actor to the scene
  renderer->AddActor(actorCenterLine);
  renderer->AddActor(actorCutter);
//  renderer->SetBackground(81,87,110);

  // Render and interact
  renderWindow->Render();
  renderWindowInteractor->Start();


  return 0;
}


