/*
 * PlotTesteVTK.h
 *
 *  Created on: Sep 1, 2011
 *      Author: igor
 */

#ifndef PLOTTESTEVTK_H_
#define PLOTTESTEVTK_H_

#include <QtGui/QWidget>
//#include <QtGui/QDialog>
//#include <QtGui/QDockWidget>
//#include <QtCore/QSettings>
#include <QtGui/QtGui>

#include "ui_PlotTesteVTK.h"

#include <QVTKWidget.h>
#include "vtkContextView.h"
#include "vtkChartXY.h"
#include "vtkPlot.h"

class PlotTesteVTK : public QWidget, public Ui_PlotTesteVTK
{
    Q_OBJECT

public:
  PlotTesteVTK();
  ~PlotTesteVTK();

  void Create();

protected:
  QVTKWidget *Widget;

  vtkContextView *View;

  vtkChartXY *chart;

//  vtkPlot *Plot;
};

#endif /* PLOTTESTEVTK_H_ */
