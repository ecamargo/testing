#include "imDICOMDump.h"


#if ITK_VERSION_MAJOR < 4

#include <sys/stat.h>

std::ostringstream imDICOMDump::DumpOutput;

//---------------------------------------------------------------------------------------------------------
int imDICOMDump::Dump (std::string arg)
{
  //redirecionando cout para DumpOutput
  std::streambuf* oldCoutStreamBuf = std::cout.rdbuf();
  std::cout.rdbuf( DumpOutput.rdbuf() );

  int parameters[18];

  parameters[0] = 0; //printdict;
  parameters[1] = 0; //dump;
  parameters[2] = 0; //print;
  parameters[3] = 0; //printcsa;
  parameters[4] = 0; //printpdb;
  parameters[5] = 0; //printelscint;
  parameters[6] = 0; //printvepro;
  parameters[7] = 0; //printsds;  MR Series Data Storage
  parameters[8] = 0; //printct3;  TOSHIBA_MEC_CT3
  parameters[9] = 0; //verbose;
  parameters[10] = 0; //warning;
  parameters[11] = 0; //debug;
  parameters[12] = 0; //error;
  parameters[13] = 0; //help;
  parameters[14] = 0; //version;
  parameters[15] = 0; //recursive;
  parameters[16] = 0; //printasn1;
  parameters[17] = 0; //mapuidnames;

  int r = Dump ( arg, parameters );

  //desfazendo o redirecionando
  std::cout.rdbuf( oldCoutStreamBuf );

  return r;
}

//---------------------------------------------------------------------------------------------------------
int imDICOMDump::Dump (std::string arg, int parameters[])
{
  if( arg.empty() )
    {
    std::cout << "Need input file" <<std::endl;
    return 1;
    }


  std::string filename;

  //se for um diretório
  if( access(arg.c_str(), 0) == 0 )
    {
    struct stat status;
    stat( arg.c_str(), &status );
    if( status.st_mode & S_IFDIR )
      {
      filename = "dirin=";
      filename += arg;
      }
    }


  //se for um arquivo
  if( access(arg.c_str(), 0) == 0 )
    {
    struct stat status;
    stat( arg.c_str(), &status );
    if( !(status.st_mode & S_IFDIR) )
      {
      filename = "filein=";
      filename += arg;
      }
    }





  int argc = 3;
  char **argv = new char*[argc];
  argv[0] = (char*)"";
  argv[1] = (char*)filename.c_str();
  argv[2] = (char*)"level=1";



  START_USAGE(usage)
  " \n PrintFile : \n                                                        ",
  " Display the header of a ACR-NEMA/PAPYRUS/DICOM File                      ",
  " usage: PrintFile {filein=inputFileName|dirin=inputDirectoryName}[level=n]",
  "                       [forceload=listOfElementsToForceLoad]              ",
  "                       [dict= privateDirectory]                           ",
  "                       [ { [noshadowseq] | [noshadow][noseq] } ] [debug]  ",
  "      level = 0,1,2 : depending on the amount of details user wants to see",
  "      listOfElementsToForceLoad : group-elem,g2-e2,... (in hexa, no space)",
  "                                of Elements to load whatever their length ",
  "      privateDirectory : source file full path name of Shadow Group elems ",
  "      noshadowseq: user doesn't want to load Private Sequences            ",
  "      noshadow   : user doesn't want to load Private groups (odd number)  ",
  "      noseq      : user doesn't want to load Sequences                    ",
  "      debug      : user wants to run the program in 'debug mode'          ",
  "      showlut :user wants to display the Palette Color (as an int array)  ",
  FINISH_USAGE


  // Initialize Arguments Manager
  gdcm::ArgMgr *am= new gdcm::ArgMgr(argc, argv);

  if (argc == 1 || am->ArgMgrDefined("usage") )
  {
     am->ArgMgrUsage(usage); // Display 'usage'
     delete am;
     return 0;
  }

  const char *fileName = am->ArgMgrGetString("filein");
  const char *dirName  = am->ArgMgrGetString("dirin");

  if ( (fileName == 0 && dirName == 0)
       ||
       (fileName != 0 && dirName != 0) )
  {
      std::cout <<std::endl
                << "Either 'filein=' or 'dirin=' must be present;"
                << std::endl << "Not both" << std::endl;
      am->ArgMgrUsage(usage); // Display 'usage'
      delete am;
      return 0;
}

  if (am->ArgMgrDefined("debug"))
     gdcm::Debug::DebugOn();

  int loadMode = gdcm::LD_ALL;
  if ( am->ArgMgrDefined("noshadowseq") )
     loadMode |= gdcm::LD_NOSHADOWSEQ;
  else
  {
  if ( am->ArgMgrDefined("noshadow") )
        loadMode |= gdcm::LD_NOSHADOW;
     if ( am->ArgMgrDefined("noseq") )
        loadMode |= gdcm::LD_NOSEQ;
  }

  int level = am->ArgMgrGetInt("level", 1);

  int forceLoadNb;
  uint16_t *elemsToForceLoad
                          = am->ArgMgrGetXInt16Enum("forceload", &forceLoadNb);

  bool showlut = ( 0 != am->ArgMgrDefined("SHOWLUT") );

  bool ddict = am->ArgMgrDefined("dict") ? true : false;
  const char *dict = 0;

  if (ddict)
  {
    dict = am->ArgMgrGetString("dict");
  }

  /* if unused Param we give up */
  if ( am->ArgMgrPrintUnusedLabels() )
  {
     am->ArgMgrUsage(usage);
     delete am;
     return 0;
  }

  delete am;  // we don't need Argument Manager any longer

  // ----------- End Arguments Manager ---------


  if (ddict)
  {
     gdcm::Global::GetDicts()->GetDefaultPubDict()->AddDict(dict);
  }

  if ( fileName != 0 ) // ====== Deal with a single file ======
  {
     // gdcm::File::IsReadable() is no usable here, because we deal with
     // any kind of gdcm-Parsable *document*
     // not only gdcm::File (as opposed to gdcm::DicomDir)

     gdcm::File *f = new gdcm::File();
     f->SetLoadMode(loadMode);
     f->SetFileName( fileName );

     for (int ri=0; ri<forceLoadNb; ri++)
     {
        f->AddForceLoadElement((uint32_t)elemsToForceLoad[2*ri],
                               (uint32_t)elemsToForceLoad[2*ri+1] );
     }

     bool res = false;
     try
       {
       res = f->Load();
       }
     catch(std::exception &ex)
       {
       std::cerr << ex.what() << std::endl;
       }
     if ( !res )
     {
        std::cout << "Cannot process file [" << fileName << "]" << std::endl;
        std::cout << "Either it doesn't exist, or it's read protected "
                  << std::endl;
        std::cout << "or it's not a Dicom File, or its 'header' is bugged"
                  << std::endl;
        std::cout << "use 'PrintFile filein=... debug' to try to guess the pb"
                  << std::endl;
        delete f;
        return 0;
     }

     gdcm::FileHelper *fh = new gdcm::FileHelper(f);
     fh->SetPrintLevel( level );

     fh->Print();

     std::cout << "\n\n" << std::endl;

     std::cout <<std::endl;
     std::cout <<" dataSize    " << fh->GetImageDataSize()    << std::endl;
     std::cout <<" dataSizeRaw " << fh->GetImageDataRawSize() << std::endl;

     int nX,nY,nZ,sPP,planarConfig;
     std::string pixelType;
     nX=f->GetXSize();
     nY=f->GetYSize();
     nZ=f->GetZSize();
     std::cout << " DIMX=" << nX << " DIMY=" << nY << " DIMZ=" << nZ
               << std::endl;

     pixelType    = f->GetPixelType();
     sPP          = f->GetSamplesPerPixel();
     planarConfig = f->GetPlanarConfiguration();

     std::cout << " pixelType= ["            << pixelType
               << "] SamplesPerPixel= ["     << sPP
               << "] PlanarConfiguration= [" << planarConfig
               << "] "<< std::endl
               << " PhotometricInterpretation= ["
                               << f->GetEntryValue(0x0028,0x0004)
               << "] "<< std::endl;

     int numberOfScalarComponents=f->GetNumberOfScalarComponents();
     std::cout << " NumberOfScalarComponents = " << numberOfScalarComponents
               <<std::endl
               << " LUT = " << (f->HasLUT() ? "TRUE" : "FALSE")
               << std::endl;

     if ( f->GetEntryValue(0x0002,0x0010) == gdcm::GDCM_NOTLOADED )
     {
        std::cout << "Transfer Syntax not loaded. " << std::endl
                  << "Better you increase MAX_SIZE_LOAD_ELEMENT_VALUE"
               << std::endl;
        return 0;
     }

     std::string transferSyntaxName = f->GetTransferSyntaxName();
     std::cout << " TransferSyntaxName= [" << transferSyntaxName << "]"
               << std::endl;
     std::cout << " SwapCode= " << f->GetSwapCode() << std::endl;
     std::cout << " ------" << std::endl;
     //std::cout << "\n\n" << std::endl;
     //std::cout << "X spacing " << f->GetXSpacing() << std::endl;
     //std::cout << "Y spacing " << f->GetYSpacing() << std::endl;
     //std::cout << "Z spacing " << f->GetZSpacing() << std::endl;

//------------------------------
     // Lets's get and print some usefull fields about 'Orientation'
     // ------------------------------------------------------------

     std::string strPatientPosition =
                                     f->GetEntryValue(0x0018,0x5100);
     if ( strPatientPosition != gdcm::GDCM_UNFOUND
       && strPatientPosition != "" )
           std::cout << "PatientPosition (0x0010,0x5100)= ["
                     << strPatientPosition << "]" << std::endl;

     std::string strViewPosition =
                                     f->GetEntryValue(0x0018,0x5101);
     if ( strViewPosition != gdcm::GDCM_UNFOUND
       && strViewPosition != "" )
           std::cout << "strViewPosition (0x0010,0x5101)= ["
                     << strViewPosition << "]" << std::endl;

    std::string strPatientOrientation =
                                     f->GetEntryValue(0x0020,0x0020);
     if ( strPatientOrientation != gdcm::GDCM_UNFOUND
       && strPatientOrientation != "")
        std::cout << "PatientOrientation (0x0020,0x0020)= ["
                  << strPatientOrientation << "]" << std::endl;

     std::string strImageOrientationPatient =
                                     f->GetEntryValue(0x0020,0x0037);
     if ( strImageOrientationPatient != gdcm::GDCM_UNFOUND
       && strImageOrientationPatient != "" )
        std::cout << "ImageOrientationPatient (0x0020,0x0037)= ["
                  << strImageOrientationPatient << "]" << std::endl;

     std::string strImageOrientationRET =
                                     f->GetEntryValue(0x0020,0x0035);
     if ( strImageOrientationRET != gdcm::GDCM_UNFOUND
       && strImageOrientationRET != "" )
        std::cout << "ImageOrientationRET (0x0020,0x0035)= ["
                  << strImageOrientationRET << "]" << std::endl;

     // Let's compute 'user friendly' results about 'Orientation'
     // ---------------------------------------------------------

     gdcm::Orientation o;

     if ( strImageOrientationPatient != gdcm::GDCM_UNFOUND ||
          strImageOrientationRET     != gdcm::GDCM_UNFOUND )
     {

        gdcm::OrientationType orient = o.GetOrientationType( f );

        std::cout << "TypeOrientation = " << orient << " (-> "
                  << o.GetOrientationTypeString(orient) << " )" << std::endl;
     }

     std::string ori = o.GetOrientation ( f );
     if (ori != "\\" )
        std::cout << "Orientation [" << ori << "]" << std::endl;

//------------------------------


     // Display the LUT as an int array (for debugging purpose)
     if ( f->HasLUT() && showlut )
     {
        uint8_t* lutrgba = fh->GetLutRGBA();
        if ( lutrgba == 0 )
        {
           std::cout << "Lut RGBA (Palette Color) not built " << std::endl;

          // Nothing is written yet to get LUT Data user friendly
          // The following is to be moved into a PixelRedaConvert method

           gdcm::SeqEntry *modLutSeq = f->GetSeqEntry(0x0028,0x3000);
           if ( modLutSeq !=0 )
           {
              gdcm::SQItem *sqi= modLutSeq->GetFirstSQItem();
              if ( !sqi )
              {
              std::string lutDescriptor = sqi->GetEntryValue(0x0028,0x3002);
                 int length;   // LUT length in Bytes
                 int deb;      // Subscript of the first Lut Value
                 int nbits;    // Lut item size (in Bits)
                 int nbRead;   // nb of items in LUT descriptor (must be = 3)

                 nbRead = sscanf( lutDescriptor.c_str(),
                                   "%d\\%d\\%d",
                                    &length, &deb, &nbits );
                 if ( nbRead != 3 )
                 {
                     //gdcmWarningMacro( "Wrong LUT descriptor" );
                     std::cout << "Wrong LUT descriptor" << std::endl;
                 }
                 gdcm::BinEntry *b = sqi->GetBinEntry(0x0028,0x3006);
                 if ( b != 0 )
                 {
                    if ( b->GetLength() != 0 )
                    {
                       std::cout << "---------------------------------------"
                              << " We should never reach this point      "
                              << std::endl;
                       //LoadEntryBinArea(b);    //LUT Data (CTX dependent)
                    }
                }
             }
           }
           else
              std::cout << "No LUT Data (0x0028,0x3000) found " << std::endl;
       }
        else
        {
           if ( fh->GetLutItemSize() == 8 )
           {
              for (int i=0;i<fh->GetLutItemNumber();i++)
                 std::cout << i << " : \t"
                        << (int)(lutrgba[i*4])   << " "
                        << (int)(lutrgba[i*4+1]) << " "
                        << (int)(lutrgba[i*4+2]) << std::endl;
           }
           else // LutItemSize assumed to be = 16
           {
              uint16_t* lutrgba16 = (uint16_t*)lutrgba;
              for (int i=0;i<fh->GetLutItemNumber();i++)
                 std::cout << i << " : \t"
                        << (int)(lutrgba16[i*4])   << " "
                        << (int)(lutrgba16[i*4+1]) << " "
                        << (int)(lutrgba16[i*4+2]) << std::endl;
           }
        }
     }
     else if (showlut)
     {
        std::cout << "Try LUT Data "<< std::endl;
        ShowLutData(f);
     }

     //if( !f->gdcm::Document::IsReadable())
     // Try downcast to please MSVC
    if ( !((gdcm::Document *)f)->IsReadable() )
    {
        std::cout <<std::endl<<fileName<<" is NOT 'gdcm parsable'"<<std::endl;
     }

     if (f->IsReadable())
        std::cout <<std::endl<<fileName<<" is Readable"<<std::endl;
     else if ( f->GetSeqEntry(0x0041,0x1010) )
     {
        std::cout <<std::endl<<fileName<<" looks like a 'PAPYRUS image' file"
                  <<std::endl;
     }
     else if ( f->GetSeqEntry(0x0004,0x1220) )
     {
        std::cout <<std::endl<<fileName<<" looks like a 'DICOMDIR file'"
                  <<std::endl;
     }
     else
     {
        std::cout <<std::endl<<fileName<<" doesn't look like an image file "
            <<std::endl;
     }

     std::cout<<std::flush;
     delete f;
     delete fh;
     return 0;
  }
  else  // ====== Deal with a Directory ======
  {
     std::cout << "dirName [" << dirName << "]" << std::endl;
     gdcm::DirList dirList(dirName,1); // gets recursively the file list
     gdcm::DirListType fileList = dirList.GetFilenames();
     gdcm::File *f;
     bool res;
     for( gdcm::DirListType::iterator it  = fileList.begin();
                                it != fileList.end();
                                ++it )
     {
        std::cout << std::endl<<" Start processing :[" << it->c_str() << "]"
                  << std::endl;
        f = new gdcm::File();
        f->SetLoadMode(loadMode);
        f->SetFileName( it->c_str() );

        for (int ri=0; ri<forceLoadNb; ri++)
        {
           printf("%04x,%04x\n",elemsToForceLoad[2*ri],
                                elemsToForceLoad[2*ri+1]);
           f->AddForceLoadElement((uint32_t)elemsToForceLoad[2*ri],
                                  (uint32_t)elemsToForceLoad[2*ri+1]);
        }
        res = f->Load();

        if ( !res )
        {
           std::cout << "Cannot process file [" << it->c_str() << "]"
                     << std::endl;
           std::cout << "Either it doesn't exist, or it's read protected "
                     << std::endl;
           std::cout << "or it's not a Dicom File, or its 'header' is bugged"
                     << std::endl;
           std::cout << "use 'PrintFile filein=... debug' "
                     << "to try to guess the pb"
                     << std::endl;
           delete f;
           continue;
        }

        gdcm::FileHelper *fh = new gdcm::FileHelper(f);
        fh->SetPrintLevel( level );
        fh->Print();

//------------------------------

     // Lets's get and print some usefull fields about 'Orientation'
     // ------------------------------------------------------------

     std::string strPatientPosition =
                                     f->GetEntryValue(0x0018,0x5100);
     if ( strPatientPosition != gdcm::GDCM_UNFOUND
       && strPatientPosition != "" )
           std::cout << "PatientPosition (0x0010,0x5100)= ["
                     << strPatientPosition << "]" << std::endl;

     std::string strViewPosition =
                                     f->GetEntryValue(0x0018,0x5101);
     if ( strViewPosition != gdcm::GDCM_UNFOUND
       && strViewPosition != "" )
           std::cout << "strViewPosition (0x0010,0x5101)= ["
                     << strViewPosition << "]" << std::endl;

    std::string strPatientOrientation =
                                     f->GetEntryValue(0x0020,0x0020);
     if ( strPatientOrientation != gdcm::GDCM_UNFOUND
       && strPatientOrientation != "")
        std::cout << "PatientOrientation (0x0020,0x0020)= ["
                  << strPatientOrientation << "]" << std::endl;

     std::string strImageOrientationPatient =
                                     f->GetEntryValue(0x0020,0x0037);
     if ( strImageOrientationPatient != gdcm::GDCM_UNFOUND
       && strImageOrientationPatient != "" )
        std::cout << "ImageOrientationPatient (0x0020,0x0037)= ["
                  << strImageOrientationPatient << "]" << std::endl;

     std::string strImageOrientationRET =
                                     f->GetEntryValue(0x0020,0x0035);
     if ( strImageOrientationRET != gdcm::GDCM_UNFOUND
       && strImageOrientationRET != "" )
        std::cout << "ImageOrientationRET (0x0020,0x0035)= ["
                  << strImageOrientationRET << "]" << std::endl;

     // Let's compute 'user friendly' results about 'Orientation'
     // ---------------------------------------------------------

     gdcm::Orientation o;

     if ( strImageOrientationPatient != gdcm::GDCM_UNFOUND ||
          strImageOrientationRET     != gdcm::GDCM_UNFOUND )
     {

        gdcm::OrientationType orient = o.GetOrientationType( f );

        std::cout << "TypeOrientation = " << orient << " (-> "
                  << o.GetOrientationTypeString(orient) << " )" << std::endl;
     }

     std::string ori = o.GetOrientation ( f );
     if (ori != "\\" )
        std::cout << "Orientation [" << ori << "]" << std::endl;

//-------------------------------


        if (f->IsReadable())
           std::cout <<std::endl<<it->c_str()<<" is Readable"<<std::endl;
        else
           std::cout <<std::endl<<it->c_str()<<" is NOT Readable"<<std::endl;
        std::cout << "\n\n" << std::endl;
        delete f;
        delete fh;
     }
     std::cout<<std::flush;
  }













  return 0;
}

//---------------------------------------------------------------------------------------------------------
std::string imDICOMDump::GetDumpOutput()
{
  return DumpOutput.str();
}

//---------------------------------------------------------------------------------------------------------
void imDICOMDump::ShowLutData(gdcm::File *f)
{
  gdcm::SeqEntry *modLutSeq = f->GetSeqEntry(0x0028,0x3000);
  if ( modLutSeq !=0 )
    {
    gdcm::SQItem *sqi= modLutSeq->GetFirstSQItem();
    if ( sqi != 0 )
      {
      std::string lutDescriptor = sqi->GetEntryValue(0x0028,0x3002);
      if (   /*lutDescriptor   == GDCM_UNFOUND*/ 0 )
        {
        //gdcmWarningMacro( "LUT Descriptor is missing" );
        std::cout << "LUT Descriptor is missing" << std::endl;
        return;
        }
      int length;   // LUT length in Bytes
      int deb;      // Subscript of the first Lut Value
      int nbits;    // Lut item size (in Bits)

      int nbRead;    // nb of items in LUT descriptor (must be = 3)

      nbRead = sscanf( lutDescriptor.c_str(),
          "%d\\%d\\%d",
          &length, &deb, &nbits );
      std::cout << "length " << length
          << " deb " << deb
          << " nbits " << nbits
          << std::endl;
      if ( nbRead != 3 )
        {
        //gdcmWarningMacro( "Wrong LUT descriptor" );
        std::cout << "Wrong LUT descriptor" << std::endl;
        }
      //LUT Data (CTX dependent)
      gdcm::BinEntry *b = sqi->GetBinEntry(0x0028,0x3006);
      if ( b != 0 )
        {
        int BitsAllocated = f->GetBitsAllocated();
        if ( BitsAllocated <= 8 )
          {
          int mult;
          if ( ( nbits == 16 ) && ( BitsAllocated == 8 ) )
            {
            // when LUT item size is different than pixel size
            mult = 2; // high byte must be = low byte
            }
          else
            {
            // See PS 3.3-2003 C.11.1.1.2 p 619
            mult = 1;
            }
          uint8_t *lut = b->GetBinArea();
          for( int i=0; i < length; ++i )
            {
            std::cout << i+deb << " : \t"
                << (int) (lut[i*mult + 1]) << std::endl;
            }
          }
        else
          {
          uint16_t *lut = (uint16_t *)(b->GetBinArea());
          for( int i=0; i < length; ++i )
            {
            std::cout << i+deb << " : \t"
                << (int) (((uint16_t *)lut)[i])
                << std::endl;
            }
          }
        }
      else
        std::cout << "No LUT Data BinEntry (0x0028,0x3006) found?!? "
        << std::endl;
      }
    else
      std::cout << "No First SQ Item within (0x0028,0x3000) ?!? "
      << std::endl;
    }
  else
    std::cout << "No LUT Data SeqEntry (0x0028,0x3000) found "
    << std::endl;
}

#else
#include "gdcmPrivateTag.h"

int imDICOMDump::color = 0;
int imDICOMDump::ignoreerrors = 0;
std::ostringstream imDICOMDump::DumpOutput;


//---------------------------------------------------------------------------------------------------------
template <typename T>static void cleanup::printvaluet(std::istream & is, uint32_t numels)
{
  T buffer;
  for( uint32_t i = 0; i < numels; ++i )
    {
    if( i ) std::cout << "\\";
    is.read( (char*)&buffer, sizeof(T) );
    std::cout << buffer;
    }
}

//---------------------------------------------------------------------------------------------------------
static void cleanup::printvalue(std::istream &is, uint32_t type, uint32_t numels, uint32_t pos)
{
  assert( numels > 0 );
  std::streampos start = is.tellg();
  is.seekg( pos );
  std::cout << "[";
  typedef char (string81)[81]; // 80'th byte == 0
  assert( sizeof( string81 ) == 81 );
  switch( type )
    {
  case TYPE_FLOAT:
    printvaluet<float>(is, numels);
    break;
  case TYPE_INT32:
    printvaluet<int32_t>(is, numels);
    break;
  case TYPE_STRING:
    printvaluet<string81>(is, numels);
    break;
  case TYPE_UINT32:
    printvaluet<uint32_t>(is, numels);
    break;
  default:
    assert( 0 );
    }
  std::cout << "]";
  std::cout << " # " << numels;
  is.seekg( start );
}

//---------------------------------------------------------------------------------------------------------
static void cleanup::printbinary(std::istream &is, PDFElement const & pdfel )
{
  const char *bufferref = pdfel.getname();
  std::cout << "  " << bufferref << " ";
  uint32_t type = pdfel.gettype();
  uint32_t numels = pdfel.getnumelems();
  uint32_t dummy = pdfel.getdummy();
  assert( dummy == 0 ); (void)dummy;
  uint32_t offset = pdfel.getoffset();
  uint32_t pos = (uint32_t)(offset + is.tellg() - 4);
  printvalue(is, type, numels, pos);
}

//---------------------------------------------------------------------------------------------------------
static void cleanup::ProcessSDSData( std::istream & is )
{
  // havent been able to figure out what was the begin meant for
  is.seekg( 0x20 - 8 );
  uint32_t version = 0;
  is.read( (char*)&version, sizeof(version) );
  assert( version == 8 );
  uint32_t numel = 0;
  is.read( (char*)&numel, sizeof(numel) );
  for( uint32_t el = 0; el < numel; ++el )
    {
    PDFElement pdfel;
    assert( sizeof(pdfel) == 50 );
    is.read( (char*)&pdfel, 50 );
    if( *pdfel.getname() )
      {
      printbinary( is, pdfel );
      std::cout << std::endl;
      }
    }

}

//---------------------------------------------------------------------------------------------------------
// PMS MR Series Data Storage
static int cleanup::DumpPMS_MRSDS(const gdcm::DataSet & ds)
{
  const gdcm::PrivateTag tdata(0x2005,0x32,"Philips MR Imaging DD 002");
  if( !ds.FindDataElement( tdata ) ) return 1;
  const gdcm::DataElement &data = ds.GetDataElement( tdata );
  gdcm::SmartPointer<gdcm::SequenceOfItems> sqi = data.GetValueAsSQ();
  if( !sqi ) return 1;
  std::cout << "PMS Dumping info from tag " << tdata << std::endl;
  gdcm::SequenceOfItems::ConstIterator it = sqi->Begin();
  for( ; it != sqi->End(); ++it )
    {
    const gdcm::Item & item = *it;
    const gdcm::DataSet & nestedds = item.GetNestedDataSet();
    const gdcm::PrivateTag tprotocoldataname(0x2005,0x37,"Philips MR Imaging DD 002");
    const gdcm::DataElement & protocoldataname = nestedds.GetDataElement( tprotocoldataname );
    const gdcm::ByteValue *bv1 = protocoldataname.GetByteValue();
    const gdcm::PrivateTag tprotocoldatatype(0x2005,0x39,"Philips MR Imaging DD 002");
    const gdcm::DataElement & protocoldatatype = nestedds.GetDataElement( tprotocoldatatype );
    const gdcm::ByteValue *bv2 = protocoldatatype.GetByteValue();
    const gdcm::PrivateTag tprotocoldatablock(0x2005,0x44,"Philips MR Imaging DD 002");
    const gdcm::DataElement & protocoldatablock = nestedds.GetDataElement( tprotocoldatablock );
    const gdcm::ByteValue *bv3 = protocoldatablock.GetByteValue();
    const gdcm::PrivateTag tprotocoldatabool(0x2005,0x47,"Philips MR Imaging DD 002");
    const gdcm::DataElement & protocoldatabool = nestedds.GetDataElement( tprotocoldatabool );
    const gdcm::ByteValue *bv4 = protocoldatabool.GetByteValue();
    std::string s1;
    if( bv1 )
      {
      s1 = std::string( bv1->GetPointer(), bv1->GetLength() );
      }
    std::string s2;
    if( bv2 )
      {
      s2 = std::string( bv2->GetPointer(), bv2->GetLength() );
      }
    std::string s3;
    if( bv3 )
      {
      s3 = std::string( bv3->GetPointer(), bv3->GetLength() );
      }
    std::string s4;
    if( bv4 )
      {
      s4 = std::string( bv4->GetPointer(), bv4->GetLength() );
      }
    std::istringstream is( s3 );
    std::cout << "PMS/Item name: [" << s1 << "/" << s2 << "/" << s4 << "]" << std::endl;
    ProcessSDSData( is );
    }
  return 0;
}

//---------------------------------------------------------------------------------------------------------
static int cleanup::DumpTOSHIBA_MEC_CT3(const gdcm::DataSet & ds)
{
  const gdcm::PrivateTag tdata(0x7005,0x10,"TOSHIBA_MEC_CT3");
  if( !ds.FindDataElement( tdata ) ) return 1;
  const gdcm::DataElement &data = ds.GetDataElement( tdata );

  const gdcm::ByteValue *bv = data.GetByteValue();
  if( !bv ) return 1;

  const int offset = 24;
  if( bv->GetLength() < offset )
    {
    std::cerr << "Not enough header" << std::endl;
    return 1;
    }
  std::istringstream is0;
  const std::string str0 = std::string( bv->GetPointer(), offset );
  is0.str( str0 );
  gdcm::ImplicitDataElement ide0;
  ide0.Read<gdcm::SwapperNoOp>(is0);
  gdcm::ImplicitDataElement ide1;
  ide1.Read<gdcm::SwapperNoOp>(is0);

  gdcm::Attribute<0x0,0x0> at0;
  at0.SetFromDataElement( ide0 );
  if( at0.GetValue() != 12 )
    {
    std::cerr << "Bogus header value #0" << std::endl;
    return 1;
    }
  gdcm::Attribute<0x0,0x1> at1;
  at1.SetFromDataElement( ide1 );

  const unsigned int dlen = bv->GetLength() - offset;
  if( at1.GetValue() != dlen )
    {
    std::cerr << "Bogus header value #1" << std::endl;
    return 1;
    }
  std::istringstream is1;
  const std::string str1 = std::string( bv->GetPointer() + offset, bv->GetLength() - offset);
  is1.str( str1 );

  gdcm::Reader r;
  r.SetStream( is1 );
  if( !r.Read() )
    {
    std::cerr << "Could not read CT Private Data 2" << std::endl;
    return 1;
    }

  gdcm::Printer printer;
  printer.SetFile ( r.GetFile() );
  printer.SetColor( imDICOMDump::color != 0 );
//  printer.Print( std::cout );
  printer.Print( imDICOMDump::DumpOutput );

  return 0;
}

//---------------------------------------------------------------------------------------------------------
static bool cleanup::ProcessData( const char *buf, size_t len )
{
  Data2 data2;
  const size_t s = sizeof(data2);
  assert( len >= s); (void)len;
  // VIMDATA2 is generally 2048 bytes, while s = 1786
  // the end is filled with \0 bytes
  memcpy(&data2, buf, s);

//  data2.Print( std::cout );
  data2.Print( imDICOMDump::DumpOutput );
  return true;
}

//---------------------------------------------------------------------------------------------------------
static int cleanup::DumpVEPRO(const gdcm::DataSet & ds)
{
  // 01f7,1026
  const gdcm::ByteValue *bv2 = NULL;
  const gdcm::PrivateTag tdata1(0x55,0x0020,"VEPRO VIF 3.0 DATA");
  const gdcm::PrivateTag tdata2(0x55,0x0020,"VEPRO VIM 5.0 DATA");
  // Prefer VIF over VIM ?
  if( ds.FindDataElement( tdata1 ) )
    {
    std::cout  << "VIF DATA: " << tdata1 << "\n";
    const gdcm::DataElement &data = ds.GetDataElement( tdata1 );
    bv2 = data.GetByteValue();
    }
  else if( ds.FindDataElement( tdata2 ) )
    {
    std::cout  << "VIMDATA2: " << tdata2 << "\n";
    const gdcm::DataElement &data = ds.GetDataElement( tdata2 );
    bv2 = data.GetByteValue();
    }

  if( bv2 )
    {
    ProcessData( bv2->GetPointer(), bv2->GetLength() );
    return 0;
    }

  return 1;
}

//---------------------------------------------------------------------------------------------------------
// ELSCINT1
static bool cleanup::readastring(std::string &out, const char *input )
{
  out.clear();
  while( *input )
    {
    out.push_back( *input++ );
    }
  return true;
}

//---------------------------------------------------------------------------------------------------------
static int cleanup::DumpEl2_new(const gdcm::DataSet & ds)
{
  // 01f7,1026
  const gdcm::PrivateTag t01f7_26(0x01f7,0x1026,"ELSCINT1");
  if( !ds.FindDataElement( t01f7_26 ) ) return 1;
  const gdcm::DataElement& de01f7_26 = ds.GetDataElement( t01f7_26 );
  if ( de01f7_26.IsEmpty() ) return 1;
  const gdcm::ByteValue * bv = de01f7_26.GetByteValue();

  const char *begin = bv->GetPointer();
  uint32_t val0[3];
  memcpy( &val0, begin, sizeof( val0 ) );
  assert( val0[0] == 0xF22D );
  begin += sizeof( val0 );

  // 1A 98 06 00 -> start element
  // Next is a string (can be NULL)
  // then number of (nested) elements

  std::cout << "ELSCINT1 Dumping info from tag " << t01f7_26 << std::endl;
  info i;
  size_t o;
  assert( val0[1] == 0x1 );
  for( uint32_t idx = 0; idx < val0[2]; ++idx )
    {
    o = i.Read( begin );
    std::cout << std::endl;
    begin += o;
    }

  return 0;
}




//---------------------------------------------------------------------------------------------------------
template <typename TPrinter> int imDICOMDump::DoOperation(const std::string & filename)
{
  gdcm::Reader reader;
  reader.SetFileName( filename.c_str() );
  bool success = reader.Read();
  if( !success && !ignoreerrors )
    {
    std::cerr << "Failed to read: " << filename << std::endl;
    return 1;
    }

  TPrinter printer;
  printer.SetFile ( reader.GetFile() );
  printer.SetColor( color != 0);
//  printer.Print( std::cout );
  printer.Print( DumpOutput );

  // Only return success when file read succeeded not depending whether or not we printed it
  return success ? 0 : 1;
}

//---------------------------------------------------------------------------------------------------------
int imDICOMDump::PrintASN1(const std::string & filename, bool verbose)
{
  (void)verbose;
  gdcm::Reader reader;
  reader.SetFileName( filename.c_str() );
  if( !reader.Read() )
    {
    std::cerr << "Failed to read: " << filename << std::endl;
    return 1;
    }
  const gdcm::DataSet& ds = reader.GetFile().GetDataSet();
  gdcm::Tag tencryptedattributessequence(0x0400,0x0500);
  if( !ds.FindDataElement( tencryptedattributessequence ) )
    {
    return 1;
    }
  const gdcm::DataElement &encryptedattributessequence = ds.GetDataElement( tencryptedattributessequence );
  //const gdcm::SequenceOfItems * sqi = encryptedattributessequence.GetSequenceOfItems();
  gdcm::SmartPointer<gdcm::SequenceOfItems> sqi = encryptedattributessequence.GetValueAsSQ();
  if( !sqi->GetNumberOfItems() )
    {
    return 1;
    }
  const gdcm::Item &item1 = sqi->GetItem(1);
  const gdcm::DataSet &subds = item1.GetNestedDataSet();

  gdcm::Tag tencryptedcontent(0x0400,0x0520);
  if( !subds.FindDataElement( tencryptedcontent) )
    {
    return 1;
    }
  const gdcm::DataElement &encryptedcontent = subds.GetDataElement( tencryptedcontent );
  const gdcm::ByteValue *bv = encryptedcontent.GetByteValue();

  bool b = gdcm::ASN1::ParseDump( bv->GetPointer(), bv->GetLength() );
  if( !b ) return 1;
  return 0;
}

//---------------------------------------------------------------------------------------------------------
int imDICOMDump::PrintELSCINT(const std::string & filename, bool verbose)
{
  (void)verbose;
  gdcm::Reader reader;
  reader.SetFileName( filename.c_str() );
  if( !reader.Read() )
    {
    std::cerr << "Failed to read: " << filename << std::endl;
    return 1;
    }

  const gdcm::DataSet& ds = reader.GetFile().GetDataSet();
  int ret = cleanup::DumpEl2_new( ds );

  return ret;
}

//---------------------------------------------------------------------------------------------------------
int imDICOMDump::PrintVEPRO(const std::string & filename, bool verbose)
{
  (void)verbose;
  gdcm::Reader reader;
  reader.SetFileName( filename.c_str() );
  if( !reader.Read() )
    {
    std::cerr << "Failed to read: " << filename << std::endl;
    return 1;
    }

  const gdcm::DataSet& ds = reader.GetFile().GetDataSet();
  int ret = cleanup::DumpVEPRO( ds );

  return ret;
}

//---------------------------------------------------------------------------------------------------------
int imDICOMDump::PrintSDS(const std::string & filename, bool verbose)
{
  (void)verbose;
  gdcm::Reader reader;
  reader.SetFileName( filename.c_str() );
  if( !reader.Read() )
    {
    std::cerr << "Failed to read: " << filename << std::endl;
    return 1;
    }

  const gdcm::DataSet& ds = reader.GetFile().GetDataSet();
  int ret = cleanup::DumpPMS_MRSDS( ds );

  return ret;
}

//---------------------------------------------------------------------------------------------------------
int imDICOMDump::PrintCT3(const std::string & filename, bool verbose)
{
  (void)verbose;
  gdcm::Reader reader;
  reader.SetFileName( filename.c_str() );
  if( !reader.Read() )
    {
    std::cerr << "Failed to read: " << filename << std::endl;
    return 1;
    }

  const gdcm::DataSet& ds = reader.GetFile().GetDataSet();
  int ret = cleanup::DumpTOSHIBA_MEC_CT3( ds );

  return ret;
}

//---------------------------------------------------------------------------------------------------------
int imDICOMDump::PrintPDB(const std::string & filename, bool verbose)
{
  (void)verbose;
  gdcm::Reader reader;
  reader.SetFileName( filename.c_str() );
  if( !reader.Read() )
    {
    std::cerr << "Failed to read: " << filename << std::endl;
    return 1;
    }

  gdcm::PDBHeader pdb;
  const gdcm::DataSet& ds = reader.GetFile().GetDataSet();

  const gdcm::PrivateTag &t1 = pdb.GetPDBInfoTag();

  bool found = false;
  int ret = 0;
  if( ds.FindDataElement( t1 ) )
    {
    pdb.LoadFromDataElement( ds.GetDataElement( t1 ) );
//    pdb.Print( std::cout );
    pdb.Print( DumpOutput );
    found = true;
    }
  if( !found )
    {
    std::cout << "no pdb tag found" << std::endl;
    ret = 1;
    }

  return ret;
}

//---------------------------------------------------------------------------------------------------------
int imDICOMDump::PrintCSA(const std::string & filename)
{
  gdcm::Reader reader;
  reader.SetFileName( filename.c_str() );
  if( !reader.Read() )
    {
    std::cerr << "Failed to read: " << filename << std::endl;
    return 1;
    }

  gdcm::CSAHeader csa;
  const gdcm::DataSet& ds = reader.GetFile().GetDataSet();

  const gdcm::PrivateTag &t1 = csa.GetCSAImageHeaderInfoTag();
  const gdcm::PrivateTag &t2 = csa.GetCSASeriesHeaderInfoTag();
  const gdcm::PrivateTag &t3 = csa.GetCSADataInfo();

  bool found = false;
  int ret = 0;
  if( ds.FindDataElement( t1 ) )
    {
    csa.LoadFromDataElement( ds.GetDataElement( t1 ) );
//    csa.Print( std::cout );
    csa.Print( DumpOutput );
    found = true;
    if( csa.GetFormat() == gdcm::CSAHeader::ZEROED_OUT )
      {
      std::cout << "CSA Header has been zero-out (contains only 0)" << std::endl;
      ret = 1;
      }
    else if( csa.GetFormat() == gdcm::CSAHeader::DATASET_FORMAT )
      {
      gdcm::Printer p;
      gdcm::File f;
      f.SetDataSet( csa.GetDataSet() );
      p.SetFile( f );
//      p.Print( std::cout );
      p.Print( DumpOutput );
      }
    }
  if( ds.FindDataElement( t2 ) )
    {
    csa.LoadFromDataElement( ds.GetDataElement( t2 ) );
//    csa.Print( std::cout );
    csa.Print( DumpOutput );
    found = true;
    if( csa.GetFormat() == gdcm::CSAHeader::ZEROED_OUT )
      {
      std::cout << "CSA Header has been zero-out (contains only 0)" << std::endl;
      ret = 1;
      }
    else if( csa.GetFormat() == gdcm::CSAHeader::DATASET_FORMAT )
      {
      gdcm::Printer p;
      gdcm::File f;
      f.SetDataSet( csa.GetDataSet() );
      p.SetFile( f );
//      p.Print( std::cout );
      p.Print( DumpOutput );
      }
    }
  if( ds.FindDataElement( t3 ) )
    {
    csa.LoadFromDataElement( ds.GetDataElement( t3 ) );
//    csa.Print( std::cout );
    csa.Print( DumpOutput );
    found = true;
    if( csa.GetFormat() == gdcm::CSAHeader::ZEROED_OUT )
      {
      std::cout << "CSA Header has been zero-out (contains only 0)" << std::endl;
      ret = 1;
      }
    else if( csa.GetFormat() == gdcm::CSAHeader::INTERFILE )
      {
      const char *interfile = csa.GetInterfile();
      if( interfile ) std::cout << interfile << std::endl;
      }
    else if( csa.GetFormat() == gdcm::CSAHeader::DATASET_FORMAT )
      {
      gdcm::Printer p;
      gdcm::File f;
      f.SetDataSet( csa.GetDataSet() );
      p.SetFile( f );
//      p.Print( std::cout );
      p.Print( DumpOutput );
      }
    }
  if( !found )
    {
    std::cout << "no csa tag found" << std::endl;
    ret = 1;
    }

  return ret;
}


//---------------------------------------------------------------------------------------------------------
void imDICOMDump::PrintVersion()
{
  std::cout << "imDICOMDump: gdcm " << gdcm::Version::GetVersion() << " ";
  const char date[] = "$Date$";
  std::cout << date << std::endl;
}

//---------------------------------------------------------------------------------------------------------
void imDICOMDump::PrintHelp()
{
  PrintVersion();
  std::cout << "Usage: imDICOMDump [OPTION]... FILE..." << std::endl;
  std::cout << "dumps a DICOM file, it will display the structure and values contained in the specified DICOM file\n";
  std::cout << "Parameter (required):" << std::endl;
  std::cout << "  -i --input     DICOM filename or directory" << std::endl;
  std::cout << "Options:" << std::endl;
  std::cout << "  -x --xml-dict       generate the XML dict (only private elements for now)." << std::endl;
  std::cout << "  -r --recursive      recursive." << std::endl;
  std::cout << "  -d --dump           dump value (limited use)." << std::endl;
  std::cout << "  -p --print          print value instead of simply dumping (default)." << std::endl;
  std::cout << "  -c --color          print in color." << std::endl;
  std::cout << "  -C --csa            print SIEMENS CSA Header (0029,[12]0,SIEMENS CSA HEADER)." << std::endl;
  std::cout << "  -P --pdb            print GEMS Protocol Data Block (0025,1b,GEMS_SERS_01)." << std::endl;
  std::cout << "     --elscint        print ELSCINT Protocol Information (01f7,26,ELSCINT1)." << std::endl;
  std::cout << "     --vepro          print VEPRO Protocol Information (0055,20,VEPRO VIF 3.0 DATA)." << std::endl;
  std::cout << "                         or VEPRO Protocol Information (0055,20,VEPRO VIM 5.0 DATA)." << std::endl;
  std::cout << "     --sds            print Philips MR Series Data Storage (1.3.46.670589.11.0.0.12.2) Information (2005,32,Philips MR Imaging DD 002)." << std::endl;
  std::cout << "     --ct3            print CT Private Data 2 (7005,10,TOSHIBA_MEC_CT3)." << std::endl;
  std::cout << "  -A --asn1           print encapsulated ASN1 structure >(0400,0520)." << std::endl;
  std::cout << "     --map-uid-names  map UID to names." << std::endl;
  std::cout << "General Options:" << std::endl;
  std::cout << "  -V --verbose   more verbose (warning+error)." << std::endl;
  std::cout << "  -W --warning   print warning info." << std::endl;
  std::cout << "  -D --debug     print debug info." << std::endl;
  std::cout << "  -E --error     print error info." << std::endl;
  std::cout << "  -h --help      print help." << std::endl;
  std::cout << "  -v --version   print version." << std::endl;
  std::cout << "Special Options:" << std::endl;
  std::cout << "  -I --ignore-errors   print even if file is corrupted." << std::endl;
}

//---------------------------------------------------------------------------------------------------------
int imDICOMDump::Dump (std::string arg)
{
  int parameters[18];

  parameters[0] = 0; //printdict;
  parameters[1] = 0; //dump;
  parameters[2] = 0; //print;
  parameters[3] = 0; //printcsa;
  parameters[4] = 0; //printpdb;
  parameters[5] = 0; //printelscint;
  parameters[6] = 0; //printvepro;
  parameters[7] = 0; //printsds;  MR Series Data Storage
  parameters[8] = 0; //printct3;  TOSHIBA_MEC_CT3
  parameters[9] = 0; //verbose;
  parameters[10] = 0; //warning;
  parameters[11] = 0; //debug;
  parameters[12] = 0; //error;
  parameters[13] = 0; //help;
  parameters[14] = 0; //version;
  parameters[15] = 0; //recursive;
  parameters[16] = 0; //printasn1;
  parameters[17] = 0; //mapuidnames;

  return Dump ( arg, parameters );
}

//---------------------------------------------------------------------------------------------------------
int imDICOMDump::Dump (std::string arg, int parameters[])
{
  std::string filename;
  filename = arg;

  int printdict = parameters[0];
  int dump = parameters[1];
  int print = parameters[2];
  int printcsa = parameters[3];
  int printpdb = parameters[4];
  int printelscint = parameters[5];
  int printvepro = parameters[6];
  int printsds = parameters[7];
  int printct3 = parameters[8];
  int verbose = parameters[9];
  int warning = parameters[10];
  int debug = parameters[11];
  int error = parameters[12];
  int help = parameters[13];
  int version = parameters[14];
  int recursive = parameters[15];
  int printasn1 = parameters[16];
  int mapuidnames = parameters[17];




  if( version )
    {
    //std::cout << "version" << std::endl;
    PrintVersion();
    return 0;
    }

  if( help )
    {
    //std::cout << "help" << std::endl;
    PrintHelp();
    return 0;
    }

  // check if d or p are passed, only one at a time
  if( print || printdict )
    {
    if ( print && printdict )
      {
      std::cerr << "d or p" << std::endl;
      return 1;
      }
    }
  if( filename.empty() )
    {
    //std::cerr << "Need input file (-i)\n";
    PrintHelp();
    return 1;
    }
  // Debug is a little too verbose
  gdcm::Trace::SetDebug( debug != 0);
  gdcm::Trace::SetWarning( warning != 0);
  gdcm::Trace::SetError( error != 0);
  // when verbose is true, make sure warning+error are turned on:
  if( verbose )
    {
    gdcm::Trace::SetWarning( verbose != 0);
    gdcm::Trace::SetError( verbose!= 0);
    }

  if( mapuidnames )
    {
    std::cerr << "Not handled for now" << std::endl;
    }

  // else
  int res = 0;
  if( !gdcm::System::FileExists(filename.c_str()) )
    {
    std::cerr << "no such file: " << filename << std::endl;
    return 1;
    }
  else if( gdcm::System::FileIsDirectory( filename.c_str() ) )
    {
    gdcm::Directory d;
    d.Load(filename, recursive!= 0);
    gdcm::Directory::FilenamesType const &filenames = d.GetFilenames();
    for( gdcm::Directory::FilenamesType::const_iterator it = filenames.begin(); it != filenames.end(); ++it )
      {
      if( printdict )
        {
        res += DoOperation<gdcm::DictPrinter>(*it);
        }
      else if( printasn1 )
        {
        res += PrintASN1(*it, verbose!= 0);
        }
      else if( printvepro )
        {
        res += PrintVEPRO(*it, verbose!= 0);
        }
      else if( printsds )
        {
        res += PrintSDS(*it, verbose!= 0);
        }
      else if( printct3 )
        {
        res += PrintCT3(*it, verbose!= 0);
        }
      else if( printelscint )
        {
        res += PrintELSCINT(*it, verbose!= 0);
        }
      else if( printpdb )
        {
        res += PrintPDB(*it, verbose!= 0);
        }
      else if( printcsa )
        {
        res += PrintCSA(*it);
        }
      else if( dump )
        {
        res += DoOperation<gdcm::Dumper>(*it);
        }
      else
        {
        res += DoOperation<gdcm::Printer>(*it);
        }
      if( verbose ) std::cerr << *it << std::endl;
      }
    if( verbose ) std::cerr << "Total: " << filenames.size() << " files were processed" << std::endl;
    }
  else
    {
    assert( gdcm::System::FileExists(filename.c_str()) );
    if( printdict )
      {
      res += DoOperation<gdcm::DictPrinter>(filename);
      }
    else if( printasn1 )
      {
      res += PrintASN1(filename, verbose!= 0);
      }
    else if( printvepro )
      {
      res += PrintVEPRO(filename, verbose!= 0);
      }
    else if( printsds )
      {
      res += PrintSDS(filename, verbose!= 0);
      }
    else if( printct3 )
      {
      res += PrintCT3(filename, verbose!= 0);
      }
    else if( printelscint )
      {
      res += PrintELSCINT(filename, verbose!= 0);
      }
    else if( printpdb )
      {
      res += PrintPDB(filename, verbose!= 0);
      }
    else if( printcsa )
      {
      res += PrintCSA(filename);
      }
    else if( dump )
      {
      res += DoOperation<gdcm::Dumper>(filename);
      }
    else
      {
      res += DoOperation<gdcm::Printer>(filename);
      }
    // ...
    if ( verbose )
      std::cerr << "Filename: " << filename << std::endl;
    }

  return res;
}

#endif
