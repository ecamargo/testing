#include <iostream>

#include "imDICOMDump.h"

using namespace std;


//-----------------------------------------------------------------------------
int main( int argc, char **argv )
{
  imDICOMDump *dump = new imDICOMDump();

  string fileName = "/home/nfs/images/FUNCAOVE_7/IM-0001-0531.dcm";
//  string fileName = "/home/nfs/images/FUNCAOVE_7/";
//  string fileName = "/home/nfs/images/Fernando 2/dicomdir";
//  string fileName = "/home/nfs/images/Ivus/20090204/CASE64/RUN1/PD6118II";
//  string fileName = "/home/nfs/images/Ivus/DICOMDIR";


  dump->Dump( fileName );
  std::cout << dump->GetDumpOutput() << std::endl;

  delete dump;
  return 0;
}


