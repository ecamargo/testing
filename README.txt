
To clone this repository, make sure git, cmake and gcc are installed on your system and issue the following:

git clone https://ecamargo@bitbucket.org/ecamargo/testing.git Testing


Compilation
======================================================================================================

cd Testing
mkdir build
cd build
ccmake ../
	For compile with debug informations "CMAKE_BUILD_TYPE" = "Debug"
	Configure
	Generate
make

The executable and libraries will be created in build/bin



